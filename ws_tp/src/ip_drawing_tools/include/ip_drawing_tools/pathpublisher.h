#pragma once

#include <string>
#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <tf/transform_broadcaster.h>
#include <eigen3/Eigen/Dense>
#include <visualization_msgs/Marker.h>

/**
 * This class allows to create a trajectory using nav_msgs::Path and
 * publish it to a defined topic.
 */
struct PathPublisher {
	using Path   = nav_msgs::Path;
	using Point  = geometry_msgs::Point;
	using Pose   = geometry_msgs::Pose; 
	using TransformStamped = geometry_msgs::TransformStamped;
	static constexpr double orientationThreshold = 1e-3;

private:
	std::string       _childFrame;
	ros::NodeHandle   _node;
	ros::Publisher    _markerPub;
	Path              _path;
	tf::TransformBroadcaster _frameBr;
	Pose              _oldPose;
	Point             _oldPoint;

public:
	PathPublisher() {}

	/** 
	 * @param topicName the name of the private published topic
	 * @param refFrame the frame used as a static frame of reference
	 * @param childFrame the moving frame
	 */
	PathPublisher(std::string const & topicName, 
			      std::string const & refFrame,
				  std::string const & childFrame);
	
	PathPublisher & operator = (PathPublisher &&);

	/** @return the path containing all added poses */
	Path const & path() const { return _path; }

	/** @param pose a new PoseStamped to add to the path */
	void push(geometry_msgs::PoseStamped const & pose);

	/**
	 * Create a PoseStamped with default orientation and add it to the path
	 * @param pt a new PointStamped to add to the path
	 */
	void push(geometry_msgs::PointStamped const & pt);

	 /**
	 * Create a PoseStamped with default orientation and add it to the path
	 * @param pt a new PointStamped to add to the path
	 * @param t the frame transform to publish
	 */
	void push(geometry_msgs::PointStamped const & pt, TransformStamped const & t);
        
	/**
	 * Create a PoseStamped with default orientation and add it to the path
	 * @param pt a vector 3 corresponding to a point to add to the path
	 */
	template <class Derived>
	void push(Eigen::MatrixBase<Derived> const & pt);

	/**
	 * Publish a new point in the trajectory
	 * @param pt the point to publish
	 * @param time the time when the point has been created
	 */
	void push(Point const & pt, ros::Time time = ros::Time::now());

	/**
	 * Publish a new pose in the trajectory
	 * @param p the pose to publish
	 * @param time the time when the point has been created
	 */
	void push(Pose const & p, ros::Time time = ros::Time::now());

	/**
	 * Publish a new pose in the trajectory
	 * @param pt the point to publish
	 * @param t the frame transform to publish
	 * @param time the time when the point has been created
	 */        
	void push(Point const & pt, TransformStamped const & t, ros::Time time = ros::Time::now());
};


template <class Derived>
void PathPublisher::push(Eigen::MatrixBase<Derived> const & pt) {
	Point p;
	p.x = pt(0);
	p.y = pt(1);
	p.z = pt(2);
	push(p);
}
