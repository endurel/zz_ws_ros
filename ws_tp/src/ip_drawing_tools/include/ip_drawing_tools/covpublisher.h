#pragma once

#include <ros/ros.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Eigenvalues>
#include <visualization_msgs/Marker.h>
#include <string>

/**
 * This class allows to publish markers for rviz. This markers represent uncertainty ellipses
 * corresponding to covariance.
 */
struct CovPublisher {
	using Marker = visualization_msgs::Marker;

private:
	ros::NodeHandle _node;
	ros::Publisher  _pub;
	Marker          _marker;
//	Marker          _base;

public:
	/** 
	 * @param the name of the piblisher topic
	 * @param the name of the frame
	 */
	CovPublisher(std::string const & topicName, std::string const & frame);

	/**
	 * Compute an ellipse corresponding to the new pose.
	 * @param pose the pose and the covariance of a 3D point.
	 */
	void publish(Eigen::Vector4d const & pose, Eigen::Matrix4d const & cov);

	/**
	 * Publish an ellipse corresponding to the covariance of a 3D position
	 * @param pos a position vector (only the 3 first members are used)
	 * @param cov the covariance of the position (only the top left 3x3 corner is used)
	 */
	template <class Vec, class Mat>
	void publish(Eigen::MatrixBase<Vec> const & pos, Eigen::MatrixBase<Mat> const & cov);

	/** Change the ellipse color (red, blue, green, alpha) */
	void color(float r, float g, float b, float a);
	void color(std_msgs::ColorRGBA const & color) { _marker.color = color; }
};


template <class Vec, class Mat>
void CovPublisher::publish(
	Eigen::MatrixBase<Vec> const & pos, 
	Eigen::MatrixBase<Mat> const & cov)
{
	auto positionCov = cov.template topLeftCorner<3, 3>();
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigSolver{positionCov};
	
	// Apply orientation from eigen vectors
	Eigen::Matrix3d eigVecs = eigSolver.eigenvectors();
	if (!eigVecs.col(0).cross(eigVecs.col(1)).isApprox(eigVecs.col(2)))
		eigVecs.col(2) = -eigVecs.col(2);

	Eigen::Quaterniond quat{eigVecs};
	_marker.pose.orientation.w = quat.w();
	_marker.pose.orientation.x = quat.x();
	_marker.pose.orientation.y = quat.y();
	_marker.pose.orientation.z = quat.z();

	// Apply translation
	_marker.pose.position.x = pos(0);
	_marker.pose.position.y = pos(1);
	_marker.pose.position.z = pos(2);

	// Apply scaling from eigen values
	Eigen::Array<double, 3, 1> eigValues = (11.345 * eigSolver.eigenvalues()).array().sqrt();
	_marker.scale.x = 2 * eigValues(0);
	_marker.scale.y = 2 * eigValues(1);
	_marker.scale.z = 2 * eigValues(2);

	// publish message
	_marker.header.stamp = ros::Time::now();
	_pub.publish(_marker);
}
