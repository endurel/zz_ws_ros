#include "ip_drawing_tools/covpublisher.h"

using visualization_msgs::Marker;

CovPublisher::CovPublisher(std::string const & topicName, std::string const & frame)
	: _node{"~"}, _pub{_node.advertise<Marker>(topicName, 1)} 
{
	_marker.header.frame_id = frame;
	_marker.type    = Marker::SPHERE;
	_marker.action  = Marker::ADD;
	_marker.ns      = _node.getNamespace();
	_marker.id      = 0;
	_marker.color.r = 1;
	_marker.color.g = 0;
	_marker.color.b = 0;
	_marker.color.a = .5;

//	_base.header.frame_id = frame;
//	_base.type    = Marker::LINE_LIST;
//	_base.action  = Marker::ADD;
//	_base.ns      = "base";
//	_base.id      = 0;
//	_base.scale.x = .1;
}


void CovPublisher::publish(Eigen::Vector4d const & pose, Eigen::Matrix4d const & cov) {
	auto positionCov = cov.topLeftCorner<3, 3>();
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigSolver{positionCov};
	
	// Apply orientation from eigen vectors
	Eigen::Matrix3d eigVecs = eigSolver.eigenvectors();
	if (!eigVecs.col(0).cross(eigVecs.col(1)).isApprox(eigVecs.col(2)))
		eigVecs.col(2) = -eigVecs.col(2);

	Eigen::Quaterniond quat{eigVecs};
	_marker.pose.orientation.w = quat.w();
	_marker.pose.orientation.x = quat.x();
	_marker.pose.orientation.y = quat.y();
	_marker.pose.orientation.z = quat.z();

	// Apply translation
	_marker.pose.position.x = pose(0);
	_marker.pose.position.y = pose(1);
	_marker.pose.position.z = pose(2);

	// Apply scaling from eigen values
	Eigen::Array<double, 3, 1> eigValues = (11.345 * eigSolver.eigenvalues()).array().sqrt();
	_marker.scale.x = 2 * eigValues(0);
	_marker.scale.y = 2 * eigValues(1);
	_marker.scale.z = 2 * eigValues(2);

	// publish message
	_marker.header.stamp = ros::Time::now();
	_pub.publish(_marker);
//	++_marker.id;
	
//	Eigen::Matrix3d vecs = eigVecs.array().rowwise() * eigValues.transpose();
//	geometry_msgs::Point pt;
//
//	_base.points.clear();
//	_base.colors.clear();
//	for (std::size_t i = 0; i < 3; ++i) {
//		pt.x = pose(0);
//		pt.y = pose(1);
//		pt.z = pose(2);
//		_base.points.push_back(pt);
//		pt.x += vecs(0, i);
//		pt.y += vecs(1, i);
//		pt.z += vecs(2, i);
//		_base.points.push_back(pt);
//		Marker::_color_type clr;
//		clr.r = i == 0;
//		clr.g = i == 1;
//		clr.b = i == 2;
//		clr.a = 1;
//		_base.colors.push_back(clr);
//		_base.colors.push_back(clr);
//		_base.header.stamp = ros::Time::now();
//	}
//	_pub.publish(_base);
}


void CovPublisher::color(float r, float g, float b, float a) {
	_marker.color.r = r;
	_marker.color.g = g;
	_marker.color.b = b;
	_marker.color.a = a;
}

