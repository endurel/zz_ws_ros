#include "ip_drawing_tools/pathpublisher.h"


PathPublisher::PathPublisher(std::string const & topicName, 
							 std::string const & refFrame,
							 std::string const & childFrame)
	: _childFrame{childFrame},
	  _node{"~"}, 
	  _markerPub{_node.advertise<Path>(topicName + "_marker", 1)}
{
	_oldPose.orientation.w = 1;

	_path.header.frame_id = refFrame;
	_path.header.seq = 0;
}


PathPublisher & PathPublisher::operator = (PathPublisher && pp) {
	if (&pp != this) {
		std::swap(_childFrame, pp._childFrame);
		std::swap(_node, pp._node);
		std::swap(_markerPub, pp._markerPub);
		std::swap(_path, pp._path);
		std::swap(_frameBr, pp._frameBr);
		std::swap(_oldPose, pp._oldPose);
		std::swap(_oldPoint, pp._oldPoint);
	}

	return *this;
}


void PathPublisher::push(geometry_msgs::PoseStamped const & ps) {
	push(ps.pose, ps.header.stamp);
}


void PathPublisher::push(geometry_msgs::PointStamped const & pt) {
	push(pt.point, pt.header.stamp);
}

void PathPublisher::push(geometry_msgs::PointStamped const & pt, TransformStamped const & t) {
        push(pt.point, t, pt.header.stamp);
}


void PathPublisher::push(Point const & pt, ros::Time time) {
	Pose p;
	p.position = pt;

	auto const & oldPos = _oldPose.position;
	double dx = pt.x - oldPos.x, dy = pt.y - oldPos.y;

	// Orientation is updated only if the point is moving
	if (dx*dx + dy*dy > orientationThreshold) {
		double yaw = std::atan2(dy, dx);
		p.orientation = tf::createQuaternionMsgFromYaw(yaw);
	}
	else p.orientation = _oldPose.orientation;
	push(p, time);
}


void PathPublisher::push(Pose const & p, ros::Time time) {
	auto const & pt = p.position;
        static bool first = false;
	_path.header.stamp = time;

	if (first) {
		// The vector contains only 2 points corresponding to the line to draw
		geometry_msgs::PoseStamped psOld, ps;
		psOld.header = _path.header;
		psOld.pose = _oldPose;
		ps.header = _path.header;
		ps.pose = p;

		_path.poses.clear();
		_path.poses.push_back(psOld);
		_path.poses.push_back(ps);
		++_path.header.seq;

		_markerPub.publish(_path);
		
	}
	else
		first = true;

	// Publish the TF between the reference frame and the frame of the pose
	tf::Quaternion q;
	tf::quaternionMsgToTF(p.orientation, q);
	_frameBr.sendTransform(tf::StampedTransform(
		tf::Transform{std::move(q), tf::Vector3{pt.x, pt.y, pt.z}}, 
		time, _path.header.frame_id, _childFrame));

	_oldPose = p;
	
}

void PathPublisher::push(Point const & pt, TransformStamped const & t ,ros::Time time) {
	static bool first = false;
	_path.header.stamp = time;

	if (first) {
		// The vector contains only 2 points corresponding to the line to draw
		geometry_msgs::PoseStamped psOld, ps;
		psOld.pose.position = _oldPoint;
		ps.pose.position = pt;
		ps.pose.orientation = t.transform.rotation;

		_path.poses.clear();
		_path.poses.push_back(psOld);
		_path.poses.push_back(ps);
		++_path.header.seq;

		_markerPub.publish(_path);
	}
	else
		first = true;
	
	_frameBr.sendTransform(t);

	_oldPoint = pt;
}
