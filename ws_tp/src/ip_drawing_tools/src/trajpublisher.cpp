#include "ip_drawing_tools/trajpublisher.h"


TrajPublisher::TrajPublisher(std::string const & topicName, 
							 std::string const & refFrame,
							 std::string const & childFrame)
	: _childFrame{childFrame},
	  _node{"~"}, 
	  _markerPub{_node.advertise<Marker>(topicName + "_marker", 1)}
{
	_oldPose.orientation.w = 1;

	_marker.header.frame_id = refFrame;
	_marker.type    = Marker::LINE_LIST;
	_marker.action  = Marker::ADD;
	_marker.ns      = _node.getNamespace();
	_marker.id      = 0;
	_marker.scale.x = .2;

	Marker::_color_type clr;
	clr.r = 1;
	clr.g = 1;
	clr.b = 1;
	clr.a = 1;
	_marker.colors.push_back(clr);
	_marker.colors.push_back(clr);
}


void TrajPublisher::push(geometry_msgs::PoseStamped const & ps) {
	push(ps.pose, ps.header.stamp);
}


void TrajPublisher::push(geometry_msgs::PointStamped const & pt) {
	push(pt.point, pt.header.stamp);
}

void TrajPublisher::push(geometry_msgs::PointStamped const & pt, TransformStamped const & t) {
        push(pt.point, t, pt.header.stamp);
}


void TrajPublisher::push(Point const & pt, ros::Time time) {
	Pose p;
	p.position = pt;

	auto const & oldPos = _oldPose.position;
	double dx = pt.x - oldPos.x, dy = pt.y - oldPos.y;

	// Orientation is updated only if the point is moving
	if (dx*dx + dy*dy > orientationThreshold) {
		double yaw = std::atan2(dy, dx);
		p.orientation = tf::createQuaternionMsgFromYaw(yaw);
	}
	else p.orientation = _oldPose.orientation;
	push(p, time);
}


void TrajPublisher::push(Pose const & p, ros::Time time) {
	auto const & pt = p.position;
	bool first = !_marker.header.stamp.isValid();

	_marker.header.stamp = time;

	if (!first) {
		// The vector contains only 2 points corresponding to the line to draw
		_marker.points.clear();
		_marker.points.push_back(_oldPose.position);
		_marker.points.push_back(pt);
		++_marker.id;
		_markerPub.publish(_marker);
	}

	// Publish the TF between the reference frame and the frame of the pose
	tf::Quaternion q;
	tf::quaternionMsgToTF(p.orientation, q);
	_frameBr.sendTransform(tf::StampedTransform(
		tf::Transform{std::move(q), tf::Vector3{pt.x, pt.y, pt.z}}, 
		time, _marker.header.frame_id, _childFrame));

	_oldPose = p;
}

void TrajPublisher::push(Point const & pt, TransformStamped const & t ,ros::Time time) {

         bool first = !_marker.header.stamp.isValid();

        _marker.header.stamp = time;
        
         if (!first) {
          _marker.points.clear();
          _marker.points.push_back(_oldPoint);
          _marker.points.push_back(pt);
          ++_marker.id;
          _markerPub.publish(_marker);
        }
        _frameBr.sendTransform(t);

        _oldPoint = pt;
}

void TrajPublisher::color(double r, double g, double b, double a) {
	Marker::_color_type clr;
	clr.r = r;
	clr.g = g;
	clr.b = b;
	clr.a = a;
	color(clr);
}

void TrajPublisher::color(std_msgs::ColorRGBA const & clr) {
	_marker.colors.clear();
	_marker.colors.push_back(clr);
	_marker.colors.push_back(clr);
};


