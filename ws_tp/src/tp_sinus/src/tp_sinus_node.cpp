#include "ros/ros.h"
#include "std_msgs/String.h"

#include "tp_msgs/Sinus.h"

#include <cmath>

const double pi = std::acos(-1);

int main(int argc, char ** argv)
{
    ros::init(argc, argv, "talker"); 

    ros::NodeHandle n;

    ros::Publisher sinus_pub = n.advertise<tp_msgs::Sinus>("sinus", 1000);

    ros::Rate loop_rate(10); // 10 Hz

    float x = 0;
    float d = 0.1;

    while(ros::ok())
    {
        tp_msgs::Sinus sinus_msg;
        sinus_msg.header.stamp = ros::Time::now();
        sinus_msg.header.frame_id = "odom";
        sinus_msg.x = x;
        sinus_msg.y = std::sin(x);

        x += d;

        sinus_pub.publish(sinus_msg);

        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}