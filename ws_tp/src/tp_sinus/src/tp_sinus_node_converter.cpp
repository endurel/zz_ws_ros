#include "ros/ros.h"
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"

#include "tp_msgs/Sinus.h"

#include <cmath>

ros::Publisher sinus_pub;

void callback(tp_msgs::Sinus::ConstPtr const & sinus_msg)
{
    nav_msgs::Odometry odom_msg;
    odom_msg.header = sinus_msg->header;
    odom_msg.pose.pose.position.x = sinus_msg->x;
    odom_msg.pose.pose.position.y = sinus_msg->y;
    sinus_pub.publish(odom_msg);
}

int main(int argc, char ** argv)
{
    ros::init(argc, argv, "talker_2"); 

    ros::NodeHandle n;

    ros::Subscriber sinus_sub = n.subscribe("sinus", 1000, callback);
    sinus_pub = n.advertise<nav_msgs::Odometry>("sinus_converted", 1000);

    ros::Rate loop_rate(10); // 10 Hz

    while(ros::ok())
    {
        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}