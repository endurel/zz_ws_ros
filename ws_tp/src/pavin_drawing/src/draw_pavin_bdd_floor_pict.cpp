/**

\file
\author Laurent Malaterre (2019)
\copyright 2018 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ros/ros.h>
#include <memory>
#include <visualization_msgs/Marker.h>
#include <cmath>
#include <stdlib.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
// #include "pavin_drawing/map_pavin_bdd.hpp"
#include <termios.h>
#include <unistd.h>

#define BUILD_KEYBOARD_CORRECTION 

using namespace std;
// using namespace libpavinbdd;

namespace
{
    double offset_x_fp(0.);
    double offset_y_fp(0.);
    double scale_fp(1.);
    
#ifdef BUILD_KEYBOARD_CORRECTION
    int getch()
    {
        static struct termios oldt, newt;
        tcgetattr( STDIN_FILENO, &oldt);           // save old settings
        newt = oldt;
        newt.c_lflag &= ~(ICANON);                 // disable buffering      
        tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings

        int c = getchar();  // read character (non-blocking)

        tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
        return c;
    }
#endif

    void sendImageToRviz(std::string frame_id, int id, ros::Publisher* marker_pub, cv::Mat src, double sizeSq, std::pair<double, double> LUcorner) 
    {
        visualization_msgs::Marker image;
        image.header.frame_id = frame_id;
        image.header.stamp = ros::Time::now();
        image.ns = "image";
        image.id = id;
        image.action = visualization_msgs::Marker::ADD;
        image.type = visualization_msgs::Marker::TRIANGLE_LIST;
        image.scale.x = 1.;
        image.scale.y = 1.;
        image.scale.z = 1.;

        double pix = sizeSq / src.rows;
        geometry_msgs::Point p;
        std_msgs::ColorRGBA crgb;

        for(int r = 0; r < src.rows; ++r) {
            for(int c = 0; c < src.cols; ++c) {
            cv::Vec3b intensity = src.at<cv::Vec3b>(r, c);
            crgb.r = intensity.val[2] / 255.0;
            crgb.g = intensity.val[1] / 255.0;
            crgb.b = intensity.val[0] / 255.0;
            crgb.a = 1.0;

            p.z = -0.1;
            p.x = LUcorner.first + r * pix;
            p.y = LUcorner.second + c * pix;
            image.points.push_back(p);
            image.colors.push_back(crgb);
            p.x = LUcorner.first + (r + 1) * pix;
            p.y = LUcorner.second + c * pix;
            image.points.push_back(p);
            image.colors.push_back(crgb);
            p.x = LUcorner.first + r * pix;
            p.y = LUcorner.second + (c + 1) * pix;
            image.points.push_back(p);
            image.colors.push_back(crgb);
            p.x = LUcorner.first + (r + 1) * pix;
            p.y = LUcorner.second + c * pix;
            image.points.push_back(p);
            image.colors.push_back(crgb);
            p.x = LUcorner.first + (r + 1) * pix;
            p.y = LUcorner.second + (c + 1) * pix;
            image.points.push_back(p);
            image.colors.push_back(crgb);
            p.x = LUcorner.first + r * pix;
            p.y = LUcorner.second + (c + 1) * pix;
            image.points.push_back(p);
            image.colors.push_back(crgb);
            }
        }
        marker_pub->publish(image);
        
    }
}

int main( int argc, char** argv )
{
  ros::init(argc, argv, "draw_pavin_ground_picture");
  ros::NodeHandle n;
  ros::NodeHandle node_private("~");
  image_transport::ImageTransport it(n);
  image_transport::Publisher pub = it.advertise("camera/image", 1);
  std::string file_picture;
  
  
  node_private.getParam("file_picture",file_picture);
  cv::Mat image = cv::imread(file_picture, cv::IMREAD_COLOR);
  sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
  ros::Publisher marker_pub_tr_array (n.advertise<visualization_msgs::Marker>("picture_array", 10));
  std::pair<double, double> p_offset;

  
  std::string frame_id("/map");

  node_private.getParam("pavin_drawing_frameid",frame_id);
  ROS_INFO_STREAM("pavin_drawing_frameid : " << frame_id);
  node_private.getParam("offset_x_fp",offset_x_fp);
  node_private.getParam("offset_y_fp",offset_y_fp);
  node_private.getParam("scale_fp",scale_fp);    
    
  
  p_offset.first = offset_x_fp;
  p_offset.second = -offset_y_fp;
  
  ros::Rate r(1);
  
  while (ros::ok())
  {    
    
    sendImageToRviz(frame_id,10245, &marker_pub_tr_array, image,scale_fp, p_offset); //10245

    
#ifdef BUILD_KEYBOARD_CORRECTION
    int c = getch();   // call your non-blocking input function
    if (c == 'a')
    {
        scale_fp = scale_fp +1;
        std::cout << "scale_fp +=1 : " << scale_fp << std::endl;
    }
    else if (c == 'q')
    {
        scale_fp = scale_fp -1;
        std::cout << "scale_fp -=1 : " << scale_fp << std::endl;
    }
    else if (c == 'i')
    {
        p_offset.first = p_offset.first + 0.5;
        std::cout << "p_offset.first +=0.5 : " << p_offset.first << std::endl;
    }
    else if (c == ',')
    {
        p_offset.first = p_offset.first - 0.5;
        std::cout << "p_offset.first -=0.5 : " << p_offset.first << std::endl;
    }
    else if (c == 'j')
    {
        p_offset.second = p_offset.second + 0.5;
        std::cout << "p_offset.second +=0.5 : " << p_offset.second << std::endl;
    }
    else if (c == 'l')
    {
        p_offset.second = p_offset.second - 0.5;
        std::cout << "p_offset.second -=0.5 : " << p_offset.second << std::endl;
    }
    else if (c == 's')
    {
        std::cout << "do nothing " << std::endl;
    }
#endif

    r.sleep();
  }
}

