/**

\file
\author Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <ros/ros.h>
#include <memory>
#include <visualization_msgs/Marker.h>
#include <cmath>
#include <stdlib.h>
#include "pavin_drawing/map_pavin.hpp"
#include <gps_nmea/GpsGGA.h>


using namespace gps_nmea;
using namespace std;


void setColor(visualization_msgs::Marker& m, std::vector<double> const & color);


int main( int argc, char** argv )
{
  std::string dir;
  string frame_id("/map");
    
  ros::init(argc, argv, "draw_pavin");
  ros::NodeHandle n;
  ros::NodeHandle priv_nh("~");

  ros::Publisher marker_pub (n.advertise<visualization_msgs::Marker>("visualization_marker", 10));
  ros::Rate r(1);

  if(priv_nh.hasParam("dir_pavin_map"))
  {
    priv_nh.getParam("dir_pavin_map", dir);
    ROS_INFO("Found param directory map : %s", dir.c_str());
  }
  else
  {
    priv_nh.param<std::string>("dir_pavin_map", dir, "$(find pavin_drawing)/map");
    priv_nh.setParam("dir_pavin_map", "$(find pavin_drawing)/map");
    ROS_INFO("SET param directory map : %s", dir.c_str());
  }
  
  priv_nh.getParam("pavin_drawing_frameid",frame_id);
  
  
  visualization_msgs::Marker line_list,line_road, line_wall;
  line_road.header.frame_id =line_list.header.frame_id = frame_id;
  line_road.ns =line_list.ns = "pavin";
  line_road.action =line_list.action = visualization_msgs::Marker::ADD;
  line_road.pose.orientation.w =line_list.pose.orientation.w = 1.0;
  line_road.type =line_list.type = visualization_msgs::Marker::LINE_LIST;

  // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
  line_road.scale.x =line_list.scale.x = 0.1;

  line_wall = line_road;

  line_list.id = 0;
  line_road.id = 1;
  line_wall.id = 2;

  using VecD = std::vector<double>;
  setColor(line_road, priv_nh.param("road_color", VecD{ 0,  1,  0, 1}));
  setColor(line_wall, priv_nh.param("wall_color", VecD{.5, .5, .5, 1}));
  setColor(line_list, priv_nh.param("list_color", VecD{ 0, .5, .5, 1}));

  {
    mapPavin map(&priv_nh);
    map.loadPavinFiles(dir);
    map.drawPavin(map, marker_pub ,line_list);
    map.drawRoad(map, marker_pub ,line_road);
    map.drawWalls(map, marker_pub ,line_wall);
  }

  int i = 0;
  while (ros::ok())
  {
    line_road.header.stamp = line_list.header.stamp = ros::Time::now();
    line_wall.header.stamp = line_list.header.stamp;
    marker_pub.publish(line_wall);
    ros::Duration{.05}.sleep();
    marker_pub.publish(line_list);
    ros::Duration{.05}.sleep();
    marker_pub.publish(line_road);
    r.sleep();
  }
}

void setColor(visualization_msgs::Marker& m, std::vector<double> const & color)
{
	m.color.r = color[0];
	m.color.g = color[1];
	m.color.b = color[2];
	m.color.a = color[3];
}

