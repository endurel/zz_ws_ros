/**

\file
\author Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <visualization_msgs/Marker.h>
#include <gps_nmea/GpsPlanar.h>
#include <gps_nmea/GpsGST.h>
#include <cmath>
#include <mutex>

using namespace gps_nmea;

namespace
{
  ros::Publisher actual_pos_marker_pub,
      actual_sddev_marker_pub,
      historic_pos_marker_pub;
  double offset_x, offset_y;
  GpsGST sh_gst;
  visualization_msgs::Marker historic_points;
  std::mutex mtx;
#define LOCK std::unique_lock<std::mutex> lock(mtx);
}

void callback_planar(const GpsPlanar& gpsp)
{
  if (gpsp.header.stamp == ros::Time(0))
  {
    return;
  }

  if(actual_pos_marker_pub)
  {
    visualization_msgs::Marker points;
    points.header.frame_id = "/map";
    points.header.stamp = ros::Time::now();
    points.ns = "pavin";
    points.action = visualization_msgs::Marker::ADD;

    points.id = 0;

    points.type = visualization_msgs::Marker::SPHERE;

    points.scale.x = 0.5;
    points.scale.y = 0.5;
    points.scale.z = 0.5;

    points.color.r = 1.0f;
    points.color.g = 0.f;
    points.color.b = 0.f;
    points.color.a = 0.5f;

    points.pose.position.x = gpsp.east - offset_x;
    points.pose.position.y = gpsp.north - offset_y;
    points.pose.position.z = 0.;
    points.pose.orientation.x = 0.0;
    points.pose.orientation.y = 0.0;
    points.pose.orientation.z = 0.0;
    points.pose.orientation.w = 1.0;

    actual_pos_marker_pub.publish(points);
  }

  if(historic_pos_marker_pub)
  {
    geometry_msgs::Point p;

    p.x = gpsp.east - offset_x;
    p.y = gpsp.north - offset_y;
    p.z = 0.;

    historic_points.points.push_back(p);

    historic_pos_marker_pub.publish(historic_points);
  }
  LOCK
  if(actual_sddev_marker_pub)
  {
    visualization_msgs::Marker points;
    points.header.frame_id = "/map";
    points.header.stamp = ros::Time::now();
    points.ns = "pavin";
    points.action = visualization_msgs::Marker::ADD;

    points.id = 1;

    points.type = visualization_msgs::Marker::SPHERE;

    // POINTS markers use x and y scale for width/height respectively
    points.scale.x = sh_gst.stdev_longitude;
    points.scale.y = sh_gst.stdev_latitude;
    points.scale.z = sh_gst.stdev_altitude;

    points.color.r = 0.f;
    points.color.g = 0.f;
    points.color.b = 1.0f;
    points.color.a = 0.5f;

    points.pose.position.x = gpsp.east - offset_x;
    points.pose.position.y = gpsp.north - offset_y;
    points.pose.position.z = 0.;
    points.pose.orientation.x = 0.0;
    points.pose.orientation.y = 0.0;
    points.pose.orientation.z = 0.0;
    points.pose.orientation.w = 1.0;

    actual_sddev_marker_pub.publish(points);
  }

}

void callback_gpsgst(const GpsGST& gpsgst)
{
  if (gpsgst.header.stamp == ros::Time(0))
  {
    return;
  }
  LOCK
  sh_gst = gpsgst;
}

int main( int argc, char** argv )
{
  ros::init(argc, argv, "draw_position");
  ros::NodeHandle n;

  actual_pos_marker_pub = n.advertise<visualization_msgs::Marker>("position_marker", 10);
  actual_sddev_marker_pub = n.advertise<visualization_msgs::Marker>("stddev_pos_marker", 10);
  historic_pos_marker_pub = n.advertise<visualization_msgs::Marker>("historic_pos_marker", 10);

  n.getParam("/offset_east", offset_x);
  n.getParam("/offset_nord", offset_y);

  ros::Subscriber gpsp_sub = n.subscribe("gps_planar", 10, callback_planar);
  ros::Subscriber gpsgst_sub = n.subscribe("gps_gst", 10, callback_gpsgst);

  // global historic points markers intialization
  historic_points.header.frame_id = "/map";
  historic_points.header.stamp = ros::Time::now();
  historic_points.ns = "pavin";
  historic_points.action = visualization_msgs::Marker::ADD;
  historic_points.id = 3;
  historic_points.type = visualization_msgs::Marker::POINTS;
  // POINTS markers use x and y scale for width/height respectively
  historic_points.scale.x = 0.1;
  historic_points.scale.y = 0.1;
  historic_points.scale.z = 0.1;
  historic_points.color.r = 1.f;
  historic_points.color.g = 1.f;
  historic_points.color.b = 0.f;
  historic_points.color.a = 1.0;

  ros::spin();

}

