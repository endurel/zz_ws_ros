/**

\file
\author Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include "pavin_drawing/map_pavin.hpp"

using namespace std;
using namespace gps_nmea;

mapPavin::mapPavin(ros::NodeHandle* nh_)
{
  gps_ref ref_lat, ref_lon;
  double ref_alt;
  std::string convert_type;

  std::string str_local ("geolocal");
  std::string str_utm ("utm");

  nh_->getParam("geolocal_datum/ref_lat_deg", ref_lat.deg);
  nh_->getParam("geolocal_datum/ref_lat_min", ref_lat.min);
  nh_->getParam("geolocal_datum/ref_lat_sec", ref_lat.sec);
  nh_->getParam("geolocal_datum/ref_lon_deg", ref_lon.deg);
  nh_->getParam("geolocal_datum/ref_lon_min", ref_lon.min);
  nh_->getParam("geolocal_datum/ref_lon_sec", ref_lon.sec);
  nh_->getParam("geolocal_datum/ref_alt", ref_alt);
  nh_->getParam("planar_type", convert_type);
  nh_->getParam("offset_east", offset_x);
  nh_->getParam("offset_north", offset_y);

  if (convert_type.compare(str_local) == 0)
    trans_type = GpsPlanar::TRANSFORM_TYPE_LOCAL_PLANAR;
  else if (convert_type.compare(str_utm) == 0)
    trans_type = GpsPlanar::TRANSFORM_TYPE_UTM;
  else
    trans_type = GpsPlanar::TRANSFORM_TYPE_INVALID;
  
  const GeoCoord station_clfd(Wgs84(ref_lat.deg,
                                    ref_lat.min,
                                    ref_lat.sec,
                                    ref_lon.deg,
                                    ref_lon.min,
                                    ref_lon.sec), ref_alt);

  conv = std::make_unique<GeoToPlanarConverter>(station_clfd);
}

void mapPavin::loadPavinFiles(std::string folder)
{
  // hall
  haX.resize(1);
  haY.resize(1);
  for(size_t i=0; i<haX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/hall_"<<i+1<<".txt";
    loadGpsPoints(haX[i],haY[i],ss.str());
  }

  // grillages
  grX.resize(5);
  grY.resize(5);
  for(size_t i=0; i<grX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/grillage_"<<i+1<<".txt";
    loadGpsPoints(grX[i],grY[i],ss.str());
  }
  // trottoirs
  trX.resize(5);
  trY.resize(5);
  for(size_t i=0; i<trX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/trottoir_"<<i+1<<".txt";
    loadGpsPoints(trX[i],trY[i],ss.str());
  }
  // passages pieton
  ppX.resize(17);
  ppY.resize(17);
  for(size_t i=0; i<ppX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/passage_pieton_"<<i+1<<".txt";
    loadGpsPoints(ppX[i],ppY[i],ss.str());
  }
  // lignes blanches
  lbX.resize(33);
  lbY.resize(33);
  for(size_t i=0; i<lbX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/ligne_blanche_"<<i+1<<".txt";
    loadGpsPoints(lbX[i],lbY[i],ss.str());
  }
  // contours de route
  crX.resize(8);
  crY.resize(8);
  for(size_t i=0; i<crX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/contour_route_"<<i+1<<".txt";
    loadGpsPoints(crX[i],crY[i],ss.str());
  }
  // portes
  pX.resize(4);
  pY.resize(4);
  for(size_t i=0; i<pX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/porte_"<<i+1<<".txt";
    loadGpsPoints(pX[i],pY[i],ss.str());
  }
  // blocs
  bX.resize(9);
  bY.resize(9);
  for(size_t i=0; i<bX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/bloc_"<<i+1<<".txt";
    loadGpsPoints(bX[i],bY[i],ss.str());
  }
  // parcelle de jardin autour des blocs
  tbX.resize(6);
  tbY.resize(6);
  for(size_t i=0; i<tbX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/tour_bloc_"<<i+1<<".txt";
    loadGpsPoints(tbX[i],tbY[i],ss.str());
  }
  // stop
  stopX.resize(1);
  stopY.resize(1);
  for(size_t i=0; i<stopX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/stop_"<<i+1<<".txt";
    loadGpsPoints(stopX[i],stopY[i],ss.str());
  }
  // separation stop
  sstopX.resize(1);
  sstopY.resize(1);
  for(size_t i=0; i<sstopX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/separation_stop_"<<i+1<<".txt";
    loadGpsPoints(sstopX[i],sstopY[i],ss.str());
  }
  // verdure
  vX.resize(2);
  vY.resize(2);
  for(size_t i=0; i<vX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/verdure_"<<i+1<<".txt";
    loadGpsPoints(vX[i],vY[i],ss.str());
  }
  // rond point
  rpX.resize(1);
  rpY.resize(1); //2
  for(size_t i=0; i<rpX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/rond_point_"<<i+1<<".txt";
    loadGpsPoints(rpX[i],rpY[i],ss.str());
  }
  // places de parking
  pkX.resize(2);
  pkY.resize(2);
  for(size_t i=0; i<pkX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/parking_"<<i+1<<".txt";
    loadGpsPoints(pkX[i],pkY[i],ss.str());
  }

  wX.resize(10);
  wY.resize(10);
  for(size_t i=0; i<wX.size(); i++)
  {
    stringstream ss;
    ss<<folder<<"/bloc_mur_"<<i+1<<".txt";
    loadGpsPoints(wX[i],wY[i],ss.str());
  }

}
void mapPavin::loadGpsPoints(std::vector<double>& dataX, vector<double>& dataY, std::string file)
{
  ifstream ifs(file.c_str());
    
  if( !ifs.is_open() )
  {
    std::cout << "file not found : " << file << endl;
    return;
  }
  string line;
  double lat, lon;

  while(getline(ifs,line))
  {
    istringstream iss(line);
    iss >> lon >> lat;
    
    if(trans_type==GpsPlanar::TRANSFORM_TYPE_LOCAL_PLANAR)
    {
      const auto pos = conv->position(lat,lon);

      dataX.push_back(pos.east);
      dataY.push_back(pos.north);

    }
    else if(trans_type==GpsPlanar::TRANSFORM_TYPE_UTM)
    {
      double north, east;
      std::string zone;

      LLtoUTM(lat, lon, north, east, zone);

      dataX.push_back(east);
      dataY.push_back(north);
    }
  }
}

void mapPavin::drawWalls(mapPavin& pavin, ros::Publisher& mp, visualization_msgs::Marker& m)
{

  // hall
  draw2DListOfBrokenLine(pavin.haX,pavin.haY,mp,m);

  // blocs
  draw2DListOfBrokenLine(pavin.bX,pavin.bY,mp,m);

  // now, the walls :
  draw2DListOfBrokenLine(pavin.wX,pavin.wY,mp,m);

}

void mapPavin::drawPavin(mapPavin& pavin, ros::Publisher& mp, visualization_msgs::Marker& m)
{

  // grillages
  draw2DListOfBrokenLine(pavin.grX,pavin.grY,mp,m);

  // passages pieton
  draw2DListOfBrokenLine(pavin.ppX,pavin.ppY,mp,m);

  // lignes blanches
  draw2DListOfBrokenLine(pavin.lbX,pavin.lbY,mp,m);

  // portes
  draw2DListOfBrokenLine(pavin.pX,pavin.pY,mp,m);

  // stop
  draw2DListOfBrokenLine(pavin.stopX,pavin.stopY,mp,m);

  // places de parking
  draw2DListOfBrokenLine(pavin.pkX,pavin.pkY,mp,m);

}

void mapPavin::drawRoad(mapPavin& pavin, ros::Publisher& mp, visualization_msgs::Marker& m)
{

  // trottoirs
  draw2DListOfBrokenLine(pavin.trX,pavin.trY,mp,m);

  // verdure
  draw2DListOfBrokenLine(pavin.vX,pavin.vY,mp,m);

  // rond point
  draw2DListOfBrokenLine(pavin.rpX,pavin.rpY,mp,m);

  // parcelle de jardin autour des blocs
  draw2DListOfBrokenLine(pavin.tbX,pavin.tbY,mp,m);

  // separation stop
  draw2DListOfBrokenLine(pavin.sstopX,pavin.sstopY,mp,m);

  // contours de route
  draw2DListOfBrokenLine(pavin.crX,pavin.crY,mp,m);

}

void mapPavin::setColor(visualization_msgs::Marker& m, double r,double g,double b,double a)
{
  m.color.r = r;
  m.color.g = g;
  m.color.b = b;
  m.color.a = a;

}
/*
 * Line list version
 */

void mapPavin::draw2DListOfBrokenLine(const vector<vector<double> >& dataX, const vector<vector<double> >& dataY,
                                      ros::Publisher& mp, visualization_msgs::Marker& m)
{
  geometry_msgs::Point p;

  for(size_t j=0; j<dataX.size(); j++) // object list
  {
    for(size_t i=0; i<dataX[j].size()-1; i++) // point coordinates
    {
      p.x = dataX[j][i] - offset_x;
      p.y = dataY[j][i] - offset_y;
      p.z = 0.;
      m.points.push_back(p);
      p.x = dataX[j][i+1] - offset_x;
      p.y = dataY[j][i+1] - offset_y;
      p.z = 0.;
      m.points.push_back(p);
    }
  }

  mp.publish(m);

}
