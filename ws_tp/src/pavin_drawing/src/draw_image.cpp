#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <visualization_msgs/Marker.h>

void sendImageToRviz(std::string frame_id, int id, ros::Publisher* marker_pub, cv::Mat src, double sizeSq, std::pair<double, double> LUcorner) 
{
    visualization_msgs::Marker image;
    image.header.frame_id = frame_id;
    image.header.stamp = ros::Time::now();
    image.ns = "image";
    image.id = id;
    image.action = visualization_msgs::Marker::ADD;
    image.type = visualization_msgs::Marker::TRIANGLE_LIST;
    image.scale.x = 1;
    image.scale.y = 1;
    image.scale.z = 1;

    double pix = sizeSq / src.rows;
        geometry_msgs::Point p;
    std_msgs::ColorRGBA crgb;

    for(int r = 0; r < src.rows; ++r) {
        for(int c = 0; c < src.rows; ++c) {
        cv::Vec3b intensity = src.at<cv::Vec3b>(r, c);
        crgb.r = intensity.val[2] / 255.0;
        crgb.g = intensity.val[1] / 255.0;
        crgb.b = intensity.val[0] / 255.0;
        crgb.a = 1.0;

        p.z = 0;
        p.x = LUcorner.first + r * pix;
        p.y = LUcorner.second + c * pix;
        image.points.push_back(p);
        image.colors.push_back(crgb);
        p.x = LUcorner.first + (r + 1) * pix;
        p.y = LUcorner.second + c * pix;
        image.points.push_back(p);
        image.colors.push_back(crgb);
        p.x = LUcorner.first + r * pix;
        p.y = LUcorner.second + (c + 1) * pix;
        image.points.push_back(p);
        image.colors.push_back(crgb);
        p.x = LUcorner.first + (r + 1) * pix;
        p.y = LUcorner.second + c * pix;
        image.points.push_back(p);
        image.colors.push_back(crgb);
        p.x = LUcorner.first + (r + 1) * pix;
        p.y = LUcorner.second + (c + 1) * pix;
        image.points.push_back(p);
        image.colors.push_back(crgb);
        p.x = LUcorner.first + r * pix;
        p.y = LUcorner.second + (c + 1) * pix;
        image.points.push_back(p);
        image.colors.push_back(crgb);
        }
    }
    marker_pub->publish(image);
    
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_publisher");
  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);
  image_transport::Publisher pub = it.advertise("camera/image", 1);
  cv::Mat image = cv::imread("/home/malaterre/Bureau/Capture_video_pavin_75m.png", cv::IMREAD_COLOR);
//   cv::waitKey(30);
  sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
  ros::Publisher marker_pub_tr_array (nh.advertise<visualization_msgs::Marker>("visualization_marker_tr_array", 10));
  std::pair<double, double> p_offset;
  p_offset.first = 0.;
  p_offset.second = 0.;
  
  
  ros::Rate loop_rate(5);
  while (nh.ok()) {
    pub.publish(msg);
    sendImageToRviz("map",485, &marker_pub_tr_array, image,50, p_offset);
    ros::spinOnce();
    loop_rate.sleep();
  }
}
