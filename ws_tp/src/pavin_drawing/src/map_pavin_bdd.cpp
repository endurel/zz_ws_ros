/**

\file
\author Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include "pavin_drawing/map_pavin_bdd.hpp"

using namespace std;
using namespace libpavinbdd;

const std::string mapPavinBDD::str_local ("geolocal");
const std::string mapPavinBDD::str_utm ("utm");
const std::string mapPavinBDD::str_lambertIIe ("lambertIIe");

mapPavinBDD::mapPavinBDD(ros::NodeHandle* nh_)
{
  bool apply_offset(false);
  bool apply_secundary_offset(false);
  
  nh_->param<bool>("print_elements", print_elements,bool(false));
  nh_->param<bool>("apply_offset", apply_offset, bool(false));
  nh_->param<bool>("apply_secundary_offset", apply_secundary_offset, bool(false));

  if(apply_offset)
  {
    ROS_INFO("GET apply_offset flag : TRUE : allowed");
    nh_->param<double>("offset_east", offset_x,-67.9736);
    nh_->param<double>("offset_north", offset_y,-82.1364);
    ROS_INFO("---> GET offset_x  value : %fm and offset_y  value : %fm", offset_x, offset_y);
  }
  else
    ROS_INFO("GET apply_offset flag : FALSE : not allowed");

  if(apply_secundary_offset)
  {
    ROS_INFO("GET apply_secundary_offset flag : TRUE : allowed");
    nh_->param<double>("secundary_offset_east", additional_offset_x,0.);
    nh_->param<double>("secundary_offset_north", additional_offset_y,0.);
  ROS_INFO("GET _additional_offset_x  value : %fm and _additional_offset_y  value : %fm", additional_offset_x, additional_offset_y);
  }
  else
    ROS_INFO("GET apply_secundary_offset flag : FALSE : not allowed");

  nh_->param<std::string>("planar_type",convert_type, "geolocal");   
  ROS_INFO("GET  %s reference planar transform ", convert_type.c_str());
  
  if (convert_type.compare(str_local) == 0)
  {
    offset_x += additional_offset_x;
    offset_y += additional_offset_y;
  }
  else if (convert_type.compare(str_utm) == 0)
  {    
    nh_->getParam("utm_datum/ref_x", ref_x);
    nh_->getParam("utm_datum/ref_y", ref_y);
    
    ROS_INFO("GET  ref_x : %f  et ref_x : %f", ref_x, ref_y);
    offset_x += ref_x + additional_offset_x;
    offset_y += ref_y + additional_offset_y;
  }
  else if (convert_type.compare(str_lambertIIe) == 0)
  {
    nh_->getParam("lambert_datum/ref_x", ref_x);
    nh_->getParam("lambert_datum/ref_y", ref_y);
    
    ROS_INFO("GET  ref_x : %f  et ref_x : %f", ref_x, ref_y);
    offset_x += ref_x + additional_offset_x;
    offset_y += ref_y + additional_offset_y;
    
  }
  else
    ROS_INFO("No convert type param detected : apply default geolocal offset");
}

void mapPavinBDD::drawLines(std::vector<libpavinbdd::Line *> l, visualization_msgs::Marker& m)
{
  double x1,y1,x2,y2;
  geometry_msgs::Point p;

  for (size_t i=0; i<l.size(); ++i)
  {
    l.at(i)->points(x1,y1,x2,y2);
    
    p.x = x1 - offset_x;
    p.y = y1 - offset_y;
    p.z = 0.;
    
    m.points.push_back(p);
    p.x = x2 - offset_x;
    p.y = y2 - offset_y;
    p.z = 0.;
    m.points.push_back(p);
  }
}

void mapPavinBDD::drawLines(std::vector<libpavinbdd::Line *> l, visualization_msgs::Marker& m, std::string label, double depth, double heigh)
{
    double x1,y1,x2,y2;
    geometry_msgs::Point p;
    double x,y;
    MakeGazeboItems gIt;
    
    gIt.setLabel(label);
    gIt.setWallDepth(depth);
    gIt.setWallHeigh(heigh);
    
    for (size_t i=0; i<l.size(); ++i)
    {

        l.at(i)->points(x1,y1,x2,y2);
        
        p.x = x1 - offset_x;
        p.y = y1 - offset_y;
        p.z = 0.;
        x=p.x;
        y=p.y;
        
        m.points.push_back(p);
        p.x = x2 - offset_x;
        p.y = y2 - offset_y;
        p.z = 0.;
        m.points.push_back(p);
       
        if (label.compare("Sidewalk") == 0)
        {
            gIt.setPositions(x,y,p.x,p.y);
            gIt.computeRelative();
            gIt.setIndex(int(i));
            if(print_elements)
                gIt.print();
        } 
       else if (label.compare("Wall") == 0)
        {
            gIt.setPositions(x,y,p.x,p.y);
            gIt.computeRelative();
            gIt.setIndex(int(i));
            if(print_elements)
                gIt.print();
        }
        else  if (label.compare("Fence") == 0 )
        {
            gIt.setPositions(x,y,p.x,p.y);
            gIt.computeRelative();
            gIt.setIndex(int(i));
            if(print_elements)
                gIt.print();
        }
       
    }
}

void mapPavinBDD::drawPolygons(std::vector<libpavinbdd::Polygon *> poly, visualization_msgs::Marker& m)
{
  geometry_msgs::Point p;
  
  for (size_t i=0; i<poly.size(); ++i)
  {
    for (size_t j=0 ; j<(poly.at(i)->ringExtCount()-1 ); ++j)
    {
        p.x = poly.at(i)->ext.x(j) - offset_x;
        p.y = poly.at(i)->ext.y(j) - offset_y;
        p.z = 0.;
        m.points.push_back(p);
        p.x = poly.at(i)->ext.x(j+1) - offset_x;
        p.y = poly.at(i)->ext.y(j+1) - offset_y;
        p.z = 0.;
        m.points.push_back(p);
    }
  }
}

void mapPavinBDD::drawPolygons(std::vector<libpavinbdd::Polygon *> poly, visualization_msgs::Marker& m, std::string label, double depth, double heigh)
{
  geometry_msgs::Point p;
  double x,y;
  MakeGazeboItems gIt;
  
  gIt.setLabel(label);
  gIt.setWallDepth(depth);
  gIt.setWallHeigh(heigh);
  
  for (size_t i=0; i<poly.size(); ++i)
  {
    for (size_t j=0 ; j<(poly.at(i)->ringExtCount()-1 ); ++j)
    {
        p.x = poly.at(i)->ext.x(j) - offset_x;
        p.y = poly.at(i)->ext.y(j) - offset_y;
        p.z = 0.;
        x=p.x;
        y=p.y;
        m.points.push_back(p);
        p.x = poly.at(i)->ext.x(j+1) - offset_x;
        p.y = poly.at(i)->ext.y(j+1) - offset_y;
        p.z = 0.;
        m.points.push_back(p);
        if (label.compare("RoadMark") == 0 )
        {
            gIt.setPositions(x,y,p.x,p.y);
            gIt.computeRelative();
            gIt.setIndex(int(i)*10+j);
            if(print_elements)
                gIt.print();
        }
    }
  }
}

void mapPavinBDD::drawCircles(std::vector<Circle *> circle, ros::Publisher& mp, visualization_msgs::Marker& m)
{
  for (size_t i=0; i<circle.size(); ++i)
  {

    m.pose.position.x = circle[i]->xCenter() - offset_x;
    m.pose.position.y = circle[i]->yCenter() - offset_y;
    m.pose.position.z = 0.5;
    m.scale.x = circle[i]->radius();
    m.scale.y = circle[i]->radius();
    m.scale.z = 1;
    m.id = circle[i]->keyGeom();
  }
}

void mapPavinBDD::drawCircle(Circle circle, visualization_msgs::Marker& m, int i, double heigth)
{
    m.pose.position.x = circle.xCenter() - offset_x;
    m.pose.position.y = circle.yCenter() - offset_y;
    m.pose.position.z = heigth/2;
    m.scale.x = circle.radius();
    m.scale.y = circle.radius();
    m.scale.z = heigth;
    m.id = i;
}

void mapPavinBDD::setColor(visualization_msgs::Marker& m, double r,double g,double b,double a)
{
  m.color.r = r;
  m.color.g = g;
  m.color.b = b;
  m.color.a = a;

}

