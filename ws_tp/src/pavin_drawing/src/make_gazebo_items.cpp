/**

\file
\author Laurent Malaterre (2019)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "pavin_drawing/make_gazebo_items.hpp"
#include "pavin_drawing/string_collection_make_gazebo.hpp"
#include <cmath>
#include <iostream>

using namespace std;
using namespace string_collection_make_gazebo;

MakeGazeboItems::MakeGazeboItems()
{
}

bool MakeGazeboItems::compute()
{
    ComputeLength();
    ComputeCenterItem();
    ComputeRotation();
    ComputeCoordinatesCenter();
}

bool MakeGazeboItems::computeLine()
{
    ComputeLength();
    ComputeCenterLine();
    ComputeRotation();
}

bool MakeGazeboItems::computeRelative()
{
    ComputeLength();
    ComputeRotation();
    findOppositeShapePoint();
    ComputeCenterLine2();
}
void MakeGazeboItems::findOppositeShapePoint()
{   
    xB0_ = xA_ + length_ * std::cos(theta_) - (-p_) * std::sin(theta_);
    yB0_ = yA_ + length_ * std::sin(theta_) + (-p_) * std::cos(theta_);

}

// NOTE calcul de la longueur de l'objet.
void MakeGazeboItems::ComputeLength()
{
//     std::cout << "xA_ : " << xA_ << " xB_ : " << xB_ << std::endl;
//     std::cout << "yA_ : " << yA_ << " yB_ : " << yB_ << std::endl;
//     std::cout << "length_ : " << length_  << std::endl;
    length_ = sqrt(pow((xB_-xA_),2.0)+pow((yB_-yA_),2.0));
//     std::cout << "length : " << world_item_text_1 << "  : " << length_ << std::endl;
}

// NOTE calcul du centre de l'objet. En coordonnées relatives au point A.
void MakeGazeboItems::ComputeCenterItem()
{
    xC0_=(length_-xA_)/2.0;
    yC0_=p_/2.;
}

void MakeGazeboItems::ComputeCenterLine()
{
    xCtheta_= (xB_+xA_)/2.0;
    yCtheta_= (yB_+yA_)/2.0;
}

void MakeGazeboItems::ComputeCenterLine2()
{
    xCtheta_= (xB0_+xA_)/2.0;
    yCtheta_= (yB0_+yA_)/2.0;
}


// NOTE calcul de l'orientation de l'objet
void MakeGazeboItems::ComputeRotation()
{
    theta_=atan2((yB_-yA_),(xB_-xA_));
}

void MakeGazeboItems::ComputeCoordinatesCenter()
{
    double x = xC0_-xA_;
    double y = yC0_-yA_;
    xCtheta_= x*cos(theta_) + y*sin(theta_) + xA_;
    yCtheta_= -x*sin(theta_) + y*cos(theta_) + yA_;
}
    
void MakeGazeboItems::setPositions(const double xA,const double yA,const double xB,const double yB)
{
    xA_=xA;
    yA_=yA;
    xB_=xB;
    yB_=yB;
}
    
void MakeGazeboItems::setWallDepth(const double p)
{
    p_=p;
}
   
void MakeGazeboItems::setWallHeigh(const double h)
{
    h_=h;
}

void MakeGazeboItems::setLabel(std::string l)
{
    label_ = l;
}

void MakeGazeboItems::setIndex(int i)
{
    index_ = i;
}

void MakeGazeboItems::print()
{
//     std::cout << "Points : xA_ = " << xA_ << " yA = " << yA_<< " xB_ = " << xB_ << " yB = " << yB_ << std::endl;
//     std::cout << "length_ : " << length_ << std::endl;
    std::cout << "     <link name='"<< label_ << "_" << index_<< "'>" << std::endl;
    std::cout << "          <collision name='" << label_ << "_" << index_<<"_Collision'>" << std::endl;
    std::cout << "               <geometry>\n\
                <box>" << std::endl;
                
    std::cout << "                  <size>"<<  length_ << " " << p_ << " " << h_ << "</size>" << std::endl;
               
    std::cout << "              </box>\n\
            </geometry>" << std::endl;
    std::cout << "              <pose frame=''>0 0 " << h_/2 << " 0 0 0</pose>"<< std::endl;
    
    std::cout << world_item_text_2 << std::endl;
    
    std::cout << "          <visual name='" << label_ << "_" << index_<<"_Visual'>" << std::endl;
    std::cout << "              <pose frame=''>0 0 " << h_/2 << " 0 0 0</pose>"<< std::endl;
    std::cout << "               <geometry>\n\
                <box>" << std::endl;
                
    std::cout << "                  <size>"<<  length_ << " " << p_ << " " << h_ << "</size>" << std::endl;
               
    std::cout << "              </box>\n\
            </geometry>" << std::endl;
            
    std::cout << world_item_text_3 << std::endl;
    
    if (label_.compare("Sidewalk") == 0)
        std::cout << "              <name>Gazebo/DarkGrey</name>" << std::endl;
    else if (label_.compare("Wall") == 0)
        std::cout << "              <name>Gazebo/Grey</name>" << std::endl;
    else if (label_.compare("Fence") == 0)
        std::cout << "              <name>Gazebo/Green</name>" << std::endl;
    else if (label_.compare("RoadMark") == 0)
        std::cout << "              <name>Gazebo/WhiteGlow</name>" << std::endl;

    std::cout << world_item_text_4 << std::endl;
    
    std::cout << "          <pose frame=''>"<< xCtheta_ << " " << yCtheta_ << " 0 0 0 " << theta_ << " </pose>"<< std::endl;
    
    std::cout << world_item_text_5 << std::endl;
}
          
