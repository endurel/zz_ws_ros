/**

\file
\author Laurent Malaterre (2018)
\copyright 2018 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ros/ros.h>
#include <memory>
#include <visualization_msgs/Marker.h>
#include <cmath>
#include <stdlib.h>
#include "pavin_drawing/map_pavin_bdd.hpp"

using namespace std;
using namespace libpavinbdd;

void setColor(visualization_msgs::Marker& m, std::vector<double> const & color);

int main( int argc, char** argv )
{
  ros::init(argc, argv, "draw_pavin_bdd");
  ros::NodeHandle n;
  ros::NodeHandle node_private("~");
  libpavinbdd::PavinBDD bdd;
  
  std::string frame_id("/map");
  std::string data_base_name("pavin_geolocal_CLFD");
  std::string visualization_marker_topic_name("visualization_marker");
  
  node_private.getParam("data_base_name",data_base_name);
  node_private.getParam("pavin_drawing_frameid",frame_id);
  node_private.getParam("visualization_marker_topic_name",visualization_marker_topic_name);
  
  ROS_INFO_STREAM("data_base_name : " << data_base_name);
  bdd.open("127.0.0.1",data_base_name.c_str(),"gisuser","gisuser");
  
  if(bdd.isOpen())
      ROS_INFO_STREAM( "BDD is Open ");
  else
      ROS_INFO_STREAM( "BDD is NOT Open ");
  
  std::vector<libpavinbdd::Polygon *> polygons_roadMarks_bdd;
  std::vector<libpavinbdd::Line *>  lines_road_bdd,
      lines_wall_bdd,
      lines_fence_bdd;
  std::vector<libpavinbdd::Circle *>   circle_light_signs_bdd, circle_landmarks_bdd;
  vector<libpavinbdd::Circle *>::iterator clsbdd_ptr;
  
  ros::Publisher marker_pub (n.advertise<visualization_msgs::Marker>(visualization_marker_topic_name, 100));
  ros::Rate r(1);
  
  mapPavinBDD map(&node_private);
  
  visualization_msgs::Marker line_roadMarks,line_road, line_wall, line_fence, circle_light_signs_marker, circle_landmark_marker;
  std::vector<visualization_msgs::Marker> circle_light_signs, circle_landmark;
  vector<visualization_msgs::Marker>::iterator cls_ptr; 
  
  line_road.header.frame_id = frame_id;
  line_roadMarks.header.frame_id = frame_id;
  line_wall.header.frame_id = frame_id;
  line_fence.header.frame_id = frame_id;
  circle_light_signs_marker.header.frame_id = frame_id;
  circle_landmark_marker.header.frame_id = frame_id;
  
  line_road.ns = "pavin_bdd";
  line_roadMarks.ns = "pavin_bdd";
  line_wall.ns = "pavin_bdd";
  line_fence.ns = "pavin_bdd";
  circle_light_signs_marker.ns = "pavin_bdd";
  circle_landmark_marker.ns = "pavin_bdd";

  line_road.action = visualization_msgs::Marker::ADD;
  line_roadMarks.action = visualization_msgs::Marker::ADD;
  line_wall.action = visualization_msgs::Marker::ADD;
  line_fence.action = visualization_msgs::Marker::ADD;
  circle_light_signs_marker.action = visualization_msgs::Marker::ADD;
  circle_landmark_marker.action = visualization_msgs::Marker::ADD;
  
  line_road.pose.orientation.w = 1.0;
  line_roadMarks.pose.orientation.w = 1.0;
  line_wall.pose.orientation.w = 1.0;
  line_fence.pose.orientation.w = 1.0;
  circle_light_signs_marker.pose.orientation.w = 1.0;
  circle_landmark_marker.pose.orientation.w = 1.0;
  
  line_roadMarks.id = 0;
  line_road.id = 1;
  line_wall.id = 2;
  line_fence.id = 3;
  circle_light_signs_marker.id = 4;  // set by default, we apply the pavingeom number for id when sending
  circle_landmark_marker.id = 5; // set by default, we apply the pavingeom number for id when sending
  
  line_road.type = visualization_msgs::Marker::LINE_LIST;
  line_roadMarks.type = visualization_msgs::Marker::LINE_LIST;
  line_wall.type = visualization_msgs::Marker::LINE_LIST;
  line_fence.type = visualization_msgs::Marker::LINE_LIST;
  circle_light_signs_marker.type = visualization_msgs::Marker::CYLINDER;
  circle_landmark_marker.type = visualization_msgs::Marker::CYLINDER;
  
  line_road.scale.x  = 0.1;
  line_roadMarks.scale.x = 0.1;
  line_wall.scale.x = 0.1;
  line_fence.scale.x = 0.1;
  circle_light_signs_marker.scale.x = 0.1;
  circle_landmark_marker.scale.x = 0.1;
  
  using VecD = std::vector<double>;
  setColor(line_road, node_private.param("road_color", VecD{ 0.,1.,1.,1.}));
  setColor(line_roadMarks, node_private.param("roadMarks_color", VecD{ 1.,1.,1.,1.}));
  setColor(line_wall, node_private.param("wall_color", VecD{.5,.5,.5,1}));
  setColor(line_fence, node_private.param("fence_color", VecD{ 1.,1.,0.,1.}));      
  setColor(circle_light_signs_marker, node_private.param("circle_light_signs_color", VecD{1.,0.,0.,1.}));
  setColor(circle_landmark_marker, node_private.param("circle_landmark_color", VecD{1.,1.,1.,1.}));
  
  line_road.header.stamp = ros::Time::now();
  line_roadMarks.header.stamp = ros::Time::now();
  line_wall.header.stamp = ros::Time::now();
  line_fence.header.stamp = ros::Time::now();
  circle_light_signs_marker.header.stamp = ros::Time::now();
  circle_landmark_marker.header.stamp = ros::Time::now();
  
  // NOTE Draw Roads
  bdd.getAllSidewalks(lines_road_bdd);
  map.drawLines(lines_road_bdd ,line_road,"Sidewalk",0.1, 0.01);
  for (size_t i=0; i<lines_road_bdd.size(); ++i) delete lines_road_bdd.at(i);
    lines_road_bdd.clear();
  
  // NOTE Draw Roads Marks
  bdd.getAllRoadMark(polygons_roadMarks_bdd);
  map.drawPolygons(polygons_roadMarks_bdd, line_roadMarks,"RoadMark",0.05, 0.01);
  for (size_t i=0; i<polygons_roadMarks_bdd.size(); ++i) delete polygons_roadMarks_bdd.at(i);
    polygons_roadMarks_bdd.clear();
  
  // NOTE Draw External fences
  bdd.getAllFenceExt(lines_fence_bdd);
  map.drawLines(lines_fence_bdd ,line_fence,"Fence",-0.01,1.5);
  for (size_t i=0; i<lines_fence_bdd.size(); ++i) delete lines_fence_bdd.at(i);
    lines_fence_bdd.clear(); 
  
  // NOTE Draw External face of the walls
  bdd.getAllWallExt(lines_wall_bdd);
  map.drawLines(lines_wall_bdd, line_wall, "Wall",-0.15,2.5);
  for (size_t i=0; i<lines_wall_bdd.size(); ++i) delete lines_wall_bdd.at(i);
    lines_wall_bdd.clear();  
  
  // NOTE Draw Light and RoadSigns Circles
  bdd.getAllCircles(circle_light_signs_bdd);    
  for (size_t i=0; i<circle_light_signs_bdd.size(); ++i)
  {
    Circle circle = *circle_light_signs_bdd.at(i);
    // NOTE Identification arbitraire à partir de 100 des markers cercle.
    // Idealement lire keyGeom mais la recupération à ce niveau est buggée.
    map.drawCircle(circle, circle_light_signs_marker,i+100, 2);
    circle_light_signs.push_back(circle_light_signs_marker);
  }
  for (size_t i=0; i<circle_light_signs_bdd.size(); ++i) delete circle_light_signs_bdd.at(i);
    circle_light_signs_bdd.clear();
  
  // NOTE Draw Landmarks Circles
  bdd.getLandMarksCircles(circle_landmarks_bdd);    
  for (size_t i=0; i<circle_landmarks_bdd.size(); ++i)
  {
    Circle circle_lmrk = *circle_landmarks_bdd.at(i);
    // NOTE Identification arbitraire à partir de 100 des markers cercle.
    // Idealement lire keyGeom mais la recupération à ce niveau est buggée.
    map.drawCircle(circle_lmrk, circle_landmark_marker,i+200, 0.05);
    circle_landmark.push_back(circle_landmark_marker);
  }
  for (size_t i=0; i<circle_landmarks_bdd.size(); ++i) delete circle_landmarks_bdd.at(i);
    circle_landmarks_bdd.clear();
  
  while (ros::ok())
  {    
    marker_pub.publish(line_road);
    marker_pub.publish(line_roadMarks);
    marker_pub.publish(line_fence);
    marker_pub.publish(line_wall);

    for(cls_ptr = circle_light_signs.begin(); cls_ptr < circle_light_signs.end(); cls_ptr++)
    {
        marker_pub.publish(*cls_ptr);
    }
    for(cls_ptr = circle_landmark.begin(); cls_ptr < circle_landmark.end(); cls_ptr++)
    {
        marker_pub.publish(*cls_ptr);
    }
    r.sleep();
  }
}

void setColor(visualization_msgs::Marker& m, std::vector<double> const & color)
{
  m.color.r = color[0];
  m.color.g = color[1];
  m.color.b = color[2];
  m.color.a = color[3];
}
