/**

\file
\author Laurent Malaterre (2018)
\copyright 2018 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef MAPPAVIN_BDD_HPP
#define MAPPAVIN_BDD_HPP

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <string>
#include <vector>
#include <math.h>
#include <memory>
#include "ip_postgis_bdd_lib/pavinbdd.hpp"
#include "ip_postgis_bdd_lib/polygon.hpp"
#include "ip_postgis_bdd_lib/line.hpp"
#include "ip_postgis_bdd_lib/circle.hpp"
#include "pavin_drawing/make_gazebo_items.hpp"

using namespace std;
// using namespace gps_nmea;

class mapPavinBDD
{
public:
  static const std::string str_local;
  static const std::string str_utm;
  static const std::string str_lambertIIe;

public:
  mapPavinBDD() :
    offset_x(0.0),
    offset_y(0.0),
    ref_x(0.),
    ref_y(0.)
  {
  }
  explicit mapPavinBDD(ros::NodeHandle*);

  virtual ~mapPavinBDD()
  {
  }

  void drawLines(std::vector<libpavinbdd::Line *>,  visualization_msgs::Marker&);
  void drawLines(std::vector<libpavinbdd::Line *>,  visualization_msgs::Marker&,  std::string, double, double);
  void drawPolygons(std::vector<libpavinbdd::Polygon *>,  visualization_msgs::Marker&);
  void drawPolygons(std::vector<libpavinbdd::Polygon *>,  visualization_msgs::Marker&,  std::string, double, double);
  void drawCircles(std::vector<libpavinbdd::Circle *>, ros::Publisher&, visualization_msgs::Marker&);
  void drawCircle(libpavinbdd::Circle , visualization_msgs::Marker&, int, double heigth = 0.5);
  void setColor(visualization_msgs::Marker& , double ,double ,double ,double );
  // NOTE Decalage pour local geoplanar (base 'IGN referenced' + (offset_x, offset_y))
  double offset_x, offset_y;
  double additional_offset_x;
  double additional_offset_y;
  bool _altitude_valued;
  double ref_x, ref_y;
  std::string convert_type;
  bool print_elements;

};

#endif
