/**

\file
\author Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef MAPPAVIN_HPP
#define MAPPAVIN_HPP

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <string>
#include <vector>
#include <math.h>
#include <memory>
#include "projection/LocalPlanarCoord.hpp"
#include "projection/UTMCoord.hpp"
// #include "projection/LambertCoord.hpp"
#include <gps_nmea/GpsPlanar.h>

using namespace std;
using namespace gps_nmea;

class mapPavin
{
public:

  // NOTE Decalage pour local geoplanar (base 'OPGC')
  mapPavin() :
    offset_x(0.00),
    offset_y(0.0)
  {
  }
  explicit mapPavin(ros::NodeHandle*);

  virtual ~mapPavin()
  {
  }

  void loadPavinFiles(std::string );
  void drawPavin(mapPavin& , ros::Publisher& , visualization_msgs::Marker& );
  void drawRoad(mapPavin& , ros::Publisher& , visualization_msgs::Marker& );
  void drawWalls(mapPavin& , ros::Publisher& , visualization_msgs::Marker& );
private :
  void loadGpsPoints(std::vector<double>& , std::vector<double>& , std::string );
  void setColor(visualization_msgs::Marker& , double ,double ,double ,double );
  void draw2DListOfBrokenLine(const vector<vector<double> >& , const vector<vector<double> >& ,
                              ros::Publisher& , visualization_msgs::Marker& );
  std::vector<std::vector<double> > mapXY, haX, grX, trX, ppX, lbX, crX, pX, bX, tbX, stopX, sstopX, vX, rpX, pkX, wX;
  std::vector<std::vector<double> > haY, grY, trY, ppY, lbY, crY, pY, bY, tbY, stopY, sstopY, vY, rpY, pkY, wY;

  double offset_x, offset_y;
  uint8_t trans_type = GpsPlanar::TRANSFORM_TYPE_LOCAL_PLANAR;
  std::unique_ptr<GeoToPlanarConverter> conv;

//   geoconv::LambertIIe l2e;
};

#endif
