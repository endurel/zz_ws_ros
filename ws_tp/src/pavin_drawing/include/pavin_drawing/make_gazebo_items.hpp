/**

\file
\author Laurent Malaterre (2019)
\copyright 2018 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef MAKE_GAZEBO_ITEMS_HPP
#define MAKE_GAZEBO_ITEMS_HPP

#include <string>


class MakeGazeboItems
{
public:
    MakeGazeboItems();
    bool compute();
    bool computeLine();
    bool computeRelative();
    void print();
    void reset();
    void setPositions(const double,const double,const double,const double);
    void setWallDepth(const double);
    void setWallHeigh(const double);
    void setLabel(std::string);
    void setIndex(int);
    std::string getLabel(){return label_;};
    
private:
    void ComputeLength();
    void ComputeCenterItem();
    void ComputeCenterLine();
    void ComputeCenterLine2();
    void findOppositeShapePoint();
    void ComputeRotation();
    void ComputeCoordinatesCenter();
    
    std::string label_;
    int index_;
    double xA_,yA_,xB_,yB_,p_,h_,xB0_,yB0_;
    double xC0_,yC0_,xCtheta_,yCtheta_;
    double theta_;
    double length_;
};

#endif
