/**

\file
\author Laurent Malaterre (2019)
\copyright 2018 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef STRING_COLLECTION_MAKE_GAZEBO_HPP
#define STRING_COLLECTION_MAKE_GAZEBO_HPP

#include <string>

namespace string_collection_make_gazebo
{

const std::string world_item_text_1 = "\n\
    <link name='Wall_0'>\n\
        <collision name='Wall_0_Collision'>\n\
          <geometry>\n\
            <box>\n";

const std::string world_item_text_2 = "\
          <max_contacts>10</max_contacts>\n\
          <surface>\n\
            <contact>\n\
              <ode/>\n\
            </contact>\n\
            <bounce/>\n\
            <friction>\n\
              <torsional>\n\
                <ode/>\n\
              </torsional>\n\
              <ode/>\n\
            </friction>\n\
          </surface>\n\
        </collision>";

const std::string world_item_text_3 = "\
          <material>\n\
            <script>\n\
                <uri>file://media/materials/scripts/gazebo.material</uri>";

const std::string world_item_text_4 = "\
            </script>\n\
            <ambient>1 1 1 1</ambient>\n\
          </material>\n\
        </visual>";
        
const std::string world_item_text_5 = "\
        <self_collide>0</self_collide>\n\
        <kinematic>0</kinematic>\n\
        <gravity>1</gravity>\n\
      </link>";

}
#endif
