#include <cmath>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>

std::ofstream results;
int counter;

std::mutex results_mutex;
std::mutex done_mutex;

bool odom_done;
bool rtk_done;
bool nat_done;

void odometry_callback(const nav_msgs::Path::ConstPtr & msg)
{
    if (!odom_done)
    {
	results_mutex.lock();
	std::cout << "Odometry callback\n";
	results << msg->poses[counter].pose.position.x << "," << msg->poses[counter].pose.position.y << ",";
	results_mutex.unlock();

	done_mutex.lock();
	odom_done = true;
	done_mutex.unlock();
    }
}

void gps_rtk_callback(const nav_msgs::Path::ConstPtr& msg)
{
    if (!rtk_done)
    {
	results_mutex.lock();
	std::cout << "Rtk callback\n";
	results << msg->poses[counter].pose.position.x << "," << msg->poses[counter].pose.position.y << ",";
	results_mutex.unlock();

	done_mutex.lock();
	rtk_done = true;
	done_mutex.unlock();
    }
}

void gps_natural_callback(const nav_msgs::Path::ConstPtr& msg)
{
    if (!nat_done)
    {
	results_mutex.lock();
	std::cout << "Natural callback\n";
	results << msg->poses[counter].pose.position.x << "," << msg->poses[counter].pose.position.y << "\n";
	results_mutex.unlock();

	done_mutex.lock();
	nat_done = true;
	done_mutex.unlock();
    }
}

int main(int argc, char ** argv)
{
    ros::init(argc, argv, "bag_pavin_listener"); 

    ros::NodeHandle n;

    results.open("results.csv");
    counter = 0;
    odom_done = false;
    rtk_done = false;
    nat_done = false;

    ros::Subscriber odom_sub = n.subscribe("/path_odom_robot", 3, odometry_callback);
    ros::Subscriber gps_rtk_sub = n.subscribe("/path_gps", 3, gps_rtk_callback);
    ros::Subscriber gps_natural_sub = n.subscribe("/path_gps_natural", 3, gps_natural_callback);

    ros::Rate loop_rate(30); // 300 Hz

    while(ros::ok())
    {
	while (ros::ok() && !(odom_done && rtk_done && nat_done))
	{
	    ros::spinOnce();
	    loop_rate.sleep();
	}

	done_mutex.lock();
	odom_done = false;
	rtk_done = false;
	nat_done = false;
	counter++;
	done_mutex.unlock();

	loop_rate.sleep();
    }

    results.close();

    return 0;
}
