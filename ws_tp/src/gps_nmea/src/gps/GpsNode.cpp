/**

\file
\author Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ros/ros.h>
#include <memory>
#include "gps/SerialGps.hpp"

using namespace gps_nmea;

class gpsNode {
  public:       
    gpsNode(std::string sd , unsigned int br,std::string frame_id, ros::NodeHandle& nh)
    {
      serial_dev_ = sd;
      baudrate_ = br;
      nh_ = nh;
      frame_id_ = frame_id;
    }
    ;

  bool on_start()
  {

    switch(baudrate_)
    {

    case 115200  :
      serial_settings_ = std::make_unique<SerialLink::Settings>(
                           SerialLink::Settings::Speed::B_115200,
                           SerialLink::Settings::StopBits::One,
                           SerialLink::Settings::Parity::Ignore);
      break;

    case 9600  :
      serial_settings_ = std::make_unique<SerialLink::Settings>(
                           SerialLink::Settings::Speed::B_9600,
                           SerialLink::Settings::StopBits::One,
                           SerialLink::Settings::Parity::Ignore);
      break;
    }

    gps = std::make_unique<SerialGps>( serial_dev_, *serial_settings_, frame_id_,  nh_);

    ROS_INFO("GPS opened");
    return true;
  }

private:
  std::unique_ptr<SerialGps> gps;
  std::string serial_dev_;
  unsigned int  baudrate_;
  std::string frame_id_;
  std::unique_ptr<SerialLink::Settings> serial_settings_;
  ros::NodeHandle nh_;

};

int main(int argc, char ** argv)
{
  ros::init(argc, argv, "gps_driver_node");
  ros::NodeHandle nh(""), nh_param("~");

  std::string serial_dev;
  nh_param.param<std::string>("serial_dev", serial_dev, "/dev/ttyUSB0");
  ROS_INFO("GET param serial device %s", serial_dev.c_str());

  int bd;
  nh_param.param<int>("baudrate", bd, 115200);
  ROS_INFO("GET baudrate bd value : %d", bd);
  
  std::string frame_id;
  nh_param.param<std::string>("frame_id", frame_id, "gps");
  ROS_INFO("GET param frame_id %s", frame_id.c_str());
    
  gpsNode gps_node(serial_dev, (unsigned int) bd, frame_id, nh);
  if (!gps_node.on_start())
    return -1;

  ros::spin();

}
