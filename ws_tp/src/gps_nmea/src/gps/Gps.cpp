/**

\file
\author Stéphane Witzmann (2014)
\copyright 2014 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <ros/ros.h>
#include <iostream>
#include <functional>
#include <string>
#include <stdexcept>
#include <sstream>
#include "vbus_sockets/Selector.hpp"
#include "gps/Gps.hpp"
#include <gps_nmea/GpsStringFix.h>


using namespace std;
using namespace gps_nmea;

namespace
{

  template<typename... Args>
  std::function<void(void)> thread_bind(const std::string &name, Args &&... args)
  {
    const auto f = std::bind(args...);

    return [=]()
    {
      try
      {
        f();
      }
      catch (const std::exception &e)
      {
        std::clog<< " internal error: "<< e.what() << std::endl;
      }
      catch (...)
      {
        std::clog << name << " internal error." << std::endl;
      }
    };
  }

  std::string make_thread_name(const std::string e)
  {
    return (std::string("Gps [") + e + "]");
  }

  string byteArrayToString(const uint8_t *arr, int size)
  {
    std::ostringstream convert;

    for (int a = 0; a < size; a++)
    {
      convert << (char)arr[a];
    }

    return convert.str();
  }

  void process_one(const uint8_t *s, size_t l, ros::Time ts, const std::string &frame_id , ros::Publisher& msg)
  {
    GpsStringFix sentence;
    
    // TODO remove after manip
    static int cpt = 0;
    
    std::string str(byteArrayToString(s,l));
    sentence.header.stamp = ts;
    sentence.header.frame_id = frame_id;
    sentence.sentence = str;
    
    // TODO remove after manip
    ROS_INFO("Trame : %d \r",cpt++);

    msg.publish(sentence);
  }

  void process_input_buffer(uint8_t *buffer, size_t &buf_length,
    ros::Time old_ts, ros::Time new_ts, const std::string &frame_id, ros::Publisher& msg)
  {
    size_t start = 0;

    for (bool first = true; true; first = false)
    {
      // Find 1st '$' character
      while (start < buf_length && buffer[start] != '$')
        ++start;
      if (start == buf_length)
        break;

      // Find last character (0x0d or 0x0a)
      size_t end = start + 1;
      while (end < buf_length && buffer[end] != 0x0a && buffer[end] != 0x0d)
        ++end;
      if (end == buf_length)
        break;

      // Process and prepare next loop
      process_one(buffer + start, end - start, first ? old_ts : new_ts, frame_id, msg);
      start = end + 1;
    }

    // We finished parsing the full buffer, remove the bits
    // we won't need next time
    memmove(buffer, buffer + start, buf_length - start);
    buf_length -= start;
  }
}

Gps::Gps(const gps_nmea::Descriptor &fd, const std::string &dev, const std::string &frame_id, ros::NodeHandle& nh):
  fd_(fd),
  syncpair_(),
  nh_(nh),
  frame_id_ (frame_id),
  thread_(thread_bind(make_thread_name(dev), &Gps::thread_main, this))
{
  gps_sentence_pub_ = nh_.advertise<GpsStringFix>("gps_sentence", 1);
}

Gps::~Gps()
{
  syncpair_.signal();
  thread_.join();
}

void Gps::thread_main()
{
  constexpr size_t buf_size = 200;
  uint8_t buffer[buf_size];
  size_t cur_length = 0;
  ros::Time old_ts(0);

  while (true)
  {
    gps_nmea::Selector s;
    s.add_reader(syncpair_);
    s.add_reader(fd_);
    s();

    if (s.is_readable(syncpair_))
      return;

    if (s.is_readable(fd_))
    {
      // NOTE: a nonempty buffer here means it contains a partial
      // frame, so we need to keep the associated timestamp.
      // All the next frames will use a current timestamp.
      const bool was_empty = (cur_length == 0);

      const size_t r = fd_.read(buffer + cur_length, buf_size - cur_length);
      if (r == 0)
        throw std::runtime_error("EOF");
      cur_length += r;


      ros::Time new_ts = ros::Time::now();
      if (was_empty)
        old_ts = new_ts;

      process_input_buffer(buffer, cur_length, old_ts, new_ts, frame_id_, gps_sentence_pub_);

      // Handle worst-case scenario: the frame is larger than our buffer
      if (cur_length == buf_size)
        throw std::runtime_error("GPS frame is larger than our buffer");
    }
  }

}

