/**

\file
\author Stéphane Witzmann (2017) Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdexcept>
#include <functional>
#include <map>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <locale>
#include <chrono>

#include <ros/ros.h>
#include "parser/GpsFrame.hpp"
#include <gps_nmea/Date_gps.h>

namespace gps_nmea
{

  void check_count_eq(const NmeaFrame &frame, unsigned count, const std::string &name)
  {
    if (frame.size() != count)
      throw std::runtime_error(std::string("Bad argument count for frame ") + name);
  }

  void check_count_min(const NmeaFrame &frame, unsigned count, const std::string &name)
  {
    if (frame.size() < count)
      throw std::runtime_error(std::string("Not enough arguments in frame ") + name);
  }

  template<typename T>
  T read(const std::string &s)
  {
    return T(std::stoul(s));
  }

  unsigned char read_digit(char c)
  {
    if (c < '0' || c > '9')
      throw std::runtime_error(std::string("Invalid digit: ") + c);
    return (c - '0');
  }

  unsigned char read_digits(const char *s)
  {
    return (read_digit(s[0]) * 10 + read_digit(s[1]));
  }

  template<>
  Date_gps read<Date_gps>(const std::string &s)
  {
    if (s.length() != 6)
      throw std::runtime_error(std::string("Bad date format: ") + s);

    Date_gps date;
    date.day = read_digits(s.c_str());
    date.month = read_digits(s.c_str() + 2);
    date.year = read_digits(s.c_str() + 4);

    return date;

  }

  template<>
  double read<double>(const std::string &s)
  {
    return std::stod(s);
  }

  template<>
  int read<int>(const std::string &s)
  {
    return std::stoi(s);
  }
  
  template<>
  std::chrono::milliseconds read<std::chrono::milliseconds>(const std::string &s)
  {
    std::chrono::milliseconds chrono(0);

    const size_t l = s.length();
    if (l != 9 && l != 6)
      throw std::runtime_error("Wrong length in time field");

    chrono += std::chrono::hours(read_digits(s.c_str()));
    chrono += std::chrono::minutes(read_digits(s.c_str() + 2));
    chrono += std::chrono::seconds(read_digits(s.c_str() + 4));

    if (s.length() == 9)
    {
      if (s[6] != '.')
        throw std::runtime_error(". not found in time field");
      chrono += std::chrono::milliseconds(int(read_digits(s.c_str() + 7)) * 10);
    }

    return chrono;
  }

  template<>
  ros::Time read<ros::Time>(const std::string &s)
  {
    std::chrono::milliseconds chrono(0);
    ros::Time rosChrono(0);

    const size_t l = s.length();
    if (l != 9 && l != 6)
      throw std::runtime_error("Wrong length in time field");

    chrono += std::chrono::hours(read_digits(s.c_str()));
    chrono += std::chrono::minutes(read_digits(s.c_str() + 2));
    chrono += std::chrono::seconds(read_digits(s.c_str() + 4));

    rosChrono.sec = (int32_t)chrono.count();

    if (s.length() == 9)
    {
      if (s[6] != '.')
        throw std::runtime_error(". not found in time field");
      chrono = std::chrono::milliseconds(int(read_digits(s.c_str() + 7)) * 10);
    }

    rosChrono.nsec = (int32_t)chrono.count() *1000000;

    return rosChrono;
  }

// NOTE: we assume that d and b have already been zeroed
  template<typename T>
  void read(T &x,  uint8_t/*bool*/ &b, const std::string &s)
  {
    try
    {
      x = read<T>(s);
      b = true;
    }
    catch (...)
    {
    }
  }

// idem
  template<typename T>
  void read(T &x, uint8_t/*bool*/ &b, const std::string &s, const std::string &unit, const std::string &expected_unit)
  {
    if (unit == expected_unit)
      read(x,b,s);
  }

  void read_position(WGS84&x,  uint8_t/*bool*/ &b, const std::string &latitude, const std::string &latitude_dir,
                     const std::string &longitude, const std::string &longitude_dir)
  {
    try
    {
      if (latitude.length() < 5)
        throw std::runtime_error(std::string("Bad latitude, too short: ") + latitude);

      if (latitude[4] != '.')
        throw std::runtime_error(std::string("Bad latitude format: ") + latitude);

      if (longitude.length() < 6)
        throw std::runtime_error(std::string("Bad longitude, too short: ") + longitude);

      if (longitude[5] != '.')
        throw std::runtime_error(std::string("Bad longitude format: ") + longitude);

      long double lat = std::stold(latitude.substr(0,2)) + std::stold(latitude.substr(2)) / 60.L;
      long double lon = std::stold(longitude.substr(0,3)) + std::stold(longitude.substr(3)) / 60.L;

      if (latitude_dir == "S")
        lat = -lat;
      else if (latitude_dir != "N")
        throw std::runtime_error(std::string("Bad latitude direction: ") + latitude_dir);

      if (longitude_dir == "W")
        lon = -lon;
      else if (longitude_dir != "E")
        throw std::runtime_error(std::string("Bad longitude direction: ") + longitude_dir);

      x.latitude = lat;
      x.longitude = lon;
      b = true;
    }
    catch (...)
    {
    }
  }

  GpsGGA parse_gga(const NmeaFrame &input, const std_msgs::Header& h, const bool &spec)
  {
    check_count_eq(input, 15, "GGA");

    GpsGGA frame;

    frame.header = h;

    read(frame.time, frame.time_enabled, input[1]);
    read_position(frame.position, frame.position_enabled, input[2], input[3], input[4], input[5]);
    read(frame.position_type, frame.position_type_enabled, input[6]);
    read(frame.satellites, frame.satellites_enabled, input[7]);
    read(frame.hdop, frame.hdop_enabled, input[8]);
    read(frame.altitude, frame.altitude_enabled, input[9], input[10], "M");
    read(frame.geoidal_separation, frame.geoidal_separation_enabled, input[11], input[12], "M");
    read(frame.diff_age, frame.diff_age_enabled, input[13]);
    read(frame.base_station_id, frame.base_id_enabled, input[14]);

    return frame;
  }

  GpsRMC parse_rmc(const NmeaFrame &input, const std_msgs::Header& h, const bool &spec)
  {

    check_count_eq(input, 13, "RMC");

    GpsRMC frame;

    frame.header = h;

    read(frame.time, frame.time_enabled, input[1]);
    if (input[2] == "A")
    {
      frame.position_valid = GpsRMC::POSITION_VALID_ACTIVE;
      frame.position_valid_enabled = true;
    }
    else if (input[2] == "V")
    {
      frame.position_valid = GpsRMC::POSITION_VALID_VOID;
      frame.position_valid_enabled = true;
    }
    read_position(frame.position, frame.position_enabled, input[3], input[4], input[5], input[6]);
    read(frame.speedOverGround, frame.speedOverGround_enabled, input[7]);
    read(frame.trackAngle, frame.trackAngle_enabled, input[8]);
    read(frame.date, frame.date_enabled, input[9]);

    uint8_t magnetic = false;
    double magnetic_value = 0.;
    read(magnetic_value, magnetic, input[10]);
    if (magnetic && input[11] == "E")
    {
      frame.magneticVariation = magnetic_value;
      frame.magnetic_var_direction = GpsRMC::MAGNETIC_VARIATION_EAST;
      frame.magneticVariation_enabled = true;
    }
    else if (magnetic && input[11] == "W")
    {
      frame.magneticVariation = magnetic_value;
      frame.magnetic_var_direction = GpsRMC::MAGNETIC_VARIATION_WEST;
      frame.magneticVariation_enabled = true;
    }
    if (input[12] == "A")
    {
      frame.mode = GpsRMC::GPS_ENABLED_MODE_AUTONOMOUS;
      frame.mode_enabled = true;
    }
    else if (input[12] == "D")
    {
      frame.mode = GpsRMC::GPS_ENABLED_MODE_DIFFERENTIAL;
      frame.mode_enabled = true;
    }
    else if (input[12] == "E")
    {
      frame.mode = GpsRMC::GPS_ENABLED_MODE_DIFFERENTIAL;
      frame.mode_enabled = true;
    }
    else if (input[12] == "N")
    {
      frame.mode = GpsRMC::GPS_ENABLED_MODE_INVALID;
      frame.mode_enabled = true;
    }

    return frame;
  }

  GpsVTG parse_vtg(const NmeaFrame &input, const std_msgs::Header& h, const bool &spec)
  {
    check_count_eq(input, 10, "VTG");

    GpsVTG frame;

    frame.header = h;

    if (input[9] == "A")
    {
      frame.mode = GpsVTG::GPS_MODE_AUTONOMOUS;
      frame.mode_enabled = true;
    }
    else if (input[9] == "D")
    {
      frame.mode = GpsVTG::GPS_MODE_DIFFERENTIAL;
      frame.mode_enabled = true;
    }
    else if (input[9] == "N")
    {
      frame.mode = GpsVTG::GPS_MODE_INVALID;
      frame.mode_enabled = true;
    }

    read(frame.cog_true, frame.cog_true_enabled, input[1], input[2], "T");
    read(frame.cog_magnetic, frame.cog_magnetic_enabled, input[3], input[4], "M");
    read(frame.sog_knots, frame.sog_knots_enabled, input[5], input[6], "N");
    read(frame.sog_kmh, frame.sog_kmh_enabled, input[7], input[8], "K");

    return frame;
  }

  GpsGST parse_gst(const NmeaFrame &input, const std_msgs::Header& h, const bool &spec)
  {
    if(!spec)   
      check_count_eq(input, 9, "GST");
    else
      check_count_min(input, 9, "GST");
    
    GpsGST frame;

    frame.header = h;

    if (input[0] == "GPGST")
      frame.source = GpsGST::SATELLITE_SOURCE_GPS;
    else if (input[0] == "GLGST")
      frame.source = GpsGST::SATELLITE_SOURCE_GLONASS;
    else if (input[0] == "GNGST")
      frame.source = GpsGST::SATELLITE_SOURCE_SEVERAL;
    else
      throw std::logic_error(std::string("GST parser called on the wrong frame: ") + input[0]);

    read(frame.time, frame.time_enabled, input[1]);
    read(frame.stdev_range, frame.stdev_range_enabled, input[2]);
    read(frame.stdev_semimajor, frame.stdev_semimajor_enabled, input[3]);
    read(frame.stdev_semiminor, frame.stdev_semiminor_enabled, input[4]);
    read(frame.semimajor_orientation, frame.semimajor_orientation_enabled, input[5]);
    read(frame.stdev_latitude, frame.stdev_latitude_enabled, input[6]);
    read(frame.stdev_longitude, frame.stdev_longitude_enabled, input[7]);
    read(frame.stdev_altitude, frame.stdev_altitude_enabled, input[8]);

    return frame;
  }

  GpsCRT parse_crt(const NmeaFrame &input, const std_msgs::Header& h, const bool &spec)
  {
    check_count_eq(input, 18, "CRT");

    GpsCRT frame;

    frame.header = h;

    read(frame.position_mode, frame.position_mode_enabled, input[2]);
    read(frame.count, frame.count_enabled, input[3]);
    read(frame.time, frame.time_enabled, input[4]);
    read(frame.ecef_x, frame.ecef_x_enabled, input[5]);
    read(frame.ecef_y, frame.ecef_y_enabled, input[6]);
    read(frame.ecef_z, frame.ecef_z_enabled, input[7]);
    read(frame.clock_offset, frame.clock_offset_enabled, input[8]);
    read(frame.v_x, frame.v_x_enabled, input[9]);
    read(frame.v_y, frame.v_y_enabled, input[10]);
    read(frame.v_z, frame.v_z_enabled, input[11]);
    read(frame.clock_drift, frame.clock_drift_enabled, input[12]);
    read(frame.pdop, frame.pdop_enabled, input[13]);
    read(frame.hdop, frame.hdop_enabled, input[14]);
    read(frame.vdop, frame.vdop_enabled, input[15]);
    read(frame.tdop, frame.tdop_enabled, input[16]);

    if (input[17].size() == 4)
    {
      uint8_t tmp[4];

      memcpy(tmp,input[17].data(), 4);
      frame.firmware_enabled = true;

      for(int a=0; a<4 ; a++)
        frame.firmware[a]=tmp[a];
    }

    return frame;
  }

  GpsLTN parse_ltn(const NmeaFrame &input, const std_msgs::Header& h, const bool &spec)
  {
    check_count_eq(input, 3, "LTN");

    GpsLTN frame;

    frame.header = h;

    uint32_t x = 0;
    read(x, frame.latency_enabled, input[2]);
    if (frame.latency_enabled)
    {
      ros::Duration t(0);
      t.nsec = x * 1000000;
      frame.latency = t;
    }
    return frame;
  }

  GpsGSV parse_gsv(const NmeaFrame &input, const std_msgs::Header& h, const bool &spec)
  {
    GpsGSV frame;
    uint8_t foo;
    
    frame.header = h;

    read(frame.total_nb_messages, frame.total_nb_messages_enabled, input[1]);
    read(frame.message_number, frame.message_number_enabled, input[2]);
    read(frame.total_nb_satellites_in_view, frame.total_nb_satellites_in_view_enabled,  input[3]);
    
    switch(input.size())
    {
      case 20 : 
        for(size_t i=0 ; i<4 ; i++)
        {
          read(frame.satellite_PRN_nb[i], foo, input[(i*4)+4]);
          read(frame.elevation[i], foo, input[(i*4)+5]);
          read(frame.azimuth[i], foo, input[(i*4)+6]);
          read(frame.snr[i], foo, input[(i*4)+7]);
        }
        break;
        
      case 16 : 
        for(size_t i=0 ; i<3 ; i++)
        {
          read(frame.satellite_PRN_nb[i], foo, input[(i*4)+4]);
          read(frame.elevation[i], foo, input[(i*4)+5]);
          read(frame.azimuth[i], foo, input[(i*4)+6]);
          read(frame.snr[i], foo, input[(i*4)+7]);
        }
        break;
      case 12 : 
        for(size_t i=0 ; i<2 ; i++)
        {
          read(frame.satellite_PRN_nb[i], foo, input[(i*4)+4]);
          read(frame.elevation[i], foo, input[(i*4)+5]);
          read(frame.azimuth[i], foo, input[(i*4)+6]);
          read(frame.snr[i], foo, input[(i*4)+7]);
        }
        break;
      case 8 : 
        read(frame.satellite_PRN_nb[0], foo, input[4]);
        read(frame.elevation[0], foo, input[5]);
        read(frame.azimuth[0], foo, input[6]);
        read(frame.snr[0], foo, input[7]);
        break;
      default :
        throw std::runtime_error(std::string("Bad argument count for frame GSV"));
    }

    return frame;
  }

  
} // namespace
