/**

\file
\author Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ros/ros.h>
#include <string>
#include "parser/GpsFrame.hpp"
#include <gps_nmea/GpsStringFix.h>

using namespace gps_nmea;

namespace
{
  std::string parser_type("");
  bool soft_error_catching(false);
  uint64_t nb_frames(0);
  uint64_t err_count(0);
  bool spec_zmax(false);
}

struct Parser
{
  virtual void parse(const NmeaFrame &, const std_msgs::Header&, const bool &) = 0;
  virtual ~Parser() {}
};



template <typename T>
struct SpecializedParser: public Parser
{
  SpecializedParser(ros::NodeHandle &nh, const std::string &topic, std::function<T(const NmeaFrame &, const std_msgs::Header&, const bool &)> actual_parser):
    actual_parser_(actual_parser),
    publisher_(nh.advertise<T>(topic, 1))
  {
  }

  virtual void parse(const NmeaFrame &input, const std_msgs::Header& h, const bool & s) override
  {
    T f = actual_parser_(input,h,s);
    publisher_.publish(f);
  }

protected:
  std::function<T(const NmeaFrame &, const std_msgs::Header &, const bool &)> actual_parser_;
  ros::Publisher publisher_;
};

struct ParserGGA: public SpecializedParser<GpsGGA>
{
  explicit ParserGGA(ros::NodeHandle &nh): SpecializedParser(nh, "gps_gga", parse_gga) {}
};

struct ParserRMC: public SpecializedParser<GpsRMC>
{
  explicit ParserRMC(ros::NodeHandle &nh): SpecializedParser(nh, "gps_rmc", parse_rmc) {}
};

struct ParserVTG: public SpecializedParser<GpsVTG>
{
  explicit ParserVTG(ros::NodeHandle &nh): SpecializedParser(nh, "gps_vtg", parse_vtg) {}
};

struct ParserGST: public SpecializedParser<GpsGST>
{
  explicit ParserGST(ros::NodeHandle &nh): SpecializedParser(nh, "gps_gst", parse_gst) {}
};

struct ParserLTN: public SpecializedParser<GpsLTN>
{
  explicit ParserLTN(ros::NodeHandle &nh): SpecializedParser(nh, "gps_ltn", parse_ltn) {}
};

struct ParserCRT: public SpecializedParser<GpsCRT>
{
  explicit ParserCRT(ros::NodeHandle &nh): SpecializedParser(nh, "gps_crt", parse_crt) {}
};

struct ParserGSV: public SpecializedParser<GpsGSV>
{
  explicit ParserGSV(ros::NodeHandle &nh): SpecializedParser(nh, "gps_gsv", parse_gsv) {}
};

struct Parsers: private std::map<std::string, std::unique_ptr<Parser>>
{
  explicit Parsers(ros::NodeHandle &nh)
  {
    emplace("GPGGA", std::make_unique<ParserGGA>(nh));
    emplace("GNGGA", std::make_unique<ParserGGA>(nh));
    emplace("GPRMC", std::make_unique<ParserRMC>(nh));
    emplace("GPVTG", std::make_unique<ParserVTG>(nh));
    emplace("GPGST", std::make_unique<ParserGST>(nh));
    emplace("GNGST", std::make_unique<ParserGST>(nh));
    emplace("GLGST", std::make_unique<ParserGST>(nh));
    emplace("LTN", std::make_unique<ParserLTN>(nh));
    emplace("CRT", std::make_unique<ParserCRT>(nh));
    emplace("GPGSV", std::make_unique<ParserGSV>(nh));
  }

  void parser(const GpsStringFix &fix, NmeaFrame::Parser p)
  {
    try
    {
      NmeaFrame nmea(fix.sentence,  p);

      if (nmea.empty())
        return;

      const std::string &id = (nmea[0] == "PASHR" && nmea.size() > 1) ? nmea[1] : nmea[0];
      const auto i = find(id);
      if (i != end())
      {
        // special workaroud for zmax magellan use
        if(nmea[0] == "GPGST" && spec_zmax)
          (*i).second->parse(nmea, fix.header,true);
        else
          (*i).second->parse(nmea, fix.header,false);
      }
    }
    catch(std::exception &e)
    {
      ROS_ERROR_STREAM(e.what());
    }
  }
  
  void parse(const GpsStringFix &fix)
  {
    std::string str_enabled ("enabled");
    std::string str_disabled ("disabled");
    std::string str_ignore ("ignore");
    
    NmeaFrame::Parser p;
    
    if (parser_type.compare(str_ignore) == 0)
	p = NmeaFrame::Parser::Ignore;
    else if (parser_type.compare(str_disabled) == 0)
	p = NmeaFrame::Parser::Disabled;
    else
	p = NmeaFrame::Parser::Enabled;
    
    nb_frames++;
    
    if(soft_error_catching)
    {  
      try
      {
	parser(fix, p);
      }
      catch(std::exception& e)
      {
	err_count++;
	ROS_WARN_STREAM("Parser has caught error : " << e.what() << " : [" << err_count << "]/[" << nb_frames << "]" );
      }
    }
    else
    {
      parser(fix, p);
    }
  }
};


std::function<void(const GpsStringFix&)> actual_callback;


void callback(const GpsStringFix& fix)
{
  actual_callback(fix);

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "gps_parser_node");
  ros::NodeHandle node;
  ros::NodeHandle node_private("~");

  node_private.param<std::string>("parser_type",parser_type, "enabled");
  ROS_INFO("GET parser_type :  %s ", parser_type.c_str());
  
  node_private.param<bool>("soft_error_catching",soft_error_catching, false);
  if(soft_error_catching)
    ROS_INFO("GET soft_error_catching :  true ");
  else
    ROS_INFO("GET soft_error_catching :  false ");
  
  node_private.param<bool>("special_zmax_parsing",spec_zmax, false);
  if(spec_zmax)
    ROS_INFO("GET special_zmax_parsing :  true ");
  else
    ROS_INFO("GET special_zmax_parsing :  false ");
  
  Parsers parsers(node);

  actual_callback = [&](const GpsStringFix &fix)
  {
    parsers.parse(fix);
  };

  ros::Subscriber sentence_fix_sub = node.subscribe("gps_sentence", 10, callback);

  ros::spin();
}
