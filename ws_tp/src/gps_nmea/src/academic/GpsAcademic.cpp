#include <ros/ros.h>
#include <gps_nmea/GpsGGA.h>
#include <gps_nmea/GpsGST.h>
#include <gps_nmea/WGS84.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/NavSatStatus.h>


using namespace gps_nmea;


class Gps
{
private:
  ros::NodeHandle _node;
  std::string _topic;
  ros::Publisher _gps_navSatFix_pub;
  std::string _frame_id_academic;

public:
  Gps();
  ros::NodeHandle getNode()
  {
    return _node;
  }
  void gpsCallback(const gps_nmea::GpsGGA& msg);
};

Gps::Gps():_node{"~"},
  _topic{_node.param("publisher/academic_gps", std::string("/fix"))},
  _frame_id_academic{_node.param("publisher/frame_id", std::string("dgps"))}
  
{
  _gps_navSatFix_pub= _node.advertise<sensor_msgs::NavSatFix>(_topic, 10);
}

void Gps::gpsCallback(const gps_nmea::GpsGGA& msg)
{
  sensor_msgs::NavSatFix n;

  n.header = msg.header;
  n.header.frame_id = _frame_id_academic;
  n.latitude = msg.position.latitude;
  n.longitude = msg.position.longitude;
  n.altitude =  msg.altitude;
// NOTE : as long as it hasn't been ROS-informed, @IP we use RTK and STATUS_GBAS_FIX is the sharpest positionning mode informed. So we fill with STATUS_GBAS_FIX...
  n.status.status = sensor_msgs::NavSatStatus::STATUS_GBAS_FIX;
// NOTE : as long as it hasn't been ROS-informed, @IP we use SERVICE_GLONASS for GPS & GLONASS.
  n.status.service = sensor_msgs::NavSatStatus::SERVICE_GLONASS;
// NOTE : we don't fill covariance. for that we must use also /gps_gst topic.

  _gps_navSatFix_pub.publish(n);

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "gps_academic");
  Gps gps;
  std::string topic = gps.getNode().param("subscriber/consumer_gps", std::string("/gps_gga"));
  ros::Subscriber sub = gps.getNode().subscribe(topic, 1, &Gps::gpsCallback, &gps);

  ros::spin();

}
