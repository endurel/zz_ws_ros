#include <ros/ros.h>
#include <gps_nmea/GpsGGA.h>
#include <gps_nmea/WGS84.h>
#include <geometry_msgs/Point.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/NavSatStatus.h>


using namespace gps_nmea;


class Gps
{
private:
  ros::NodeHandle _node;
  std::string _topic;
  ros::Publisher _gps_gga_pub;
  std::string _frame_id_academic;
  double _offset_x;
  double _offset_y;

public:
  Gps();
  ros::NodeHandle getNode()
  {
    return _node;
  }
  void gpsCallback(const sensor_msgs::NavSatFix& msg);
};

Gps::Gps():_node{"~"},
  _topic{_node.param("publisher/gga_gps", std::string("/gps_gga"))},
  _frame_id_academic{_node.param("publisher/frame_id", std::string("dgps"))}
  
{
  bool apply_offset(false);
  
  _node.param<bool>("apply_offset", apply_offset, bool(true));
  
  if(apply_offset)
  {
    ROS_INFO("GET apply_offset flag : TRUE : allowed");
    _node.param<double>("offset_east", _offset_x,-0.001);
    _node.param<double>("offset_north", _offset_y,-0.001);
    ROS_INFO("---> GET offset_x  value : %fm and offset_y  value : %fm", _offset_x, _offset_y);
  }
  else
  { ;
    _offset_x = 0.0;
    _offset_y = 0.0;
    ROS_INFO("GET apply_offset flag : FALSE : not allowed");
  }
  
  _gps_gga_pub= _node.advertise<gps_nmea::GpsGGA>(_topic, 10);
}

void Gps::gpsCallback(const sensor_msgs::NavSatFix& msg)
{
  gps_nmea::GpsGGA gga;

  gga.header = msg.header;
  gga.header.frame_id = _frame_id_academic;
  gga.position.latitude = msg.latitude - _offset_y;
  gga.position.longitude = msg.longitude - _offset_x;
  gga.geoidal_separation = 51.150;
  gga.altitude =  msg.altitude + 358.00;

  gga.position_enabled = true;
  gga.altitude_enabled = true;
  gga.geoidal_separation_enabled = true;
  _gps_gga_pub.publish(gga);

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "gps_antiacademic");
  Gps gps;
  std::string topic = gps.getNode().param("subscriber/consumer_gps", std::string("/fix"));
  ros::Subscriber sub = gps.getNode().subscribe(topic, 1, &Gps::gpsCallback, &gps);

  ros::spin();

}
