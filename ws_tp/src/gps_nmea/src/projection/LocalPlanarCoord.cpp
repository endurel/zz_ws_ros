/**

\file
\author Stéphane Witzmann (2014)
\copyright 2014 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <cmath>
#include <stdexcept>

#include "projection/LocalPlanarCoord.hpp"

using  namespace gps_nmea;

namespace
{
  EcefCoord rotate(const EcefCoord &ec, const long double *r)
  {
    return EcefCoord(
             ec.x*r[0] + ec.y*r[1] + ec.z*r[2],
             ec.x*r[3] + ec.y*r[4] + ec.z*r[5],
             ec.x*r[6] + ec.y*r[7] + ec.z*r[8]
           );
  }

  EcefCoord operator-(const EcefCoord &ec1, const EcefCoord &ec2)
  {
    return EcefCoord(ec1.x - ec2.x, ec1.y - ec2.y, ec1.z - ec2.z);
  }

  PlanarCoord toPlanar(const EcefCoord &ec)
  {
    return PlanarCoord(ec.x, ec.y, ec.z);
  }
}

// See http://geoengine.nga.mil/geospatial/SW_TOOLS/NIMAMUSE/webinter/geotrans2/help/elliptab.htm
const Ellipsoid Ellipsoid::wgs84(6378137.L, 6356752.3142L);


// Doc: http://en.wikipedia.org/wiki/Geodetic_datum#Geodetic_to.2Ffrom_ECEF_coordinates
EcefCoord::EcefCoord(const GeoCoord &gc)
{
  const long double n = Ellipsoid::wgs84.a/sqrtl(1 - powl(Ellipsoid::wgs84.e*sinl(gc.phi), 2));
  x = (n + gc.h)*cosl(gc.phi)*cosl(gc.lambda);
  y = (n + gc.h)*cosl(gc.phi)*sinl(gc.lambda);
  z = (n*(1 - powl(Ellipsoid::wgs84.e, 2)) + gc.h)*sinl(gc.phi);
}

GeoToPlanarConverter::GeoToPlanarConverter(const GeoCoord &gc, double gs):
  r_(),
  t_(gc),
  default_geoidal_separation_(gs)
{
  const long double cosphi = cosl(gc.phi);
  const long double sinphi = sinl(gc.phi);
  const long double coslam = cosl(gc.lambda);
  const long double sinlam = sinl(gc.lambda);
  r_[0] = -sinphi*coslam;
  r_[1] = -sinphi*sinlam;
  r_[2] = cosphi;
  r_[3] = -sinlam;
  r_[4] = coslam;
  r_[5] = 0.L;
  r_[6] = cosphi*coslam;
  r_[7] = cosphi*sinlam;
  r_[8] = sinphi;
}

GeoToPlanarConverter::GeoToPlanarConverter(const Wgs84 &w, long double h, double gs):
  GeoToPlanarConverter(GeoCoord(w, h), gs)
{
}

PlanarCoord GeoToPlanarConverter::position(const GeoCoord &gc) const
{
  return position(EcefCoord(gc));
}

PlanarCoord GeoToPlanarConverter::position(const EcefCoord &ec) const
{
  return toPlanar((rotate(ec - t_, r_)));
}

PlanarCoord GeoToPlanarConverter::velocity(const EcefCoord &ec) const
{
  return toPlanar(rotate(ec, r_));
}

PlanarCoord GeoToPlanarConverter::position(const GpsGGA &f) const
{
  if (!can_read_position(f))
    throw std::runtime_error("Missing position field(s) in GGA frame");

  Wgs84 pos(f.position.latitude, f.position.longitude);
  return position(GeoCoord(pos, f.geoidal_separation + f.altitude));
}

PlanarCoord GeoToPlanarConverter::position(const GpsCRT &f) const
{
  if (!can_read_position(f))
    throw std::runtime_error("Missing position field(s) in CRT frame");
  return position(EcefCoord(f.ecef_x, f.ecef_y, f.ecef_z));
}

PlanarCoord GeoToPlanarConverter::position(const GpsRMC &f) const
{
  if (!can_read_position(f))
    throw std::runtime_error("Missing position field(s) in RMC frame");

  Wgs84 pos(f.position.latitude, f.position.longitude);
  return position(GeoCoord(pos, 0.));
}

PlanarCoord GeoToPlanarConverter::position(const double &lat, const double &lon) const
{
  Wgs84 pos(lat, lon);
  return position(GeoCoord(pos, 0.));
}

PlanarCoord GeoToPlanarConverter::velocity(const GpsCRT &f) const
{
  if (!can_read_velocity(f))
    throw std::runtime_error("Missing velocity field(s) in CRT frame");
  return velocity(EcefCoord(f.v_x, f.v_y, f.v_z));
}

PlanarCoord GeoToPlanarConverter::stdev(const GpsGST &f)
{
  return PlanarCoord(f.stdev_latitude, f.stdev_longitude, f.stdev_altitude);
}

bool GeoToPlanarConverter::can_read_position(const GpsGGA &f)
{
  return (f.position_enabled && f.altitude_enabled && f.geoidal_separation_enabled);
}

bool GeoToPlanarConverter::can_read_position(const GpsCRT &f)
{
  return (f.ecef_x_enabled && f.ecef_y_enabled && f.ecef_z_enabled);
}

bool GeoToPlanarConverter::can_read_position(const GpsRMC &f)
{
  return (f.position_enabled);
}

bool GeoToPlanarConverter::can_read_velocity(const GpsCRT &f)
{
  return (f.v_x_enabled && f.v_y_enabled && f.v_z_enabled);
}

bool GeoToPlanarConverter::can_read_stdev(const GpsGST &f)
{
  return (f.stdev_latitude_enabled && f.stdev_longitude_enabled && f.stdev_altitude_enabled);
}

