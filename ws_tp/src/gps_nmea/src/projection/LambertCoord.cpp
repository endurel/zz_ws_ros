#include <cmath>
#include <stdexcept>

#include "projection/LambertCoord.hpp"

using  namespace gps_nmea;

const long double Lambert::_e = 0.08248325676;
const long double Lambert::_long0 = 0.04079234433198;

Lambert::~Lambert(){}

LambertI::LambertI(const Wgs84 &wgs) : Lambert(wgs){
        _n = 0.7604059656;
        _C = 11603796.98;
        _Xs = 600000;
        _Ys = 5657616.674;
        convert(wgs);
        }
LambertI::~LambertI(){}

LambertII::LambertII(const Wgs84 &wgs) : Lambert(wgs){
        _n = 0.7289686274;
        _C = 11745793.39;
        _Xs = 600000;
        _Ys = 6199695.768;
        convert(wgs);
        }
LambertII::~LambertII(){}


LambertIII::LambertIII(const Wgs84 &wgs) : Lambert(wgs){
        _n = 0.6959127966;
        _C = 11947992.52;
        _Xs = 600000;
        _Ys = 6791905.085;
        convert(wgs);
        }
        
LambertIII::~LambertIII(){}

  
LambertIIe::LambertIIe(const Wgs84 &wgs) : Lambert(wgs){
        _n = 0.7289686274;
        _C = 11745793.39;
        _Xs = 600000;
        _Ys = 8199695.768;
        convert(wgs);
        }

LambertIIe::~LambertIIe(){}

