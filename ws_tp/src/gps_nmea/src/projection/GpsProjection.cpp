/**

\file
\author Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ros/ros.h>
#include <string>
#include <memory>
#include <gps_nmea/GpsGGA.h>
#include <gps_nmea/GpsPlanar.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PointStamped.h>
#include "projection/LocalPlanarCoord.hpp"
#include "projection/UTMCoord.hpp"
#include "projection/LambertCoord.hpp"

using namespace gps_nmea;

namespace
{
  ros::Publisher gps_planar_pub;
  ros::Publisher gps_odometry_pub;
  ros::Publisher gps_point_st_pub;

  std::unique_ptr<GeoToPlanarConverter> conv;
  gps_ref ref_lat, ref_lon;
  double ref_alt(0.), geoid_height(0.);
  double offset_x(0.), offset_y(0.);
  bool apply_offset(false);
  double secund_offset_x(0.), secund_offset_y(0.);
  bool apply_secundary_offset(false);
  uint8_t trans_type(GpsPlanar::TRANSFORM_TYPE_LOCAL_PLANAR);
  std::string convert_type("");
  bool publish_odom(false);
  bool publish_point_stamped(false);
  
  std::string frame_id_odom, frame_id_planar, frame_id_ps;
   
  std::string to_string(const GpsGGA &f)
  {
    std::stringstream ss;
    ss << "GGA";
    if (f.position_enabled)
    {
      ss << " latitude: " << std::to_string(f.position.latitude) << "°";
      ss << " longitude: " << std::to_string(f.position.longitude) << "°";
    }
    if (conv->can_read_position(f))
    {
      const auto pos = conv->position(f);
      ss << " north: " << pos.north << "m";
      ss << " east: " << pos.east << "m";
      ss << " up: " << pos.up << "m";
    }

    return ss.str();
  }

  void initialize_params()
  {
    ros::NodeHandle node_private("~");
        
        node_private.param<int>("geolocal_datum/ref_lat_deg",ref_lat.deg, 45);
        node_private.param<int>("geolocal_datum/ref_lat_min",ref_lat.min, 45);
        node_private.param<double>("geolocal_datum/ref_lat_sec",ref_lat.sec, 39.45581);
        ROS_INFO("GET ref_lat  value : %ddeg %dmin %fs", ref_lat.deg, ref_lat.min, ref_lat.sec);
        node_private.param<int>("geolocal_datum/ref_lon_deg",ref_lon.deg, 3);
        node_private.param<int>("geolocal_datum/ref_lon_min",ref_lon.min, 6);
        node_private.param<double>("geolocal_datum/ref_lon_sec",ref_lon.sec, 39.85802);
        ROS_INFO("GET ref_lon  value : %ddeg %dmin %fs", ref_lon.deg, ref_lon.min, ref_lon.sec);
        node_private.param<double>("geolocal_datum/ref_alt",ref_alt, 473.600);  
        node_private.param<double>("geolocal_datum/geoid_height",geoid_height, 51.150);
        node_private.param<std::string>("planar_type",convert_type, "geolocal");   
        ROS_INFO("GET ref_alt  value : %fm in %s reference planar transform and local geoid_height for geoid reference : %fm", ref_alt, convert_type.c_str(),geoid_height);

        node_private.param<std::string>("frame_id_odom", frame_id_odom, "odometry");
        node_private.param<std::string>("frame_id_planar", frame_id_planar, "gps");
        ROS_INFO("GET param frame_id_odom %s ,  frame_id_planar %s", frame_id_odom.c_str(), frame_id_planar.c_str());
        
        node_private.param<bool>("apply_offset", apply_offset, bool(true));
        if(apply_offset)
        {
            ROS_INFO("GET apply_offset flag : TRUE : allowed");
            node_private.param<double>("offset_east", offset_x,0.);
            node_private.param<double>("offset_north", offset_y, 0.);
            ROS_INFO("---> GET offset_x  value : %fm and offset_y  value : %fm", offset_x, offset_y);
        }
        else
            ROS_INFO("GET apply_offset flag : FALSE : not allowed");

        node_private.param<bool>("apply_secundary_offset", apply_secundary_offset, bool(true));

        if(apply_secundary_offset)
        {
            ROS_INFO("GET apply_secundary_offset flag : TRUE : allowed");
            node_private.param<double>("secundary_offset_east", secund_offset_x, 0.);
            node_private.param<double>("secundary_offset_north", secund_offset_y, 0.);
            ROS_INFO("---> GET secund_offset_x  value : %fm and secund_offset_y  value : %fm", secund_offset_x, secund_offset_y);
        }
        else
            ROS_INFO("GET apply_secundary_offset flag : FALSE : not allowed");
        
        node_private.param<bool>("publish_odom", publish_odom, bool(true));
        node_private.param<bool>("publish_point_stamped", publish_point_stamped, bool(true));
        node_private.param<std::string>("frame_id_ps", frame_id_ps, "dgps");
    }

    void initialize_geoconversion()
    {
        std::string str_local ("geolocal");
        std::string str_utm ("utm");
        std::string str_lambertIIe ("lambertIIe");
            

        
        if (convert_type.compare(str_local) == 0)
            trans_type = GpsPlanar::TRANSFORM_TYPE_LOCAL_PLANAR;
        else if (convert_type.compare(str_utm) == 0)
            trans_type = GpsPlanar::TRANSFORM_TYPE_UTM;
        else if (convert_type.compare(str_lambertIIe) == 0)
            trans_type = GpsPlanar::TRANSFORM_TYPE_LAMBERT2E;
        else
            trans_type = GpsPlanar::TRANSFORM_TYPE_INVALID;

        const GeoCoord station_clfd(Wgs84(ref_lat.deg,
                                            ref_lat.min,
                                            ref_lat.sec,
                                            ref_lon.deg,
                                            ref_lon.min,
                                            ref_lon.sec), geoid_height);

        conv = std::make_unique<GeoToPlanarConverter>(station_clfd);
    }
}

void callback(const GpsGGA& gga)
{
  if (gga.header.stamp == ros::Time(0))
  {
    return;
  }

  // NOTE if debug needded : std::cout << to_string(gga)  << std::endl;

  if (conv->can_read_position(gga))
  {
    const auto pos = conv->position(gga);
            
    if(gps_planar_pub)
    {
      GpsPlanar planar;
      geometry_msgs::PointStamped ps;
      
      std::string zone;
      planar.header = gga.header;
      planar.header.frame_id = frame_id_planar;
      if(trans_type==GpsPlanar::TRANSFORM_TYPE_LOCAL_PLANAR)
      {
        planar.transform_type = GpsPlanar::TRANSFORM_TYPE_LOCAL_PLANAR;
        planar.east = pos.east - offset_x - secund_offset_x;
        planar.north = pos.north - offset_y - secund_offset_y;
        planar.up = pos.up + gga.geoidal_separation;
      }
      else if(trans_type==GpsPlanar::TRANSFORM_TYPE_UTM)
      {
        planar.transform_type = GpsPlanar::TRANSFORM_TYPE_UTM;
        LLtoUTM(gga.position.latitude, gga.position.longitude, planar.north, planar.east, zone);
      }
      else if(trans_type==GpsPlanar::TRANSFORM_TYPE_LAMBERT2E)
      {     
        planar.transform_type = GpsPlanar::TRANSFORM_TYPE_LAMBERT2E;
        LambertIIe l2e(Wgs84(gga.position.latitude, gga.position.longitude, gga.altitude));
        planar.east = l2e._x + STATIC_ERROR_CONV_LIIe_X;
        planar.north = l2e._y + STATIC_ERROR_CONV_LIIe_Y;
        planar.up = l2e._z;
      }
      
      ps.header = gga.header;
      ps.header.frame_id = frame_id_ps;
      ps.point.x = planar.east;
      ps.point.y = planar.north;
      ps.point.z = planar.up;
      
      gps_planar_pub.publish(planar);
      
      if(publish_point_stamped)
        gps_point_st_pub.publish(ps);
    }
    
    if(gps_odometry_pub && publish_odom)
    {
      nav_msgs::Odometry odom;
      std::string zone;
      odom.header = gga.header;
      odom.header.frame_id = frame_id_odom;
      if(trans_type==GpsPlanar::TRANSFORM_TYPE_LOCAL_PLANAR)
      {
        // (1) we debarately forget to inform the transform_type which is usually used in gps_planar frames.
        // odometry frames are not designed for. 
        // So be careful when employing gps planar transformation scales (UTM or Geolocal or others...) 
        // (2) note also x is east ant y is north
        odom.pose.pose.position.x = pos.east - offset_x - secund_offset_x;
        odom.pose.pose.position.y = pos.north - offset_y - secund_offset_y;
        odom.pose.pose.position.z = pos.up + gga.geoidal_separation;
      }
      else if(trans_type==GpsPlanar::TRANSFORM_TYPE_UTM)
      {
        LLtoUTM(gga.position.latitude, gga.position.longitude, odom.pose.pose.position.y, odom.pose.pose.position.x, zone);
      }
      else if(trans_type==GpsPlanar::TRANSFORM_TYPE_LAMBERT2E)
      {
        LambertIIe l2e(Wgs84(gga.position.latitude, gga.position.longitude, gga.altitude));
        odom.pose.pose.position.x = l2e._x- 0.024 + STATIC_ERROR_CONV_LIIe_X;
        odom.pose.pose.position.y = l2e._y - 2000000 - 0.737 + STATIC_ERROR_CONV_LIIe_Y;
        odom.pose.pose.position.z = l2e._z;
      }
      gps_odometry_pub.publish(odom);
    }
  }
}


int main (int argc, char **argv)
{
  ros::init(argc, argv, "gps_projection");
  ros::NodeHandle node;

  initialize_params();
  initialize_geoconversion();

  gps_planar_pub = node.advertise<GpsPlanar>("gps_planar", 10);
  gps_odometry_pub = node.advertise<nav_msgs::Odometry>("gps_odom", 10);
  gps_point_st_pub = node.advertise<geometry_msgs::PointStamped>("gps_ps", 10);
  ros::Subscriber gga_sub = node.subscribe("gps_gga", 10, callback);

  ros::spin();
}

