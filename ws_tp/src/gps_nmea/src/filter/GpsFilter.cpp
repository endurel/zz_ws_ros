/**

\file
\author Laurent Malaterre (2018)
\copyright 2018 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ros/ros.h>
#include <string>
#include <memory>
#include <gps_nmea/GpsGGA.h>
#include <gps_nmea/GpsPlanar.h>
#include <nav_msgs/Odometry.h>

using namespace gps_nmea;

namespace
{
  ros::Publisher gps_planar_pub;
  ros::Publisher gps_odometry_pub;
  ros::Publisher gps_gga_pub;
  
  bool apply_gga_process(false);
  bool apply_planar_process(false);
  bool apply_odom_process(false);
  
  std::string gga_sub_name,
              gga_pub_name;
  std::string planar_sub_name,
              planar_pub_name;
  std::string odom_sub_name,
              odom_pub_name;
  
  double      windowing_filter;

  double compute_filter(const double & vc, double & vf_prec)
  {
    return (1.-windowing_filter)*vf_prec + windowing_filter*vc;
  }

  void callback_gga(const GpsGGA& gga)
  {
    GpsGGA gga_filtered;
    static double prec_lon (0.),
                  prec_lat (0.),
                  prec_alt (0.);
              
    gga_filtered.header = gga.header;
    gga_filtered.position.longitude = compute_filter(gga.position.longitude,prec_lon);
    gga_filtered.position.latitude = compute_filter(gga.position.longitude,prec_lon);
    gga_filtered.altitude = compute_filter(gga.position.longitude,prec_lon);
    
    prec_lon = gga_filtered.position.longitude;
    prec_lat = gga_filtered.position.latitude;
    prec_alt = gga_filtered.altitude;
                  
    gps_gga_pub.publish(gga_filtered);            
    
  }

  void callback_planar(const GpsPlanar& planar)
  {
    GpsPlanar planar_filtered;
    static double prec_east (0.),
                  prec_north (0.),
                  prec_alt (0.); 
    
    planar_filtered.header = planar.header;
    planar_filtered.east = compute_filter(planar.east,prec_east);
    planar_filtered.north = compute_filter(planar.north,prec_north);
    planar_filtered.up = compute_filter(planar.up,prec_alt);;              
                  
    prec_east = planar_filtered.east;
    prec_north = planar_filtered.north;
    prec_alt= planar_filtered.up;
    
    gps_planar_pub.publish(planar_filtered);
    
  }

  void callback_odom(const nav_msgs::Odometry& odom)
  {
    nav_msgs::Odometry odom_filtered;
    static double prec_east (0.),
                  prec_north (0.),
                  prec_alt (0.);
                  
    odom_filtered.header = odom.header;
    odom_filtered.pose.pose.position.x = compute_filter(odom.pose.pose.position.x,prec_east);
    odom_filtered.pose.pose.position.y = compute_filter(odom.pose.pose.position.y,prec_north);
    odom_filtered.pose.pose.position.z = compute_filter(odom.pose.pose.position.z,prec_alt);
    
    prec_east = odom_filtered.pose.pose.position.x;
    prec_north = odom_filtered.pose.pose.position.y;
    prec_alt = odom_filtered.pose.pose.position.z;
    
    gps_odometry_pub.publish(odom_filtered);
    
  }
}


int main (int argc, char **argv)
{
  ros::init(argc, argv, "gps_filter");
  ros::NodeHandle node;
  ros::NodeHandle node_private("~");
  ros::Subscriber gga_sub, planar_sub, odom_sub;
  
  node_private.param<bool>("select/gga", apply_gga_process, bool(false));
  node_private.param<bool>("select/planar", apply_planar_process, bool(false));
  node_private.param<bool>("select/odom", apply_odom_process, bool(false));
  
  if(apply_gga_process)
  {
    ROS_INFO("----> select/gga ");
    node_private.param<std::string>("subscribe/gga",gga_sub_name, "gps_gga"); 
    node_private.param<std::string>("publish/gga",gga_pub_name, "gps_gga_filtered"); 
    gps_gga_pub = node.advertise<GpsGGA>(gga_pub_name, 10);  
    gga_sub = node.subscribe(gga_sub_name, 10, callback_gga);
  }
  
  if(apply_planar_process)
  {
    ROS_INFO("----> select/planar ");
    node_private.param<std::string>("subscribe/planar",planar_sub_name, "gps_planar");
    ROS_INFO_STREAM("GET  subscribe/planar " << planar_sub_name);
    node_private.param<std::string>("publish/planar",planar_pub_name, "gps_planar_filtered"); 
    gps_planar_pub = node.advertise<GpsPlanar>(planar_pub_name, 10);  
    planar_sub = node.subscribe(planar_sub_name, 10, callback_planar);
  }
  
  if(apply_odom_process)
  {
    ROS_INFO("----> select/odom ");
    node_private.param<std::string>("subscribe/odom",odom_sub_name, "gps_odom"); 
    node_private.param<std::string>("publish/odom",odom_pub_name, "gps_odom_filtered"); 
    gps_odometry_pub = node.advertise<nav_msgs::Odometry>(odom_pub_name, 10);  
    odom_sub = node.subscribe(odom_sub_name, 10, callback_odom);
  }

  node_private.param<double>("windowing_filter",windowing_filter, 0.5);
  ROS_INFO_STREAM("GET  windowing_filter " << windowing_filter);
  
  ros::spin();
}
