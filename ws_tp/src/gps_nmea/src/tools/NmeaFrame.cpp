/**

\file
\author Stéphane Witzmann (2014)
\copyright 2014 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <cstring>
#include <stdexcept>
#include <iostream>

#include "tools/NmeaFrame.hpp"

using  namespace gps_nmea;

namespace
{
  uint8_t read_hex_digit(char c)
  {
    if (c >= '0' && c <= '9')
      return (c - '0');

    if (c >= 'A' && c <= 'F')
      return (c - 'A' + 10);

    if (c >= 'a' && c <= 'f')
      return (c - 'a' + 10);

    throw std::runtime_error("Invalid hex digit");
  }

  uint8_t read_hex_byte(const char *s)
  {
    const uint8_t c1 = read_hex_digit(s[0]) << 4;
    const uint8_t c2 = read_hex_digit(s[1]);
    return (c1 | c2);
  }

  std::string write_hex_byte(uint8_t x)
  {
    static const char lut[] = "0123456789ABCDEF";

    std::string s;
    s.resize(2);

    s[0] = lut[x >> 4];
    s[1] = lut[x & 0xf];

    return s;
  }
}

NmeaFrame::NmeaFrame()
{
}

NmeaFrame::NmeaFrame(const char *s, size_t l, NmeaFrame::Parser parser)
{
  if (parser != Parser::Disabled)
  {
    // Check format
    if (l < 4) // minimal frame: $*xx
      throw std::runtime_error("Short NMEA frame");

    if (s[0] != '$')
      throw std::runtime_error("Bad 1st character (not $) in NMEA frame");

    s += 1; // skip $
    l -= 4; // and end before *xx

    if (s[l] != '*')
      throw std::runtime_error("* not found in NMEA frame");
  }

  if (parser == Parser::Enabled)
  {
    // Compute and check checksum
    const uint8_t ref_checksum = read_hex_byte(s + l + 1);
    uint8_t checksum = 0;
    for (size_t i = 0; i < l; ++i)
      checksum ^= s[i];

    if (checksum != ref_checksum)
      throw std::runtime_error("Bad NMEA frame checksum");
  }

  // Split
  size_t beg = 0;
  for (size_t i = 0; i < l; ++i)
  {
    if (s[i] == ',')
    {
      emplace_back(s + beg, i - beg);
      beg = i + 1;
    }
  }
  emplace_back(s + beg, l - beg);
  
}

NmeaFrame::NmeaFrame(const char *s, NmeaFrame::Parser parser):
  NmeaFrame(s, strlen(s), parser)
{
}

NmeaFrame::NmeaFrame(const std::string &s, NmeaFrame::Parser parser):
  NmeaFrame(s.data(), s.length(), parser)
{
}

uint8_t NmeaFrame::checksum() const
{
  uint8_t checksum = 0;

  for (const auto &s: *this)
  {
    for (const uint8_t c: s)
      checksum ^= c;
  }

  if (size() % 2 == 0)
    checksum ^= ',';

  return checksum;
}

std::ostream & operator<<(std::ostream &o, const NmeaFrame &f)
{
  bool first = true;
  for (const auto &s: f)
  {
    if (!first)
      o << ",";
    o << s;
    first = false;
  }

  o << "*" << write_hex_byte(f.checksum());

  return o;
}

