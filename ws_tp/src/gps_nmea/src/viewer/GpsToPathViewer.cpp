/**

\file
\author Cyrille Pierre (2017) Laurent Malaterre (2017)
\copyright 2018 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ros/ros.h>
#include <gps_nmea/GpsPlanar.h>
#include <geometry_msgs/Point.h>
#include <ip_drawing_tools/pathpublisher.h>

namespace 
{
const std::string str_local ("geolocal");
const std::string str_utm ("utm");
const std::string str_lambertIIe ("lambertIIe");
}

class GpsPath
{
private:
  ros::NodeHandle _node;
  ros::NodeHandle _globalnode;
  std::string _frame;
  std::string _child_frame;
  std::string _topic;
  PathPublisher _traj_pub;
  bool _altitude_valued =false;
  double _additional_offset_x =0.;
  double _additional_offset_y =0.;
  double _offset_x =0.;
  double _offset_y =0.;
  double _ref_x =0.; 
  double _ref_y =0.;
  std::string _convert_type ="";
  
public:
  GpsPath();
  ros::NodeHandle getNode()
  {
    return _node;
  }
  ros::NodeHandle getGlobalNode()
  {
    return _globalnode;
  }
  void gpsCallback(const gps_nmea::GpsPlanar& msg);
};

GpsPath::GpsPath():
  _node{"~"},
  _frame{_node.param("frame_id/world", std::string("map"))},
  _child_frame{_node.param("frame_id/robot", std::string("robot"))},
  _topic{_node.param("publisher/traj", std::string("gps_traj"))},
  _traj_pub{ros::NodeHandle{}.getNamespace()+"/"+_topic, _frame, _child_frame}
{
  _node.getParam("values/additional_offset_x", _additional_offset_x);  
  _node.getParam("values/additional_offset_y", _additional_offset_y);
  _node.getParam("values/altitude", _altitude_valued);
  
  // NOTE Il y a 2 decalages a prendre en compte. Ils sont volontairement distigués:
  // - décalage du au systeme de reprojection, typiquement Lambert ou UTM 
  // (réduction des valeurs de calculs) : ref_x et ref_y 
  // - décalage additionnel : erreur de mesure diverse : additional_offset_x et y
  
  // NOTE Ces valeurs sont paramétrées dans deux fichiers séparés : 
  // - projection.yaml
  // - transform_param.yaml

  _node.param<std::string>("planar_type",_convert_type, "geolocal");   
  ROS_INFO("GET  %s reference planar transform ", _convert_type.c_str());
  
  if (_convert_type.compare(str_local) == 0)
    ;
  else if (_convert_type.compare(str_utm) == 0)
  {
    _node.getParam("utm_datum/ref_x", _ref_x);
    _node.getParam("utm_datum/ref_y", _ref_y);
    
    ROS_INFO("GET  ref_x : %f  et ref_x : %f", _ref_x, _ref_y);
  }
  else if (_convert_type.compare(str_lambertIIe) == 0)
  {
    _node.getParam("lambert_datum/ref_x", _ref_x);
    _node.getParam("lambert_datum/ref_y", _ref_y);
    
    ROS_INFO("GET  ref_x : %f  et ref_x : %f", _ref_x, _ref_y);
    
  }
  else
    ROS_INFO("No convert type param detected : apply default geolocal offset");
}

void GpsPath::gpsCallback(const gps_nmea::GpsPlanar& msg)
{
  geometry_msgs::PointStamped gps_point;

  gps_point.header = msg.header;
  gps_point.header.frame_id = _frame;
  gps_point.point.x = msg.east - _additional_offset_x - _ref_x;
  gps_point.point.y = msg.north - _additional_offset_y - _ref_y;

  if(_altitude_valued)
    gps_point.point.z = msg.up;
  else
    gps_point.point.z = 0.;
  
  _traj_pub.push(gps_point);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "gps_path_publisher");
  
  GpsPath gps;
  std::string topic = gps.getNode().param("subscriber/gps", std::string("gps_planarrrrrrr"));
  ros::Subscriber sub = gps.getGlobalNode().subscribe(topic, 1, &GpsPath::gpsCallback, &gps);
  
  ros::spin();

}
