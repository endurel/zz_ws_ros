/**

\file
\author Stéphane Witzmann (2014)
\copyright 2014 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef IP_ROS_SENSORS_GPS_GPS_HPP
#define IP_ROS_SENSORS_GPS_GPS_HPP

#include <string>
#include <memory>
#include <thread>
#include <ros/ros.h>
#include "vbus_sockets/SyncPair.hpp"
#include "vbus_sockets/Descriptor.hpp"

namespace gps_nmea
{

class Gps 
{
public:
  
  Gps(const gps_nmea::Descriptor &, const std::string &,  const std::string & , ros::NodeHandle& nh);
  ~Gps();

private:
  
  void thread_main();

  const gps_nmea::Descriptor &fd_;
  const gps_nmea::SyncPair syncpair_;
  std::string frame_id_;
  std::thread thread_;
  ros::NodeHandle nh_;
  ros::Publisher gps_sentence_pub_;
};

}

#endif
