/**

\file
\author Laurent Malaterre (2018)
\copyright 2018 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef IP_ROS_LAMBERT_COORD_HPP
#define IP_ROS_LAMBERT_COORD_HPP

#include "projection/LocalPlanarCoord.hpp"

#define STATIC_ERROR_CONV_LIIe_X 51.1810 - 0.42
#define STATIC_ERROR_CONV_LIIe_Y 4.4500 - 0.297

namespace gps_nmea
{



class Lambert{
        static const long double _long0;
        static const long double _e;
        
protected:
        long double _n;
        long double _C;
        long double _Xs;
        long double _Ys;

public :
        long double _x;
        long double _y;
        long double _z;

        explicit Lambert(const Wgs84 &wgs) :  _n(1.0), _C(0.0), _Xs(0.0), _Ys(0.0),
                                              _x(0.0), _y(0.0), _z(0.0) {}
        virtual ~Lambert()=0;

protected :
        
    virtual void convert(const Wgs84 &wgs)
    {
      // From Degree to radian
      long double latitude = wgs.latitude*M_PI/180.0;
      long double longitude = wgs.longitude*M_PI/180.0;

      long double R, Lambda, L;

      L = log/*ln*/( tan(M_PI/4.0 + latitude/2.0) 
                                *   
                    pow( 
                            (1.0-_e*sin(latitude)) 
                                    / 
                            (1.0+_e*sin(latitude))
                                    ,        
                                _e/2.0
                        ) 
              );

      R = _C*exp(-_n*L);
      Lambda = _n*(longitude - _long0);

      _x = _Xs + R*sin(Lambda);
      _y = _Ys - R*cos(Lambda);
      _z = wgs.altitude;
    }           
};

struct LambertI : public Lambert{
  
  explicit LambertI(const Wgs84 &wgs);
  virtual ~LambertI();
};

struct LambertII : public Lambert{
  
  explicit LambertII(const Wgs84 &wgs);
  virtual ~LambertII();
};


struct LambertIII : public Lambert{
  
  explicit LambertIII(const Wgs84 &wgs);
  virtual ~LambertIII();
};

struct LambertIIe : public Lambert {
  
  explicit LambertIIe(const Wgs84 &wgs);
  virtual ~LambertIIe();
};

}
# endif
