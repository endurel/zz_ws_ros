/**

\file
\author Chuck Gantz- chuck.gantz@globalstar.com , Laurent Malaterre (2017)
\copyright 2017 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef IP_ROS_UTM_COORD_HPP
#define IP_ROS_UTM_COORD_HPP


#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>

using namespace std;

namespace gps_nmea
{

  const double RADIANS_PER_DEGREE = M_PI/180.0;
  const double DEGREES_PER_RADIAN = 180.0/M_PI;

// WGS84 Parameters
  const double WGS84_A = 6378137.0;               // major axis
  const double WGS84_B = 6356752.31424518;        // minor axis
  const double WGS84_F = 0.0033528107;            // ellipsoid flattening
  const double WGS84_E = 0.0818191908;            // first eccentricity
  const double WGS84_EP = 0.0820944379;           // second eccentricity

// UTM Parameters
  const double UTM_K0 = 0.9996;                   // scale factor
  const double UTM_FE = 500000.0;                 // false easting
  const double UTM_FN_N = 0.0;                    // false northing on north hemisphere
  const double UTM_FN_S = 10000000.0;             // false northing on south hemisphere
  const double UTM_E2 = (WGS84_E*WGS84_E);        // e^2
  const double UTM_E4 = (UTM_E2*UTM_E2);          // e^4
  const double UTM_E6 = (UTM_E4*UTM_E2);          // e^6
  const double UTM_EP2 = (UTM_E2/(1-UTM_E2));     // e'^2

  void UTM(double, double, double*, double*);
  char UTMLetterDesignator(double);
  void LLtoUTM(const double , const double, double &, double &, char*);
  void LLtoUTM(const double , const double , double &, double &, std::string &);
  void UTMtoLL(const double, const double, const char*, double&, double& );
  void UTMtoLL(const double, const double, std::string, double&, double&);

}
# endif
