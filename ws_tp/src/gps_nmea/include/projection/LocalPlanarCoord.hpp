/**

\file
\author Stéphane Witzmann (2014)
\copyright 2014 Institut Pascal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef IP_ROS_SENSORS_TOOLS_COORD_HPP
#define IP_ROS_SENSORS_TOOLS_COORD_HPP

#include <cmath>
#include "gps_nmea/GpsGGA.h"
#include "gps_nmea/GpsCRT.h"
#include "gps_nmea/GpsRMC.h"
#include "gps_nmea/GpsGST.h"

namespace gps_nmea
{

  struct gps_ref
  {
    int deg;
    int min;
    double sec;
  };

  struct Ellipsoid
  {
    static const Ellipsoid wgs84;

    Ellipsoid(long double a_, long double b_):
      a(a_),
      b(b_),
      e(sqrtl(1.L - powl(b_/a_, 2.L))),
      f((a_ - b_)/a_)
    {
    }

    long double a, b, e, f;
  };

  struct Wgs84
  {
    Wgs84() {}
    constexpr Wgs84(int latd, int latm, long double lats, int lond, int lonm, long double lons):
      latitude((long double)(latd) + ((long double)(latm)/60.L) + lats/3600.L),
      longitude((long double)(lond) + ((long double)(lonm)/60.L) + lons/3600.L),
      altitude(0.)
    {
    }
    constexpr Wgs84(int latd, long double latm, int lond, long double lonm):
      latitude((long double)(latd) + latm/60.L),
      longitude((long double)(lond) + lonm/60.L),
      altitude(0.)
    {
    }
    constexpr explicit Wgs84(long double lat, long double lon):
      latitude(lat),
      longitude(lon),
      altitude(0.)
    {
    }
    
    constexpr explicit Wgs84(long double lat, long double lon, long double alt):
      latitude(lat),
      longitude(lon),
      altitude(alt)
    {
    }


    long double latitude;  // +: N, -: S --- unit: degrees
    long double longitude; // +: E, -: W --- unit: degrees, origin: Greenwich
    long double altitude;
  };

  struct GeoCoord
  {
    GeoCoord() {}
    constexpr GeoCoord(long double phi_, long double lambda_, long double h_):
      phi(phi_),
      lambda(lambda_),
      h(h_)
    {
    }
    constexpr GeoCoord(const Wgs84 &w, long double h_):
      phi(M_PIl*w.latitude/180.L),
      lambda(M_PIl*w.longitude/180.L),
      h(h_)
    {
    }

    long double phi, lambda, h;
  };

  struct EcefCoord
  {
    EcefCoord() {}
    constexpr EcefCoord(long double x_, long double y_, long double z_):
      x(x_),
      y(y_),
      z(z_)
    {
    }
    EcefCoord(const GeoCoord &);

    long double x, y, z;
  };

  struct PlanarCoord
  {
    PlanarCoord() {}
    constexpr PlanarCoord(long double n, long double e, long double u):
      north(n),
      east(e),
      up(u)
    {
    }

    long double north, east, up;
  };

  class GeoToPlanarConverter
  {
  public:
    explicit GeoToPlanarConverter(const GeoCoord &, double = 0.);
    GeoToPlanarConverter(const Wgs84 &, long double, double = 0.);

    PlanarCoord position(const GeoCoord &gc) const;
    PlanarCoord position(const EcefCoord &) const;
    PlanarCoord velocity(const EcefCoord &) const;

    PlanarCoord position(const GpsGGA &) const;
    PlanarCoord position(const GpsCRT &) const;
    PlanarCoord position(const GpsRMC &) const;
    PlanarCoord position(const double &, const double &) const;
    PlanarCoord velocity(const GpsCRT &) const;

    static PlanarCoord stdev(const GpsGST &);

    static bool can_read_position(const GpsGGA &);
    static bool can_read_position(const GpsCRT &);
    static bool can_read_position(const GpsRMC &);
    static bool can_read_velocity(const GpsCRT &);
    static bool can_read_stdev(const GpsGST &);

  private:
    long double r_[9];
    EcefCoord t_;
    double default_geoidal_separation_;
  };

}
#endif
