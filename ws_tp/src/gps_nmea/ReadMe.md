# **gps_nmea**
--------

## 1 - Overview
--------
Package ROS pour gps de type NMEA.

Date : 15/05/2017.

* Auteur 1 : Laurent Malaterre (2017) laurent.malaterre@uca.fr (ROS migration developper, architecture management,gps utilities viewer/filter/academic )
* Auteur 2 : Stephane Witzmann (2014) stephane.witzmann@univ-bpclermont.fr (gps utilities driver/parser/projection, vbus_sockets)
* Auteur 3 : Chuck Gantz (?) chuck.gantz@globalstar.com (UTM conversions)

Company : [Institut Pascal](http://ip.univ-bpclermont.fr/index.php/fr/)

## 2 - Description
-----------
Ce package comprend la connection à un capteur gps serie (RTK ou non) et des outils de transformation des données: 
- interpretation (parseur) des trames 
- reprojection plane des coordonnées
- fourniture de messages *'ROS academic'*
- filtrage passe bas basique
- fourniture de messages de visualisation et de transformation de repère

![gps_nmea_process](./doc/gps_nmea_data_process_diagram_v1.png)

## 3 - Utilitaires
-----

### a - gps_nmea_driver:

Pour lancer le node :
```
roslaunch gps_nmea driver.launch
```

Il prend en parametre les caractéristiques contenues dans **config/serial_gps.yaml**. For example:

```yaml
serial_dev: '/dev/ttyACM0'
baudrate: 115200
``` 
Nous pouvons aussi lancer de façon parametrable avec :
```
roslaunch gps_nmea driver_byarg.launch
```

#### Parameters
*~config_serial* (*string*, default: "$(find gps_nmea)/config/serial_gps.yaml")
: chemin du fichier de configuration du port serie du GPS (format .yaml)

-----

### b - gps_nmea_parser :
Interprète les différentes trames produites par la norme NMEA afin de generer un topic par type de trame.
Pour lancer le node :

```
roslaunch gps_nmea parser.launch
```
#### Parameters

*~parser_type* (*string*, default: "enabled")
: Choice are enabled, disabled, ignore. Policy for manage frame delemiter characters and checksum (see include/tools/NmeaFrame.hpp).

*~soft_error_catching* (*bool*, default: "false")
: To set if we want a hard error processing in case of sensor frame error ("false" setting case). In this case, the node is sevely stopped at first occurrence.
Otherwise, with "true" set, we just inform the type and the actual ratio of frame errors.
Of course, In both case we don't publish any wrong result on output topics.

-----

### c - gps_nmea_projection :
Transforme les coordonnées sphériques WGS84 (longitude, latitude, altitude) et coordonnées cartésiennes (east, north, up) grace à des methodes de transformation planaires normalisées.

Pour lancer le node :
```
roslaunch gps_nmea projection.launch
```

La projection peut donc êre déclinée en 3 types de conversions planes normalisées : 
- **géographique locale** ('geolocal')
- **UTM** ('utm' pour Universal Transverse Mercator)
- **LambertII etendu**.

Dans les 3 cas ces reprojections utilisent le système Géodésique Wgs84.

Il prend en parametre les caractéristiques contenues dans 
- **config/projection_gps.yaml**.

**Noter** la variable **planar_type** qui configure le systeme de coordonnées.

Pour la conversion *geolocal* : Les references lat, lon et alt sont les coordonnées du point de reference tangent local. Ce point est exprimé deg/min/sec.
Par exemple sur PAVIN nous nous referons à un point connu (il pourrait être n'importe lequel en fait) et détaillé [IGN](http://rgp.ign.fr/STATIONS/#CLFD).

-----

### d - gps_nmea_academic :
Reprend les messages au format *local* (GpsGGA.msg) pour les traduire dans un format plus utilisé dans la communauté de ROS (*sensor_msgs::NavSatFix*).

#### Parameters

*/publisher/academic_gps* (*string*, default: "/gps/fix")
: nom du topic entrant

*/subscriber/consumer_gps* (*string*, default: "/gps_gga")
: nom du topic sortant

*/publisher/frame_id* (*string*, default: "dgps")
: frame_id sortant

Il est possible de faire l'inverse grâce au node **gps_nmea_antiacademic**

-----

### e - gps_nmea_filter :
Propose un filtrage de premier ordre des coordonnées planaires.
```
vs(n) = (1 - alpha).vs(n-1) + alpha.vi(n)
```
Il prend en parametre les caractéristiques contenues dans 
- **config/filter_param.yaml**.

**Noter** le parametre de fenetrage alpha referencé **windowing**

-----

### f - gps_nmea_viewer :
Genere des messages de visualisation exploitable par RViz des points GPS. Deux types de messages sont proposés :
- **Path** avec *nav_msgs::Path*
- **Traj** avec de simples Markers : *visualization_msgs::Marker*.

Il produit aussi des messages de transformation de repere de type tf : *tf::StampedTransform*.

Il prend en parametre les caractéristiques contenues dans 
- **config/projection.yaml**.
- **config/transform_param.yaml**.

-----

### g - all_gpsnmea_play_bag
Pour lancer la chaine complète avec la lecture d'un fichier d'enregistrement ROS (bag) :
```
roslaunch gps_nmea all_gpsnmea_play_bag.launch
```

remarque : le repertoire /bag n'est pas versionné ni le fichier .bag fourni. Il suffit d'en produire un avec une acquisition de topic /gps_sentence ou demander à l'autheur (laurent.malaterre@uca.fr)

## 4 - Divers
-----

### a - dependances:
Le paquet **gps_nmea** fonctionne avec *vbus_sockets*.

Les executables *gps_nmea_pathviewer* et *gps_nmea_trajviewer* utilise *ip_drawing_tools*.

-----
### b - TODO:

-----
### c - materiel testé:
* Ublox AEK-4T et 6T
* Ashtech Proflex 800
* Ublox NeoM8P

-----

### d - Le nmea :
Quelques references 
* pour les nuls [wikipedia](https://fr.wikipedia.org/wiki/NMEA_0183).
* pour les costauds [gpsinformation](http://www.gpsinformation.org/dale/nmea.htm).
* pdf de sirf dans **./doc/NMEA Reference Manual-Rev2.1-Dec07.pdf**.




