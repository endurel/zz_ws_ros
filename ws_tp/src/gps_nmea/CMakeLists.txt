cmake_minimum_required(VERSION 2.8.3)
project(gps_nmea)

## Add support for C++11, supported in ROS Kinetic and newer
 add_definitions(-std=c++14)
 
set(DEPS
  message_filters
  nav_msgs
  roscpp
  sensor_msgs
  std_msgs
  tf
  ip_drawing_tools
)

find_package(catkin REQUIRED COMPONENTS
  message_generation
  ${DEPS}
)

################################################
## Declare ROS messages, services and actions ##
################################################

add_message_files(
  FILES
    GpsStringFix.msg
    GpsPlanar.msg
    WGS84.msg
    Date_gps.msg
    GpsGGA.msg
    GpsGST.msg
    GpsVTG.msg
    GpsCRT.msg
    GpsRMC.msg
    GpsLTN.msg
    GpsGSV.msg
)

generate_messages(DEPENDENCIES 
  nav_msgs
  sensor_msgs
  std_msgs
)



###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES gps_nmea
  CATKIN_DEPENDS message_runtime ${DEPS}
#  DEPENDS system_lib
)

###########
## Build ##
###########

include_directories(
  include ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
FILE(GLOB CppFiles      src/projection/UTMCoord.cpp
                        src/projection/LocalPlanarCoord.cpp
			src/projection/LambertCoord.cpp
)
add_library(${PROJECT_NAME} ${CppFiles}
)

## Declare a C++ executable

add_executable(${PROJECT_NAME}_filter
     src/filter/GpsFilter.cpp
 )

add_executable(${PROJECT_NAME}_trajviewer
     src/viewer/GpsToTrajViewer.cpp
 )

add_executable(${PROJECT_NAME}_pathviewer
     src/viewer/GpsToPathViewer.cpp
 )

add_executable(${PROJECT_NAME}_academic
     src/academic/GpsAcademic.cpp
 )

 add_executable(${PROJECT_NAME}_antiacademic
     src/academic/GpsAntiAcademic.cpp
 )
 
add_executable(${PROJECT_NAME}_projection
     src/projection/UTMCoord.cpp
     src/projection/LocalPlanarCoord.cpp
     src/projection/LambertCoord.cpp
     src/projection/GpsProjection.cpp
 )

add_executable(${PROJECT_NAME}_parser
     src/tools/NmeaFrame.cpp
     src/parser/GpsFrame.cpp
     src/parser/GpsParser.cpp
 )

add_executable(${PROJECT_NAME}_driver
     src/vbus_sockets/SocketPair.cpp
     src/vbus_sockets/SyncPair.cpp
     src/vbus_sockets/Selector.cpp
     src/vbus_sockets/Descriptor.cpp
     src/serial/SerialLink.cpp
     src/gps/SerialGps.cpp
     src/gps/Gps.cpp
     src/gps/GpsNode.cpp
 )

## Add cmake target dependencies of the executable

add_dependencies(${PROJECT_NAME}
   ${PROJECT_NAME}_generate_messages_cpp 
   ${${PROJECT_NAME}_EXPORTED_TARGETS} 
   ${catkin_EXPORTED_TARGETS}
 )
 
add_dependencies(${PROJECT_NAME}_filter
   ${PROJECT_NAME}_generate_messages_cpp 
   ${${PROJECT_NAME}_EXPORTED_TARGETS} 
   ${catkin_EXPORTED_TARGETS}
 )

add_dependencies(${PROJECT_NAME}_trajviewer
   ${PROJECT_NAME}_generate_messages_cpp 
   ${${PROJECT_NAME}_EXPORTED_TARGETS} 
   ${catkin_EXPORTED_TARGETS}
 )

add_dependencies(${PROJECT_NAME}_pathviewer
   ${PROJECT_NAME}_generate_messages_cpp 
   ${${PROJECT_NAME}_EXPORTED_TARGETS} 
   ${catkin_EXPORTED_TARGETS}
 )

add_dependencies(${PROJECT_NAME}_academic
   ${PROJECT_NAME}_generate_messages_cpp 
   ${${PROJECT_NAME}_EXPORTED_TARGETS} 
   ${catkin_EXPORTED_TARGETS}
 )

 add_dependencies(${PROJECT_NAME}_antiacademic
   ${PROJECT_NAME}_generate_messages_cpp 
   ${${PROJECT_NAME}_EXPORTED_TARGETS} 
   ${catkin_EXPORTED_TARGETS}
 )
 
add_dependencies(${PROJECT_NAME}_projection
   ${PROJECT_NAME}_generate_messages_cpp 
   ${${PROJECT_NAME}_EXPORTED_TARGETS} 
   ${catkin_EXPORTED_TARGETS}
 )

add_dependencies(${PROJECT_NAME}_parser
   ${PROJECT_NAME}_generate_messages_cpp 
   ${${PROJECT_NAME}_EXPORTED_TARGETS} 
   ${catkin_EXPORTED_TARGETS}
 )

add_dependencies(${PROJECT_NAME}_driver
   ${PROJECT_NAME}_generate_messages_cpp 
   ${${PROJECT_NAME}_EXPORTED_TARGETS} 
   ${catkin_EXPORTED_TARGETS}
 )


## Specify libraries to link a library or executable target against

 target_link_libraries(${PROJECT_NAME}_filter
   ${catkin_LIBRARIES}
 )

 target_link_libraries(${PROJECT_NAME}_trajviewer
   ${catkin_LIBRARIES}
 )

 target_link_libraries(${PROJECT_NAME}_pathviewer
   ${catkin_LIBRARIES}
 )

 target_link_libraries(${PROJECT_NAME}_academic
   ${catkin_LIBRARIES}
 )

 target_link_libraries(${PROJECT_NAME}_antiacademic
   ${catkin_LIBRARIES}
 )
 
 target_link_libraries(${PROJECT_NAME}_projection
   ${catkin_LIBRARIES} 
 )

 target_link_libraries(${PROJECT_NAME}_parser
   ${catkin_LIBRARIES}
 )

 target_link_libraries(${PROJECT_NAME}_driver
   ${catkin_LIBRARIES}
 )

#############
## Install ##
#############

## Mark cpp header files for installation

 install(DIRECTORY include/${PROJECT_NAME}_filter/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h"
   PATTERN ".svn" EXCLUDE
 )

 install(DIRECTORY include/${PROJECT_NAME}_trajviewer/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h"
   PATTERN ".svn" EXCLUDE
 )

 install(DIRECTORY include/${PROJECT_NAME}_pathviewer/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h"
   PATTERN ".svn" EXCLUDE
 )

 install(DIRECTORY include/${PROJECT_NAME}_academic/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h"
   PATTERN ".svn" EXCLUDE
 )

 install(DIRECTORY include/${PROJECT_NAME}_antiacademic/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h"
   PATTERN ".svn" EXCLUDE
 )
 
 install(DIRECTORY include/${PROJECT_NAME}_projection/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h"
   PATTERN ".svn" EXCLUDE
 )

 install(DIRECTORY include/${PROJECT_NAME}_parser/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h"
   PATTERN ".svn" EXCLUDE
 )

 install(DIRECTORY include/${PROJECT_NAME}_driver/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h"
   PATTERN ".svn" EXCLUDE
 )

install(TARGETS ${PROJECT_NAME}_filter
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(TARGETS ${PROJECT_NAME}_trajviewer
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(TARGETS ${PROJECT_NAME}_pathviewer
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(TARGETS ${PROJECT_NAME}_academic
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(TARGETS ${PROJECT_NAME}_antiacademic
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(TARGETS ${PROJECT_NAME}_projection
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(TARGETS ${PROJECT_NAME}_parser
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(TARGETS ${PROJECT_NAME}_driver
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
install(DIRECTORY
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)
