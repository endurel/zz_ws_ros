(cl:in-package gps_nmea-msg)
(cl:export '(HEADER-VAL
          HEADER
          COG_TRUE_ENABLED-VAL
          COG_TRUE_ENABLED
          COG_MAGNETIC_ENABLED-VAL
          COG_MAGNETIC_ENABLED
          SOG_KNOTS_ENABLED-VAL
          SOG_KNOTS_ENABLED
          SOG_KMH_ENABLED-VAL
          SOG_KMH_ENABLED
          MODE_ENABLED-VAL
          MODE_ENABLED
          COG_TRUE-VAL
          COG_TRUE
          COG_MAGNETIC-VAL
          COG_MAGNETIC
          SOG_KNOTS-VAL
          SOG_KNOTS
          SOG_KMH-VAL
          SOG_KMH
          MODE-VAL
          MODE
))