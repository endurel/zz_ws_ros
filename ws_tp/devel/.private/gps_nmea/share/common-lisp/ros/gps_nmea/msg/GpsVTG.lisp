; Auto-generated. Do not edit!


(cl:in-package gps_nmea-msg)


;//! \htmlinclude GpsVTG.msg.html

(cl:defclass <GpsVTG> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (cog_true_enabled
    :reader cog_true_enabled
    :initarg :cog_true_enabled
    :type cl:boolean
    :initform cl:nil)
   (cog_magnetic_enabled
    :reader cog_magnetic_enabled
    :initarg :cog_magnetic_enabled
    :type cl:boolean
    :initform cl:nil)
   (sog_knots_enabled
    :reader sog_knots_enabled
    :initarg :sog_knots_enabled
    :type cl:boolean
    :initform cl:nil)
   (sog_kmh_enabled
    :reader sog_kmh_enabled
    :initarg :sog_kmh_enabled
    :type cl:boolean
    :initform cl:nil)
   (mode_enabled
    :reader mode_enabled
    :initarg :mode_enabled
    :type cl:boolean
    :initform cl:nil)
   (cog_true
    :reader cog_true
    :initarg :cog_true
    :type cl:float
    :initform 0.0)
   (cog_magnetic
    :reader cog_magnetic
    :initarg :cog_magnetic
    :type cl:float
    :initform 0.0)
   (sog_knots
    :reader sog_knots
    :initarg :sog_knots
    :type cl:float
    :initform 0.0)
   (sog_kmh
    :reader sog_kmh
    :initarg :sog_kmh
    :type cl:float
    :initform 0.0)
   (mode
    :reader mode
    :initarg :mode
    :type cl:fixnum
    :initform 0))
)

(cl:defclass GpsVTG (<GpsVTG>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GpsVTG>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GpsVTG)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_nmea-msg:<GpsVTG> is deprecated: use gps_nmea-msg:GpsVTG instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:header-val is deprecated.  Use gps_nmea-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'cog_true_enabled-val :lambda-list '(m))
(cl:defmethod cog_true_enabled-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:cog_true_enabled-val is deprecated.  Use gps_nmea-msg:cog_true_enabled instead.")
  (cog_true_enabled m))

(cl:ensure-generic-function 'cog_magnetic_enabled-val :lambda-list '(m))
(cl:defmethod cog_magnetic_enabled-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:cog_magnetic_enabled-val is deprecated.  Use gps_nmea-msg:cog_magnetic_enabled instead.")
  (cog_magnetic_enabled m))

(cl:ensure-generic-function 'sog_knots_enabled-val :lambda-list '(m))
(cl:defmethod sog_knots_enabled-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:sog_knots_enabled-val is deprecated.  Use gps_nmea-msg:sog_knots_enabled instead.")
  (sog_knots_enabled m))

(cl:ensure-generic-function 'sog_kmh_enabled-val :lambda-list '(m))
(cl:defmethod sog_kmh_enabled-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:sog_kmh_enabled-val is deprecated.  Use gps_nmea-msg:sog_kmh_enabled instead.")
  (sog_kmh_enabled m))

(cl:ensure-generic-function 'mode_enabled-val :lambda-list '(m))
(cl:defmethod mode_enabled-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:mode_enabled-val is deprecated.  Use gps_nmea-msg:mode_enabled instead.")
  (mode_enabled m))

(cl:ensure-generic-function 'cog_true-val :lambda-list '(m))
(cl:defmethod cog_true-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:cog_true-val is deprecated.  Use gps_nmea-msg:cog_true instead.")
  (cog_true m))

(cl:ensure-generic-function 'cog_magnetic-val :lambda-list '(m))
(cl:defmethod cog_magnetic-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:cog_magnetic-val is deprecated.  Use gps_nmea-msg:cog_magnetic instead.")
  (cog_magnetic m))

(cl:ensure-generic-function 'sog_knots-val :lambda-list '(m))
(cl:defmethod sog_knots-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:sog_knots-val is deprecated.  Use gps_nmea-msg:sog_knots instead.")
  (sog_knots m))

(cl:ensure-generic-function 'sog_kmh-val :lambda-list '(m))
(cl:defmethod sog_kmh-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:sog_kmh-val is deprecated.  Use gps_nmea-msg:sog_kmh instead.")
  (sog_kmh m))

(cl:ensure-generic-function 'mode-val :lambda-list '(m))
(cl:defmethod mode-val ((m <GpsVTG>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:mode-val is deprecated.  Use gps_nmea-msg:mode instead.")
  (mode m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<GpsVTG>)))
    "Constants for message type '<GpsVTG>"
  '((:GPS_MODE_AUTONOMOUS . 0)
    (:GPS_MODE_DIFFERENTIAL . 1)
    (:GPS_MODE_INVALID . 2))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'GpsVTG)))
    "Constants for message type 'GpsVTG"
  '((:GPS_MODE_AUTONOMOUS . 0)
    (:GPS_MODE_DIFFERENTIAL . 1)
    (:GPS_MODE_INVALID . 2))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GpsVTG>) ostream)
  "Serializes a message object of type '<GpsVTG>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'cog_true_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'cog_magnetic_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'sog_knots_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'sog_kmh_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'mode_enabled) 1 0)) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'cog_true))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'cog_magnetic))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'sog_knots))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'sog_kmh))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'mode)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GpsVTG>) istream)
  "Deserializes a message object of type '<GpsVTG>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'cog_true_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'cog_magnetic_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'sog_knots_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'sog_kmh_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'mode_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'cog_true) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'cog_magnetic) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'sog_knots) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'sog_kmh) (roslisp-utils:decode-double-float-bits bits)))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'mode)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GpsVTG>)))
  "Returns string type for a message object of type '<GpsVTG>"
  "gps_nmea/GpsVTG")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GpsVTG)))
  "Returns string type for a message object of type 'GpsVTG"
  "gps_nmea/GpsVTG")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GpsVTG>)))
  "Returns md5sum for a message object of type '<GpsVTG>"
  "61315a94881ac3a1456599c4e29fa56f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GpsVTG)))
  "Returns md5sum for a message object of type 'GpsVTG"
  "61315a94881ac3a1456599c4e29fa56f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GpsVTG>)))
  "Returns full string definition for message of type '<GpsVTG>"
  (cl:format cl:nil "Header header~%~%bool cog_true_enabled~%bool cog_magnetic_enabled~%bool sog_knots_enabled~%bool sog_kmh_enabled~%bool mode_enabled~%~%#Course over ground orientation with respect to true north~%float64 cog_true~%# Course over ground orientation with respect to magnetic north~%float64 cog_magnetic~%# Speed over ground in knots~%float64 sog_knots~%#Speed over ground in km/h~%float64 sog_kmh~%~%#GPS mode ~%# 0 : Autonomous mode~%# 1 : Differential mode~%# 2 : Data not valid~%uint8 GPS_MODE_AUTONOMOUS = 0~%uint8 GPS_MODE_DIFFERENTIAL = 1~%uint8 GPS_MODE_INVALID = 2~%uint8 mode~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GpsVTG)))
  "Returns full string definition for message of type 'GpsVTG"
  (cl:format cl:nil "Header header~%~%bool cog_true_enabled~%bool cog_magnetic_enabled~%bool sog_knots_enabled~%bool sog_kmh_enabled~%bool mode_enabled~%~%#Course over ground orientation with respect to true north~%float64 cog_true~%# Course over ground orientation with respect to magnetic north~%float64 cog_magnetic~%# Speed over ground in knots~%float64 sog_knots~%#Speed over ground in km/h~%float64 sog_kmh~%~%#GPS mode ~%# 0 : Autonomous mode~%# 1 : Differential mode~%# 2 : Data not valid~%uint8 GPS_MODE_AUTONOMOUS = 0~%uint8 GPS_MODE_DIFFERENTIAL = 1~%uint8 GPS_MODE_INVALID = 2~%uint8 mode~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GpsVTG>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
     8
     8
     8
     8
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GpsVTG>))
  "Converts a ROS message object to a list"
  (cl:list 'GpsVTG
    (cl:cons ':header (header msg))
    (cl:cons ':cog_true_enabled (cog_true_enabled msg))
    (cl:cons ':cog_magnetic_enabled (cog_magnetic_enabled msg))
    (cl:cons ':sog_knots_enabled (sog_knots_enabled msg))
    (cl:cons ':sog_kmh_enabled (sog_kmh_enabled msg))
    (cl:cons ':mode_enabled (mode_enabled msg))
    (cl:cons ':cog_true (cog_true msg))
    (cl:cons ':cog_magnetic (cog_magnetic msg))
    (cl:cons ':sog_knots (sog_knots msg))
    (cl:cons ':sog_kmh (sog_kmh msg))
    (cl:cons ':mode (mode msg))
))
