(cl:in-package gps_nmea-msg)
(cl:export '(LATITUDE-VAL
          LATITUDE
          LONGITUDE-VAL
          LONGITUDE
))