; Auto-generated. Do not edit!


(cl:in-package gps_nmea-msg)


;//! \htmlinclude GpsGGA.msg.html

(cl:defclass <GpsGGA> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (time_enabled
    :reader time_enabled
    :initarg :time_enabled
    :type cl:boolean
    :initform cl:nil)
   (position_enabled
    :reader position_enabled
    :initarg :position_enabled
    :type cl:boolean
    :initform cl:nil)
   (position_type_enabled
    :reader position_type_enabled
    :initarg :position_type_enabled
    :type cl:boolean
    :initform cl:nil)
   (satellites_enabled
    :reader satellites_enabled
    :initarg :satellites_enabled
    :type cl:boolean
    :initform cl:nil)
   (hdop_enabled
    :reader hdop_enabled
    :initarg :hdop_enabled
    :type cl:boolean
    :initform cl:nil)
   (altitude_enabled
    :reader altitude_enabled
    :initarg :altitude_enabled
    :type cl:boolean
    :initform cl:nil)
   (geoidal_separation_enabled
    :reader geoidal_separation_enabled
    :initarg :geoidal_separation_enabled
    :type cl:boolean
    :initform cl:nil)
   (diff_age_enabled
    :reader diff_age_enabled
    :initarg :diff_age_enabled
    :type cl:boolean
    :initform cl:nil)
   (base_id_enabled
    :reader base_id_enabled
    :initarg :base_id_enabled
    :type cl:boolean
    :initform cl:nil)
   (time
    :reader time
    :initarg :time
    :type cl:real
    :initform 0)
   (position
    :reader position
    :initarg :position
    :type gps_nmea-msg:WGS84
    :initform (cl:make-instance 'gps_nmea-msg:WGS84))
   (position_type
    :reader position_type
    :initarg :position_type
    :type cl:fixnum
    :initform 0)
   (satellites
    :reader satellites
    :initarg :satellites
    :type cl:fixnum
    :initform 0)
   (hdop
    :reader hdop
    :initarg :hdop
    :type cl:float
    :initform 0.0)
   (altitude
    :reader altitude
    :initarg :altitude
    :type cl:float
    :initform 0.0)
   (geoidal_separation
    :reader geoidal_separation
    :initarg :geoidal_separation
    :type cl:float
    :initform 0.0)
   (diff_age
    :reader diff_age
    :initarg :diff_age
    :type cl:real
    :initform 0)
   (base_station_id
    :reader base_station_id
    :initarg :base_station_id
    :type cl:fixnum
    :initform 0))
)

(cl:defclass GpsGGA (<GpsGGA>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GpsGGA>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GpsGGA)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_nmea-msg:<GpsGGA> is deprecated: use gps_nmea-msg:GpsGGA instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:header-val is deprecated.  Use gps_nmea-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'time_enabled-val :lambda-list '(m))
(cl:defmethod time_enabled-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:time_enabled-val is deprecated.  Use gps_nmea-msg:time_enabled instead.")
  (time_enabled m))

(cl:ensure-generic-function 'position_enabled-val :lambda-list '(m))
(cl:defmethod position_enabled-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position_enabled-val is deprecated.  Use gps_nmea-msg:position_enabled instead.")
  (position_enabled m))

(cl:ensure-generic-function 'position_type_enabled-val :lambda-list '(m))
(cl:defmethod position_type_enabled-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position_type_enabled-val is deprecated.  Use gps_nmea-msg:position_type_enabled instead.")
  (position_type_enabled m))

(cl:ensure-generic-function 'satellites_enabled-val :lambda-list '(m))
(cl:defmethod satellites_enabled-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:satellites_enabled-val is deprecated.  Use gps_nmea-msg:satellites_enabled instead.")
  (satellites_enabled m))

(cl:ensure-generic-function 'hdop_enabled-val :lambda-list '(m))
(cl:defmethod hdop_enabled-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:hdop_enabled-val is deprecated.  Use gps_nmea-msg:hdop_enabled instead.")
  (hdop_enabled m))

(cl:ensure-generic-function 'altitude_enabled-val :lambda-list '(m))
(cl:defmethod altitude_enabled-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:altitude_enabled-val is deprecated.  Use gps_nmea-msg:altitude_enabled instead.")
  (altitude_enabled m))

(cl:ensure-generic-function 'geoidal_separation_enabled-val :lambda-list '(m))
(cl:defmethod geoidal_separation_enabled-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:geoidal_separation_enabled-val is deprecated.  Use gps_nmea-msg:geoidal_separation_enabled instead.")
  (geoidal_separation_enabled m))

(cl:ensure-generic-function 'diff_age_enabled-val :lambda-list '(m))
(cl:defmethod diff_age_enabled-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:diff_age_enabled-val is deprecated.  Use gps_nmea-msg:diff_age_enabled instead.")
  (diff_age_enabled m))

(cl:ensure-generic-function 'base_id_enabled-val :lambda-list '(m))
(cl:defmethod base_id_enabled-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:base_id_enabled-val is deprecated.  Use gps_nmea-msg:base_id_enabled instead.")
  (base_id_enabled m))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:time-val is deprecated.  Use gps_nmea-msg:time instead.")
  (time m))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position-val is deprecated.  Use gps_nmea-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'position_type-val :lambda-list '(m))
(cl:defmethod position_type-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position_type-val is deprecated.  Use gps_nmea-msg:position_type instead.")
  (position_type m))

(cl:ensure-generic-function 'satellites-val :lambda-list '(m))
(cl:defmethod satellites-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:satellites-val is deprecated.  Use gps_nmea-msg:satellites instead.")
  (satellites m))

(cl:ensure-generic-function 'hdop-val :lambda-list '(m))
(cl:defmethod hdop-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:hdop-val is deprecated.  Use gps_nmea-msg:hdop instead.")
  (hdop m))

(cl:ensure-generic-function 'altitude-val :lambda-list '(m))
(cl:defmethod altitude-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:altitude-val is deprecated.  Use gps_nmea-msg:altitude instead.")
  (altitude m))

(cl:ensure-generic-function 'geoidal_separation-val :lambda-list '(m))
(cl:defmethod geoidal_separation-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:geoidal_separation-val is deprecated.  Use gps_nmea-msg:geoidal_separation instead.")
  (geoidal_separation m))

(cl:ensure-generic-function 'diff_age-val :lambda-list '(m))
(cl:defmethod diff_age-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:diff_age-val is deprecated.  Use gps_nmea-msg:diff_age instead.")
  (diff_age m))

(cl:ensure-generic-function 'base_station_id-val :lambda-list '(m))
(cl:defmethod base_station_id-val ((m <GpsGGA>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:base_station_id-val is deprecated.  Use gps_nmea-msg:base_station_id instead.")
  (base_station_id m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<GpsGGA>)))
    "Constants for message type '<GpsGGA>"
  '((:POSITION_TYPE_INVALID . 0)
    (:POSITION_TYPE_AUTONOMOUS . 1)
    (:POSITION_TYPE_DIFFERENTIAL . 2)
    (:POSITION_TYPE_RTKFIXED . 4)
    (:POSITION_TYPE_RTKFLOAT . 5))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'GpsGGA)))
    "Constants for message type 'GpsGGA"
  '((:POSITION_TYPE_INVALID . 0)
    (:POSITION_TYPE_AUTONOMOUS . 1)
    (:POSITION_TYPE_DIFFERENTIAL . 2)
    (:POSITION_TYPE_RTKFIXED . 4)
    (:POSITION_TYPE_RTKFLOAT . 5))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GpsGGA>) ostream)
  "Serializes a message object of type '<GpsGGA>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'time_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'position_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'position_type_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'satellites_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'hdop_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'altitude_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'geoidal_separation_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'diff_age_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'base_id_enabled) 1 0)) ostream)
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'time)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'time) (cl:floor (cl:slot-value msg 'time)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'position) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'position_type)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'satellites)) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'hdop))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'altitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'geoidal_separation))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'diff_age)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'diff_age) (cl:floor (cl:slot-value msg 'diff_age)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'base_station_id)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GpsGGA>) istream)
  "Deserializes a message object of type '<GpsGGA>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'time_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'position_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'position_type_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'satellites_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'hdop_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'altitude_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'geoidal_separation_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'diff_age_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'base_id_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'time) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'position) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'position_type)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'satellites)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'hdop) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'altitude) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'geoidal_separation) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'diff_age) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'base_station_id)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GpsGGA>)))
  "Returns string type for a message object of type '<GpsGGA>"
  "gps_nmea/GpsGGA")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GpsGGA)))
  "Returns string type for a message object of type 'GpsGGA"
  "gps_nmea/GpsGGA")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GpsGGA>)))
  "Returns md5sum for a message object of type '<GpsGGA>"
  "18c21ea63ef156a3d80f5cebf18705d1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GpsGGA)))
  "Returns md5sum for a message object of type 'GpsGGA"
  "18c21ea63ef156a3d80f5cebf18705d1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GpsGGA>)))
  "Returns full string definition for message of type '<GpsGGA>"
  (cl:format cl:nil "Header header~%~%bool time_enabled~%bool position_enabled~%bool position_type_enabled~%bool satellites_enabled~%bool hdop_enabled~%bool altitude_enabled~%bool geoidal_separation_enabled~%bool diff_age_enabled~%bool base_id_enabled~%~%# Current UTC time~%time time~%# WGS84 latitude and longitude~%WGS84 position  ~%~%# Position type~%# 0 : Position not available or invalid~%# 1 : Autonomous position~%# 2 : RTCM Differential SBAS Differential~%# 4 : RTK fixed~%# 5 : RTK float~%~%uint8 POSITION_TYPE_INVALID = 0~%uint8 POSITION_TYPE_AUTONOMOUS = 1~%uint8 POSITION_TYPE_DIFFERENTIAL = 2~%uint8 POSITION_TYPE_RTKFIXED = 4~%uint8 POSITION_TYPE_RTKFLOAT = 5~%uint8 position_type              ~%~%# Number of GNSS Satellites being used in the position computation~%uint8 satellites  ~%# Horizontal dilution of precision~%float64 hdop ~%# Altitude, in meters, above mean seal level~%float64 altitude~%# Geoidal separation in meters~%float64 geoidal_separation ~%# Age of differential corrections, in seconds~%duration diff_age~%# Base station ID (RTCM only)~%uint8 base_station_id~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: gps_nmea/WGS84~%# Latitude (degrees). Positive is north of equator; negative is south. ~%# +: N, -: S --- unit: degrees~%float64 latitude~%~%# Longitude (degrees). Positive is east of prime meridian, negative west.~%# +: E, -: W --- unit: degrees, origin: Greenwich~%float64 longitude~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GpsGGA)))
  "Returns full string definition for message of type 'GpsGGA"
  (cl:format cl:nil "Header header~%~%bool time_enabled~%bool position_enabled~%bool position_type_enabled~%bool satellites_enabled~%bool hdop_enabled~%bool altitude_enabled~%bool geoidal_separation_enabled~%bool diff_age_enabled~%bool base_id_enabled~%~%# Current UTC time~%time time~%# WGS84 latitude and longitude~%WGS84 position  ~%~%# Position type~%# 0 : Position not available or invalid~%# 1 : Autonomous position~%# 2 : RTCM Differential SBAS Differential~%# 4 : RTK fixed~%# 5 : RTK float~%~%uint8 POSITION_TYPE_INVALID = 0~%uint8 POSITION_TYPE_AUTONOMOUS = 1~%uint8 POSITION_TYPE_DIFFERENTIAL = 2~%uint8 POSITION_TYPE_RTKFIXED = 4~%uint8 POSITION_TYPE_RTKFLOAT = 5~%uint8 position_type              ~%~%# Number of GNSS Satellites being used in the position computation~%uint8 satellites  ~%# Horizontal dilution of precision~%float64 hdop ~%# Altitude, in meters, above mean seal level~%float64 altitude~%# Geoidal separation in meters~%float64 geoidal_separation ~%# Age of differential corrections, in seconds~%duration diff_age~%# Base station ID (RTCM only)~%uint8 base_station_id~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: gps_nmea/WGS84~%# Latitude (degrees). Positive is north of equator; negative is south. ~%# +: N, -: S --- unit: degrees~%float64 latitude~%~%# Longitude (degrees). Positive is east of prime meridian, negative west.~%# +: E, -: W --- unit: degrees, origin: Greenwich~%float64 longitude~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GpsGGA>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
     1
     1
     1
     1
     8
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'position))
     1
     1
     8
     8
     8
     8
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GpsGGA>))
  "Converts a ROS message object to a list"
  (cl:list 'GpsGGA
    (cl:cons ':header (header msg))
    (cl:cons ':time_enabled (time_enabled msg))
    (cl:cons ':position_enabled (position_enabled msg))
    (cl:cons ':position_type_enabled (position_type_enabled msg))
    (cl:cons ':satellites_enabled (satellites_enabled msg))
    (cl:cons ':hdop_enabled (hdop_enabled msg))
    (cl:cons ':altitude_enabled (altitude_enabled msg))
    (cl:cons ':geoidal_separation_enabled (geoidal_separation_enabled msg))
    (cl:cons ':diff_age_enabled (diff_age_enabled msg))
    (cl:cons ':base_id_enabled (base_id_enabled msg))
    (cl:cons ':time (time msg))
    (cl:cons ':position (position msg))
    (cl:cons ':position_type (position_type msg))
    (cl:cons ':satellites (satellites msg))
    (cl:cons ':hdop (hdop msg))
    (cl:cons ':altitude (altitude msg))
    (cl:cons ':geoidal_separation (geoidal_separation msg))
    (cl:cons ':diff_age (diff_age msg))
    (cl:cons ':base_station_id (base_station_id msg))
))
