(cl:defpackage gps_nmea-msg
  (:use )
  (:export
   "<DATE_GPS>"
   "DATE_GPS"
   "<GPSCRT>"
   "GPSCRT"
   "<GPSGGA>"
   "GPSGGA"
   "<GPSGST>"
   "GPSGST"
   "<GPSGSV>"
   "GPSGSV"
   "<GPSLTN>"
   "GPSLTN"
   "<GPSPLANAR>"
   "GPSPLANAR"
   "<GPSRMC>"
   "GPSRMC"
   "<GPSSTRINGFIX>"
   "GPSSTRINGFIX"
   "<GPSVTG>"
   "GPSVTG"
   "<WGS84>"
   "WGS84"
  ))

