(cl:in-package gps_nmea-msg)
(cl:export '(HEADER-VAL
          HEADER
          LATENCY_ENABLED-VAL
          LATENCY_ENABLED
          LATENCY-VAL
          LATENCY
))