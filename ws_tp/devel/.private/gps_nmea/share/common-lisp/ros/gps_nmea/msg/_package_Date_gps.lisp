(cl:in-package gps_nmea-msg)
(cl:export '(HEADER-VAL
          HEADER
          DAY-VAL
          DAY
          MONTH-VAL
          MONTH
          YEAR-VAL
          YEAR
))