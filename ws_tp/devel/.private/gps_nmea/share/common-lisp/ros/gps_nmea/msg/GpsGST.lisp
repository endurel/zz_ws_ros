; Auto-generated. Do not edit!


(cl:in-package gps_nmea-msg)


;//! \htmlinclude GpsGST.msg.html

(cl:defclass <GpsGST> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (time_enabled
    :reader time_enabled
    :initarg :time_enabled
    :type cl:boolean
    :initform cl:nil)
   (stdev_range_enabled
    :reader stdev_range_enabled
    :initarg :stdev_range_enabled
    :type cl:boolean
    :initform cl:nil)
   (stdev_semimajor_enabled
    :reader stdev_semimajor_enabled
    :initarg :stdev_semimajor_enabled
    :type cl:boolean
    :initform cl:nil)
   (stdev_semiminor_enabled
    :reader stdev_semiminor_enabled
    :initarg :stdev_semiminor_enabled
    :type cl:boolean
    :initform cl:nil)
   (semimajor_orientation_enabled
    :reader semimajor_orientation_enabled
    :initarg :semimajor_orientation_enabled
    :type cl:boolean
    :initform cl:nil)
   (stdev_latitude_enabled
    :reader stdev_latitude_enabled
    :initarg :stdev_latitude_enabled
    :type cl:boolean
    :initform cl:nil)
   (stdev_longitude_enabled
    :reader stdev_longitude_enabled
    :initarg :stdev_longitude_enabled
    :type cl:boolean
    :initform cl:nil)
   (stdev_altitude_enabled
    :reader stdev_altitude_enabled
    :initarg :stdev_altitude_enabled
    :type cl:boolean
    :initform cl:nil)
   (source
    :reader source
    :initarg :source
    :type cl:fixnum
    :initform 0)
   (time
    :reader time
    :initarg :time
    :type cl:real
    :initform 0)
   (stdev_range
    :reader stdev_range
    :initarg :stdev_range
    :type cl:float
    :initform 0.0)
   (stdev_semimajor
    :reader stdev_semimajor
    :initarg :stdev_semimajor
    :type cl:float
    :initform 0.0)
   (stdev_semiminor
    :reader stdev_semiminor
    :initarg :stdev_semiminor
    :type cl:float
    :initform 0.0)
   (semimajor_orientation
    :reader semimajor_orientation
    :initarg :semimajor_orientation
    :type cl:float
    :initform 0.0)
   (stdev_latitude
    :reader stdev_latitude
    :initarg :stdev_latitude
    :type cl:float
    :initform 0.0)
   (stdev_longitude
    :reader stdev_longitude
    :initarg :stdev_longitude
    :type cl:float
    :initform 0.0)
   (stdev_altitude
    :reader stdev_altitude
    :initarg :stdev_altitude
    :type cl:float
    :initform 0.0))
)

(cl:defclass GpsGST (<GpsGST>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GpsGST>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GpsGST)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_nmea-msg:<GpsGST> is deprecated: use gps_nmea-msg:GpsGST instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:header-val is deprecated.  Use gps_nmea-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'time_enabled-val :lambda-list '(m))
(cl:defmethod time_enabled-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:time_enabled-val is deprecated.  Use gps_nmea-msg:time_enabled instead.")
  (time_enabled m))

(cl:ensure-generic-function 'stdev_range_enabled-val :lambda-list '(m))
(cl:defmethod stdev_range_enabled-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_range_enabled-val is deprecated.  Use gps_nmea-msg:stdev_range_enabled instead.")
  (stdev_range_enabled m))

(cl:ensure-generic-function 'stdev_semimajor_enabled-val :lambda-list '(m))
(cl:defmethod stdev_semimajor_enabled-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_semimajor_enabled-val is deprecated.  Use gps_nmea-msg:stdev_semimajor_enabled instead.")
  (stdev_semimajor_enabled m))

(cl:ensure-generic-function 'stdev_semiminor_enabled-val :lambda-list '(m))
(cl:defmethod stdev_semiminor_enabled-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_semiminor_enabled-val is deprecated.  Use gps_nmea-msg:stdev_semiminor_enabled instead.")
  (stdev_semiminor_enabled m))

(cl:ensure-generic-function 'semimajor_orientation_enabled-val :lambda-list '(m))
(cl:defmethod semimajor_orientation_enabled-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:semimajor_orientation_enabled-val is deprecated.  Use gps_nmea-msg:semimajor_orientation_enabled instead.")
  (semimajor_orientation_enabled m))

(cl:ensure-generic-function 'stdev_latitude_enabled-val :lambda-list '(m))
(cl:defmethod stdev_latitude_enabled-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_latitude_enabled-val is deprecated.  Use gps_nmea-msg:stdev_latitude_enabled instead.")
  (stdev_latitude_enabled m))

(cl:ensure-generic-function 'stdev_longitude_enabled-val :lambda-list '(m))
(cl:defmethod stdev_longitude_enabled-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_longitude_enabled-val is deprecated.  Use gps_nmea-msg:stdev_longitude_enabled instead.")
  (stdev_longitude_enabled m))

(cl:ensure-generic-function 'stdev_altitude_enabled-val :lambda-list '(m))
(cl:defmethod stdev_altitude_enabled-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_altitude_enabled-val is deprecated.  Use gps_nmea-msg:stdev_altitude_enabled instead.")
  (stdev_altitude_enabled m))

(cl:ensure-generic-function 'source-val :lambda-list '(m))
(cl:defmethod source-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:source-val is deprecated.  Use gps_nmea-msg:source instead.")
  (source m))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:time-val is deprecated.  Use gps_nmea-msg:time instead.")
  (time m))

(cl:ensure-generic-function 'stdev_range-val :lambda-list '(m))
(cl:defmethod stdev_range-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_range-val is deprecated.  Use gps_nmea-msg:stdev_range instead.")
  (stdev_range m))

(cl:ensure-generic-function 'stdev_semimajor-val :lambda-list '(m))
(cl:defmethod stdev_semimajor-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_semimajor-val is deprecated.  Use gps_nmea-msg:stdev_semimajor instead.")
  (stdev_semimajor m))

(cl:ensure-generic-function 'stdev_semiminor-val :lambda-list '(m))
(cl:defmethod stdev_semiminor-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_semiminor-val is deprecated.  Use gps_nmea-msg:stdev_semiminor instead.")
  (stdev_semiminor m))

(cl:ensure-generic-function 'semimajor_orientation-val :lambda-list '(m))
(cl:defmethod semimajor_orientation-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:semimajor_orientation-val is deprecated.  Use gps_nmea-msg:semimajor_orientation instead.")
  (semimajor_orientation m))

(cl:ensure-generic-function 'stdev_latitude-val :lambda-list '(m))
(cl:defmethod stdev_latitude-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_latitude-val is deprecated.  Use gps_nmea-msg:stdev_latitude instead.")
  (stdev_latitude m))

(cl:ensure-generic-function 'stdev_longitude-val :lambda-list '(m))
(cl:defmethod stdev_longitude-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_longitude-val is deprecated.  Use gps_nmea-msg:stdev_longitude instead.")
  (stdev_longitude m))

(cl:ensure-generic-function 'stdev_altitude-val :lambda-list '(m))
(cl:defmethod stdev_altitude-val ((m <GpsGST>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:stdev_altitude-val is deprecated.  Use gps_nmea-msg:stdev_altitude instead.")
  (stdev_altitude m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<GpsGST>)))
    "Constants for message type '<GpsGST>"
  '((:SATELLITE_SOURCE_GPS . 0)
    (:SATELLITE_SOURCE_GLONASS . 1)
    (:SATELLITE_SOURCE_SEVERAL . 2))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'GpsGST)))
    "Constants for message type 'GpsGST"
  '((:SATELLITE_SOURCE_GPS . 0)
    (:SATELLITE_SOURCE_GLONASS . 1)
    (:SATELLITE_SOURCE_SEVERAL . 2))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GpsGST>) ostream)
  "Serializes a message object of type '<GpsGST>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'time_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'stdev_range_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'stdev_semimajor_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'stdev_semiminor_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'semimajor_orientation_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'stdev_latitude_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'stdev_longitude_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'stdev_altitude_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'source)) ostream)
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'time)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'time) (cl:floor (cl:slot-value msg 'time)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'stdev_range))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'stdev_semimajor))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'stdev_semiminor))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'semimajor_orientation))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'stdev_latitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'stdev_longitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'stdev_altitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GpsGST>) istream)
  "Deserializes a message object of type '<GpsGST>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'time_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'stdev_range_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'stdev_semimajor_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'stdev_semiminor_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'semimajor_orientation_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'stdev_latitude_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'stdev_longitude_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'stdev_altitude_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'source)) (cl:read-byte istream))
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'time) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'stdev_range) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'stdev_semimajor) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'stdev_semiminor) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'semimajor_orientation) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'stdev_latitude) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'stdev_longitude) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'stdev_altitude) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GpsGST>)))
  "Returns string type for a message object of type '<GpsGST>"
  "gps_nmea/GpsGST")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GpsGST)))
  "Returns string type for a message object of type 'GpsGST"
  "gps_nmea/GpsGST")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GpsGST>)))
  "Returns md5sum for a message object of type '<GpsGST>"
  "46d580dc2c2da339f4753af61f94de20")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GpsGST)))
  "Returns md5sum for a message object of type 'GpsGST"
  "46d580dc2c2da339f4753af61f94de20")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GpsGST>)))
  "Returns full string definition for message of type '<GpsGST>"
  (cl:format cl:nil "Header header~%~%bool time_enabled~%bool stdev_range_enabled~%bool stdev_semimajor_enabled~%bool stdev_semiminor_enabled~%bool semimajor_orientation_enabled~%bool stdev_latitude_enabled~%bool stdev_longitude_enabled~%bool stdev_altitude_enabled~%~%# Source GPS Fix~%# 0 : Only GPS satellites are used~%# 1 : Only GLONASS satellites are used~%# 2 : Several constellations (GPS, SBAS, GLONASS) are used.~%uint8 SATELLITE_SOURCE_GPS = 0~%uint8 SATELLITE_SOURCE_GLONASS = 1~%uint8 SATELLITE_SOURCE_SEVERAL = 2~%uint8 source~%~%# Current UTC time of position~%time time~%# RMS value of standard deviation of range inputs (DGNSS corrections included), in meters~%float64 stdev_range~%# Standard deviation of semi-major axis of error ellipse, in meters~%float64 stdev_semimajor~%# Standard deviation of semi-minor axis of error ellipse, in meters~%float64 stdev_semiminor~%# Orientation of semi-major axis of error ellipse, in degrees from true North~%float64 semimajor_orientation~%# Standard deviation of latitude error, in meters~%float64 stdev_latitude~%# Standard deviation of longitude error, in meters~%float64 stdev_longitude~%# Standard deviation of altitude error, in meters~%float64 stdev_altitude~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GpsGST)))
  "Returns full string definition for message of type 'GpsGST"
  (cl:format cl:nil "Header header~%~%bool time_enabled~%bool stdev_range_enabled~%bool stdev_semimajor_enabled~%bool stdev_semiminor_enabled~%bool semimajor_orientation_enabled~%bool stdev_latitude_enabled~%bool stdev_longitude_enabled~%bool stdev_altitude_enabled~%~%# Source GPS Fix~%# 0 : Only GPS satellites are used~%# 1 : Only GLONASS satellites are used~%# 2 : Several constellations (GPS, SBAS, GLONASS) are used.~%uint8 SATELLITE_SOURCE_GPS = 0~%uint8 SATELLITE_SOURCE_GLONASS = 1~%uint8 SATELLITE_SOURCE_SEVERAL = 2~%uint8 source~%~%# Current UTC time of position~%time time~%# RMS value of standard deviation of range inputs (DGNSS corrections included), in meters~%float64 stdev_range~%# Standard deviation of semi-major axis of error ellipse, in meters~%float64 stdev_semimajor~%# Standard deviation of semi-minor axis of error ellipse, in meters~%float64 stdev_semiminor~%# Orientation of semi-major axis of error ellipse, in degrees from true North~%float64 semimajor_orientation~%# Standard deviation of latitude error, in meters~%float64 stdev_latitude~%# Standard deviation of longitude error, in meters~%float64 stdev_longitude~%# Standard deviation of altitude error, in meters~%float64 stdev_altitude~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GpsGST>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
     1
     1
     1
     1
     8
     8
     8
     8
     8
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GpsGST>))
  "Converts a ROS message object to a list"
  (cl:list 'GpsGST
    (cl:cons ':header (header msg))
    (cl:cons ':time_enabled (time_enabled msg))
    (cl:cons ':stdev_range_enabled (stdev_range_enabled msg))
    (cl:cons ':stdev_semimajor_enabled (stdev_semimajor_enabled msg))
    (cl:cons ':stdev_semiminor_enabled (stdev_semiminor_enabled msg))
    (cl:cons ':semimajor_orientation_enabled (semimajor_orientation_enabled msg))
    (cl:cons ':stdev_latitude_enabled (stdev_latitude_enabled msg))
    (cl:cons ':stdev_longitude_enabled (stdev_longitude_enabled msg))
    (cl:cons ':stdev_altitude_enabled (stdev_altitude_enabled msg))
    (cl:cons ':source (source msg))
    (cl:cons ':time (time msg))
    (cl:cons ':stdev_range (stdev_range msg))
    (cl:cons ':stdev_semimajor (stdev_semimajor msg))
    (cl:cons ':stdev_semiminor (stdev_semiminor msg))
    (cl:cons ':semimajor_orientation (semimajor_orientation msg))
    (cl:cons ':stdev_latitude (stdev_latitude msg))
    (cl:cons ':stdev_longitude (stdev_longitude msg))
    (cl:cons ':stdev_altitude (stdev_altitude msg))
))
