; Auto-generated. Do not edit!


(cl:in-package gps_nmea-msg)


;//! \htmlinclude GpsLTN.msg.html

(cl:defclass <GpsLTN> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (latency_enabled
    :reader latency_enabled
    :initarg :latency_enabled
    :type cl:boolean
    :initform cl:nil)
   (latency
    :reader latency
    :initarg :latency
    :type cl:real
    :initform 0))
)

(cl:defclass GpsLTN (<GpsLTN>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GpsLTN>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GpsLTN)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_nmea-msg:<GpsLTN> is deprecated: use gps_nmea-msg:GpsLTN instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GpsLTN>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:header-val is deprecated.  Use gps_nmea-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'latency_enabled-val :lambda-list '(m))
(cl:defmethod latency_enabled-val ((m <GpsLTN>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:latency_enabled-val is deprecated.  Use gps_nmea-msg:latency_enabled instead.")
  (latency_enabled m))

(cl:ensure-generic-function 'latency-val :lambda-list '(m))
(cl:defmethod latency-val ((m <GpsLTN>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:latency-val is deprecated.  Use gps_nmea-msg:latency instead.")
  (latency m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GpsLTN>) ostream)
  "Serializes a message object of type '<GpsLTN>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'latency_enabled) 1 0)) ostream)
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'latency)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'latency) (cl:floor (cl:slot-value msg 'latency)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GpsLTN>) istream)
  "Deserializes a message object of type '<GpsLTN>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'latency_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'latency) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GpsLTN>)))
  "Returns string type for a message object of type '<GpsLTN>"
  "gps_nmea/GpsLTN")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GpsLTN)))
  "Returns string type for a message object of type 'GpsLTN"
  "gps_nmea/GpsLTN")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GpsLTN>)))
  "Returns md5sum for a message object of type '<GpsLTN>"
  "18fa69ccf54af4a6a11e6712354a87c7")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GpsLTN)))
  "Returns md5sum for a message object of type 'GpsLTN"
  "18fa69ccf54af4a6a11e6712354a87c7")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GpsLTN>)))
  "Returns full string definition for message of type '<GpsLTN>"
  (cl:format cl:nil "~%Header header~%~%bool latency_enabled~%~%#Latency IS ORIGINALLY FEATURED in milliseconds~%duration latency~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GpsLTN)))
  "Returns full string definition for message of type 'GpsLTN"
  (cl:format cl:nil "~%Header header~%~%bool latency_enabled~%~%#Latency IS ORIGINALLY FEATURED in milliseconds~%duration latency~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GpsLTN>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GpsLTN>))
  "Converts a ROS message object to a list"
  (cl:list 'GpsLTN
    (cl:cons ':header (header msg))
    (cl:cons ':latency_enabled (latency_enabled msg))
    (cl:cons ':latency (latency msg))
))
