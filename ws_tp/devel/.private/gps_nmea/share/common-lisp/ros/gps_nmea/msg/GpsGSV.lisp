; Auto-generated. Do not edit!


(cl:in-package gps_nmea-msg)


;//! \htmlinclude GpsGSV.msg.html

(cl:defclass <GpsGSV> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (total_nb_messages_enabled
    :reader total_nb_messages_enabled
    :initarg :total_nb_messages_enabled
    :type cl:boolean
    :initform cl:nil)
   (message_number_enabled
    :reader message_number_enabled
    :initarg :message_number_enabled
    :type cl:boolean
    :initform cl:nil)
   (total_nb_satellites_in_view_enabled
    :reader total_nb_satellites_in_view_enabled
    :initarg :total_nb_satellites_in_view_enabled
    :type cl:boolean
    :initform cl:nil)
   (total_nb_messages
    :reader total_nb_messages
    :initarg :total_nb_messages
    :type cl:fixnum
    :initform 0)
   (message_number
    :reader message_number
    :initarg :message_number
    :type cl:fixnum
    :initform 0)
   (total_nb_satellites_in_view
    :reader total_nb_satellites_in_view
    :initarg :total_nb_satellites_in_view
    :type cl:fixnum
    :initform 0)
   (satellite_PRN_nb
    :reader satellite_PRN_nb
    :initarg :satellite_PRN_nb
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 4 :element-type 'cl:fixnum :initial-element 0))
   (elevation
    :reader elevation
    :initarg :elevation
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 4 :element-type 'cl:fixnum :initial-element 0))
   (azimuth
    :reader azimuth
    :initarg :azimuth
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 4 :element-type 'cl:fixnum :initial-element 0))
   (snr
    :reader snr
    :initarg :snr
    :type (cl:vector cl:fixnum)
   :initform (cl:make-array 4 :element-type 'cl:fixnum :initial-element 0)))
)

(cl:defclass GpsGSV (<GpsGSV>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GpsGSV>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GpsGSV)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_nmea-msg:<GpsGSV> is deprecated: use gps_nmea-msg:GpsGSV instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:header-val is deprecated.  Use gps_nmea-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'total_nb_messages_enabled-val :lambda-list '(m))
(cl:defmethod total_nb_messages_enabled-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:total_nb_messages_enabled-val is deprecated.  Use gps_nmea-msg:total_nb_messages_enabled instead.")
  (total_nb_messages_enabled m))

(cl:ensure-generic-function 'message_number_enabled-val :lambda-list '(m))
(cl:defmethod message_number_enabled-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:message_number_enabled-val is deprecated.  Use gps_nmea-msg:message_number_enabled instead.")
  (message_number_enabled m))

(cl:ensure-generic-function 'total_nb_satellites_in_view_enabled-val :lambda-list '(m))
(cl:defmethod total_nb_satellites_in_view_enabled-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:total_nb_satellites_in_view_enabled-val is deprecated.  Use gps_nmea-msg:total_nb_satellites_in_view_enabled instead.")
  (total_nb_satellites_in_view_enabled m))

(cl:ensure-generic-function 'total_nb_messages-val :lambda-list '(m))
(cl:defmethod total_nb_messages-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:total_nb_messages-val is deprecated.  Use gps_nmea-msg:total_nb_messages instead.")
  (total_nb_messages m))

(cl:ensure-generic-function 'message_number-val :lambda-list '(m))
(cl:defmethod message_number-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:message_number-val is deprecated.  Use gps_nmea-msg:message_number instead.")
  (message_number m))

(cl:ensure-generic-function 'total_nb_satellites_in_view-val :lambda-list '(m))
(cl:defmethod total_nb_satellites_in_view-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:total_nb_satellites_in_view-val is deprecated.  Use gps_nmea-msg:total_nb_satellites_in_view instead.")
  (total_nb_satellites_in_view m))

(cl:ensure-generic-function 'satellite_PRN_nb-val :lambda-list '(m))
(cl:defmethod satellite_PRN_nb-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:satellite_PRN_nb-val is deprecated.  Use gps_nmea-msg:satellite_PRN_nb instead.")
  (satellite_PRN_nb m))

(cl:ensure-generic-function 'elevation-val :lambda-list '(m))
(cl:defmethod elevation-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:elevation-val is deprecated.  Use gps_nmea-msg:elevation instead.")
  (elevation m))

(cl:ensure-generic-function 'azimuth-val :lambda-list '(m))
(cl:defmethod azimuth-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:azimuth-val is deprecated.  Use gps_nmea-msg:azimuth instead.")
  (azimuth m))

(cl:ensure-generic-function 'snr-val :lambda-list '(m))
(cl:defmethod snr-val ((m <GpsGSV>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:snr-val is deprecated.  Use gps_nmea-msg:snr instead.")
  (snr m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GpsGSV>) ostream)
  "Serializes a message object of type '<GpsGSV>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'total_nb_messages_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'message_number_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'total_nb_satellites_in_view_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'total_nb_messages)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'message_number)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'total_nb_satellites_in_view)) ostream)
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream))
   (cl:slot-value msg 'satellite_PRN_nb))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream))
   (cl:slot-value msg 'elevation))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream))
   (cl:slot-value msg 'azimuth))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream))
   (cl:slot-value msg 'snr))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GpsGSV>) istream)
  "Deserializes a message object of type '<GpsGSV>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'total_nb_messages_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'message_number_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'total_nb_satellites_in_view_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'total_nb_messages)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'message_number)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'total_nb_satellites_in_view)) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'satellite_PRN_nb) (cl:make-array 4))
  (cl:let ((vals (cl:slot-value msg 'satellite_PRN_nb)))
    (cl:dotimes (i 4)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream))))
  (cl:setf (cl:slot-value msg 'elevation) (cl:make-array 4))
  (cl:let ((vals (cl:slot-value msg 'elevation)))
    (cl:dotimes (i 4)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream))))
  (cl:setf (cl:slot-value msg 'azimuth) (cl:make-array 4))
  (cl:let ((vals (cl:slot-value msg 'azimuth)))
    (cl:dotimes (i 4)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream))))
  (cl:setf (cl:slot-value msg 'snr) (cl:make-array 4))
  (cl:let ((vals (cl:slot-value msg 'snr)))
    (cl:dotimes (i 4)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GpsGSV>)))
  "Returns string type for a message object of type '<GpsGSV>"
  "gps_nmea/GpsGSV")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GpsGSV)))
  "Returns string type for a message object of type 'GpsGSV"
  "gps_nmea/GpsGSV")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GpsGSV>)))
  "Returns md5sum for a message object of type '<GpsGSV>"
  "cdc5e950abcc9b46a1d184c241ffb895")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GpsGSV)))
  "Returns md5sum for a message object of type 'GpsGSV"
  "cdc5e950abcc9b46a1d184c241ffb895")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GpsGSV>)))
  "Returns full string definition for message of type '<GpsGSV>"
  (cl:format cl:nil "Header header~%~%bool total_nb_messages_enabled~%bool message_number_enabled~%bool total_nb_satellites_in_view_enabled~%~%~%# Total number of messages of this type in this cycle~%uint8 total_nb_messages~%# Message number~%uint8 message_number~%# Total number of SVs in view~%uint8 total_nb_satellites_in_view~%~%uint8[4] satellite_PRN_nb~%uint8[4] elevation~%uint8[4] azimuth ~%uint8[4] snr~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GpsGSV)))
  "Returns full string definition for message of type 'GpsGSV"
  (cl:format cl:nil "Header header~%~%bool total_nb_messages_enabled~%bool message_number_enabled~%bool total_nb_satellites_in_view_enabled~%~%~%# Total number of messages of this type in this cycle~%uint8 total_nb_messages~%# Message number~%uint8 message_number~%# Total number of SVs in view~%uint8 total_nb_satellites_in_view~%~%uint8[4] satellite_PRN_nb~%uint8[4] elevation~%uint8[4] azimuth ~%uint8[4] snr~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GpsGSV>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
     1
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'satellite_PRN_nb) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'elevation) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'azimuth) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'snr) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GpsGSV>))
  "Converts a ROS message object to a list"
  (cl:list 'GpsGSV
    (cl:cons ':header (header msg))
    (cl:cons ':total_nb_messages_enabled (total_nb_messages_enabled msg))
    (cl:cons ':message_number_enabled (message_number_enabled msg))
    (cl:cons ':total_nb_satellites_in_view_enabled (total_nb_satellites_in_view_enabled msg))
    (cl:cons ':total_nb_messages (total_nb_messages msg))
    (cl:cons ':message_number (message_number msg))
    (cl:cons ':total_nb_satellites_in_view (total_nb_satellites_in_view msg))
    (cl:cons ':satellite_PRN_nb (satellite_PRN_nb msg))
    (cl:cons ':elevation (elevation msg))
    (cl:cons ':azimuth (azimuth msg))
    (cl:cons ':snr (snr msg))
))
