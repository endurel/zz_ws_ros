; Auto-generated. Do not edit!


(cl:in-package gps_nmea-msg)


;//! \htmlinclude GpsCRT.msg.html

(cl:defclass <GpsCRT> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (position_mode_enabled
    :reader position_mode_enabled
    :initarg :position_mode_enabled
    :type cl:boolean
    :initform cl:nil)
   (count_enabled
    :reader count_enabled
    :initarg :count_enabled
    :type cl:boolean
    :initform cl:nil)
   (time_enabled
    :reader time_enabled
    :initarg :time_enabled
    :type cl:boolean
    :initform cl:nil)
   (ecef_x_enabled
    :reader ecef_x_enabled
    :initarg :ecef_x_enabled
    :type cl:boolean
    :initform cl:nil)
   (ecef_y_enabled
    :reader ecef_y_enabled
    :initarg :ecef_y_enabled
    :type cl:boolean
    :initform cl:nil)
   (ecef_z_enabled
    :reader ecef_z_enabled
    :initarg :ecef_z_enabled
    :type cl:boolean
    :initform cl:nil)
   (clock_offset_enabled
    :reader clock_offset_enabled
    :initarg :clock_offset_enabled
    :type cl:boolean
    :initform cl:nil)
   (v_x_enabled
    :reader v_x_enabled
    :initarg :v_x_enabled
    :type cl:boolean
    :initform cl:nil)
   (v_y_enabled
    :reader v_y_enabled
    :initarg :v_y_enabled
    :type cl:boolean
    :initform cl:nil)
   (v_z_enabled
    :reader v_z_enabled
    :initarg :v_z_enabled
    :type cl:boolean
    :initform cl:nil)
   (clock_drift_enabled
    :reader clock_drift_enabled
    :initarg :clock_drift_enabled
    :type cl:boolean
    :initform cl:nil)
   (pdop_enabled
    :reader pdop_enabled
    :initarg :pdop_enabled
    :type cl:boolean
    :initform cl:nil)
   (hdop_enabled
    :reader hdop_enabled
    :initarg :hdop_enabled
    :type cl:boolean
    :initform cl:nil)
   (vdop_enabled
    :reader vdop_enabled
    :initarg :vdop_enabled
    :type cl:boolean
    :initform cl:nil)
   (tdop_enabled
    :reader tdop_enabled
    :initarg :tdop_enabled
    :type cl:boolean
    :initform cl:nil)
   (firmware_enabled
    :reader firmware_enabled
    :initarg :firmware_enabled
    :type cl:boolean
    :initform cl:nil)
   (position_mode
    :reader position_mode
    :initarg :position_mode
    :type cl:fixnum
    :initform 0)
   (count
    :reader count
    :initarg :count
    :type cl:fixnum
    :initform 0)
   (time
    :reader time
    :initarg :time
    :type cl:real
    :initform 0)
   (ecef_x
    :reader ecef_x
    :initarg :ecef_x
    :type cl:float
    :initform 0.0)
   (ecef_y
    :reader ecef_y
    :initarg :ecef_y
    :type cl:float
    :initform 0.0)
   (ecef_z
    :reader ecef_z
    :initarg :ecef_z
    :type cl:float
    :initform 0.0)
   (clock_offset
    :reader clock_offset
    :initarg :clock_offset
    :type cl:float
    :initform 0.0)
   (v_x
    :reader v_x
    :initarg :v_x
    :type cl:float
    :initform 0.0)
   (v_y
    :reader v_y
    :initarg :v_y
    :type cl:float
    :initform 0.0)
   (v_z
    :reader v_z
    :initarg :v_z
    :type cl:float
    :initform 0.0)
   (clock_drift
    :reader clock_drift
    :initarg :clock_drift
    :type cl:float
    :initform 0.0)
   (pdop
    :reader pdop
    :initarg :pdop
    :type cl:float
    :initform 0.0)
   (hdop
    :reader hdop
    :initarg :hdop
    :type cl:float
    :initform 0.0)
   (vdop
    :reader vdop
    :initarg :vdop
    :type cl:float
    :initform 0.0)
   (tdop
    :reader tdop
    :initarg :tdop
    :type cl:float
    :initform 0.0)
   (firmware
    :reader firmware
    :initarg :firmware
    :type (cl:vector cl:integer)
   :initform (cl:make-array 5 :element-type 'cl:integer :initial-element 0)))
)

(cl:defclass GpsCRT (<GpsCRT>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GpsCRT>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GpsCRT)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_nmea-msg:<GpsCRT> is deprecated: use gps_nmea-msg:GpsCRT instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:header-val is deprecated.  Use gps_nmea-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'position_mode_enabled-val :lambda-list '(m))
(cl:defmethod position_mode_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position_mode_enabled-val is deprecated.  Use gps_nmea-msg:position_mode_enabled instead.")
  (position_mode_enabled m))

(cl:ensure-generic-function 'count_enabled-val :lambda-list '(m))
(cl:defmethod count_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:count_enabled-val is deprecated.  Use gps_nmea-msg:count_enabled instead.")
  (count_enabled m))

(cl:ensure-generic-function 'time_enabled-val :lambda-list '(m))
(cl:defmethod time_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:time_enabled-val is deprecated.  Use gps_nmea-msg:time_enabled instead.")
  (time_enabled m))

(cl:ensure-generic-function 'ecef_x_enabled-val :lambda-list '(m))
(cl:defmethod ecef_x_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:ecef_x_enabled-val is deprecated.  Use gps_nmea-msg:ecef_x_enabled instead.")
  (ecef_x_enabled m))

(cl:ensure-generic-function 'ecef_y_enabled-val :lambda-list '(m))
(cl:defmethod ecef_y_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:ecef_y_enabled-val is deprecated.  Use gps_nmea-msg:ecef_y_enabled instead.")
  (ecef_y_enabled m))

(cl:ensure-generic-function 'ecef_z_enabled-val :lambda-list '(m))
(cl:defmethod ecef_z_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:ecef_z_enabled-val is deprecated.  Use gps_nmea-msg:ecef_z_enabled instead.")
  (ecef_z_enabled m))

(cl:ensure-generic-function 'clock_offset_enabled-val :lambda-list '(m))
(cl:defmethod clock_offset_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:clock_offset_enabled-val is deprecated.  Use gps_nmea-msg:clock_offset_enabled instead.")
  (clock_offset_enabled m))

(cl:ensure-generic-function 'v_x_enabled-val :lambda-list '(m))
(cl:defmethod v_x_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:v_x_enabled-val is deprecated.  Use gps_nmea-msg:v_x_enabled instead.")
  (v_x_enabled m))

(cl:ensure-generic-function 'v_y_enabled-val :lambda-list '(m))
(cl:defmethod v_y_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:v_y_enabled-val is deprecated.  Use gps_nmea-msg:v_y_enabled instead.")
  (v_y_enabled m))

(cl:ensure-generic-function 'v_z_enabled-val :lambda-list '(m))
(cl:defmethod v_z_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:v_z_enabled-val is deprecated.  Use gps_nmea-msg:v_z_enabled instead.")
  (v_z_enabled m))

(cl:ensure-generic-function 'clock_drift_enabled-val :lambda-list '(m))
(cl:defmethod clock_drift_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:clock_drift_enabled-val is deprecated.  Use gps_nmea-msg:clock_drift_enabled instead.")
  (clock_drift_enabled m))

(cl:ensure-generic-function 'pdop_enabled-val :lambda-list '(m))
(cl:defmethod pdop_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:pdop_enabled-val is deprecated.  Use gps_nmea-msg:pdop_enabled instead.")
  (pdop_enabled m))

(cl:ensure-generic-function 'hdop_enabled-val :lambda-list '(m))
(cl:defmethod hdop_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:hdop_enabled-val is deprecated.  Use gps_nmea-msg:hdop_enabled instead.")
  (hdop_enabled m))

(cl:ensure-generic-function 'vdop_enabled-val :lambda-list '(m))
(cl:defmethod vdop_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:vdop_enabled-val is deprecated.  Use gps_nmea-msg:vdop_enabled instead.")
  (vdop_enabled m))

(cl:ensure-generic-function 'tdop_enabled-val :lambda-list '(m))
(cl:defmethod tdop_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:tdop_enabled-val is deprecated.  Use gps_nmea-msg:tdop_enabled instead.")
  (tdop_enabled m))

(cl:ensure-generic-function 'firmware_enabled-val :lambda-list '(m))
(cl:defmethod firmware_enabled-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:firmware_enabled-val is deprecated.  Use gps_nmea-msg:firmware_enabled instead.")
  (firmware_enabled m))

(cl:ensure-generic-function 'position_mode-val :lambda-list '(m))
(cl:defmethod position_mode-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position_mode-val is deprecated.  Use gps_nmea-msg:position_mode instead.")
  (position_mode m))

(cl:ensure-generic-function 'count-val :lambda-list '(m))
(cl:defmethod count-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:count-val is deprecated.  Use gps_nmea-msg:count instead.")
  (count m))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:time-val is deprecated.  Use gps_nmea-msg:time instead.")
  (time m))

(cl:ensure-generic-function 'ecef_x-val :lambda-list '(m))
(cl:defmethod ecef_x-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:ecef_x-val is deprecated.  Use gps_nmea-msg:ecef_x instead.")
  (ecef_x m))

(cl:ensure-generic-function 'ecef_y-val :lambda-list '(m))
(cl:defmethod ecef_y-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:ecef_y-val is deprecated.  Use gps_nmea-msg:ecef_y instead.")
  (ecef_y m))

(cl:ensure-generic-function 'ecef_z-val :lambda-list '(m))
(cl:defmethod ecef_z-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:ecef_z-val is deprecated.  Use gps_nmea-msg:ecef_z instead.")
  (ecef_z m))

(cl:ensure-generic-function 'clock_offset-val :lambda-list '(m))
(cl:defmethod clock_offset-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:clock_offset-val is deprecated.  Use gps_nmea-msg:clock_offset instead.")
  (clock_offset m))

(cl:ensure-generic-function 'v_x-val :lambda-list '(m))
(cl:defmethod v_x-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:v_x-val is deprecated.  Use gps_nmea-msg:v_x instead.")
  (v_x m))

(cl:ensure-generic-function 'v_y-val :lambda-list '(m))
(cl:defmethod v_y-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:v_y-val is deprecated.  Use gps_nmea-msg:v_y instead.")
  (v_y m))

(cl:ensure-generic-function 'v_z-val :lambda-list '(m))
(cl:defmethod v_z-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:v_z-val is deprecated.  Use gps_nmea-msg:v_z instead.")
  (v_z m))

(cl:ensure-generic-function 'clock_drift-val :lambda-list '(m))
(cl:defmethod clock_drift-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:clock_drift-val is deprecated.  Use gps_nmea-msg:clock_drift instead.")
  (clock_drift m))

(cl:ensure-generic-function 'pdop-val :lambda-list '(m))
(cl:defmethod pdop-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:pdop-val is deprecated.  Use gps_nmea-msg:pdop instead.")
  (pdop m))

(cl:ensure-generic-function 'hdop-val :lambda-list '(m))
(cl:defmethod hdop-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:hdop-val is deprecated.  Use gps_nmea-msg:hdop instead.")
  (hdop m))

(cl:ensure-generic-function 'vdop-val :lambda-list '(m))
(cl:defmethod vdop-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:vdop-val is deprecated.  Use gps_nmea-msg:vdop instead.")
  (vdop m))

(cl:ensure-generic-function 'tdop-val :lambda-list '(m))
(cl:defmethod tdop-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:tdop-val is deprecated.  Use gps_nmea-msg:tdop instead.")
  (tdop m))

(cl:ensure-generic-function 'firmware-val :lambda-list '(m))
(cl:defmethod firmware-val ((m <GpsCRT>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:firmware-val is deprecated.  Use gps_nmea-msg:firmware instead.")
  (firmware m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<GpsCRT>)))
    "Constants for message type '<GpsCRT>"
  '((:POSITION_MODE_AUTONOMOUS . 0)
    (:POSITION_MODE_RTCM . 1)
    (:POSITION_MODE_RTKFLOAT . 2)
    (:POSITION_MODE_RTKFIXED . 3))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'GpsCRT)))
    "Constants for message type 'GpsCRT"
  '((:POSITION_MODE_AUTONOMOUS . 0)
    (:POSITION_MODE_RTCM . 1)
    (:POSITION_MODE_RTKFLOAT . 2)
    (:POSITION_MODE_RTKFIXED . 3))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GpsCRT>) ostream)
  "Serializes a message object of type '<GpsCRT>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'position_mode_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'count_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'time_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ecef_x_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ecef_y_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ecef_z_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'clock_offset_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'v_x_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'v_y_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'v_z_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'clock_drift_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'pdop_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'hdop_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'vdop_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'tdop_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'firmware_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'position_mode)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'count)) ostream)
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'time)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'time) (cl:floor (cl:slot-value msg 'time)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'ecef_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'ecef_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'ecef_z))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'clock_offset))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'v_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'v_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'v_z))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'clock_drift))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'pdop))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'hdop))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'vdop))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'tdop))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:write-byte (cl:ldb (cl:byte 8 0) ele) ostream))
   (cl:slot-value msg 'firmware))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GpsCRT>) istream)
  "Deserializes a message object of type '<GpsCRT>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'position_mode_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'count_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'time_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'ecef_x_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'ecef_y_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'ecef_z_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'clock_offset_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'v_x_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'v_y_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'v_z_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'clock_drift_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'pdop_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'hdop_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'vdop_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'tdop_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'firmware_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'position_mode)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'count)) (cl:read-byte istream))
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'time) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ecef_x) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ecef_y) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ecef_z) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'clock_offset) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'v_x) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'v_y) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'v_z) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'clock_drift) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pdop) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'hdop) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'vdop) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'tdop) (roslisp-utils:decode-double-float-bits bits)))
  (cl:setf (cl:slot-value msg 'firmware) (cl:make-array 5))
  (cl:let ((vals (cl:slot-value msg 'firmware)))
    (cl:dotimes (i 5)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:aref vals i)) (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GpsCRT>)))
  "Returns string type for a message object of type '<GpsCRT>"
  "gps_nmea/GpsCRT")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GpsCRT)))
  "Returns string type for a message object of type 'GpsCRT"
  "gps_nmea/GpsCRT")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GpsCRT>)))
  "Returns md5sum for a message object of type '<GpsCRT>"
  "dab59c118ba3827d16dc59c7d383a215")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GpsCRT)))
  "Returns md5sum for a message object of type 'GpsCRT"
  "dab59c118ba3827d16dc59c7d383a215")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GpsCRT>)))
  "Returns full string definition for message of type '<GpsCRT>"
  (cl:format cl:nil "Header header~%~%bool position_mode_enabled~%bool count_enabled~%bool time_enabled~%bool ecef_x_enabled~%bool ecef_y_enabled~%bool ecef_z_enabled~%bool clock_offset_enabled~%bool v_x_enabled~%bool v_y_enabled~%bool v_z_enabled~%bool clock_drift_enabled~%bool pdop_enabled~%bool hdop_enabled~%bool vdop_enabled~%bool tdop_enabled~%bool firmware_enabled~%~%# Position Mode~%# 0 : Autonomous~%# 1 : RTCM or SBAS differential~%# 2 : RTK float~%# 3 : RTK fixed~%uint8 POSITION_MODE_AUTONOMOUS = 0~%uint8 POSITION_MODE_RTCM = 1~%uint8 POSITION_MODE_RTKFLOAT = 2~%uint8 POSITION_MODE_RTKFIXED = 3~%uint8 position_mode~%~%# Count of SVs used in position computation~%uint8 count~%# UTC time~%time time~%# ECEF X coordinate, in meters~%float64 ecef_x~%# ECEF Y coordinate, in meters~%float64 ecef_y~%# ECEF Z coordinate, in meters~%float64 ecef_z~%# Receiver clock offset, in meters~%float64 clock_offset~%# Velocity vector, X component, in m/s~%float64 v_x~%# Velocity vector, Y component, in m/s~%float64 v_y~%# Velocity vector, Z component, in m/s~%float64 v_z~%# Receiver clock drift, in m/s~%float64 clock_drift~%# Positional dilution of precision~%float64 pdop~%# Horizontal dilution of precision~%float64 hdop~%# Vertical dilution of precision~%float64 vdop~%# Time dilution of precision~%float64 tdop~%# Firmware version ID (4 letters)~%char[5] firmware~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GpsCRT)))
  "Returns full string definition for message of type 'GpsCRT"
  (cl:format cl:nil "Header header~%~%bool position_mode_enabled~%bool count_enabled~%bool time_enabled~%bool ecef_x_enabled~%bool ecef_y_enabled~%bool ecef_z_enabled~%bool clock_offset_enabled~%bool v_x_enabled~%bool v_y_enabled~%bool v_z_enabled~%bool clock_drift_enabled~%bool pdop_enabled~%bool hdop_enabled~%bool vdop_enabled~%bool tdop_enabled~%bool firmware_enabled~%~%# Position Mode~%# 0 : Autonomous~%# 1 : RTCM or SBAS differential~%# 2 : RTK float~%# 3 : RTK fixed~%uint8 POSITION_MODE_AUTONOMOUS = 0~%uint8 POSITION_MODE_RTCM = 1~%uint8 POSITION_MODE_RTKFLOAT = 2~%uint8 POSITION_MODE_RTKFIXED = 3~%uint8 position_mode~%~%# Count of SVs used in position computation~%uint8 count~%# UTC time~%time time~%# ECEF X coordinate, in meters~%float64 ecef_x~%# ECEF Y coordinate, in meters~%float64 ecef_y~%# ECEF Z coordinate, in meters~%float64 ecef_z~%# Receiver clock offset, in meters~%float64 clock_offset~%# Velocity vector, X component, in m/s~%float64 v_x~%# Velocity vector, Y component, in m/s~%float64 v_y~%# Velocity vector, Z component, in m/s~%float64 v_z~%# Receiver clock drift, in m/s~%float64 clock_drift~%# Positional dilution of precision~%float64 pdop~%# Horizontal dilution of precision~%float64 hdop~%# Vertical dilution of precision~%float64 vdop~%# Time dilution of precision~%float64 tdop~%# Firmware version ID (4 letters)~%char[5] firmware~%~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GpsCRT>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     1
     8
     8
     8
     8
     8
     8
     8
     8
     8
     8
     8
     8
     8
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'firmware) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 1)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GpsCRT>))
  "Converts a ROS message object to a list"
  (cl:list 'GpsCRT
    (cl:cons ':header (header msg))
    (cl:cons ':position_mode_enabled (position_mode_enabled msg))
    (cl:cons ':count_enabled (count_enabled msg))
    (cl:cons ':time_enabled (time_enabled msg))
    (cl:cons ':ecef_x_enabled (ecef_x_enabled msg))
    (cl:cons ':ecef_y_enabled (ecef_y_enabled msg))
    (cl:cons ':ecef_z_enabled (ecef_z_enabled msg))
    (cl:cons ':clock_offset_enabled (clock_offset_enabled msg))
    (cl:cons ':v_x_enabled (v_x_enabled msg))
    (cl:cons ':v_y_enabled (v_y_enabled msg))
    (cl:cons ':v_z_enabled (v_z_enabled msg))
    (cl:cons ':clock_drift_enabled (clock_drift_enabled msg))
    (cl:cons ':pdop_enabled (pdop_enabled msg))
    (cl:cons ':hdop_enabled (hdop_enabled msg))
    (cl:cons ':vdop_enabled (vdop_enabled msg))
    (cl:cons ':tdop_enabled (tdop_enabled msg))
    (cl:cons ':firmware_enabled (firmware_enabled msg))
    (cl:cons ':position_mode (position_mode msg))
    (cl:cons ':count (count msg))
    (cl:cons ':time (time msg))
    (cl:cons ':ecef_x (ecef_x msg))
    (cl:cons ':ecef_y (ecef_y msg))
    (cl:cons ':ecef_z (ecef_z msg))
    (cl:cons ':clock_offset (clock_offset msg))
    (cl:cons ':v_x (v_x msg))
    (cl:cons ':v_y (v_y msg))
    (cl:cons ':v_z (v_z msg))
    (cl:cons ':clock_drift (clock_drift msg))
    (cl:cons ':pdop (pdop msg))
    (cl:cons ':hdop (hdop msg))
    (cl:cons ':vdop (vdop msg))
    (cl:cons ':tdop (tdop msg))
    (cl:cons ':firmware (firmware msg))
))
