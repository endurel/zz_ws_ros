; Auto-generated. Do not edit!


(cl:in-package gps_nmea-msg)


;//! \htmlinclude GpsRMC.msg.html

(cl:defclass <GpsRMC> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (time_enabled
    :reader time_enabled
    :initarg :time_enabled
    :type cl:boolean
    :initform cl:nil)
   (position_valid_enabled
    :reader position_valid_enabled
    :initarg :position_valid_enabled
    :type cl:boolean
    :initform cl:nil)
   (position_enabled
    :reader position_enabled
    :initarg :position_enabled
    :type cl:boolean
    :initform cl:nil)
   (speedOverGround_enabled
    :reader speedOverGround_enabled
    :initarg :speedOverGround_enabled
    :type cl:boolean
    :initform cl:nil)
   (trackAngle_enabled
    :reader trackAngle_enabled
    :initarg :trackAngle_enabled
    :type cl:boolean
    :initform cl:nil)
   (date_enabled
    :reader date_enabled
    :initarg :date_enabled
    :type cl:boolean
    :initform cl:nil)
   (magneticVariation_enabled
    :reader magneticVariation_enabled
    :initarg :magneticVariation_enabled
    :type cl:boolean
    :initform cl:nil)
   (mode_enabled
    :reader mode_enabled
    :initarg :mode_enabled
    :type cl:boolean
    :initform cl:nil)
   (time
    :reader time
    :initarg :time
    :type cl:real
    :initform 0)
   (position_valid
    :reader position_valid
    :initarg :position_valid
    :type cl:fixnum
    :initform 0)
   (position
    :reader position
    :initarg :position
    :type gps_nmea-msg:WGS84
    :initform (cl:make-instance 'gps_nmea-msg:WGS84))
   (speedOverGround
    :reader speedOverGround
    :initarg :speedOverGround
    :type cl:float
    :initform 0.0)
   (trackAngle
    :reader trackAngle
    :initarg :trackAngle
    :type cl:float
    :initform 0.0)
   (date
    :reader date
    :initarg :date
    :type gps_nmea-msg:Date_gps
    :initform (cl:make-instance 'gps_nmea-msg:Date_gps))
   (magneticVariation
    :reader magneticVariation
    :initarg :magneticVariation
    :type cl:float
    :initform 0.0)
   (magnetic_var_direction
    :reader magnetic_var_direction
    :initarg :magnetic_var_direction
    :type cl:fixnum
    :initform 0)
   (mode
    :reader mode
    :initarg :mode
    :type cl:fixnum
    :initform 0))
)

(cl:defclass GpsRMC (<GpsRMC>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GpsRMC>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GpsRMC)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_nmea-msg:<GpsRMC> is deprecated: use gps_nmea-msg:GpsRMC instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:header-val is deprecated.  Use gps_nmea-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'time_enabled-val :lambda-list '(m))
(cl:defmethod time_enabled-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:time_enabled-val is deprecated.  Use gps_nmea-msg:time_enabled instead.")
  (time_enabled m))

(cl:ensure-generic-function 'position_valid_enabled-val :lambda-list '(m))
(cl:defmethod position_valid_enabled-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position_valid_enabled-val is deprecated.  Use gps_nmea-msg:position_valid_enabled instead.")
  (position_valid_enabled m))

(cl:ensure-generic-function 'position_enabled-val :lambda-list '(m))
(cl:defmethod position_enabled-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position_enabled-val is deprecated.  Use gps_nmea-msg:position_enabled instead.")
  (position_enabled m))

(cl:ensure-generic-function 'speedOverGround_enabled-val :lambda-list '(m))
(cl:defmethod speedOverGround_enabled-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:speedOverGround_enabled-val is deprecated.  Use gps_nmea-msg:speedOverGround_enabled instead.")
  (speedOverGround_enabled m))

(cl:ensure-generic-function 'trackAngle_enabled-val :lambda-list '(m))
(cl:defmethod trackAngle_enabled-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:trackAngle_enabled-val is deprecated.  Use gps_nmea-msg:trackAngle_enabled instead.")
  (trackAngle_enabled m))

(cl:ensure-generic-function 'date_enabled-val :lambda-list '(m))
(cl:defmethod date_enabled-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:date_enabled-val is deprecated.  Use gps_nmea-msg:date_enabled instead.")
  (date_enabled m))

(cl:ensure-generic-function 'magneticVariation_enabled-val :lambda-list '(m))
(cl:defmethod magneticVariation_enabled-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:magneticVariation_enabled-val is deprecated.  Use gps_nmea-msg:magneticVariation_enabled instead.")
  (magneticVariation_enabled m))

(cl:ensure-generic-function 'mode_enabled-val :lambda-list '(m))
(cl:defmethod mode_enabled-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:mode_enabled-val is deprecated.  Use gps_nmea-msg:mode_enabled instead.")
  (mode_enabled m))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:time-val is deprecated.  Use gps_nmea-msg:time instead.")
  (time m))

(cl:ensure-generic-function 'position_valid-val :lambda-list '(m))
(cl:defmethod position_valid-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position_valid-val is deprecated.  Use gps_nmea-msg:position_valid instead.")
  (position_valid m))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:position-val is deprecated.  Use gps_nmea-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'speedOverGround-val :lambda-list '(m))
(cl:defmethod speedOverGround-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:speedOverGround-val is deprecated.  Use gps_nmea-msg:speedOverGround instead.")
  (speedOverGround m))

(cl:ensure-generic-function 'trackAngle-val :lambda-list '(m))
(cl:defmethod trackAngle-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:trackAngle-val is deprecated.  Use gps_nmea-msg:trackAngle instead.")
  (trackAngle m))

(cl:ensure-generic-function 'date-val :lambda-list '(m))
(cl:defmethod date-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:date-val is deprecated.  Use gps_nmea-msg:date instead.")
  (date m))

(cl:ensure-generic-function 'magneticVariation-val :lambda-list '(m))
(cl:defmethod magneticVariation-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:magneticVariation-val is deprecated.  Use gps_nmea-msg:magneticVariation instead.")
  (magneticVariation m))

(cl:ensure-generic-function 'magnetic_var_direction-val :lambda-list '(m))
(cl:defmethod magnetic_var_direction-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:magnetic_var_direction-val is deprecated.  Use gps_nmea-msg:magnetic_var_direction instead.")
  (magnetic_var_direction m))

(cl:ensure-generic-function 'mode-val :lambda-list '(m))
(cl:defmethod mode-val ((m <GpsRMC>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:mode-val is deprecated.  Use gps_nmea-msg:mode instead.")
  (mode m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<GpsRMC>)))
    "Constants for message type '<GpsRMC>"
  '((:POSITION_VALID_VOID . 0)
    (:POSITION_VALID_ACTIVE . 1)
    (:MAGNETIC_VARIATION_EAST . 0)
    (:MAGNETIC_VARIATION_WEST . 1)
    (:GPS_ENABLED_MODE_INVALID . 0)
    (:GPS_ENABLED_MODE_AUTONOMOUS . 1)
    (:GPS_ENABLED_MODE_DIFFERENTIAL . 2)
    (:GPS_ENABLED_MODE_ESTIMATED . 3))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'GpsRMC)))
    "Constants for message type 'GpsRMC"
  '((:POSITION_VALID_VOID . 0)
    (:POSITION_VALID_ACTIVE . 1)
    (:MAGNETIC_VARIATION_EAST . 0)
    (:MAGNETIC_VARIATION_WEST . 1)
    (:GPS_ENABLED_MODE_INVALID . 0)
    (:GPS_ENABLED_MODE_AUTONOMOUS . 1)
    (:GPS_ENABLED_MODE_DIFFERENTIAL . 2)
    (:GPS_ENABLED_MODE_ESTIMATED . 3))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GpsRMC>) ostream)
  "Serializes a message object of type '<GpsRMC>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'time_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'position_valid_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'position_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'speedOverGround_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'trackAngle_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'date_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'magneticVariation_enabled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'mode_enabled) 1 0)) ostream)
  (cl:let ((__sec (cl:floor (cl:slot-value msg 'time)))
        (__nsec (cl:round (cl:* 1e9 (cl:- (cl:slot-value msg 'time) (cl:floor (cl:slot-value msg 'time)))))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __sec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 0) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __nsec) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __nsec) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'position_valid)) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'position) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'speedOverGround))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'trackAngle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'date) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'magneticVariation))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'magnetic_var_direction)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'mode)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GpsRMC>) istream)
  "Deserializes a message object of type '<GpsRMC>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'time_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'position_valid_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'position_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'speedOverGround_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'trackAngle_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'date_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'magneticVariation_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'mode_enabled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__sec 0) (__nsec 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __sec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 0) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __nsec) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __nsec) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'time) (cl:+ (cl:coerce __sec 'cl:double-float) (cl:/ __nsec 1e9))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'position_valid)) (cl:read-byte istream))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'position) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'speedOverGround) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'trackAngle) (roslisp-utils:decode-double-float-bits bits)))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'date) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'magneticVariation) (roslisp-utils:decode-double-float-bits bits)))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'magnetic_var_direction)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'mode)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GpsRMC>)))
  "Returns string type for a message object of type '<GpsRMC>"
  "gps_nmea/GpsRMC")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GpsRMC)))
  "Returns string type for a message object of type 'GpsRMC"
  "gps_nmea/GpsRMC")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GpsRMC>)))
  "Returns md5sum for a message object of type '<GpsRMC>"
  "ecae7bc96c99e205f05bee9f6e358c53")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GpsRMC)))
  "Returns md5sum for a message object of type 'GpsRMC"
  "ecae7bc96c99e205f05bee9f6e358c53")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GpsRMC>)))
  "Returns full string definition for message of type '<GpsRMC>"
  (cl:format cl:nil "Header header~%~%bool time_enabled~%bool position_valid_enabled~%bool position_enabled~%bool speedOverGround_enabled~%bool trackAngle_enabled~%bool date_enabled~%bool magneticVariation_enabled~%bool mode_enabled~%~%# Current UTC time~%time time~%~%# Position Status~%# 0 : NAV Receiver warning~%# 1 : Valid position~%uint8 POSITION_VALID_VOID = 0~%uint8 POSITION_VALID_ACTIVE = 1~%uint8 position_valid~%~%# WGS84 latitude and longitude~%WGS84 position~%# Vitesse curviligne (m/s)~%float64 speedOverGround~%# Cap en degres (0deg=360deg is north)~%float64 trackAngle~%~%# Current UTC date~%Date_gps date~%# Magnetic variation~%float64  magneticVariation~%~%# Magnetic variation magnetic_var_direction~%uint8 MAGNETIC_VARIATION_EAST = 0~%uint8 MAGNETIC_VARIATION_WEST = 1~%uint8 magnetic_var_direction~%~%# Position mode~%# 0 : Data not valid~%# 1 : Autonomous mode~%# 2 : Differential mode~%# 3 : Estimated mode~%uint8 GPS_ENABLED_MODE_INVALID = 0~%uint8 GPS_ENABLED_MODE_AUTONOMOUS = 1~%uint8 GPS_ENABLED_MODE_DIFFERENTIAL = 2~%uint8 GPS_ENABLED_MODE_ESTIMATED = 3~%uint8 mode~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: gps_nmea/WGS84~%# Latitude (degrees). Positive is north of equator; negative is south. ~%# +: N, -: S --- unit: degrees~%float64 latitude~%~%# Longitude (degrees). Positive is east of prime meridian, negative west.~%# +: E, -: W --- unit: degrees, origin: Greenwich~%float64 longitude~%================================================================================~%MSG: gps_nmea/Date_gps~%Header header~%# 1-31~%uint8 day~%# 1-12~%uint8 month~%# 00+ (2000)~%uint8 year~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GpsRMC)))
  "Returns full string definition for message of type 'GpsRMC"
  (cl:format cl:nil "Header header~%~%bool time_enabled~%bool position_valid_enabled~%bool position_enabled~%bool speedOverGround_enabled~%bool trackAngle_enabled~%bool date_enabled~%bool magneticVariation_enabled~%bool mode_enabled~%~%# Current UTC time~%time time~%~%# Position Status~%# 0 : NAV Receiver warning~%# 1 : Valid position~%uint8 POSITION_VALID_VOID = 0~%uint8 POSITION_VALID_ACTIVE = 1~%uint8 position_valid~%~%# WGS84 latitude and longitude~%WGS84 position~%# Vitesse curviligne (m/s)~%float64 speedOverGround~%# Cap en degres (0deg=360deg is north)~%float64 trackAngle~%~%# Current UTC date~%Date_gps date~%# Magnetic variation~%float64  magneticVariation~%~%# Magnetic variation magnetic_var_direction~%uint8 MAGNETIC_VARIATION_EAST = 0~%uint8 MAGNETIC_VARIATION_WEST = 1~%uint8 magnetic_var_direction~%~%# Position mode~%# 0 : Data not valid~%# 1 : Autonomous mode~%# 2 : Differential mode~%# 3 : Estimated mode~%uint8 GPS_ENABLED_MODE_INVALID = 0~%uint8 GPS_ENABLED_MODE_AUTONOMOUS = 1~%uint8 GPS_ENABLED_MODE_DIFFERENTIAL = 2~%uint8 GPS_ENABLED_MODE_ESTIMATED = 3~%uint8 mode~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: gps_nmea/WGS84~%# Latitude (degrees). Positive is north of equator; negative is south. ~%# +: N, -: S --- unit: degrees~%float64 latitude~%~%# Longitude (degrees). Positive is east of prime meridian, negative west.~%# +: E, -: W --- unit: degrees, origin: Greenwich~%float64 longitude~%================================================================================~%MSG: gps_nmea/Date_gps~%Header header~%# 1-31~%uint8 day~%# 1-12~%uint8 month~%# 00+ (2000)~%uint8 year~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GpsRMC>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
     1
     1
     1
     1
     1
     8
     1
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'position))
     8
     8
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'date))
     8
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GpsRMC>))
  "Converts a ROS message object to a list"
  (cl:list 'GpsRMC
    (cl:cons ':header (header msg))
    (cl:cons ':time_enabled (time_enabled msg))
    (cl:cons ':position_valid_enabled (position_valid_enabled msg))
    (cl:cons ':position_enabled (position_enabled msg))
    (cl:cons ':speedOverGround_enabled (speedOverGround_enabled msg))
    (cl:cons ':trackAngle_enabled (trackAngle_enabled msg))
    (cl:cons ':date_enabled (date_enabled msg))
    (cl:cons ':magneticVariation_enabled (magneticVariation_enabled msg))
    (cl:cons ':mode_enabled (mode_enabled msg))
    (cl:cons ':time (time msg))
    (cl:cons ':position_valid (position_valid msg))
    (cl:cons ':position (position msg))
    (cl:cons ':speedOverGround (speedOverGround msg))
    (cl:cons ':trackAngle (trackAngle msg))
    (cl:cons ':date (date msg))
    (cl:cons ':magneticVariation (magneticVariation msg))
    (cl:cons ':magnetic_var_direction (magnetic_var_direction msg))
    (cl:cons ':mode (mode msg))
))
