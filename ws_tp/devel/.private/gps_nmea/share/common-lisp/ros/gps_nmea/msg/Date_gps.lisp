; Auto-generated. Do not edit!


(cl:in-package gps_nmea-msg)


;//! \htmlinclude Date_gps.msg.html

(cl:defclass <Date_gps> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (day
    :reader day
    :initarg :day
    :type cl:fixnum
    :initform 0)
   (month
    :reader month
    :initarg :month
    :type cl:fixnum
    :initform 0)
   (year
    :reader year
    :initarg :year
    :type cl:fixnum
    :initform 0))
)

(cl:defclass Date_gps (<Date_gps>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Date_gps>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Date_gps)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_nmea-msg:<Date_gps> is deprecated: use gps_nmea-msg:Date_gps instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <Date_gps>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:header-val is deprecated.  Use gps_nmea-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'day-val :lambda-list '(m))
(cl:defmethod day-val ((m <Date_gps>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:day-val is deprecated.  Use gps_nmea-msg:day instead.")
  (day m))

(cl:ensure-generic-function 'month-val :lambda-list '(m))
(cl:defmethod month-val ((m <Date_gps>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:month-val is deprecated.  Use gps_nmea-msg:month instead.")
  (month m))

(cl:ensure-generic-function 'year-val :lambda-list '(m))
(cl:defmethod year-val ((m <Date_gps>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:year-val is deprecated.  Use gps_nmea-msg:year instead.")
  (year m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Date_gps>) ostream)
  "Serializes a message object of type '<Date_gps>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'day)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'month)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'year)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Date_gps>) istream)
  "Deserializes a message object of type '<Date_gps>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'day)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'month)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'year)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Date_gps>)))
  "Returns string type for a message object of type '<Date_gps>"
  "gps_nmea/Date_gps")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Date_gps)))
  "Returns string type for a message object of type 'Date_gps"
  "gps_nmea/Date_gps")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Date_gps>)))
  "Returns md5sum for a message object of type '<Date_gps>"
  "3914d0ca25a0745148b2fe857f569ec8")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Date_gps)))
  "Returns md5sum for a message object of type 'Date_gps"
  "3914d0ca25a0745148b2fe857f569ec8")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Date_gps>)))
  "Returns full string definition for message of type '<Date_gps>"
  (cl:format cl:nil "Header header~%# 1-31~%uint8 day~%# 1-12~%uint8 month~%# 00+ (2000)~%uint8 year~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Date_gps)))
  "Returns full string definition for message of type 'Date_gps"
  (cl:format cl:nil "Header header~%# 1-31~%uint8 day~%# 1-12~%uint8 month~%# 00+ (2000)~%uint8 year~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Date_gps>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Date_gps>))
  "Converts a ROS message object to a list"
  (cl:list 'Date_gps
    (cl:cons ':header (header msg))
    (cl:cons ':day (day msg))
    (cl:cons ':month (month msg))
    (cl:cons ':year (year msg))
))
