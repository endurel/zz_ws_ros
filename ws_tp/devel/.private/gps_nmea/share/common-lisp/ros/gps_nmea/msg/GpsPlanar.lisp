; Auto-generated. Do not edit!


(cl:in-package gps_nmea-msg)


;//! \htmlinclude GpsPlanar.msg.html

(cl:defclass <GpsPlanar> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (transform_type
    :reader transform_type
    :initarg :transform_type
    :type cl:fixnum
    :initform 0)
   (east
    :reader east
    :initarg :east
    :type cl:float
    :initform 0.0)
   (north
    :reader north
    :initarg :north
    :type cl:float
    :initform 0.0)
   (up
    :reader up
    :initarg :up
    :type cl:float
    :initform 0.0))
)

(cl:defclass GpsPlanar (<GpsPlanar>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <GpsPlanar>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'GpsPlanar)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gps_nmea-msg:<GpsPlanar> is deprecated: use gps_nmea-msg:GpsPlanar instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <GpsPlanar>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:header-val is deprecated.  Use gps_nmea-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'transform_type-val :lambda-list '(m))
(cl:defmethod transform_type-val ((m <GpsPlanar>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:transform_type-val is deprecated.  Use gps_nmea-msg:transform_type instead.")
  (transform_type m))

(cl:ensure-generic-function 'east-val :lambda-list '(m))
(cl:defmethod east-val ((m <GpsPlanar>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:east-val is deprecated.  Use gps_nmea-msg:east instead.")
  (east m))

(cl:ensure-generic-function 'north-val :lambda-list '(m))
(cl:defmethod north-val ((m <GpsPlanar>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:north-val is deprecated.  Use gps_nmea-msg:north instead.")
  (north m))

(cl:ensure-generic-function 'up-val :lambda-list '(m))
(cl:defmethod up-val ((m <GpsPlanar>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gps_nmea-msg:up-val is deprecated.  Use gps_nmea-msg:up instead.")
  (up m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<GpsPlanar>)))
    "Constants for message type '<GpsPlanar>"
  '((:TRANSFORM_TYPE_INVALID . 0)
    (:TRANSFORM_TYPE_LOCAL_PLANAR . 1)
    (:TRANSFORM_TYPE_UTM . 2)
    (:TRANSFORM_TYPE_LAMBERT2E . 3))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'GpsPlanar)))
    "Constants for message type 'GpsPlanar"
  '((:TRANSFORM_TYPE_INVALID . 0)
    (:TRANSFORM_TYPE_LOCAL_PLANAR . 1)
    (:TRANSFORM_TYPE_UTM . 2)
    (:TRANSFORM_TYPE_LAMBERT2E . 3))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <GpsPlanar>) ostream)
  "Serializes a message object of type '<GpsPlanar>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'transform_type)) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'east))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'north))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'up))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <GpsPlanar>) istream)
  "Deserializes a message object of type '<GpsPlanar>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'transform_type)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'east) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'north) (roslisp-utils:decode-double-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'up) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<GpsPlanar>)))
  "Returns string type for a message object of type '<GpsPlanar>"
  "gps_nmea/GpsPlanar")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'GpsPlanar)))
  "Returns string type for a message object of type 'GpsPlanar"
  "gps_nmea/GpsPlanar")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<GpsPlanar>)))
  "Returns md5sum for a message object of type '<GpsPlanar>"
  "0c081549f0015bc62875d9599de89471")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'GpsPlanar)))
  "Returns md5sum for a message object of type 'GpsPlanar"
  "0c081549f0015bc62875d9599de89471")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<GpsPlanar>)))
  "Returns full string definition for message of type '<GpsPlanar>"
  (cl:format cl:nil "Header header~%~%# Position type~%# 0 : Transform type not available or invalid~%# 1 : Local geographic planar conversion~%# 2 : Local UTM planar conversion~%uint8 TRANSFORM_TYPE_INVALID = 0~%uint8 TRANSFORM_TYPE_LOCAL_PLANAR = 1~%uint8 TRANSFORM_TYPE_UTM = 2~%uint8 TRANSFORM_TYPE_LAMBERT2E = 3~%uint8 transform_type              ~%~%float64 east~%float64 north~%float64 up~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'GpsPlanar)))
  "Returns full string definition for message of type 'GpsPlanar"
  (cl:format cl:nil "Header header~%~%# Position type~%# 0 : Transform type not available or invalid~%# 1 : Local geographic planar conversion~%# 2 : Local UTM planar conversion~%uint8 TRANSFORM_TYPE_INVALID = 0~%uint8 TRANSFORM_TYPE_LOCAL_PLANAR = 1~%uint8 TRANSFORM_TYPE_UTM = 2~%uint8 TRANSFORM_TYPE_LAMBERT2E = 3~%uint8 transform_type              ~%~%float64 east~%float64 north~%float64 up~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <GpsPlanar>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     8
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <GpsPlanar>))
  "Converts a ROS message object to a list"
  (cl:list 'GpsPlanar
    (cl:cons ':header (header msg))
    (cl:cons ':transform_type (transform_type msg))
    (cl:cons ':east (east msg))
    (cl:cons ':north (north msg))
    (cl:cons ':up (up msg))
))
