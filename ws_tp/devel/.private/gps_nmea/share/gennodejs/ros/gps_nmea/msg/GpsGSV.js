// Auto-generated. Do not edit!

// (in-package gps_nmea.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class GpsGSV {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.total_nb_messages_enabled = null;
      this.message_number_enabled = null;
      this.total_nb_satellites_in_view_enabled = null;
      this.total_nb_messages = null;
      this.message_number = null;
      this.total_nb_satellites_in_view = null;
      this.satellite_PRN_nb = null;
      this.elevation = null;
      this.azimuth = null;
      this.snr = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('total_nb_messages_enabled')) {
        this.total_nb_messages_enabled = initObj.total_nb_messages_enabled
      }
      else {
        this.total_nb_messages_enabled = false;
      }
      if (initObj.hasOwnProperty('message_number_enabled')) {
        this.message_number_enabled = initObj.message_number_enabled
      }
      else {
        this.message_number_enabled = false;
      }
      if (initObj.hasOwnProperty('total_nb_satellites_in_view_enabled')) {
        this.total_nb_satellites_in_view_enabled = initObj.total_nb_satellites_in_view_enabled
      }
      else {
        this.total_nb_satellites_in_view_enabled = false;
      }
      if (initObj.hasOwnProperty('total_nb_messages')) {
        this.total_nb_messages = initObj.total_nb_messages
      }
      else {
        this.total_nb_messages = 0;
      }
      if (initObj.hasOwnProperty('message_number')) {
        this.message_number = initObj.message_number
      }
      else {
        this.message_number = 0;
      }
      if (initObj.hasOwnProperty('total_nb_satellites_in_view')) {
        this.total_nb_satellites_in_view = initObj.total_nb_satellites_in_view
      }
      else {
        this.total_nb_satellites_in_view = 0;
      }
      if (initObj.hasOwnProperty('satellite_PRN_nb')) {
        this.satellite_PRN_nb = initObj.satellite_PRN_nb
      }
      else {
        this.satellite_PRN_nb = new Array(4).fill(0);
      }
      if (initObj.hasOwnProperty('elevation')) {
        this.elevation = initObj.elevation
      }
      else {
        this.elevation = new Array(4).fill(0);
      }
      if (initObj.hasOwnProperty('azimuth')) {
        this.azimuth = initObj.azimuth
      }
      else {
        this.azimuth = new Array(4).fill(0);
      }
      if (initObj.hasOwnProperty('snr')) {
        this.snr = initObj.snr
      }
      else {
        this.snr = new Array(4).fill(0);
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GpsGSV
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [total_nb_messages_enabled]
    bufferOffset = _serializer.bool(obj.total_nb_messages_enabled, buffer, bufferOffset);
    // Serialize message field [message_number_enabled]
    bufferOffset = _serializer.bool(obj.message_number_enabled, buffer, bufferOffset);
    // Serialize message field [total_nb_satellites_in_view_enabled]
    bufferOffset = _serializer.bool(obj.total_nb_satellites_in_view_enabled, buffer, bufferOffset);
    // Serialize message field [total_nb_messages]
    bufferOffset = _serializer.uint8(obj.total_nb_messages, buffer, bufferOffset);
    // Serialize message field [message_number]
    bufferOffset = _serializer.uint8(obj.message_number, buffer, bufferOffset);
    // Serialize message field [total_nb_satellites_in_view]
    bufferOffset = _serializer.uint8(obj.total_nb_satellites_in_view, buffer, bufferOffset);
    // Check that the constant length array field [satellite_PRN_nb] has the right length
    if (obj.satellite_PRN_nb.length !== 4) {
      throw new Error('Unable to serialize array field satellite_PRN_nb - length must be 4')
    }
    // Serialize message field [satellite_PRN_nb]
    bufferOffset = _arraySerializer.uint8(obj.satellite_PRN_nb, buffer, bufferOffset, 4);
    // Check that the constant length array field [elevation] has the right length
    if (obj.elevation.length !== 4) {
      throw new Error('Unable to serialize array field elevation - length must be 4')
    }
    // Serialize message field [elevation]
    bufferOffset = _arraySerializer.uint8(obj.elevation, buffer, bufferOffset, 4);
    // Check that the constant length array field [azimuth] has the right length
    if (obj.azimuth.length !== 4) {
      throw new Error('Unable to serialize array field azimuth - length must be 4')
    }
    // Serialize message field [azimuth]
    bufferOffset = _arraySerializer.uint8(obj.azimuth, buffer, bufferOffset, 4);
    // Check that the constant length array field [snr] has the right length
    if (obj.snr.length !== 4) {
      throw new Error('Unable to serialize array field snr - length must be 4')
    }
    // Serialize message field [snr]
    bufferOffset = _arraySerializer.uint8(obj.snr, buffer, bufferOffset, 4);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GpsGSV
    let len;
    let data = new GpsGSV(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [total_nb_messages_enabled]
    data.total_nb_messages_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [message_number_enabled]
    data.message_number_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [total_nb_satellites_in_view_enabled]
    data.total_nb_satellites_in_view_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [total_nb_messages]
    data.total_nb_messages = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [message_number]
    data.message_number = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [total_nb_satellites_in_view]
    data.total_nb_satellites_in_view = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [satellite_PRN_nb]
    data.satellite_PRN_nb = _arrayDeserializer.uint8(buffer, bufferOffset, 4)
    // Deserialize message field [elevation]
    data.elevation = _arrayDeserializer.uint8(buffer, bufferOffset, 4)
    // Deserialize message field [azimuth]
    data.azimuth = _arrayDeserializer.uint8(buffer, bufferOffset, 4)
    // Deserialize message field [snr]
    data.snr = _arrayDeserializer.uint8(buffer, bufferOffset, 4)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 22;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gps_nmea/GpsGSV';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'cdc5e950abcc9b46a1d184c241ffb895';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    bool total_nb_messages_enabled
    bool message_number_enabled
    bool total_nb_satellites_in_view_enabled
    
    
    # Total number of messages of this type in this cycle
    uint8 total_nb_messages
    # Message number
    uint8 message_number
    # Total number of SVs in view
    uint8 total_nb_satellites_in_view
    
    uint8[4] satellite_PRN_nb
    uint8[4] elevation
    uint8[4] azimuth 
    uint8[4] snr
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GpsGSV(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.total_nb_messages_enabled !== undefined) {
      resolved.total_nb_messages_enabled = msg.total_nb_messages_enabled;
    }
    else {
      resolved.total_nb_messages_enabled = false
    }

    if (msg.message_number_enabled !== undefined) {
      resolved.message_number_enabled = msg.message_number_enabled;
    }
    else {
      resolved.message_number_enabled = false
    }

    if (msg.total_nb_satellites_in_view_enabled !== undefined) {
      resolved.total_nb_satellites_in_view_enabled = msg.total_nb_satellites_in_view_enabled;
    }
    else {
      resolved.total_nb_satellites_in_view_enabled = false
    }

    if (msg.total_nb_messages !== undefined) {
      resolved.total_nb_messages = msg.total_nb_messages;
    }
    else {
      resolved.total_nb_messages = 0
    }

    if (msg.message_number !== undefined) {
      resolved.message_number = msg.message_number;
    }
    else {
      resolved.message_number = 0
    }

    if (msg.total_nb_satellites_in_view !== undefined) {
      resolved.total_nb_satellites_in_view = msg.total_nb_satellites_in_view;
    }
    else {
      resolved.total_nb_satellites_in_view = 0
    }

    if (msg.satellite_PRN_nb !== undefined) {
      resolved.satellite_PRN_nb = msg.satellite_PRN_nb;
    }
    else {
      resolved.satellite_PRN_nb = new Array(4).fill(0)
    }

    if (msg.elevation !== undefined) {
      resolved.elevation = msg.elevation;
    }
    else {
      resolved.elevation = new Array(4).fill(0)
    }

    if (msg.azimuth !== undefined) {
      resolved.azimuth = msg.azimuth;
    }
    else {
      resolved.azimuth = new Array(4).fill(0)
    }

    if (msg.snr !== undefined) {
      resolved.snr = msg.snr;
    }
    else {
      resolved.snr = new Array(4).fill(0)
    }

    return resolved;
    }
};

module.exports = GpsGSV;
