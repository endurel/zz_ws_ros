// Auto-generated. Do not edit!

// (in-package gps_nmea.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let WGS84 = require('./WGS84.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class GpsGGA {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.time_enabled = null;
      this.position_enabled = null;
      this.position_type_enabled = null;
      this.satellites_enabled = null;
      this.hdop_enabled = null;
      this.altitude_enabled = null;
      this.geoidal_separation_enabled = null;
      this.diff_age_enabled = null;
      this.base_id_enabled = null;
      this.time = null;
      this.position = null;
      this.position_type = null;
      this.satellites = null;
      this.hdop = null;
      this.altitude = null;
      this.geoidal_separation = null;
      this.diff_age = null;
      this.base_station_id = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('time_enabled')) {
        this.time_enabled = initObj.time_enabled
      }
      else {
        this.time_enabled = false;
      }
      if (initObj.hasOwnProperty('position_enabled')) {
        this.position_enabled = initObj.position_enabled
      }
      else {
        this.position_enabled = false;
      }
      if (initObj.hasOwnProperty('position_type_enabled')) {
        this.position_type_enabled = initObj.position_type_enabled
      }
      else {
        this.position_type_enabled = false;
      }
      if (initObj.hasOwnProperty('satellites_enabled')) {
        this.satellites_enabled = initObj.satellites_enabled
      }
      else {
        this.satellites_enabled = false;
      }
      if (initObj.hasOwnProperty('hdop_enabled')) {
        this.hdop_enabled = initObj.hdop_enabled
      }
      else {
        this.hdop_enabled = false;
      }
      if (initObj.hasOwnProperty('altitude_enabled')) {
        this.altitude_enabled = initObj.altitude_enabled
      }
      else {
        this.altitude_enabled = false;
      }
      if (initObj.hasOwnProperty('geoidal_separation_enabled')) {
        this.geoidal_separation_enabled = initObj.geoidal_separation_enabled
      }
      else {
        this.geoidal_separation_enabled = false;
      }
      if (initObj.hasOwnProperty('diff_age_enabled')) {
        this.diff_age_enabled = initObj.diff_age_enabled
      }
      else {
        this.diff_age_enabled = false;
      }
      if (initObj.hasOwnProperty('base_id_enabled')) {
        this.base_id_enabled = initObj.base_id_enabled
      }
      else {
        this.base_id_enabled = false;
      }
      if (initObj.hasOwnProperty('time')) {
        this.time = initObj.time
      }
      else {
        this.time = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('position')) {
        this.position = initObj.position
      }
      else {
        this.position = new WGS84();
      }
      if (initObj.hasOwnProperty('position_type')) {
        this.position_type = initObj.position_type
      }
      else {
        this.position_type = 0;
      }
      if (initObj.hasOwnProperty('satellites')) {
        this.satellites = initObj.satellites
      }
      else {
        this.satellites = 0;
      }
      if (initObj.hasOwnProperty('hdop')) {
        this.hdop = initObj.hdop
      }
      else {
        this.hdop = 0.0;
      }
      if (initObj.hasOwnProperty('altitude')) {
        this.altitude = initObj.altitude
      }
      else {
        this.altitude = 0.0;
      }
      if (initObj.hasOwnProperty('geoidal_separation')) {
        this.geoidal_separation = initObj.geoidal_separation
      }
      else {
        this.geoidal_separation = 0.0;
      }
      if (initObj.hasOwnProperty('diff_age')) {
        this.diff_age = initObj.diff_age
      }
      else {
        this.diff_age = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('base_station_id')) {
        this.base_station_id = initObj.base_station_id
      }
      else {
        this.base_station_id = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GpsGGA
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [time_enabled]
    bufferOffset = _serializer.bool(obj.time_enabled, buffer, bufferOffset);
    // Serialize message field [position_enabled]
    bufferOffset = _serializer.bool(obj.position_enabled, buffer, bufferOffset);
    // Serialize message field [position_type_enabled]
    bufferOffset = _serializer.bool(obj.position_type_enabled, buffer, bufferOffset);
    // Serialize message field [satellites_enabled]
    bufferOffset = _serializer.bool(obj.satellites_enabled, buffer, bufferOffset);
    // Serialize message field [hdop_enabled]
    bufferOffset = _serializer.bool(obj.hdop_enabled, buffer, bufferOffset);
    // Serialize message field [altitude_enabled]
    bufferOffset = _serializer.bool(obj.altitude_enabled, buffer, bufferOffset);
    // Serialize message field [geoidal_separation_enabled]
    bufferOffset = _serializer.bool(obj.geoidal_separation_enabled, buffer, bufferOffset);
    // Serialize message field [diff_age_enabled]
    bufferOffset = _serializer.bool(obj.diff_age_enabled, buffer, bufferOffset);
    // Serialize message field [base_id_enabled]
    bufferOffset = _serializer.bool(obj.base_id_enabled, buffer, bufferOffset);
    // Serialize message field [time]
    bufferOffset = _serializer.time(obj.time, buffer, bufferOffset);
    // Serialize message field [position]
    bufferOffset = WGS84.serialize(obj.position, buffer, bufferOffset);
    // Serialize message field [position_type]
    bufferOffset = _serializer.uint8(obj.position_type, buffer, bufferOffset);
    // Serialize message field [satellites]
    bufferOffset = _serializer.uint8(obj.satellites, buffer, bufferOffset);
    // Serialize message field [hdop]
    bufferOffset = _serializer.float64(obj.hdop, buffer, bufferOffset);
    // Serialize message field [altitude]
    bufferOffset = _serializer.float64(obj.altitude, buffer, bufferOffset);
    // Serialize message field [geoidal_separation]
    bufferOffset = _serializer.float64(obj.geoidal_separation, buffer, bufferOffset);
    // Serialize message field [diff_age]
    bufferOffset = _serializer.duration(obj.diff_age, buffer, bufferOffset);
    // Serialize message field [base_station_id]
    bufferOffset = _serializer.uint8(obj.base_station_id, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GpsGGA
    let len;
    let data = new GpsGGA(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [time_enabled]
    data.time_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [position_enabled]
    data.position_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [position_type_enabled]
    data.position_type_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [satellites_enabled]
    data.satellites_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [hdop_enabled]
    data.hdop_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [altitude_enabled]
    data.altitude_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [geoidal_separation_enabled]
    data.geoidal_separation_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [diff_age_enabled]
    data.diff_age_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [base_id_enabled]
    data.base_id_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [time]
    data.time = _deserializer.time(buffer, bufferOffset);
    // Deserialize message field [position]
    data.position = WGS84.deserialize(buffer, bufferOffset);
    // Deserialize message field [position_type]
    data.position_type = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [satellites]
    data.satellites = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [hdop]
    data.hdop = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [altitude]
    data.altitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [geoidal_separation]
    data.geoidal_separation = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [diff_age]
    data.diff_age = _deserializer.duration(buffer, bufferOffset);
    // Deserialize message field [base_station_id]
    data.base_station_id = _deserializer.uint8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 68;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gps_nmea/GpsGGA';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '18c21ea63ef156a3d80f5cebf18705d1';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    bool time_enabled
    bool position_enabled
    bool position_type_enabled
    bool satellites_enabled
    bool hdop_enabled
    bool altitude_enabled
    bool geoidal_separation_enabled
    bool diff_age_enabled
    bool base_id_enabled
    
    # Current UTC time
    time time
    # WGS84 latitude and longitude
    WGS84 position  
    
    # Position type
    # 0 : Position not available or invalid
    # 1 : Autonomous position
    # 2 : RTCM Differential SBAS Differential
    # 4 : RTK fixed
    # 5 : RTK float
    
    uint8 POSITION_TYPE_INVALID = 0
    uint8 POSITION_TYPE_AUTONOMOUS = 1
    uint8 POSITION_TYPE_DIFFERENTIAL = 2
    uint8 POSITION_TYPE_RTKFIXED = 4
    uint8 POSITION_TYPE_RTKFLOAT = 5
    uint8 position_type              
    
    # Number of GNSS Satellites being used in the position computation
    uint8 satellites  
    # Horizontal dilution of precision
    float64 hdop 
    # Altitude, in meters, above mean seal level
    float64 altitude
    # Geoidal separation in meters
    float64 geoidal_separation 
    # Age of differential corrections, in seconds
    duration diff_age
    # Base station ID (RTCM only)
    uint8 base_station_id
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: gps_nmea/WGS84
    # Latitude (degrees). Positive is north of equator; negative is south. 
    # +: N, -: S --- unit: degrees
    float64 latitude
    
    # Longitude (degrees). Positive is east of prime meridian, negative west.
    # +: E, -: W --- unit: degrees, origin: Greenwich
    float64 longitude
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GpsGGA(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.time_enabled !== undefined) {
      resolved.time_enabled = msg.time_enabled;
    }
    else {
      resolved.time_enabled = false
    }

    if (msg.position_enabled !== undefined) {
      resolved.position_enabled = msg.position_enabled;
    }
    else {
      resolved.position_enabled = false
    }

    if (msg.position_type_enabled !== undefined) {
      resolved.position_type_enabled = msg.position_type_enabled;
    }
    else {
      resolved.position_type_enabled = false
    }

    if (msg.satellites_enabled !== undefined) {
      resolved.satellites_enabled = msg.satellites_enabled;
    }
    else {
      resolved.satellites_enabled = false
    }

    if (msg.hdop_enabled !== undefined) {
      resolved.hdop_enabled = msg.hdop_enabled;
    }
    else {
      resolved.hdop_enabled = false
    }

    if (msg.altitude_enabled !== undefined) {
      resolved.altitude_enabled = msg.altitude_enabled;
    }
    else {
      resolved.altitude_enabled = false
    }

    if (msg.geoidal_separation_enabled !== undefined) {
      resolved.geoidal_separation_enabled = msg.geoidal_separation_enabled;
    }
    else {
      resolved.geoidal_separation_enabled = false
    }

    if (msg.diff_age_enabled !== undefined) {
      resolved.diff_age_enabled = msg.diff_age_enabled;
    }
    else {
      resolved.diff_age_enabled = false
    }

    if (msg.base_id_enabled !== undefined) {
      resolved.base_id_enabled = msg.base_id_enabled;
    }
    else {
      resolved.base_id_enabled = false
    }

    if (msg.time !== undefined) {
      resolved.time = msg.time;
    }
    else {
      resolved.time = {secs: 0, nsecs: 0}
    }

    if (msg.position !== undefined) {
      resolved.position = WGS84.Resolve(msg.position)
    }
    else {
      resolved.position = new WGS84()
    }

    if (msg.position_type !== undefined) {
      resolved.position_type = msg.position_type;
    }
    else {
      resolved.position_type = 0
    }

    if (msg.satellites !== undefined) {
      resolved.satellites = msg.satellites;
    }
    else {
      resolved.satellites = 0
    }

    if (msg.hdop !== undefined) {
      resolved.hdop = msg.hdop;
    }
    else {
      resolved.hdop = 0.0
    }

    if (msg.altitude !== undefined) {
      resolved.altitude = msg.altitude;
    }
    else {
      resolved.altitude = 0.0
    }

    if (msg.geoidal_separation !== undefined) {
      resolved.geoidal_separation = msg.geoidal_separation;
    }
    else {
      resolved.geoidal_separation = 0.0
    }

    if (msg.diff_age !== undefined) {
      resolved.diff_age = msg.diff_age;
    }
    else {
      resolved.diff_age = {secs: 0, nsecs: 0}
    }

    if (msg.base_station_id !== undefined) {
      resolved.base_station_id = msg.base_station_id;
    }
    else {
      resolved.base_station_id = 0
    }

    return resolved;
    }
};

// Constants for message
GpsGGA.Constants = {
  POSITION_TYPE_INVALID: 0,
  POSITION_TYPE_AUTONOMOUS: 1,
  POSITION_TYPE_DIFFERENTIAL: 2,
  POSITION_TYPE_RTKFIXED: 4,
  POSITION_TYPE_RTKFLOAT: 5,
}

module.exports = GpsGGA;
