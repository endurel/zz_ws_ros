// Auto-generated. Do not edit!

// (in-package gps_nmea.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let WGS84 = require('./WGS84.js');
let Date_gps = require('./Date_gps.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class GpsRMC {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.time_enabled = null;
      this.position_valid_enabled = null;
      this.position_enabled = null;
      this.speedOverGround_enabled = null;
      this.trackAngle_enabled = null;
      this.date_enabled = null;
      this.magneticVariation_enabled = null;
      this.mode_enabled = null;
      this.time = null;
      this.position_valid = null;
      this.position = null;
      this.speedOverGround = null;
      this.trackAngle = null;
      this.date = null;
      this.magneticVariation = null;
      this.magnetic_var_direction = null;
      this.mode = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('time_enabled')) {
        this.time_enabled = initObj.time_enabled
      }
      else {
        this.time_enabled = false;
      }
      if (initObj.hasOwnProperty('position_valid_enabled')) {
        this.position_valid_enabled = initObj.position_valid_enabled
      }
      else {
        this.position_valid_enabled = false;
      }
      if (initObj.hasOwnProperty('position_enabled')) {
        this.position_enabled = initObj.position_enabled
      }
      else {
        this.position_enabled = false;
      }
      if (initObj.hasOwnProperty('speedOverGround_enabled')) {
        this.speedOverGround_enabled = initObj.speedOverGround_enabled
      }
      else {
        this.speedOverGround_enabled = false;
      }
      if (initObj.hasOwnProperty('trackAngle_enabled')) {
        this.trackAngle_enabled = initObj.trackAngle_enabled
      }
      else {
        this.trackAngle_enabled = false;
      }
      if (initObj.hasOwnProperty('date_enabled')) {
        this.date_enabled = initObj.date_enabled
      }
      else {
        this.date_enabled = false;
      }
      if (initObj.hasOwnProperty('magneticVariation_enabled')) {
        this.magneticVariation_enabled = initObj.magneticVariation_enabled
      }
      else {
        this.magneticVariation_enabled = false;
      }
      if (initObj.hasOwnProperty('mode_enabled')) {
        this.mode_enabled = initObj.mode_enabled
      }
      else {
        this.mode_enabled = false;
      }
      if (initObj.hasOwnProperty('time')) {
        this.time = initObj.time
      }
      else {
        this.time = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('position_valid')) {
        this.position_valid = initObj.position_valid
      }
      else {
        this.position_valid = 0;
      }
      if (initObj.hasOwnProperty('position')) {
        this.position = initObj.position
      }
      else {
        this.position = new WGS84();
      }
      if (initObj.hasOwnProperty('speedOverGround')) {
        this.speedOverGround = initObj.speedOverGround
      }
      else {
        this.speedOverGround = 0.0;
      }
      if (initObj.hasOwnProperty('trackAngle')) {
        this.trackAngle = initObj.trackAngle
      }
      else {
        this.trackAngle = 0.0;
      }
      if (initObj.hasOwnProperty('date')) {
        this.date = initObj.date
      }
      else {
        this.date = new Date_gps();
      }
      if (initObj.hasOwnProperty('magneticVariation')) {
        this.magneticVariation = initObj.magneticVariation
      }
      else {
        this.magneticVariation = 0.0;
      }
      if (initObj.hasOwnProperty('magnetic_var_direction')) {
        this.magnetic_var_direction = initObj.magnetic_var_direction
      }
      else {
        this.magnetic_var_direction = 0;
      }
      if (initObj.hasOwnProperty('mode')) {
        this.mode = initObj.mode
      }
      else {
        this.mode = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GpsRMC
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [time_enabled]
    bufferOffset = _serializer.bool(obj.time_enabled, buffer, bufferOffset);
    // Serialize message field [position_valid_enabled]
    bufferOffset = _serializer.bool(obj.position_valid_enabled, buffer, bufferOffset);
    // Serialize message field [position_enabled]
    bufferOffset = _serializer.bool(obj.position_enabled, buffer, bufferOffset);
    // Serialize message field [speedOverGround_enabled]
    bufferOffset = _serializer.bool(obj.speedOverGround_enabled, buffer, bufferOffset);
    // Serialize message field [trackAngle_enabled]
    bufferOffset = _serializer.bool(obj.trackAngle_enabled, buffer, bufferOffset);
    // Serialize message field [date_enabled]
    bufferOffset = _serializer.bool(obj.date_enabled, buffer, bufferOffset);
    // Serialize message field [magneticVariation_enabled]
    bufferOffset = _serializer.bool(obj.magneticVariation_enabled, buffer, bufferOffset);
    // Serialize message field [mode_enabled]
    bufferOffset = _serializer.bool(obj.mode_enabled, buffer, bufferOffset);
    // Serialize message field [time]
    bufferOffset = _serializer.time(obj.time, buffer, bufferOffset);
    // Serialize message field [position_valid]
    bufferOffset = _serializer.uint8(obj.position_valid, buffer, bufferOffset);
    // Serialize message field [position]
    bufferOffset = WGS84.serialize(obj.position, buffer, bufferOffset);
    // Serialize message field [speedOverGround]
    bufferOffset = _serializer.float64(obj.speedOverGround, buffer, bufferOffset);
    // Serialize message field [trackAngle]
    bufferOffset = _serializer.float64(obj.trackAngle, buffer, bufferOffset);
    // Serialize message field [date]
    bufferOffset = Date_gps.serialize(obj.date, buffer, bufferOffset);
    // Serialize message field [magneticVariation]
    bufferOffset = _serializer.float64(obj.magneticVariation, buffer, bufferOffset);
    // Serialize message field [magnetic_var_direction]
    bufferOffset = _serializer.uint8(obj.magnetic_var_direction, buffer, bufferOffset);
    // Serialize message field [mode]
    bufferOffset = _serializer.uint8(obj.mode, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GpsRMC
    let len;
    let data = new GpsRMC(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [time_enabled]
    data.time_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [position_valid_enabled]
    data.position_valid_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [position_enabled]
    data.position_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [speedOverGround_enabled]
    data.speedOverGround_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [trackAngle_enabled]
    data.trackAngle_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [date_enabled]
    data.date_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [magneticVariation_enabled]
    data.magneticVariation_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [mode_enabled]
    data.mode_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [time]
    data.time = _deserializer.time(buffer, bufferOffset);
    // Deserialize message field [position_valid]
    data.position_valid = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [position]
    data.position = WGS84.deserialize(buffer, bufferOffset);
    // Deserialize message field [speedOverGround]
    data.speedOverGround = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [trackAngle]
    data.trackAngle = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [date]
    data.date = Date_gps.deserialize(buffer, bufferOffset);
    // Deserialize message field [magneticVariation]
    data.magneticVariation = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [magnetic_var_direction]
    data.magnetic_var_direction = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [mode]
    data.mode = _deserializer.uint8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += Date_gps.getMessageSize(object.date);
    return length + 59;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gps_nmea/GpsRMC';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ecae7bc96c99e205f05bee9f6e358c53';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    bool time_enabled
    bool position_valid_enabled
    bool position_enabled
    bool speedOverGround_enabled
    bool trackAngle_enabled
    bool date_enabled
    bool magneticVariation_enabled
    bool mode_enabled
    
    # Current UTC time
    time time
    
    # Position Status
    # 0 : NAV Receiver warning
    # 1 : Valid position
    uint8 POSITION_VALID_VOID = 0
    uint8 POSITION_VALID_ACTIVE = 1
    uint8 position_valid
    
    # WGS84 latitude and longitude
    WGS84 position
    # Vitesse curviligne (m/s)
    float64 speedOverGround
    # Cap en degres (0deg=360deg is north)
    float64 trackAngle
    
    # Current UTC date
    Date_gps date
    # Magnetic variation
    float64  magneticVariation
    
    # Magnetic variation magnetic_var_direction
    uint8 MAGNETIC_VARIATION_EAST = 0
    uint8 MAGNETIC_VARIATION_WEST = 1
    uint8 magnetic_var_direction
    
    # Position mode
    # 0 : Data not valid
    # 1 : Autonomous mode
    # 2 : Differential mode
    # 3 : Estimated mode
    uint8 GPS_ENABLED_MODE_INVALID = 0
    uint8 GPS_ENABLED_MODE_AUTONOMOUS = 1
    uint8 GPS_ENABLED_MODE_DIFFERENTIAL = 2
    uint8 GPS_ENABLED_MODE_ESTIMATED = 3
    uint8 mode
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: gps_nmea/WGS84
    # Latitude (degrees). Positive is north of equator; negative is south. 
    # +: N, -: S --- unit: degrees
    float64 latitude
    
    # Longitude (degrees). Positive is east of prime meridian, negative west.
    # +: E, -: W --- unit: degrees, origin: Greenwich
    float64 longitude
    ================================================================================
    MSG: gps_nmea/Date_gps
    Header header
    # 1-31
    uint8 day
    # 1-12
    uint8 month
    # 00+ (2000)
    uint8 year
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GpsRMC(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.time_enabled !== undefined) {
      resolved.time_enabled = msg.time_enabled;
    }
    else {
      resolved.time_enabled = false
    }

    if (msg.position_valid_enabled !== undefined) {
      resolved.position_valid_enabled = msg.position_valid_enabled;
    }
    else {
      resolved.position_valid_enabled = false
    }

    if (msg.position_enabled !== undefined) {
      resolved.position_enabled = msg.position_enabled;
    }
    else {
      resolved.position_enabled = false
    }

    if (msg.speedOverGround_enabled !== undefined) {
      resolved.speedOverGround_enabled = msg.speedOverGround_enabled;
    }
    else {
      resolved.speedOverGround_enabled = false
    }

    if (msg.trackAngle_enabled !== undefined) {
      resolved.trackAngle_enabled = msg.trackAngle_enabled;
    }
    else {
      resolved.trackAngle_enabled = false
    }

    if (msg.date_enabled !== undefined) {
      resolved.date_enabled = msg.date_enabled;
    }
    else {
      resolved.date_enabled = false
    }

    if (msg.magneticVariation_enabled !== undefined) {
      resolved.magneticVariation_enabled = msg.magneticVariation_enabled;
    }
    else {
      resolved.magneticVariation_enabled = false
    }

    if (msg.mode_enabled !== undefined) {
      resolved.mode_enabled = msg.mode_enabled;
    }
    else {
      resolved.mode_enabled = false
    }

    if (msg.time !== undefined) {
      resolved.time = msg.time;
    }
    else {
      resolved.time = {secs: 0, nsecs: 0}
    }

    if (msg.position_valid !== undefined) {
      resolved.position_valid = msg.position_valid;
    }
    else {
      resolved.position_valid = 0
    }

    if (msg.position !== undefined) {
      resolved.position = WGS84.Resolve(msg.position)
    }
    else {
      resolved.position = new WGS84()
    }

    if (msg.speedOverGround !== undefined) {
      resolved.speedOverGround = msg.speedOverGround;
    }
    else {
      resolved.speedOverGround = 0.0
    }

    if (msg.trackAngle !== undefined) {
      resolved.trackAngle = msg.trackAngle;
    }
    else {
      resolved.trackAngle = 0.0
    }

    if (msg.date !== undefined) {
      resolved.date = Date_gps.Resolve(msg.date)
    }
    else {
      resolved.date = new Date_gps()
    }

    if (msg.magneticVariation !== undefined) {
      resolved.magneticVariation = msg.magneticVariation;
    }
    else {
      resolved.magneticVariation = 0.0
    }

    if (msg.magnetic_var_direction !== undefined) {
      resolved.magnetic_var_direction = msg.magnetic_var_direction;
    }
    else {
      resolved.magnetic_var_direction = 0
    }

    if (msg.mode !== undefined) {
      resolved.mode = msg.mode;
    }
    else {
      resolved.mode = 0
    }

    return resolved;
    }
};

// Constants for message
GpsRMC.Constants = {
  POSITION_VALID_VOID: 0,
  POSITION_VALID_ACTIVE: 1,
  MAGNETIC_VARIATION_EAST: 0,
  MAGNETIC_VARIATION_WEST: 1,
  GPS_ENABLED_MODE_INVALID: 0,
  GPS_ENABLED_MODE_AUTONOMOUS: 1,
  GPS_ENABLED_MODE_DIFFERENTIAL: 2,
  GPS_ENABLED_MODE_ESTIMATED: 3,
}

module.exports = GpsRMC;
