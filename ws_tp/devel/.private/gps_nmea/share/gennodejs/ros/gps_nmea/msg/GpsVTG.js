// Auto-generated. Do not edit!

// (in-package gps_nmea.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class GpsVTG {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.cog_true_enabled = null;
      this.cog_magnetic_enabled = null;
      this.sog_knots_enabled = null;
      this.sog_kmh_enabled = null;
      this.mode_enabled = null;
      this.cog_true = null;
      this.cog_magnetic = null;
      this.sog_knots = null;
      this.sog_kmh = null;
      this.mode = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('cog_true_enabled')) {
        this.cog_true_enabled = initObj.cog_true_enabled
      }
      else {
        this.cog_true_enabled = false;
      }
      if (initObj.hasOwnProperty('cog_magnetic_enabled')) {
        this.cog_magnetic_enabled = initObj.cog_magnetic_enabled
      }
      else {
        this.cog_magnetic_enabled = false;
      }
      if (initObj.hasOwnProperty('sog_knots_enabled')) {
        this.sog_knots_enabled = initObj.sog_knots_enabled
      }
      else {
        this.sog_knots_enabled = false;
      }
      if (initObj.hasOwnProperty('sog_kmh_enabled')) {
        this.sog_kmh_enabled = initObj.sog_kmh_enabled
      }
      else {
        this.sog_kmh_enabled = false;
      }
      if (initObj.hasOwnProperty('mode_enabled')) {
        this.mode_enabled = initObj.mode_enabled
      }
      else {
        this.mode_enabled = false;
      }
      if (initObj.hasOwnProperty('cog_true')) {
        this.cog_true = initObj.cog_true
      }
      else {
        this.cog_true = 0.0;
      }
      if (initObj.hasOwnProperty('cog_magnetic')) {
        this.cog_magnetic = initObj.cog_magnetic
      }
      else {
        this.cog_magnetic = 0.0;
      }
      if (initObj.hasOwnProperty('sog_knots')) {
        this.sog_knots = initObj.sog_knots
      }
      else {
        this.sog_knots = 0.0;
      }
      if (initObj.hasOwnProperty('sog_kmh')) {
        this.sog_kmh = initObj.sog_kmh
      }
      else {
        this.sog_kmh = 0.0;
      }
      if (initObj.hasOwnProperty('mode')) {
        this.mode = initObj.mode
      }
      else {
        this.mode = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GpsVTG
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [cog_true_enabled]
    bufferOffset = _serializer.bool(obj.cog_true_enabled, buffer, bufferOffset);
    // Serialize message field [cog_magnetic_enabled]
    bufferOffset = _serializer.bool(obj.cog_magnetic_enabled, buffer, bufferOffset);
    // Serialize message field [sog_knots_enabled]
    bufferOffset = _serializer.bool(obj.sog_knots_enabled, buffer, bufferOffset);
    // Serialize message field [sog_kmh_enabled]
    bufferOffset = _serializer.bool(obj.sog_kmh_enabled, buffer, bufferOffset);
    // Serialize message field [mode_enabled]
    bufferOffset = _serializer.bool(obj.mode_enabled, buffer, bufferOffset);
    // Serialize message field [cog_true]
    bufferOffset = _serializer.float64(obj.cog_true, buffer, bufferOffset);
    // Serialize message field [cog_magnetic]
    bufferOffset = _serializer.float64(obj.cog_magnetic, buffer, bufferOffset);
    // Serialize message field [sog_knots]
    bufferOffset = _serializer.float64(obj.sog_knots, buffer, bufferOffset);
    // Serialize message field [sog_kmh]
    bufferOffset = _serializer.float64(obj.sog_kmh, buffer, bufferOffset);
    // Serialize message field [mode]
    bufferOffset = _serializer.uint8(obj.mode, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GpsVTG
    let len;
    let data = new GpsVTG(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [cog_true_enabled]
    data.cog_true_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [cog_magnetic_enabled]
    data.cog_magnetic_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [sog_knots_enabled]
    data.sog_knots_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [sog_kmh_enabled]
    data.sog_kmh_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [mode_enabled]
    data.mode_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [cog_true]
    data.cog_true = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [cog_magnetic]
    data.cog_magnetic = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [sog_knots]
    data.sog_knots = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [sog_kmh]
    data.sog_kmh = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [mode]
    data.mode = _deserializer.uint8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 38;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gps_nmea/GpsVTG';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '61315a94881ac3a1456599c4e29fa56f';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    bool cog_true_enabled
    bool cog_magnetic_enabled
    bool sog_knots_enabled
    bool sog_kmh_enabled
    bool mode_enabled
    
    #Course over ground orientation with respect to true north
    float64 cog_true
    # Course over ground orientation with respect to magnetic north
    float64 cog_magnetic
    # Speed over ground in knots
    float64 sog_knots
    #Speed over ground in km/h
    float64 sog_kmh
    
    #GPS mode 
    # 0 : Autonomous mode
    # 1 : Differential mode
    # 2 : Data not valid
    uint8 GPS_MODE_AUTONOMOUS = 0
    uint8 GPS_MODE_DIFFERENTIAL = 1
    uint8 GPS_MODE_INVALID = 2
    uint8 mode
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GpsVTG(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.cog_true_enabled !== undefined) {
      resolved.cog_true_enabled = msg.cog_true_enabled;
    }
    else {
      resolved.cog_true_enabled = false
    }

    if (msg.cog_magnetic_enabled !== undefined) {
      resolved.cog_magnetic_enabled = msg.cog_magnetic_enabled;
    }
    else {
      resolved.cog_magnetic_enabled = false
    }

    if (msg.sog_knots_enabled !== undefined) {
      resolved.sog_knots_enabled = msg.sog_knots_enabled;
    }
    else {
      resolved.sog_knots_enabled = false
    }

    if (msg.sog_kmh_enabled !== undefined) {
      resolved.sog_kmh_enabled = msg.sog_kmh_enabled;
    }
    else {
      resolved.sog_kmh_enabled = false
    }

    if (msg.mode_enabled !== undefined) {
      resolved.mode_enabled = msg.mode_enabled;
    }
    else {
      resolved.mode_enabled = false
    }

    if (msg.cog_true !== undefined) {
      resolved.cog_true = msg.cog_true;
    }
    else {
      resolved.cog_true = 0.0
    }

    if (msg.cog_magnetic !== undefined) {
      resolved.cog_magnetic = msg.cog_magnetic;
    }
    else {
      resolved.cog_magnetic = 0.0
    }

    if (msg.sog_knots !== undefined) {
      resolved.sog_knots = msg.sog_knots;
    }
    else {
      resolved.sog_knots = 0.0
    }

    if (msg.sog_kmh !== undefined) {
      resolved.sog_kmh = msg.sog_kmh;
    }
    else {
      resolved.sog_kmh = 0.0
    }

    if (msg.mode !== undefined) {
      resolved.mode = msg.mode;
    }
    else {
      resolved.mode = 0
    }

    return resolved;
    }
};

// Constants for message
GpsVTG.Constants = {
  GPS_MODE_AUTONOMOUS: 0,
  GPS_MODE_DIFFERENTIAL: 1,
  GPS_MODE_INVALID: 2,
}

module.exports = GpsVTG;
