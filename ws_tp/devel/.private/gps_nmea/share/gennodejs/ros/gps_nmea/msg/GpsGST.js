// Auto-generated. Do not edit!

// (in-package gps_nmea.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class GpsGST {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.time_enabled = null;
      this.stdev_range_enabled = null;
      this.stdev_semimajor_enabled = null;
      this.stdev_semiminor_enabled = null;
      this.semimajor_orientation_enabled = null;
      this.stdev_latitude_enabled = null;
      this.stdev_longitude_enabled = null;
      this.stdev_altitude_enabled = null;
      this.source = null;
      this.time = null;
      this.stdev_range = null;
      this.stdev_semimajor = null;
      this.stdev_semiminor = null;
      this.semimajor_orientation = null;
      this.stdev_latitude = null;
      this.stdev_longitude = null;
      this.stdev_altitude = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('time_enabled')) {
        this.time_enabled = initObj.time_enabled
      }
      else {
        this.time_enabled = false;
      }
      if (initObj.hasOwnProperty('stdev_range_enabled')) {
        this.stdev_range_enabled = initObj.stdev_range_enabled
      }
      else {
        this.stdev_range_enabled = false;
      }
      if (initObj.hasOwnProperty('stdev_semimajor_enabled')) {
        this.stdev_semimajor_enabled = initObj.stdev_semimajor_enabled
      }
      else {
        this.stdev_semimajor_enabled = false;
      }
      if (initObj.hasOwnProperty('stdev_semiminor_enabled')) {
        this.stdev_semiminor_enabled = initObj.stdev_semiminor_enabled
      }
      else {
        this.stdev_semiminor_enabled = false;
      }
      if (initObj.hasOwnProperty('semimajor_orientation_enabled')) {
        this.semimajor_orientation_enabled = initObj.semimajor_orientation_enabled
      }
      else {
        this.semimajor_orientation_enabled = false;
      }
      if (initObj.hasOwnProperty('stdev_latitude_enabled')) {
        this.stdev_latitude_enabled = initObj.stdev_latitude_enabled
      }
      else {
        this.stdev_latitude_enabled = false;
      }
      if (initObj.hasOwnProperty('stdev_longitude_enabled')) {
        this.stdev_longitude_enabled = initObj.stdev_longitude_enabled
      }
      else {
        this.stdev_longitude_enabled = false;
      }
      if (initObj.hasOwnProperty('stdev_altitude_enabled')) {
        this.stdev_altitude_enabled = initObj.stdev_altitude_enabled
      }
      else {
        this.stdev_altitude_enabled = false;
      }
      if (initObj.hasOwnProperty('source')) {
        this.source = initObj.source
      }
      else {
        this.source = 0;
      }
      if (initObj.hasOwnProperty('time')) {
        this.time = initObj.time
      }
      else {
        this.time = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('stdev_range')) {
        this.stdev_range = initObj.stdev_range
      }
      else {
        this.stdev_range = 0.0;
      }
      if (initObj.hasOwnProperty('stdev_semimajor')) {
        this.stdev_semimajor = initObj.stdev_semimajor
      }
      else {
        this.stdev_semimajor = 0.0;
      }
      if (initObj.hasOwnProperty('stdev_semiminor')) {
        this.stdev_semiminor = initObj.stdev_semiminor
      }
      else {
        this.stdev_semiminor = 0.0;
      }
      if (initObj.hasOwnProperty('semimajor_orientation')) {
        this.semimajor_orientation = initObj.semimajor_orientation
      }
      else {
        this.semimajor_orientation = 0.0;
      }
      if (initObj.hasOwnProperty('stdev_latitude')) {
        this.stdev_latitude = initObj.stdev_latitude
      }
      else {
        this.stdev_latitude = 0.0;
      }
      if (initObj.hasOwnProperty('stdev_longitude')) {
        this.stdev_longitude = initObj.stdev_longitude
      }
      else {
        this.stdev_longitude = 0.0;
      }
      if (initObj.hasOwnProperty('stdev_altitude')) {
        this.stdev_altitude = initObj.stdev_altitude
      }
      else {
        this.stdev_altitude = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GpsGST
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [time_enabled]
    bufferOffset = _serializer.bool(obj.time_enabled, buffer, bufferOffset);
    // Serialize message field [stdev_range_enabled]
    bufferOffset = _serializer.bool(obj.stdev_range_enabled, buffer, bufferOffset);
    // Serialize message field [stdev_semimajor_enabled]
    bufferOffset = _serializer.bool(obj.stdev_semimajor_enabled, buffer, bufferOffset);
    // Serialize message field [stdev_semiminor_enabled]
    bufferOffset = _serializer.bool(obj.stdev_semiminor_enabled, buffer, bufferOffset);
    // Serialize message field [semimajor_orientation_enabled]
    bufferOffset = _serializer.bool(obj.semimajor_orientation_enabled, buffer, bufferOffset);
    // Serialize message field [stdev_latitude_enabled]
    bufferOffset = _serializer.bool(obj.stdev_latitude_enabled, buffer, bufferOffset);
    // Serialize message field [stdev_longitude_enabled]
    bufferOffset = _serializer.bool(obj.stdev_longitude_enabled, buffer, bufferOffset);
    // Serialize message field [stdev_altitude_enabled]
    bufferOffset = _serializer.bool(obj.stdev_altitude_enabled, buffer, bufferOffset);
    // Serialize message field [source]
    bufferOffset = _serializer.uint8(obj.source, buffer, bufferOffset);
    // Serialize message field [time]
    bufferOffset = _serializer.time(obj.time, buffer, bufferOffset);
    // Serialize message field [stdev_range]
    bufferOffset = _serializer.float64(obj.stdev_range, buffer, bufferOffset);
    // Serialize message field [stdev_semimajor]
    bufferOffset = _serializer.float64(obj.stdev_semimajor, buffer, bufferOffset);
    // Serialize message field [stdev_semiminor]
    bufferOffset = _serializer.float64(obj.stdev_semiminor, buffer, bufferOffset);
    // Serialize message field [semimajor_orientation]
    bufferOffset = _serializer.float64(obj.semimajor_orientation, buffer, bufferOffset);
    // Serialize message field [stdev_latitude]
    bufferOffset = _serializer.float64(obj.stdev_latitude, buffer, bufferOffset);
    // Serialize message field [stdev_longitude]
    bufferOffset = _serializer.float64(obj.stdev_longitude, buffer, bufferOffset);
    // Serialize message field [stdev_altitude]
    bufferOffset = _serializer.float64(obj.stdev_altitude, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GpsGST
    let len;
    let data = new GpsGST(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [time_enabled]
    data.time_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [stdev_range_enabled]
    data.stdev_range_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [stdev_semimajor_enabled]
    data.stdev_semimajor_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [stdev_semiminor_enabled]
    data.stdev_semiminor_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [semimajor_orientation_enabled]
    data.semimajor_orientation_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [stdev_latitude_enabled]
    data.stdev_latitude_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [stdev_longitude_enabled]
    data.stdev_longitude_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [stdev_altitude_enabled]
    data.stdev_altitude_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [source]
    data.source = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [time]
    data.time = _deserializer.time(buffer, bufferOffset);
    // Deserialize message field [stdev_range]
    data.stdev_range = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [stdev_semimajor]
    data.stdev_semimajor = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [stdev_semiminor]
    data.stdev_semiminor = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [semimajor_orientation]
    data.semimajor_orientation = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [stdev_latitude]
    data.stdev_latitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [stdev_longitude]
    data.stdev_longitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [stdev_altitude]
    data.stdev_altitude = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 73;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gps_nmea/GpsGST';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '46d580dc2c2da339f4753af61f94de20';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    bool time_enabled
    bool stdev_range_enabled
    bool stdev_semimajor_enabled
    bool stdev_semiminor_enabled
    bool semimajor_orientation_enabled
    bool stdev_latitude_enabled
    bool stdev_longitude_enabled
    bool stdev_altitude_enabled
    
    # Source GPS Fix
    # 0 : Only GPS satellites are used
    # 1 : Only GLONASS satellites are used
    # 2 : Several constellations (GPS, SBAS, GLONASS) are used.
    uint8 SATELLITE_SOURCE_GPS = 0
    uint8 SATELLITE_SOURCE_GLONASS = 1
    uint8 SATELLITE_SOURCE_SEVERAL = 2
    uint8 source
    
    # Current UTC time of position
    time time
    # RMS value of standard deviation of range inputs (DGNSS corrections included), in meters
    float64 stdev_range
    # Standard deviation of semi-major axis of error ellipse, in meters
    float64 stdev_semimajor
    # Standard deviation of semi-minor axis of error ellipse, in meters
    float64 stdev_semiminor
    # Orientation of semi-major axis of error ellipse, in degrees from true North
    float64 semimajor_orientation
    # Standard deviation of latitude error, in meters
    float64 stdev_latitude
    # Standard deviation of longitude error, in meters
    float64 stdev_longitude
    # Standard deviation of altitude error, in meters
    float64 stdev_altitude
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GpsGST(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.time_enabled !== undefined) {
      resolved.time_enabled = msg.time_enabled;
    }
    else {
      resolved.time_enabled = false
    }

    if (msg.stdev_range_enabled !== undefined) {
      resolved.stdev_range_enabled = msg.stdev_range_enabled;
    }
    else {
      resolved.stdev_range_enabled = false
    }

    if (msg.stdev_semimajor_enabled !== undefined) {
      resolved.stdev_semimajor_enabled = msg.stdev_semimajor_enabled;
    }
    else {
      resolved.stdev_semimajor_enabled = false
    }

    if (msg.stdev_semiminor_enabled !== undefined) {
      resolved.stdev_semiminor_enabled = msg.stdev_semiminor_enabled;
    }
    else {
      resolved.stdev_semiminor_enabled = false
    }

    if (msg.semimajor_orientation_enabled !== undefined) {
      resolved.semimajor_orientation_enabled = msg.semimajor_orientation_enabled;
    }
    else {
      resolved.semimajor_orientation_enabled = false
    }

    if (msg.stdev_latitude_enabled !== undefined) {
      resolved.stdev_latitude_enabled = msg.stdev_latitude_enabled;
    }
    else {
      resolved.stdev_latitude_enabled = false
    }

    if (msg.stdev_longitude_enabled !== undefined) {
      resolved.stdev_longitude_enabled = msg.stdev_longitude_enabled;
    }
    else {
      resolved.stdev_longitude_enabled = false
    }

    if (msg.stdev_altitude_enabled !== undefined) {
      resolved.stdev_altitude_enabled = msg.stdev_altitude_enabled;
    }
    else {
      resolved.stdev_altitude_enabled = false
    }

    if (msg.source !== undefined) {
      resolved.source = msg.source;
    }
    else {
      resolved.source = 0
    }

    if (msg.time !== undefined) {
      resolved.time = msg.time;
    }
    else {
      resolved.time = {secs: 0, nsecs: 0}
    }

    if (msg.stdev_range !== undefined) {
      resolved.stdev_range = msg.stdev_range;
    }
    else {
      resolved.stdev_range = 0.0
    }

    if (msg.stdev_semimajor !== undefined) {
      resolved.stdev_semimajor = msg.stdev_semimajor;
    }
    else {
      resolved.stdev_semimajor = 0.0
    }

    if (msg.stdev_semiminor !== undefined) {
      resolved.stdev_semiminor = msg.stdev_semiminor;
    }
    else {
      resolved.stdev_semiminor = 0.0
    }

    if (msg.semimajor_orientation !== undefined) {
      resolved.semimajor_orientation = msg.semimajor_orientation;
    }
    else {
      resolved.semimajor_orientation = 0.0
    }

    if (msg.stdev_latitude !== undefined) {
      resolved.stdev_latitude = msg.stdev_latitude;
    }
    else {
      resolved.stdev_latitude = 0.0
    }

    if (msg.stdev_longitude !== undefined) {
      resolved.stdev_longitude = msg.stdev_longitude;
    }
    else {
      resolved.stdev_longitude = 0.0
    }

    if (msg.stdev_altitude !== undefined) {
      resolved.stdev_altitude = msg.stdev_altitude;
    }
    else {
      resolved.stdev_altitude = 0.0
    }

    return resolved;
    }
};

// Constants for message
GpsGST.Constants = {
  SATELLITE_SOURCE_GPS: 0,
  SATELLITE_SOURCE_GLONASS: 1,
  SATELLITE_SOURCE_SEVERAL: 2,
}

module.exports = GpsGST;
