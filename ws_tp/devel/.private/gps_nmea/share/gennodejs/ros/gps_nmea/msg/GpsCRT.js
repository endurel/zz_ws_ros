// Auto-generated. Do not edit!

// (in-package gps_nmea.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class GpsCRT {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.position_mode_enabled = null;
      this.count_enabled = null;
      this.time_enabled = null;
      this.ecef_x_enabled = null;
      this.ecef_y_enabled = null;
      this.ecef_z_enabled = null;
      this.clock_offset_enabled = null;
      this.v_x_enabled = null;
      this.v_y_enabled = null;
      this.v_z_enabled = null;
      this.clock_drift_enabled = null;
      this.pdop_enabled = null;
      this.hdop_enabled = null;
      this.vdop_enabled = null;
      this.tdop_enabled = null;
      this.firmware_enabled = null;
      this.position_mode = null;
      this.count = null;
      this.time = null;
      this.ecef_x = null;
      this.ecef_y = null;
      this.ecef_z = null;
      this.clock_offset = null;
      this.v_x = null;
      this.v_y = null;
      this.v_z = null;
      this.clock_drift = null;
      this.pdop = null;
      this.hdop = null;
      this.vdop = null;
      this.tdop = null;
      this.firmware = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('position_mode_enabled')) {
        this.position_mode_enabled = initObj.position_mode_enabled
      }
      else {
        this.position_mode_enabled = false;
      }
      if (initObj.hasOwnProperty('count_enabled')) {
        this.count_enabled = initObj.count_enabled
      }
      else {
        this.count_enabled = false;
      }
      if (initObj.hasOwnProperty('time_enabled')) {
        this.time_enabled = initObj.time_enabled
      }
      else {
        this.time_enabled = false;
      }
      if (initObj.hasOwnProperty('ecef_x_enabled')) {
        this.ecef_x_enabled = initObj.ecef_x_enabled
      }
      else {
        this.ecef_x_enabled = false;
      }
      if (initObj.hasOwnProperty('ecef_y_enabled')) {
        this.ecef_y_enabled = initObj.ecef_y_enabled
      }
      else {
        this.ecef_y_enabled = false;
      }
      if (initObj.hasOwnProperty('ecef_z_enabled')) {
        this.ecef_z_enabled = initObj.ecef_z_enabled
      }
      else {
        this.ecef_z_enabled = false;
      }
      if (initObj.hasOwnProperty('clock_offset_enabled')) {
        this.clock_offset_enabled = initObj.clock_offset_enabled
      }
      else {
        this.clock_offset_enabled = false;
      }
      if (initObj.hasOwnProperty('v_x_enabled')) {
        this.v_x_enabled = initObj.v_x_enabled
      }
      else {
        this.v_x_enabled = false;
      }
      if (initObj.hasOwnProperty('v_y_enabled')) {
        this.v_y_enabled = initObj.v_y_enabled
      }
      else {
        this.v_y_enabled = false;
      }
      if (initObj.hasOwnProperty('v_z_enabled')) {
        this.v_z_enabled = initObj.v_z_enabled
      }
      else {
        this.v_z_enabled = false;
      }
      if (initObj.hasOwnProperty('clock_drift_enabled')) {
        this.clock_drift_enabled = initObj.clock_drift_enabled
      }
      else {
        this.clock_drift_enabled = false;
      }
      if (initObj.hasOwnProperty('pdop_enabled')) {
        this.pdop_enabled = initObj.pdop_enabled
      }
      else {
        this.pdop_enabled = false;
      }
      if (initObj.hasOwnProperty('hdop_enabled')) {
        this.hdop_enabled = initObj.hdop_enabled
      }
      else {
        this.hdop_enabled = false;
      }
      if (initObj.hasOwnProperty('vdop_enabled')) {
        this.vdop_enabled = initObj.vdop_enabled
      }
      else {
        this.vdop_enabled = false;
      }
      if (initObj.hasOwnProperty('tdop_enabled')) {
        this.tdop_enabled = initObj.tdop_enabled
      }
      else {
        this.tdop_enabled = false;
      }
      if (initObj.hasOwnProperty('firmware_enabled')) {
        this.firmware_enabled = initObj.firmware_enabled
      }
      else {
        this.firmware_enabled = false;
      }
      if (initObj.hasOwnProperty('position_mode')) {
        this.position_mode = initObj.position_mode
      }
      else {
        this.position_mode = 0;
      }
      if (initObj.hasOwnProperty('count')) {
        this.count = initObj.count
      }
      else {
        this.count = 0;
      }
      if (initObj.hasOwnProperty('time')) {
        this.time = initObj.time
      }
      else {
        this.time = {secs: 0, nsecs: 0};
      }
      if (initObj.hasOwnProperty('ecef_x')) {
        this.ecef_x = initObj.ecef_x
      }
      else {
        this.ecef_x = 0.0;
      }
      if (initObj.hasOwnProperty('ecef_y')) {
        this.ecef_y = initObj.ecef_y
      }
      else {
        this.ecef_y = 0.0;
      }
      if (initObj.hasOwnProperty('ecef_z')) {
        this.ecef_z = initObj.ecef_z
      }
      else {
        this.ecef_z = 0.0;
      }
      if (initObj.hasOwnProperty('clock_offset')) {
        this.clock_offset = initObj.clock_offset
      }
      else {
        this.clock_offset = 0.0;
      }
      if (initObj.hasOwnProperty('v_x')) {
        this.v_x = initObj.v_x
      }
      else {
        this.v_x = 0.0;
      }
      if (initObj.hasOwnProperty('v_y')) {
        this.v_y = initObj.v_y
      }
      else {
        this.v_y = 0.0;
      }
      if (initObj.hasOwnProperty('v_z')) {
        this.v_z = initObj.v_z
      }
      else {
        this.v_z = 0.0;
      }
      if (initObj.hasOwnProperty('clock_drift')) {
        this.clock_drift = initObj.clock_drift
      }
      else {
        this.clock_drift = 0.0;
      }
      if (initObj.hasOwnProperty('pdop')) {
        this.pdop = initObj.pdop
      }
      else {
        this.pdop = 0.0;
      }
      if (initObj.hasOwnProperty('hdop')) {
        this.hdop = initObj.hdop
      }
      else {
        this.hdop = 0.0;
      }
      if (initObj.hasOwnProperty('vdop')) {
        this.vdop = initObj.vdop
      }
      else {
        this.vdop = 0.0;
      }
      if (initObj.hasOwnProperty('tdop')) {
        this.tdop = initObj.tdop
      }
      else {
        this.tdop = 0.0;
      }
      if (initObj.hasOwnProperty('firmware')) {
        this.firmware = initObj.firmware
      }
      else {
        this.firmware = new Array(5).fill(0);
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type GpsCRT
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [position_mode_enabled]
    bufferOffset = _serializer.bool(obj.position_mode_enabled, buffer, bufferOffset);
    // Serialize message field [count_enabled]
    bufferOffset = _serializer.bool(obj.count_enabled, buffer, bufferOffset);
    // Serialize message field [time_enabled]
    bufferOffset = _serializer.bool(obj.time_enabled, buffer, bufferOffset);
    // Serialize message field [ecef_x_enabled]
    bufferOffset = _serializer.bool(obj.ecef_x_enabled, buffer, bufferOffset);
    // Serialize message field [ecef_y_enabled]
    bufferOffset = _serializer.bool(obj.ecef_y_enabled, buffer, bufferOffset);
    // Serialize message field [ecef_z_enabled]
    bufferOffset = _serializer.bool(obj.ecef_z_enabled, buffer, bufferOffset);
    // Serialize message field [clock_offset_enabled]
    bufferOffset = _serializer.bool(obj.clock_offset_enabled, buffer, bufferOffset);
    // Serialize message field [v_x_enabled]
    bufferOffset = _serializer.bool(obj.v_x_enabled, buffer, bufferOffset);
    // Serialize message field [v_y_enabled]
    bufferOffset = _serializer.bool(obj.v_y_enabled, buffer, bufferOffset);
    // Serialize message field [v_z_enabled]
    bufferOffset = _serializer.bool(obj.v_z_enabled, buffer, bufferOffset);
    // Serialize message field [clock_drift_enabled]
    bufferOffset = _serializer.bool(obj.clock_drift_enabled, buffer, bufferOffset);
    // Serialize message field [pdop_enabled]
    bufferOffset = _serializer.bool(obj.pdop_enabled, buffer, bufferOffset);
    // Serialize message field [hdop_enabled]
    bufferOffset = _serializer.bool(obj.hdop_enabled, buffer, bufferOffset);
    // Serialize message field [vdop_enabled]
    bufferOffset = _serializer.bool(obj.vdop_enabled, buffer, bufferOffset);
    // Serialize message field [tdop_enabled]
    bufferOffset = _serializer.bool(obj.tdop_enabled, buffer, bufferOffset);
    // Serialize message field [firmware_enabled]
    bufferOffset = _serializer.bool(obj.firmware_enabled, buffer, bufferOffset);
    // Serialize message field [position_mode]
    bufferOffset = _serializer.uint8(obj.position_mode, buffer, bufferOffset);
    // Serialize message field [count]
    bufferOffset = _serializer.uint8(obj.count, buffer, bufferOffset);
    // Serialize message field [time]
    bufferOffset = _serializer.time(obj.time, buffer, bufferOffset);
    // Serialize message field [ecef_x]
    bufferOffset = _serializer.float64(obj.ecef_x, buffer, bufferOffset);
    // Serialize message field [ecef_y]
    bufferOffset = _serializer.float64(obj.ecef_y, buffer, bufferOffset);
    // Serialize message field [ecef_z]
    bufferOffset = _serializer.float64(obj.ecef_z, buffer, bufferOffset);
    // Serialize message field [clock_offset]
    bufferOffset = _serializer.float64(obj.clock_offset, buffer, bufferOffset);
    // Serialize message field [v_x]
    bufferOffset = _serializer.float64(obj.v_x, buffer, bufferOffset);
    // Serialize message field [v_y]
    bufferOffset = _serializer.float64(obj.v_y, buffer, bufferOffset);
    // Serialize message field [v_z]
    bufferOffset = _serializer.float64(obj.v_z, buffer, bufferOffset);
    // Serialize message field [clock_drift]
    bufferOffset = _serializer.float64(obj.clock_drift, buffer, bufferOffset);
    // Serialize message field [pdop]
    bufferOffset = _serializer.float64(obj.pdop, buffer, bufferOffset);
    // Serialize message field [hdop]
    bufferOffset = _serializer.float64(obj.hdop, buffer, bufferOffset);
    // Serialize message field [vdop]
    bufferOffset = _serializer.float64(obj.vdop, buffer, bufferOffset);
    // Serialize message field [tdop]
    bufferOffset = _serializer.float64(obj.tdop, buffer, bufferOffset);
    // Check that the constant length array field [firmware] has the right length
    if (obj.firmware.length !== 5) {
      throw new Error('Unable to serialize array field firmware - length must be 5')
    }
    // Serialize message field [firmware]
    bufferOffset = _arraySerializer.char(obj.firmware, buffer, bufferOffset, 5);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type GpsCRT
    let len;
    let data = new GpsCRT(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [position_mode_enabled]
    data.position_mode_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [count_enabled]
    data.count_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [time_enabled]
    data.time_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [ecef_x_enabled]
    data.ecef_x_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [ecef_y_enabled]
    data.ecef_y_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [ecef_z_enabled]
    data.ecef_z_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [clock_offset_enabled]
    data.clock_offset_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [v_x_enabled]
    data.v_x_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [v_y_enabled]
    data.v_y_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [v_z_enabled]
    data.v_z_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [clock_drift_enabled]
    data.clock_drift_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [pdop_enabled]
    data.pdop_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [hdop_enabled]
    data.hdop_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [vdop_enabled]
    data.vdop_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [tdop_enabled]
    data.tdop_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [firmware_enabled]
    data.firmware_enabled = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [position_mode]
    data.position_mode = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [count]
    data.count = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [time]
    data.time = _deserializer.time(buffer, bufferOffset);
    // Deserialize message field [ecef_x]
    data.ecef_x = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [ecef_y]
    data.ecef_y = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [ecef_z]
    data.ecef_z = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [clock_offset]
    data.clock_offset = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [v_x]
    data.v_x = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [v_y]
    data.v_y = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [v_z]
    data.v_z = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [clock_drift]
    data.clock_drift = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [pdop]
    data.pdop = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [hdop]
    data.hdop = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [vdop]
    data.vdop = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [tdop]
    data.tdop = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [firmware]
    data.firmware = _arrayDeserializer.char(buffer, bufferOffset, 5)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 127;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gps_nmea/GpsCRT';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'dab59c118ba3827d16dc59c7d383a215';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    bool position_mode_enabled
    bool count_enabled
    bool time_enabled
    bool ecef_x_enabled
    bool ecef_y_enabled
    bool ecef_z_enabled
    bool clock_offset_enabled
    bool v_x_enabled
    bool v_y_enabled
    bool v_z_enabled
    bool clock_drift_enabled
    bool pdop_enabled
    bool hdop_enabled
    bool vdop_enabled
    bool tdop_enabled
    bool firmware_enabled
    
    # Position Mode
    # 0 : Autonomous
    # 1 : RTCM or SBAS differential
    # 2 : RTK float
    # 3 : RTK fixed
    uint8 POSITION_MODE_AUTONOMOUS = 0
    uint8 POSITION_MODE_RTCM = 1
    uint8 POSITION_MODE_RTKFLOAT = 2
    uint8 POSITION_MODE_RTKFIXED = 3
    uint8 position_mode
    
    # Count of SVs used in position computation
    uint8 count
    # UTC time
    time time
    # ECEF X coordinate, in meters
    float64 ecef_x
    # ECEF Y coordinate, in meters
    float64 ecef_y
    # ECEF Z coordinate, in meters
    float64 ecef_z
    # Receiver clock offset, in meters
    float64 clock_offset
    # Velocity vector, X component, in m/s
    float64 v_x
    # Velocity vector, Y component, in m/s
    float64 v_y
    # Velocity vector, Z component, in m/s
    float64 v_z
    # Receiver clock drift, in m/s
    float64 clock_drift
    # Positional dilution of precision
    float64 pdop
    # Horizontal dilution of precision
    float64 hdop
    # Vertical dilution of precision
    float64 vdop
    # Time dilution of precision
    float64 tdop
    # Firmware version ID (4 letters)
    char[5] firmware
    
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new GpsCRT(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.position_mode_enabled !== undefined) {
      resolved.position_mode_enabled = msg.position_mode_enabled;
    }
    else {
      resolved.position_mode_enabled = false
    }

    if (msg.count_enabled !== undefined) {
      resolved.count_enabled = msg.count_enabled;
    }
    else {
      resolved.count_enabled = false
    }

    if (msg.time_enabled !== undefined) {
      resolved.time_enabled = msg.time_enabled;
    }
    else {
      resolved.time_enabled = false
    }

    if (msg.ecef_x_enabled !== undefined) {
      resolved.ecef_x_enabled = msg.ecef_x_enabled;
    }
    else {
      resolved.ecef_x_enabled = false
    }

    if (msg.ecef_y_enabled !== undefined) {
      resolved.ecef_y_enabled = msg.ecef_y_enabled;
    }
    else {
      resolved.ecef_y_enabled = false
    }

    if (msg.ecef_z_enabled !== undefined) {
      resolved.ecef_z_enabled = msg.ecef_z_enabled;
    }
    else {
      resolved.ecef_z_enabled = false
    }

    if (msg.clock_offset_enabled !== undefined) {
      resolved.clock_offset_enabled = msg.clock_offset_enabled;
    }
    else {
      resolved.clock_offset_enabled = false
    }

    if (msg.v_x_enabled !== undefined) {
      resolved.v_x_enabled = msg.v_x_enabled;
    }
    else {
      resolved.v_x_enabled = false
    }

    if (msg.v_y_enabled !== undefined) {
      resolved.v_y_enabled = msg.v_y_enabled;
    }
    else {
      resolved.v_y_enabled = false
    }

    if (msg.v_z_enabled !== undefined) {
      resolved.v_z_enabled = msg.v_z_enabled;
    }
    else {
      resolved.v_z_enabled = false
    }

    if (msg.clock_drift_enabled !== undefined) {
      resolved.clock_drift_enabled = msg.clock_drift_enabled;
    }
    else {
      resolved.clock_drift_enabled = false
    }

    if (msg.pdop_enabled !== undefined) {
      resolved.pdop_enabled = msg.pdop_enabled;
    }
    else {
      resolved.pdop_enabled = false
    }

    if (msg.hdop_enabled !== undefined) {
      resolved.hdop_enabled = msg.hdop_enabled;
    }
    else {
      resolved.hdop_enabled = false
    }

    if (msg.vdop_enabled !== undefined) {
      resolved.vdop_enabled = msg.vdop_enabled;
    }
    else {
      resolved.vdop_enabled = false
    }

    if (msg.tdop_enabled !== undefined) {
      resolved.tdop_enabled = msg.tdop_enabled;
    }
    else {
      resolved.tdop_enabled = false
    }

    if (msg.firmware_enabled !== undefined) {
      resolved.firmware_enabled = msg.firmware_enabled;
    }
    else {
      resolved.firmware_enabled = false
    }

    if (msg.position_mode !== undefined) {
      resolved.position_mode = msg.position_mode;
    }
    else {
      resolved.position_mode = 0
    }

    if (msg.count !== undefined) {
      resolved.count = msg.count;
    }
    else {
      resolved.count = 0
    }

    if (msg.time !== undefined) {
      resolved.time = msg.time;
    }
    else {
      resolved.time = {secs: 0, nsecs: 0}
    }

    if (msg.ecef_x !== undefined) {
      resolved.ecef_x = msg.ecef_x;
    }
    else {
      resolved.ecef_x = 0.0
    }

    if (msg.ecef_y !== undefined) {
      resolved.ecef_y = msg.ecef_y;
    }
    else {
      resolved.ecef_y = 0.0
    }

    if (msg.ecef_z !== undefined) {
      resolved.ecef_z = msg.ecef_z;
    }
    else {
      resolved.ecef_z = 0.0
    }

    if (msg.clock_offset !== undefined) {
      resolved.clock_offset = msg.clock_offset;
    }
    else {
      resolved.clock_offset = 0.0
    }

    if (msg.v_x !== undefined) {
      resolved.v_x = msg.v_x;
    }
    else {
      resolved.v_x = 0.0
    }

    if (msg.v_y !== undefined) {
      resolved.v_y = msg.v_y;
    }
    else {
      resolved.v_y = 0.0
    }

    if (msg.v_z !== undefined) {
      resolved.v_z = msg.v_z;
    }
    else {
      resolved.v_z = 0.0
    }

    if (msg.clock_drift !== undefined) {
      resolved.clock_drift = msg.clock_drift;
    }
    else {
      resolved.clock_drift = 0.0
    }

    if (msg.pdop !== undefined) {
      resolved.pdop = msg.pdop;
    }
    else {
      resolved.pdop = 0.0
    }

    if (msg.hdop !== undefined) {
      resolved.hdop = msg.hdop;
    }
    else {
      resolved.hdop = 0.0
    }

    if (msg.vdop !== undefined) {
      resolved.vdop = msg.vdop;
    }
    else {
      resolved.vdop = 0.0
    }

    if (msg.tdop !== undefined) {
      resolved.tdop = msg.tdop;
    }
    else {
      resolved.tdop = 0.0
    }

    if (msg.firmware !== undefined) {
      resolved.firmware = msg.firmware;
    }
    else {
      resolved.firmware = new Array(5).fill(0)
    }

    return resolved;
    }
};

// Constants for message
GpsCRT.Constants = {
  POSITION_MODE_AUTONOMOUS: 0,
  POSITION_MODE_RTCM: 1,
  POSITION_MODE_RTKFLOAT: 2,
  POSITION_MODE_RTKFIXED: 3,
}

module.exports = GpsCRT;
