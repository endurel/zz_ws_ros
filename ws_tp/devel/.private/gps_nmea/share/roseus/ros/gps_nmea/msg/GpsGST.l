;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::GpsGST)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'GpsGST (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::GPSGST")
  (make-package "GPS_NMEA::GPSGST"))

(in-package "ROS")
;;//! \htmlinclude GpsGST.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(intern "*SATELLITE_SOURCE_GPS*" (find-package "GPS_NMEA::GPSGST"))
(shadow '*SATELLITE_SOURCE_GPS* (find-package "GPS_NMEA::GPSGST"))
(defconstant gps_nmea::GpsGST::*SATELLITE_SOURCE_GPS* 0)
(intern "*SATELLITE_SOURCE_GLONASS*" (find-package "GPS_NMEA::GPSGST"))
(shadow '*SATELLITE_SOURCE_GLONASS* (find-package "GPS_NMEA::GPSGST"))
(defconstant gps_nmea::GpsGST::*SATELLITE_SOURCE_GLONASS* 1)
(intern "*SATELLITE_SOURCE_SEVERAL*" (find-package "GPS_NMEA::GPSGST"))
(shadow '*SATELLITE_SOURCE_SEVERAL* (find-package "GPS_NMEA::GPSGST"))
(defconstant gps_nmea::GpsGST::*SATELLITE_SOURCE_SEVERAL* 2)

(defun gps_nmea::GpsGST-to-symbol (const)
  (cond
        ((= const 0) 'gps_nmea::GpsGST::*SATELLITE_SOURCE_GPS*)
        ((= const 1) 'gps_nmea::GpsGST::*SATELLITE_SOURCE_GLONASS*)
        ((= const 2) 'gps_nmea::GpsGST::*SATELLITE_SOURCE_SEVERAL*)
        (t nil)))

(defclass gps_nmea::GpsGST
  :super ros::object
  :slots (_header _time_enabled _stdev_range_enabled _stdev_semimajor_enabled _stdev_semiminor_enabled _semimajor_orientation_enabled _stdev_latitude_enabled _stdev_longitude_enabled _stdev_altitude_enabled _source _time _stdev_range _stdev_semimajor _stdev_semiminor _semimajor_orientation _stdev_latitude _stdev_longitude _stdev_altitude ))

(defmethod gps_nmea::GpsGST
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:time_enabled __time_enabled) nil)
    ((:stdev_range_enabled __stdev_range_enabled) nil)
    ((:stdev_semimajor_enabled __stdev_semimajor_enabled) nil)
    ((:stdev_semiminor_enabled __stdev_semiminor_enabled) nil)
    ((:semimajor_orientation_enabled __semimajor_orientation_enabled) nil)
    ((:stdev_latitude_enabled __stdev_latitude_enabled) nil)
    ((:stdev_longitude_enabled __stdev_longitude_enabled) nil)
    ((:stdev_altitude_enabled __stdev_altitude_enabled) nil)
    ((:source __source) 0)
    ((:time __time) (instance ros::time :init))
    ((:stdev_range __stdev_range) 0.0)
    ((:stdev_semimajor __stdev_semimajor) 0.0)
    ((:stdev_semiminor __stdev_semiminor) 0.0)
    ((:semimajor_orientation __semimajor_orientation) 0.0)
    ((:stdev_latitude __stdev_latitude) 0.0)
    ((:stdev_longitude __stdev_longitude) 0.0)
    ((:stdev_altitude __stdev_altitude) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _time_enabled __time_enabled)
   (setq _stdev_range_enabled __stdev_range_enabled)
   (setq _stdev_semimajor_enabled __stdev_semimajor_enabled)
   (setq _stdev_semiminor_enabled __stdev_semiminor_enabled)
   (setq _semimajor_orientation_enabled __semimajor_orientation_enabled)
   (setq _stdev_latitude_enabled __stdev_latitude_enabled)
   (setq _stdev_longitude_enabled __stdev_longitude_enabled)
   (setq _stdev_altitude_enabled __stdev_altitude_enabled)
   (setq _source (round __source))
   (setq _time __time)
   (setq _stdev_range (float __stdev_range))
   (setq _stdev_semimajor (float __stdev_semimajor))
   (setq _stdev_semiminor (float __stdev_semiminor))
   (setq _semimajor_orientation (float __semimajor_orientation))
   (setq _stdev_latitude (float __stdev_latitude))
   (setq _stdev_longitude (float __stdev_longitude))
   (setq _stdev_altitude (float __stdev_altitude))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:time_enabled
   (&optional (__time_enabled :null))
   (if (not (eq __time_enabled :null)) (setq _time_enabled __time_enabled)) _time_enabled)
  (:stdev_range_enabled
   (&optional (__stdev_range_enabled :null))
   (if (not (eq __stdev_range_enabled :null)) (setq _stdev_range_enabled __stdev_range_enabled)) _stdev_range_enabled)
  (:stdev_semimajor_enabled
   (&optional (__stdev_semimajor_enabled :null))
   (if (not (eq __stdev_semimajor_enabled :null)) (setq _stdev_semimajor_enabled __stdev_semimajor_enabled)) _stdev_semimajor_enabled)
  (:stdev_semiminor_enabled
   (&optional (__stdev_semiminor_enabled :null))
   (if (not (eq __stdev_semiminor_enabled :null)) (setq _stdev_semiminor_enabled __stdev_semiminor_enabled)) _stdev_semiminor_enabled)
  (:semimajor_orientation_enabled
   (&optional (__semimajor_orientation_enabled :null))
   (if (not (eq __semimajor_orientation_enabled :null)) (setq _semimajor_orientation_enabled __semimajor_orientation_enabled)) _semimajor_orientation_enabled)
  (:stdev_latitude_enabled
   (&optional (__stdev_latitude_enabled :null))
   (if (not (eq __stdev_latitude_enabled :null)) (setq _stdev_latitude_enabled __stdev_latitude_enabled)) _stdev_latitude_enabled)
  (:stdev_longitude_enabled
   (&optional (__stdev_longitude_enabled :null))
   (if (not (eq __stdev_longitude_enabled :null)) (setq _stdev_longitude_enabled __stdev_longitude_enabled)) _stdev_longitude_enabled)
  (:stdev_altitude_enabled
   (&optional (__stdev_altitude_enabled :null))
   (if (not (eq __stdev_altitude_enabled :null)) (setq _stdev_altitude_enabled __stdev_altitude_enabled)) _stdev_altitude_enabled)
  (:source
   (&optional __source)
   (if __source (setq _source __source)) _source)
  (:time
   (&optional __time)
   (if __time (setq _time __time)) _time)
  (:stdev_range
   (&optional __stdev_range)
   (if __stdev_range (setq _stdev_range __stdev_range)) _stdev_range)
  (:stdev_semimajor
   (&optional __stdev_semimajor)
   (if __stdev_semimajor (setq _stdev_semimajor __stdev_semimajor)) _stdev_semimajor)
  (:stdev_semiminor
   (&optional __stdev_semiminor)
   (if __stdev_semiminor (setq _stdev_semiminor __stdev_semiminor)) _stdev_semiminor)
  (:semimajor_orientation
   (&optional __semimajor_orientation)
   (if __semimajor_orientation (setq _semimajor_orientation __semimajor_orientation)) _semimajor_orientation)
  (:stdev_latitude
   (&optional __stdev_latitude)
   (if __stdev_latitude (setq _stdev_latitude __stdev_latitude)) _stdev_latitude)
  (:stdev_longitude
   (&optional __stdev_longitude)
   (if __stdev_longitude (setq _stdev_longitude __stdev_longitude)) _stdev_longitude)
  (:stdev_altitude
   (&optional __stdev_altitude)
   (if __stdev_altitude (setq _stdev_altitude __stdev_altitude)) _stdev_altitude)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _time_enabled
    1
    ;; bool _stdev_range_enabled
    1
    ;; bool _stdev_semimajor_enabled
    1
    ;; bool _stdev_semiminor_enabled
    1
    ;; bool _semimajor_orientation_enabled
    1
    ;; bool _stdev_latitude_enabled
    1
    ;; bool _stdev_longitude_enabled
    1
    ;; bool _stdev_altitude_enabled
    1
    ;; uint8 _source
    1
    ;; time _time
    8
    ;; float64 _stdev_range
    8
    ;; float64 _stdev_semimajor
    8
    ;; float64 _stdev_semiminor
    8
    ;; float64 _semimajor_orientation
    8
    ;; float64 _stdev_latitude
    8
    ;; float64 _stdev_longitude
    8
    ;; float64 _stdev_altitude
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _time_enabled
       (if _time_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _stdev_range_enabled
       (if _stdev_range_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _stdev_semimajor_enabled
       (if _stdev_semimajor_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _stdev_semiminor_enabled
       (if _stdev_semiminor_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _semimajor_orientation_enabled
       (if _semimajor_orientation_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _stdev_latitude_enabled
       (if _stdev_latitude_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _stdev_longitude_enabled
       (if _stdev_longitude_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _stdev_altitude_enabled
       (if _stdev_altitude_enabled (write-byte -1 s) (write-byte 0 s))
     ;; uint8 _source
       (write-byte _source s)
     ;; time _time
       (write-long (send _time :sec) s) (write-long (send _time :nsec) s)
     ;; float64 _stdev_range
       (sys::poke _stdev_range (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _stdev_semimajor
       (sys::poke _stdev_semimajor (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _stdev_semiminor
       (sys::poke _stdev_semiminor (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _semimajor_orientation
       (sys::poke _semimajor_orientation (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _stdev_latitude
       (sys::poke _stdev_latitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _stdev_longitude
       (sys::poke _stdev_longitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _stdev_altitude
       (sys::poke _stdev_altitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _time_enabled
     (setq _time_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _stdev_range_enabled
     (setq _stdev_range_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _stdev_semimajor_enabled
     (setq _stdev_semimajor_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _stdev_semiminor_enabled
     (setq _stdev_semiminor_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _semimajor_orientation_enabled
     (setq _semimajor_orientation_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _stdev_latitude_enabled
     (setq _stdev_latitude_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _stdev_longitude_enabled
     (setq _stdev_longitude_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _stdev_altitude_enabled
     (setq _stdev_altitude_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; uint8 _source
     (setq _source (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; time _time
     (send _time :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _time :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float64 _stdev_range
     (setq _stdev_range (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _stdev_semimajor
     (setq _stdev_semimajor (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _stdev_semiminor
     (setq _stdev_semiminor (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _semimajor_orientation
     (setq _semimajor_orientation (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _stdev_latitude
     (setq _stdev_latitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _stdev_longitude
     (setq _stdev_longitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _stdev_altitude
     (setq _stdev_altitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get gps_nmea::GpsGST :md5sum-) "46d580dc2c2da339f4753af61f94de20")
(setf (get gps_nmea::GpsGST :datatype-) "gps_nmea/GpsGST")
(setf (get gps_nmea::GpsGST :definition-)
      "Header header

bool time_enabled
bool stdev_range_enabled
bool stdev_semimajor_enabled
bool stdev_semiminor_enabled
bool semimajor_orientation_enabled
bool stdev_latitude_enabled
bool stdev_longitude_enabled
bool stdev_altitude_enabled

# Source GPS Fix
# 0 : Only GPS satellites are used
# 1 : Only GLONASS satellites are used
# 2 : Several constellations (GPS, SBAS, GLONASS) are used.
uint8 SATELLITE_SOURCE_GPS = 0
uint8 SATELLITE_SOURCE_GLONASS = 1
uint8 SATELLITE_SOURCE_SEVERAL = 2
uint8 source

# Current UTC time of position
time time
# RMS value of standard deviation of range inputs (DGNSS corrections included), in meters
float64 stdev_range
# Standard deviation of semi-major axis of error ellipse, in meters
float64 stdev_semimajor
# Standard deviation of semi-minor axis of error ellipse, in meters
float64 stdev_semiminor
# Orientation of semi-major axis of error ellipse, in degrees from true North
float64 semimajor_orientation
# Standard deviation of latitude error, in meters
float64 stdev_latitude
# Standard deviation of longitude error, in meters
float64 stdev_longitude
# Standard deviation of altitude error, in meters
float64 stdev_altitude

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :gps_nmea/GpsGST "46d580dc2c2da339f4753af61f94de20")


