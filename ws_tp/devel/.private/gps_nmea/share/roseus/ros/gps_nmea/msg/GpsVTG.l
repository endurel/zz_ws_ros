;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::GpsVTG)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'GpsVTG (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::GPSVTG")
  (make-package "GPS_NMEA::GPSVTG"))

(in-package "ROS")
;;//! \htmlinclude GpsVTG.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(intern "*GPS_MODE_AUTONOMOUS*" (find-package "GPS_NMEA::GPSVTG"))
(shadow '*GPS_MODE_AUTONOMOUS* (find-package "GPS_NMEA::GPSVTG"))
(defconstant gps_nmea::GpsVTG::*GPS_MODE_AUTONOMOUS* 0)
(intern "*GPS_MODE_DIFFERENTIAL*" (find-package "GPS_NMEA::GPSVTG"))
(shadow '*GPS_MODE_DIFFERENTIAL* (find-package "GPS_NMEA::GPSVTG"))
(defconstant gps_nmea::GpsVTG::*GPS_MODE_DIFFERENTIAL* 1)
(intern "*GPS_MODE_INVALID*" (find-package "GPS_NMEA::GPSVTG"))
(shadow '*GPS_MODE_INVALID* (find-package "GPS_NMEA::GPSVTG"))
(defconstant gps_nmea::GpsVTG::*GPS_MODE_INVALID* 2)

(defun gps_nmea::GpsVTG-to-symbol (const)
  (cond
        ((= const 0) 'gps_nmea::GpsVTG::*GPS_MODE_AUTONOMOUS*)
        ((= const 1) 'gps_nmea::GpsVTG::*GPS_MODE_DIFFERENTIAL*)
        ((= const 2) 'gps_nmea::GpsVTG::*GPS_MODE_INVALID*)
        (t nil)))

(defclass gps_nmea::GpsVTG
  :super ros::object
  :slots (_header _cog_true_enabled _cog_magnetic_enabled _sog_knots_enabled _sog_kmh_enabled _mode_enabled _cog_true _cog_magnetic _sog_knots _sog_kmh _mode ))

(defmethod gps_nmea::GpsVTG
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:cog_true_enabled __cog_true_enabled) nil)
    ((:cog_magnetic_enabled __cog_magnetic_enabled) nil)
    ((:sog_knots_enabled __sog_knots_enabled) nil)
    ((:sog_kmh_enabled __sog_kmh_enabled) nil)
    ((:mode_enabled __mode_enabled) nil)
    ((:cog_true __cog_true) 0.0)
    ((:cog_magnetic __cog_magnetic) 0.0)
    ((:sog_knots __sog_knots) 0.0)
    ((:sog_kmh __sog_kmh) 0.0)
    ((:mode __mode) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _cog_true_enabled __cog_true_enabled)
   (setq _cog_magnetic_enabled __cog_magnetic_enabled)
   (setq _sog_knots_enabled __sog_knots_enabled)
   (setq _sog_kmh_enabled __sog_kmh_enabled)
   (setq _mode_enabled __mode_enabled)
   (setq _cog_true (float __cog_true))
   (setq _cog_magnetic (float __cog_magnetic))
   (setq _sog_knots (float __sog_knots))
   (setq _sog_kmh (float __sog_kmh))
   (setq _mode (round __mode))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:cog_true_enabled
   (&optional (__cog_true_enabled :null))
   (if (not (eq __cog_true_enabled :null)) (setq _cog_true_enabled __cog_true_enabled)) _cog_true_enabled)
  (:cog_magnetic_enabled
   (&optional (__cog_magnetic_enabled :null))
   (if (not (eq __cog_magnetic_enabled :null)) (setq _cog_magnetic_enabled __cog_magnetic_enabled)) _cog_magnetic_enabled)
  (:sog_knots_enabled
   (&optional (__sog_knots_enabled :null))
   (if (not (eq __sog_knots_enabled :null)) (setq _sog_knots_enabled __sog_knots_enabled)) _sog_knots_enabled)
  (:sog_kmh_enabled
   (&optional (__sog_kmh_enabled :null))
   (if (not (eq __sog_kmh_enabled :null)) (setq _sog_kmh_enabled __sog_kmh_enabled)) _sog_kmh_enabled)
  (:mode_enabled
   (&optional (__mode_enabled :null))
   (if (not (eq __mode_enabled :null)) (setq _mode_enabled __mode_enabled)) _mode_enabled)
  (:cog_true
   (&optional __cog_true)
   (if __cog_true (setq _cog_true __cog_true)) _cog_true)
  (:cog_magnetic
   (&optional __cog_magnetic)
   (if __cog_magnetic (setq _cog_magnetic __cog_magnetic)) _cog_magnetic)
  (:sog_knots
   (&optional __sog_knots)
   (if __sog_knots (setq _sog_knots __sog_knots)) _sog_knots)
  (:sog_kmh
   (&optional __sog_kmh)
   (if __sog_kmh (setq _sog_kmh __sog_kmh)) _sog_kmh)
  (:mode
   (&optional __mode)
   (if __mode (setq _mode __mode)) _mode)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _cog_true_enabled
    1
    ;; bool _cog_magnetic_enabled
    1
    ;; bool _sog_knots_enabled
    1
    ;; bool _sog_kmh_enabled
    1
    ;; bool _mode_enabled
    1
    ;; float64 _cog_true
    8
    ;; float64 _cog_magnetic
    8
    ;; float64 _sog_knots
    8
    ;; float64 _sog_kmh
    8
    ;; uint8 _mode
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _cog_true_enabled
       (if _cog_true_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _cog_magnetic_enabled
       (if _cog_magnetic_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _sog_knots_enabled
       (if _sog_knots_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _sog_kmh_enabled
       (if _sog_kmh_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _mode_enabled
       (if _mode_enabled (write-byte -1 s) (write-byte 0 s))
     ;; float64 _cog_true
       (sys::poke _cog_true (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _cog_magnetic
       (sys::poke _cog_magnetic (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _sog_knots
       (sys::poke _sog_knots (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _sog_kmh
       (sys::poke _sog_kmh (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; uint8 _mode
       (write-byte _mode s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _cog_true_enabled
     (setq _cog_true_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _cog_magnetic_enabled
     (setq _cog_magnetic_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _sog_knots_enabled
     (setq _sog_knots_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _sog_kmh_enabled
     (setq _sog_kmh_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _mode_enabled
     (setq _mode_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float64 _cog_true
     (setq _cog_true (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _cog_magnetic
     (setq _cog_magnetic (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _sog_knots
     (setq _sog_knots (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _sog_kmh
     (setq _sog_kmh (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; uint8 _mode
     (setq _mode (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get gps_nmea::GpsVTG :md5sum-) "61315a94881ac3a1456599c4e29fa56f")
(setf (get gps_nmea::GpsVTG :datatype-) "gps_nmea/GpsVTG")
(setf (get gps_nmea::GpsVTG :definition-)
      "Header header

bool cog_true_enabled
bool cog_magnetic_enabled
bool sog_knots_enabled
bool sog_kmh_enabled
bool mode_enabled

#Course over ground orientation with respect to true north
float64 cog_true
# Course over ground orientation with respect to magnetic north
float64 cog_magnetic
# Speed over ground in knots
float64 sog_knots
#Speed over ground in km/h
float64 sog_kmh

#GPS mode 
# 0 : Autonomous mode
# 1 : Differential mode
# 2 : Data not valid
uint8 GPS_MODE_AUTONOMOUS = 0
uint8 GPS_MODE_DIFFERENTIAL = 1
uint8 GPS_MODE_INVALID = 2
uint8 mode

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :gps_nmea/GpsVTG "61315a94881ac3a1456599c4e29fa56f")


