;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::GpsRMC)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'GpsRMC (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::GPSRMC")
  (make-package "GPS_NMEA::GPSRMC"))

(in-package "ROS")
;;//! \htmlinclude GpsRMC.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(intern "*POSITION_VALID_VOID*" (find-package "GPS_NMEA::GPSRMC"))
(shadow '*POSITION_VALID_VOID* (find-package "GPS_NMEA::GPSRMC"))
(defconstant gps_nmea::GpsRMC::*POSITION_VALID_VOID* 0)
(intern "*POSITION_VALID_ACTIVE*" (find-package "GPS_NMEA::GPSRMC"))
(shadow '*POSITION_VALID_ACTIVE* (find-package "GPS_NMEA::GPSRMC"))
(defconstant gps_nmea::GpsRMC::*POSITION_VALID_ACTIVE* 1)
(intern "*MAGNETIC_VARIATION_EAST*" (find-package "GPS_NMEA::GPSRMC"))
(shadow '*MAGNETIC_VARIATION_EAST* (find-package "GPS_NMEA::GPSRMC"))
(defconstant gps_nmea::GpsRMC::*MAGNETIC_VARIATION_EAST* 0)
(intern "*MAGNETIC_VARIATION_WEST*" (find-package "GPS_NMEA::GPSRMC"))
(shadow '*MAGNETIC_VARIATION_WEST* (find-package "GPS_NMEA::GPSRMC"))
(defconstant gps_nmea::GpsRMC::*MAGNETIC_VARIATION_WEST* 1)
(intern "*GPS_ENABLED_MODE_INVALID*" (find-package "GPS_NMEA::GPSRMC"))
(shadow '*GPS_ENABLED_MODE_INVALID* (find-package "GPS_NMEA::GPSRMC"))
(defconstant gps_nmea::GpsRMC::*GPS_ENABLED_MODE_INVALID* 0)
(intern "*GPS_ENABLED_MODE_AUTONOMOUS*" (find-package "GPS_NMEA::GPSRMC"))
(shadow '*GPS_ENABLED_MODE_AUTONOMOUS* (find-package "GPS_NMEA::GPSRMC"))
(defconstant gps_nmea::GpsRMC::*GPS_ENABLED_MODE_AUTONOMOUS* 1)
(intern "*GPS_ENABLED_MODE_DIFFERENTIAL*" (find-package "GPS_NMEA::GPSRMC"))
(shadow '*GPS_ENABLED_MODE_DIFFERENTIAL* (find-package "GPS_NMEA::GPSRMC"))
(defconstant gps_nmea::GpsRMC::*GPS_ENABLED_MODE_DIFFERENTIAL* 2)
(intern "*GPS_ENABLED_MODE_ESTIMATED*" (find-package "GPS_NMEA::GPSRMC"))
(shadow '*GPS_ENABLED_MODE_ESTIMATED* (find-package "GPS_NMEA::GPSRMC"))
(defconstant gps_nmea::GpsRMC::*GPS_ENABLED_MODE_ESTIMATED* 3)

(defun gps_nmea::GpsRMC-to-symbol (const)
  (cond
        ((= const 0) 'gps_nmea::GpsRMC::*POSITION_VALID_VOID*)
        ((= const 1) 'gps_nmea::GpsRMC::*POSITION_VALID_ACTIVE*)
        ((= const 0) 'gps_nmea::GpsRMC::*MAGNETIC_VARIATION_EAST*)
        ((= const 1) 'gps_nmea::GpsRMC::*MAGNETIC_VARIATION_WEST*)
        ((= const 0) 'gps_nmea::GpsRMC::*GPS_ENABLED_MODE_INVALID*)
        ((= const 1) 'gps_nmea::GpsRMC::*GPS_ENABLED_MODE_AUTONOMOUS*)
        ((= const 2) 'gps_nmea::GpsRMC::*GPS_ENABLED_MODE_DIFFERENTIAL*)
        ((= const 3) 'gps_nmea::GpsRMC::*GPS_ENABLED_MODE_ESTIMATED*)
        (t nil)))

(defclass gps_nmea::GpsRMC
  :super ros::object
  :slots (_header _time_enabled _position_valid_enabled _position_enabled _speedOverGround_enabled _trackAngle_enabled _date_enabled _magneticVariation_enabled _mode_enabled _time _position_valid _position _speedOverGround _trackAngle _date _magneticVariation _magnetic_var_direction _mode ))

(defmethod gps_nmea::GpsRMC
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:time_enabled __time_enabled) nil)
    ((:position_valid_enabled __position_valid_enabled) nil)
    ((:position_enabled __position_enabled) nil)
    ((:speedOverGround_enabled __speedOverGround_enabled) nil)
    ((:trackAngle_enabled __trackAngle_enabled) nil)
    ((:date_enabled __date_enabled) nil)
    ((:magneticVariation_enabled __magneticVariation_enabled) nil)
    ((:mode_enabled __mode_enabled) nil)
    ((:time __time) (instance ros::time :init))
    ((:position_valid __position_valid) 0)
    ((:position __position) (instance gps_nmea::WGS84 :init))
    ((:speedOverGround __speedOverGround) 0.0)
    ((:trackAngle __trackAngle) 0.0)
    ((:date __date) (instance gps_nmea::Date_gps :init))
    ((:magneticVariation __magneticVariation) 0.0)
    ((:magnetic_var_direction __magnetic_var_direction) 0)
    ((:mode __mode) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _time_enabled __time_enabled)
   (setq _position_valid_enabled __position_valid_enabled)
   (setq _position_enabled __position_enabled)
   (setq _speedOverGround_enabled __speedOverGround_enabled)
   (setq _trackAngle_enabled __trackAngle_enabled)
   (setq _date_enabled __date_enabled)
   (setq _magneticVariation_enabled __magneticVariation_enabled)
   (setq _mode_enabled __mode_enabled)
   (setq _time __time)
   (setq _position_valid (round __position_valid))
   (setq _position __position)
   (setq _speedOverGround (float __speedOverGround))
   (setq _trackAngle (float __trackAngle))
   (setq _date __date)
   (setq _magneticVariation (float __magneticVariation))
   (setq _magnetic_var_direction (round __magnetic_var_direction))
   (setq _mode (round __mode))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:time_enabled
   (&optional (__time_enabled :null))
   (if (not (eq __time_enabled :null)) (setq _time_enabled __time_enabled)) _time_enabled)
  (:position_valid_enabled
   (&optional (__position_valid_enabled :null))
   (if (not (eq __position_valid_enabled :null)) (setq _position_valid_enabled __position_valid_enabled)) _position_valid_enabled)
  (:position_enabled
   (&optional (__position_enabled :null))
   (if (not (eq __position_enabled :null)) (setq _position_enabled __position_enabled)) _position_enabled)
  (:speedOverGround_enabled
   (&optional (__speedOverGround_enabled :null))
   (if (not (eq __speedOverGround_enabled :null)) (setq _speedOverGround_enabled __speedOverGround_enabled)) _speedOverGround_enabled)
  (:trackAngle_enabled
   (&optional (__trackAngle_enabled :null))
   (if (not (eq __trackAngle_enabled :null)) (setq _trackAngle_enabled __trackAngle_enabled)) _trackAngle_enabled)
  (:date_enabled
   (&optional (__date_enabled :null))
   (if (not (eq __date_enabled :null)) (setq _date_enabled __date_enabled)) _date_enabled)
  (:magneticVariation_enabled
   (&optional (__magneticVariation_enabled :null))
   (if (not (eq __magneticVariation_enabled :null)) (setq _magneticVariation_enabled __magneticVariation_enabled)) _magneticVariation_enabled)
  (:mode_enabled
   (&optional (__mode_enabled :null))
   (if (not (eq __mode_enabled :null)) (setq _mode_enabled __mode_enabled)) _mode_enabled)
  (:time
   (&optional __time)
   (if __time (setq _time __time)) _time)
  (:position_valid
   (&optional __position_valid)
   (if __position_valid (setq _position_valid __position_valid)) _position_valid)
  (:position
   (&rest __position)
   (if (keywordp (car __position))
       (send* _position __position)
     (progn
       (if __position (setq _position (car __position)))
       _position)))
  (:speedOverGround
   (&optional __speedOverGround)
   (if __speedOverGround (setq _speedOverGround __speedOverGround)) _speedOverGround)
  (:trackAngle
   (&optional __trackAngle)
   (if __trackAngle (setq _trackAngle __trackAngle)) _trackAngle)
  (:date
   (&rest __date)
   (if (keywordp (car __date))
       (send* _date __date)
     (progn
       (if __date (setq _date (car __date)))
       _date)))
  (:magneticVariation
   (&optional __magneticVariation)
   (if __magneticVariation (setq _magneticVariation __magneticVariation)) _magneticVariation)
  (:magnetic_var_direction
   (&optional __magnetic_var_direction)
   (if __magnetic_var_direction (setq _magnetic_var_direction __magnetic_var_direction)) _magnetic_var_direction)
  (:mode
   (&optional __mode)
   (if __mode (setq _mode __mode)) _mode)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _time_enabled
    1
    ;; bool _position_valid_enabled
    1
    ;; bool _position_enabled
    1
    ;; bool _speedOverGround_enabled
    1
    ;; bool _trackAngle_enabled
    1
    ;; bool _date_enabled
    1
    ;; bool _magneticVariation_enabled
    1
    ;; bool _mode_enabled
    1
    ;; time _time
    8
    ;; uint8 _position_valid
    1
    ;; gps_nmea/WGS84 _position
    (send _position :serialization-length)
    ;; float64 _speedOverGround
    8
    ;; float64 _trackAngle
    8
    ;; gps_nmea/Date_gps _date
    (send _date :serialization-length)
    ;; float64 _magneticVariation
    8
    ;; uint8 _magnetic_var_direction
    1
    ;; uint8 _mode
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _time_enabled
       (if _time_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _position_valid_enabled
       (if _position_valid_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _position_enabled
       (if _position_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _speedOverGround_enabled
       (if _speedOverGround_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _trackAngle_enabled
       (if _trackAngle_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _date_enabled
       (if _date_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _magneticVariation_enabled
       (if _magneticVariation_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _mode_enabled
       (if _mode_enabled (write-byte -1 s) (write-byte 0 s))
     ;; time _time
       (write-long (send _time :sec) s) (write-long (send _time :nsec) s)
     ;; uint8 _position_valid
       (write-byte _position_valid s)
     ;; gps_nmea/WGS84 _position
       (send _position :serialize s)
     ;; float64 _speedOverGround
       (sys::poke _speedOverGround (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _trackAngle
       (sys::poke _trackAngle (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; gps_nmea/Date_gps _date
       (send _date :serialize s)
     ;; float64 _magneticVariation
       (sys::poke _magneticVariation (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; uint8 _magnetic_var_direction
       (write-byte _magnetic_var_direction s)
     ;; uint8 _mode
       (write-byte _mode s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _time_enabled
     (setq _time_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _position_valid_enabled
     (setq _position_valid_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _position_enabled
     (setq _position_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _speedOverGround_enabled
     (setq _speedOverGround_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _trackAngle_enabled
     (setq _trackAngle_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _date_enabled
     (setq _date_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _magneticVariation_enabled
     (setq _magneticVariation_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _mode_enabled
     (setq _mode_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; time _time
     (send _time :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _time :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint8 _position_valid
     (setq _position_valid (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; gps_nmea/WGS84 _position
     (send _position :deserialize buf ptr-) (incf ptr- (send _position :serialization-length))
   ;; float64 _speedOverGround
     (setq _speedOverGround (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _trackAngle
     (setq _trackAngle (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; gps_nmea/Date_gps _date
     (send _date :deserialize buf ptr-) (incf ptr- (send _date :serialization-length))
   ;; float64 _magneticVariation
     (setq _magneticVariation (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; uint8 _magnetic_var_direction
     (setq _magnetic_var_direction (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _mode
     (setq _mode (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get gps_nmea::GpsRMC :md5sum-) "ecae7bc96c99e205f05bee9f6e358c53")
(setf (get gps_nmea::GpsRMC :datatype-) "gps_nmea/GpsRMC")
(setf (get gps_nmea::GpsRMC :definition-)
      "Header header

bool time_enabled
bool position_valid_enabled
bool position_enabled
bool speedOverGround_enabled
bool trackAngle_enabled
bool date_enabled
bool magneticVariation_enabled
bool mode_enabled

# Current UTC time
time time

# Position Status
# 0 : NAV Receiver warning
# 1 : Valid position
uint8 POSITION_VALID_VOID = 0
uint8 POSITION_VALID_ACTIVE = 1
uint8 position_valid

# WGS84 latitude and longitude
WGS84 position
# Vitesse curviligne (m/s)
float64 speedOverGround
# Cap en degres (0deg=360deg is north)
float64 trackAngle

# Current UTC date
Date_gps date
# Magnetic variation
float64  magneticVariation

# Magnetic variation magnetic_var_direction
uint8 MAGNETIC_VARIATION_EAST = 0
uint8 MAGNETIC_VARIATION_WEST = 1
uint8 magnetic_var_direction

# Position mode
# 0 : Data not valid
# 1 : Autonomous mode
# 2 : Differential mode
# 3 : Estimated mode
uint8 GPS_ENABLED_MODE_INVALID = 0
uint8 GPS_ENABLED_MODE_AUTONOMOUS = 1
uint8 GPS_ENABLED_MODE_DIFFERENTIAL = 2
uint8 GPS_ENABLED_MODE_ESTIMATED = 3
uint8 mode

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: gps_nmea/WGS84
# Latitude (degrees). Positive is north of equator; negative is south. 
# +: N, -: S --- unit: degrees
float64 latitude

# Longitude (degrees). Positive is east of prime meridian, negative west.
# +: E, -: W --- unit: degrees, origin: Greenwich
float64 longitude
================================================================================
MSG: gps_nmea/Date_gps
Header header
# 1-31
uint8 day
# 1-12
uint8 month
# 00+ (2000)
uint8 year
")



(provide :gps_nmea/GpsRMC "ecae7bc96c99e205f05bee9f6e358c53")


