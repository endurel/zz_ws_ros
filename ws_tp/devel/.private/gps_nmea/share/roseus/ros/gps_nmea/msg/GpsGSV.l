;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::GpsGSV)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'GpsGSV (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::GPSGSV")
  (make-package "GPS_NMEA::GPSGSV"))

(in-package "ROS")
;;//! \htmlinclude GpsGSV.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass gps_nmea::GpsGSV
  :super ros::object
  :slots (_header _total_nb_messages_enabled _message_number_enabled _total_nb_satellites_in_view_enabled _total_nb_messages _message_number _total_nb_satellites_in_view _satellite_PRN_nb _elevation _azimuth _snr ))

(defmethod gps_nmea::GpsGSV
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:total_nb_messages_enabled __total_nb_messages_enabled) nil)
    ((:message_number_enabled __message_number_enabled) nil)
    ((:total_nb_satellites_in_view_enabled __total_nb_satellites_in_view_enabled) nil)
    ((:total_nb_messages __total_nb_messages) 0)
    ((:message_number __message_number) 0)
    ((:total_nb_satellites_in_view __total_nb_satellites_in_view) 0)
    ((:satellite_PRN_nb __satellite_PRN_nb) (make-array 4 :initial-element 0 :element-type :char))
    ((:elevation __elevation) (make-array 4 :initial-element 0 :element-type :char))
    ((:azimuth __azimuth) (make-array 4 :initial-element 0 :element-type :char))
    ((:snr __snr) (make-array 4 :initial-element 0 :element-type :char))
    )
   (send-super :init)
   (setq _header __header)
   (setq _total_nb_messages_enabled __total_nb_messages_enabled)
   (setq _message_number_enabled __message_number_enabled)
   (setq _total_nb_satellites_in_view_enabled __total_nb_satellites_in_view_enabled)
   (setq _total_nb_messages (round __total_nb_messages))
   (setq _message_number (round __message_number))
   (setq _total_nb_satellites_in_view (round __total_nb_satellites_in_view))
   (setq _satellite_PRN_nb __satellite_PRN_nb)
   (setq _elevation __elevation)
   (setq _azimuth __azimuth)
   (setq _snr __snr)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:total_nb_messages_enabled
   (&optional (__total_nb_messages_enabled :null))
   (if (not (eq __total_nb_messages_enabled :null)) (setq _total_nb_messages_enabled __total_nb_messages_enabled)) _total_nb_messages_enabled)
  (:message_number_enabled
   (&optional (__message_number_enabled :null))
   (if (not (eq __message_number_enabled :null)) (setq _message_number_enabled __message_number_enabled)) _message_number_enabled)
  (:total_nb_satellites_in_view_enabled
   (&optional (__total_nb_satellites_in_view_enabled :null))
   (if (not (eq __total_nb_satellites_in_view_enabled :null)) (setq _total_nb_satellites_in_view_enabled __total_nb_satellites_in_view_enabled)) _total_nb_satellites_in_view_enabled)
  (:total_nb_messages
   (&optional __total_nb_messages)
   (if __total_nb_messages (setq _total_nb_messages __total_nb_messages)) _total_nb_messages)
  (:message_number
   (&optional __message_number)
   (if __message_number (setq _message_number __message_number)) _message_number)
  (:total_nb_satellites_in_view
   (&optional __total_nb_satellites_in_view)
   (if __total_nb_satellites_in_view (setq _total_nb_satellites_in_view __total_nb_satellites_in_view)) _total_nb_satellites_in_view)
  (:satellite_PRN_nb
   (&optional __satellite_PRN_nb)
   (if __satellite_PRN_nb (setq _satellite_PRN_nb __satellite_PRN_nb)) _satellite_PRN_nb)
  (:elevation
   (&optional __elevation)
   (if __elevation (setq _elevation __elevation)) _elevation)
  (:azimuth
   (&optional __azimuth)
   (if __azimuth (setq _azimuth __azimuth)) _azimuth)
  (:snr
   (&optional __snr)
   (if __snr (setq _snr __snr)) _snr)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _total_nb_messages_enabled
    1
    ;; bool _message_number_enabled
    1
    ;; bool _total_nb_satellites_in_view_enabled
    1
    ;; uint8 _total_nb_messages
    1
    ;; uint8 _message_number
    1
    ;; uint8 _total_nb_satellites_in_view
    1
    ;; uint8[4] _satellite_PRN_nb
    (* 1    4)
    ;; uint8[4] _elevation
    (* 1    4)
    ;; uint8[4] _azimuth
    (* 1    4)
    ;; uint8[4] _snr
    (* 1    4)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _total_nb_messages_enabled
       (if _total_nb_messages_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _message_number_enabled
       (if _message_number_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _total_nb_satellites_in_view_enabled
       (if _total_nb_satellites_in_view_enabled (write-byte -1 s) (write-byte 0 s))
     ;; uint8 _total_nb_messages
       (write-byte _total_nb_messages s)
     ;; uint8 _message_number
       (write-byte _message_number s)
     ;; uint8 _total_nb_satellites_in_view
       (write-byte _total_nb_satellites_in_view s)
     ;; uint8[4] _satellite_PRN_nb
     (princ _satellite_PRN_nb s)
     ;; uint8[4] _elevation
     (princ _elevation s)
     ;; uint8[4] _azimuth
     (princ _azimuth s)
     ;; uint8[4] _snr
     (princ _snr s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _total_nb_messages_enabled
     (setq _total_nb_messages_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _message_number_enabled
     (setq _message_number_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _total_nb_satellites_in_view_enabled
     (setq _total_nb_satellites_in_view_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; uint8 _total_nb_messages
     (setq _total_nb_messages (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _message_number
     (setq _message_number (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _total_nb_satellites_in_view
     (setq _total_nb_satellites_in_view (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8[4] _satellite_PRN_nb
   (setq _satellite_PRN_nb (make-array 4 :element-type :char))
   (replace _satellite_PRN_nb buf :start2 ptr-) (incf ptr- 4)
   ;; uint8[4] _elevation
   (setq _elevation (make-array 4 :element-type :char))
   (replace _elevation buf :start2 ptr-) (incf ptr- 4)
   ;; uint8[4] _azimuth
   (setq _azimuth (make-array 4 :element-type :char))
   (replace _azimuth buf :start2 ptr-) (incf ptr- 4)
   ;; uint8[4] _snr
   (setq _snr (make-array 4 :element-type :char))
   (replace _snr buf :start2 ptr-) (incf ptr- 4)
   ;;
   self)
  )

(setf (get gps_nmea::GpsGSV :md5sum-) "cdc5e950abcc9b46a1d184c241ffb895")
(setf (get gps_nmea::GpsGSV :datatype-) "gps_nmea/GpsGSV")
(setf (get gps_nmea::GpsGSV :definition-)
      "Header header

bool total_nb_messages_enabled
bool message_number_enabled
bool total_nb_satellites_in_view_enabled


# Total number of messages of this type in this cycle
uint8 total_nb_messages
# Message number
uint8 message_number
# Total number of SVs in view
uint8 total_nb_satellites_in_view

uint8[4] satellite_PRN_nb
uint8[4] elevation
uint8[4] azimuth 
uint8[4] snr

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :gps_nmea/GpsGSV "cdc5e950abcc9b46a1d184c241ffb895")


