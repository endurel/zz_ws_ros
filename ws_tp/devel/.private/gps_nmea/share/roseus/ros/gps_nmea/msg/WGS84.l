;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::WGS84)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'WGS84 (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::WGS84")
  (make-package "GPS_NMEA::WGS84"))

(in-package "ROS")
;;//! \htmlinclude WGS84.msg.html


(defclass gps_nmea::WGS84
  :super ros::object
  :slots (_latitude _longitude ))

(defmethod gps_nmea::WGS84
  (:init
   (&key
    ((:latitude __latitude) 0.0)
    ((:longitude __longitude) 0.0)
    )
   (send-super :init)
   (setq _latitude (float __latitude))
   (setq _longitude (float __longitude))
   self)
  (:latitude
   (&optional __latitude)
   (if __latitude (setq _latitude __latitude)) _latitude)
  (:longitude
   (&optional __longitude)
   (if __longitude (setq _longitude __longitude)) _longitude)
  (:serialization-length
   ()
   (+
    ;; float64 _latitude
    8
    ;; float64 _longitude
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _latitude
       (sys::poke _latitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _longitude
       (sys::poke _longitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _latitude
     (setq _latitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _longitude
     (setq _longitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get gps_nmea::WGS84 :md5sum-) "680c6dc7da65a2421a822205dcbdb600")
(setf (get gps_nmea::WGS84 :datatype-) "gps_nmea/WGS84")
(setf (get gps_nmea::WGS84 :definition-)
      "# Latitude (degrees). Positive is north of equator; negative is south. 
# +: N, -: S --- unit: degrees
float64 latitude

# Longitude (degrees). Positive is east of prime meridian, negative west.
# +: E, -: W --- unit: degrees, origin: Greenwich
float64 longitude
")



(provide :gps_nmea/WGS84 "680c6dc7da65a2421a822205dcbdb600")


