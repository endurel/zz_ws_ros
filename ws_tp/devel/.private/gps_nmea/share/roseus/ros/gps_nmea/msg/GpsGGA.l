;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::GpsGGA)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'GpsGGA (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::GPSGGA")
  (make-package "GPS_NMEA::GPSGGA"))

(in-package "ROS")
;;//! \htmlinclude GpsGGA.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(intern "*POSITION_TYPE_INVALID*" (find-package "GPS_NMEA::GPSGGA"))
(shadow '*POSITION_TYPE_INVALID* (find-package "GPS_NMEA::GPSGGA"))
(defconstant gps_nmea::GpsGGA::*POSITION_TYPE_INVALID* 0)
(intern "*POSITION_TYPE_AUTONOMOUS*" (find-package "GPS_NMEA::GPSGGA"))
(shadow '*POSITION_TYPE_AUTONOMOUS* (find-package "GPS_NMEA::GPSGGA"))
(defconstant gps_nmea::GpsGGA::*POSITION_TYPE_AUTONOMOUS* 1)
(intern "*POSITION_TYPE_DIFFERENTIAL*" (find-package "GPS_NMEA::GPSGGA"))
(shadow '*POSITION_TYPE_DIFFERENTIAL* (find-package "GPS_NMEA::GPSGGA"))
(defconstant gps_nmea::GpsGGA::*POSITION_TYPE_DIFFERENTIAL* 2)
(intern "*POSITION_TYPE_RTKFIXED*" (find-package "GPS_NMEA::GPSGGA"))
(shadow '*POSITION_TYPE_RTKFIXED* (find-package "GPS_NMEA::GPSGGA"))
(defconstant gps_nmea::GpsGGA::*POSITION_TYPE_RTKFIXED* 4)
(intern "*POSITION_TYPE_RTKFLOAT*" (find-package "GPS_NMEA::GPSGGA"))
(shadow '*POSITION_TYPE_RTKFLOAT* (find-package "GPS_NMEA::GPSGGA"))
(defconstant gps_nmea::GpsGGA::*POSITION_TYPE_RTKFLOAT* 5)

(defun gps_nmea::GpsGGA-to-symbol (const)
  (cond
        ((= const 0) 'gps_nmea::GpsGGA::*POSITION_TYPE_INVALID*)
        ((= const 1) 'gps_nmea::GpsGGA::*POSITION_TYPE_AUTONOMOUS*)
        ((= const 2) 'gps_nmea::GpsGGA::*POSITION_TYPE_DIFFERENTIAL*)
        ((= const 4) 'gps_nmea::GpsGGA::*POSITION_TYPE_RTKFIXED*)
        ((= const 5) 'gps_nmea::GpsGGA::*POSITION_TYPE_RTKFLOAT*)
        (t nil)))

(defclass gps_nmea::GpsGGA
  :super ros::object
  :slots (_header _time_enabled _position_enabled _position_type_enabled _satellites_enabled _hdop_enabled _altitude_enabled _geoidal_separation_enabled _diff_age_enabled _base_id_enabled _time _position _position_type _satellites _hdop _altitude _geoidal_separation _diff_age _base_station_id ))

(defmethod gps_nmea::GpsGGA
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:time_enabled __time_enabled) nil)
    ((:position_enabled __position_enabled) nil)
    ((:position_type_enabled __position_type_enabled) nil)
    ((:satellites_enabled __satellites_enabled) nil)
    ((:hdop_enabled __hdop_enabled) nil)
    ((:altitude_enabled __altitude_enabled) nil)
    ((:geoidal_separation_enabled __geoidal_separation_enabled) nil)
    ((:diff_age_enabled __diff_age_enabled) nil)
    ((:base_id_enabled __base_id_enabled) nil)
    ((:time __time) (instance ros::time :init))
    ((:position __position) (instance gps_nmea::WGS84 :init))
    ((:position_type __position_type) 0)
    ((:satellites __satellites) 0)
    ((:hdop __hdop) 0.0)
    ((:altitude __altitude) 0.0)
    ((:geoidal_separation __geoidal_separation) 0.0)
    ((:diff_age __diff_age) (instance ros::time :init))
    ((:base_station_id __base_station_id) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _time_enabled __time_enabled)
   (setq _position_enabled __position_enabled)
   (setq _position_type_enabled __position_type_enabled)
   (setq _satellites_enabled __satellites_enabled)
   (setq _hdop_enabled __hdop_enabled)
   (setq _altitude_enabled __altitude_enabled)
   (setq _geoidal_separation_enabled __geoidal_separation_enabled)
   (setq _diff_age_enabled __diff_age_enabled)
   (setq _base_id_enabled __base_id_enabled)
   (setq _time __time)
   (setq _position __position)
   (setq _position_type (round __position_type))
   (setq _satellites (round __satellites))
   (setq _hdop (float __hdop))
   (setq _altitude (float __altitude))
   (setq _geoidal_separation (float __geoidal_separation))
   (setq _diff_age __diff_age)
   (setq _base_station_id (round __base_station_id))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:time_enabled
   (&optional (__time_enabled :null))
   (if (not (eq __time_enabled :null)) (setq _time_enabled __time_enabled)) _time_enabled)
  (:position_enabled
   (&optional (__position_enabled :null))
   (if (not (eq __position_enabled :null)) (setq _position_enabled __position_enabled)) _position_enabled)
  (:position_type_enabled
   (&optional (__position_type_enabled :null))
   (if (not (eq __position_type_enabled :null)) (setq _position_type_enabled __position_type_enabled)) _position_type_enabled)
  (:satellites_enabled
   (&optional (__satellites_enabled :null))
   (if (not (eq __satellites_enabled :null)) (setq _satellites_enabled __satellites_enabled)) _satellites_enabled)
  (:hdop_enabled
   (&optional (__hdop_enabled :null))
   (if (not (eq __hdop_enabled :null)) (setq _hdop_enabled __hdop_enabled)) _hdop_enabled)
  (:altitude_enabled
   (&optional (__altitude_enabled :null))
   (if (not (eq __altitude_enabled :null)) (setq _altitude_enabled __altitude_enabled)) _altitude_enabled)
  (:geoidal_separation_enabled
   (&optional (__geoidal_separation_enabled :null))
   (if (not (eq __geoidal_separation_enabled :null)) (setq _geoidal_separation_enabled __geoidal_separation_enabled)) _geoidal_separation_enabled)
  (:diff_age_enabled
   (&optional (__diff_age_enabled :null))
   (if (not (eq __diff_age_enabled :null)) (setq _diff_age_enabled __diff_age_enabled)) _diff_age_enabled)
  (:base_id_enabled
   (&optional (__base_id_enabled :null))
   (if (not (eq __base_id_enabled :null)) (setq _base_id_enabled __base_id_enabled)) _base_id_enabled)
  (:time
   (&optional __time)
   (if __time (setq _time __time)) _time)
  (:position
   (&rest __position)
   (if (keywordp (car __position))
       (send* _position __position)
     (progn
       (if __position (setq _position (car __position)))
       _position)))
  (:position_type
   (&optional __position_type)
   (if __position_type (setq _position_type __position_type)) _position_type)
  (:satellites
   (&optional __satellites)
   (if __satellites (setq _satellites __satellites)) _satellites)
  (:hdop
   (&optional __hdop)
   (if __hdop (setq _hdop __hdop)) _hdop)
  (:altitude
   (&optional __altitude)
   (if __altitude (setq _altitude __altitude)) _altitude)
  (:geoidal_separation
   (&optional __geoidal_separation)
   (if __geoidal_separation (setq _geoidal_separation __geoidal_separation)) _geoidal_separation)
  (:diff_age
   (&optional __diff_age)
   (if __diff_age (setq _diff_age __diff_age)) _diff_age)
  (:base_station_id
   (&optional __base_station_id)
   (if __base_station_id (setq _base_station_id __base_station_id)) _base_station_id)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _time_enabled
    1
    ;; bool _position_enabled
    1
    ;; bool _position_type_enabled
    1
    ;; bool _satellites_enabled
    1
    ;; bool _hdop_enabled
    1
    ;; bool _altitude_enabled
    1
    ;; bool _geoidal_separation_enabled
    1
    ;; bool _diff_age_enabled
    1
    ;; bool _base_id_enabled
    1
    ;; time _time
    8
    ;; gps_nmea/WGS84 _position
    (send _position :serialization-length)
    ;; uint8 _position_type
    1
    ;; uint8 _satellites
    1
    ;; float64 _hdop
    8
    ;; float64 _altitude
    8
    ;; float64 _geoidal_separation
    8
    ;; duration _diff_age
    8
    ;; uint8 _base_station_id
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _time_enabled
       (if _time_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _position_enabled
       (if _position_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _position_type_enabled
       (if _position_type_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _satellites_enabled
       (if _satellites_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _hdop_enabled
       (if _hdop_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _altitude_enabled
       (if _altitude_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _geoidal_separation_enabled
       (if _geoidal_separation_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _diff_age_enabled
       (if _diff_age_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _base_id_enabled
       (if _base_id_enabled (write-byte -1 s) (write-byte 0 s))
     ;; time _time
       (write-long (send _time :sec) s) (write-long (send _time :nsec) s)
     ;; gps_nmea/WGS84 _position
       (send _position :serialize s)
     ;; uint8 _position_type
       (write-byte _position_type s)
     ;; uint8 _satellites
       (write-byte _satellites s)
     ;; float64 _hdop
       (sys::poke _hdop (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _altitude
       (sys::poke _altitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _geoidal_separation
       (sys::poke _geoidal_separation (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; duration _diff_age
       (write-long (send _diff_age :sec) s) (write-long (send _diff_age :nsec) s)
     ;; uint8 _base_station_id
       (write-byte _base_station_id s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _time_enabled
     (setq _time_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _position_enabled
     (setq _position_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _position_type_enabled
     (setq _position_type_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _satellites_enabled
     (setq _satellites_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _hdop_enabled
     (setq _hdop_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _altitude_enabled
     (setq _altitude_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _geoidal_separation_enabled
     (setq _geoidal_separation_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _diff_age_enabled
     (setq _diff_age_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _base_id_enabled
     (setq _base_id_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; time _time
     (send _time :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _time :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; gps_nmea/WGS84 _position
     (send _position :deserialize buf ptr-) (incf ptr- (send _position :serialization-length))
   ;; uint8 _position_type
     (setq _position_type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _satellites
     (setq _satellites (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; float64 _hdop
     (setq _hdop (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _altitude
     (setq _altitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _geoidal_separation
     (setq _geoidal_separation (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; duration _diff_age
     (send _diff_age :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _diff_age :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint8 _base_station_id
     (setq _base_station_id (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get gps_nmea::GpsGGA :md5sum-) "18c21ea63ef156a3d80f5cebf18705d1")
(setf (get gps_nmea::GpsGGA :datatype-) "gps_nmea/GpsGGA")
(setf (get gps_nmea::GpsGGA :definition-)
      "Header header

bool time_enabled
bool position_enabled
bool position_type_enabled
bool satellites_enabled
bool hdop_enabled
bool altitude_enabled
bool geoidal_separation_enabled
bool diff_age_enabled
bool base_id_enabled

# Current UTC time
time time
# WGS84 latitude and longitude
WGS84 position  

# Position type
# 0 : Position not available or invalid
# 1 : Autonomous position
# 2 : RTCM Differential SBAS Differential
# 4 : RTK fixed
# 5 : RTK float

uint8 POSITION_TYPE_INVALID = 0
uint8 POSITION_TYPE_AUTONOMOUS = 1
uint8 POSITION_TYPE_DIFFERENTIAL = 2
uint8 POSITION_TYPE_RTKFIXED = 4
uint8 POSITION_TYPE_RTKFLOAT = 5
uint8 position_type              

# Number of GNSS Satellites being used in the position computation
uint8 satellites  
# Horizontal dilution of precision
float64 hdop 
# Altitude, in meters, above mean seal level
float64 altitude
# Geoidal separation in meters
float64 geoidal_separation 
# Age of differential corrections, in seconds
duration diff_age
# Base station ID (RTCM only)
uint8 base_station_id
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: gps_nmea/WGS84
# Latitude (degrees). Positive is north of equator; negative is south. 
# +: N, -: S --- unit: degrees
float64 latitude

# Longitude (degrees). Positive is east of prime meridian, negative west.
# +: E, -: W --- unit: degrees, origin: Greenwich
float64 longitude
")



(provide :gps_nmea/GpsGGA "18c21ea63ef156a3d80f5cebf18705d1")


