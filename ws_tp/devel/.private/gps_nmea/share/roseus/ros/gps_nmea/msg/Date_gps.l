;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::Date_gps)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'Date_gps (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::DATE_GPS")
  (make-package "GPS_NMEA::DATE_GPS"))

(in-package "ROS")
;;//! \htmlinclude Date_gps.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass gps_nmea::Date_gps
  :super ros::object
  :slots (_header _day _month _year ))

(defmethod gps_nmea::Date_gps
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:day __day) 0)
    ((:month __month) 0)
    ((:year __year) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _day (round __day))
   (setq _month (round __month))
   (setq _year (round __year))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:day
   (&optional __day)
   (if __day (setq _day __day)) _day)
  (:month
   (&optional __month)
   (if __month (setq _month __month)) _month)
  (:year
   (&optional __year)
   (if __year (setq _year __year)) _year)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8 _day
    1
    ;; uint8 _month
    1
    ;; uint8 _year
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8 _day
       (write-byte _day s)
     ;; uint8 _month
       (write-byte _month s)
     ;; uint8 _year
       (write-byte _year s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8 _day
     (setq _day (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _month
     (setq _month (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _year
     (setq _year (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get gps_nmea::Date_gps :md5sum-) "3914d0ca25a0745148b2fe857f569ec8")
(setf (get gps_nmea::Date_gps :datatype-) "gps_nmea/Date_gps")
(setf (get gps_nmea::Date_gps :definition-)
      "Header header
# 1-31
uint8 day
# 1-12
uint8 month
# 00+ (2000)
uint8 year
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :gps_nmea/Date_gps "3914d0ca25a0745148b2fe857f569ec8")


