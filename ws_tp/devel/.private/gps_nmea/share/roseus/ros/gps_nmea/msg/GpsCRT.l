;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::GpsCRT)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'GpsCRT (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::GPSCRT")
  (make-package "GPS_NMEA::GPSCRT"))

(in-package "ROS")
;;//! \htmlinclude GpsCRT.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(intern "*POSITION_MODE_AUTONOMOUS*" (find-package "GPS_NMEA::GPSCRT"))
(shadow '*POSITION_MODE_AUTONOMOUS* (find-package "GPS_NMEA::GPSCRT"))
(defconstant gps_nmea::GpsCRT::*POSITION_MODE_AUTONOMOUS* 0)
(intern "*POSITION_MODE_RTCM*" (find-package "GPS_NMEA::GPSCRT"))
(shadow '*POSITION_MODE_RTCM* (find-package "GPS_NMEA::GPSCRT"))
(defconstant gps_nmea::GpsCRT::*POSITION_MODE_RTCM* 1)
(intern "*POSITION_MODE_RTKFLOAT*" (find-package "GPS_NMEA::GPSCRT"))
(shadow '*POSITION_MODE_RTKFLOAT* (find-package "GPS_NMEA::GPSCRT"))
(defconstant gps_nmea::GpsCRT::*POSITION_MODE_RTKFLOAT* 2)
(intern "*POSITION_MODE_RTKFIXED*" (find-package "GPS_NMEA::GPSCRT"))
(shadow '*POSITION_MODE_RTKFIXED* (find-package "GPS_NMEA::GPSCRT"))
(defconstant gps_nmea::GpsCRT::*POSITION_MODE_RTKFIXED* 3)

(defun gps_nmea::GpsCRT-to-symbol (const)
  (cond
        ((= const 0) 'gps_nmea::GpsCRT::*POSITION_MODE_AUTONOMOUS*)
        ((= const 1) 'gps_nmea::GpsCRT::*POSITION_MODE_RTCM*)
        ((= const 2) 'gps_nmea::GpsCRT::*POSITION_MODE_RTKFLOAT*)
        ((= const 3) 'gps_nmea::GpsCRT::*POSITION_MODE_RTKFIXED*)
        (t nil)))

(defclass gps_nmea::GpsCRT
  :super ros::object
  :slots (_header _position_mode_enabled _count_enabled _time_enabled _ecef_x_enabled _ecef_y_enabled _ecef_z_enabled _clock_offset_enabled _v_x_enabled _v_y_enabled _v_z_enabled _clock_drift_enabled _pdop_enabled _hdop_enabled _vdop_enabled _tdop_enabled _firmware_enabled _position_mode _count _time _ecef_x _ecef_y _ecef_z _clock_offset _v_x _v_y _v_z _clock_drift _pdop _hdop _vdop _tdop _firmware ))

(defmethod gps_nmea::GpsCRT
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:position_mode_enabled __position_mode_enabled) nil)
    ((:count_enabled __count_enabled) nil)
    ((:time_enabled __time_enabled) nil)
    ((:ecef_x_enabled __ecef_x_enabled) nil)
    ((:ecef_y_enabled __ecef_y_enabled) nil)
    ((:ecef_z_enabled __ecef_z_enabled) nil)
    ((:clock_offset_enabled __clock_offset_enabled) nil)
    ((:v_x_enabled __v_x_enabled) nil)
    ((:v_y_enabled __v_y_enabled) nil)
    ((:v_z_enabled __v_z_enabled) nil)
    ((:clock_drift_enabled __clock_drift_enabled) nil)
    ((:pdop_enabled __pdop_enabled) nil)
    ((:hdop_enabled __hdop_enabled) nil)
    ((:vdop_enabled __vdop_enabled) nil)
    ((:tdop_enabled __tdop_enabled) nil)
    ((:firmware_enabled __firmware_enabled) nil)
    ((:position_mode __position_mode) 0)
    ((:count __count) 0)
    ((:time __time) (instance ros::time :init))
    ((:ecef_x __ecef_x) 0.0)
    ((:ecef_y __ecef_y) 0.0)
    ((:ecef_z __ecef_z) 0.0)
    ((:clock_offset __clock_offset) 0.0)
    ((:v_x __v_x) 0.0)
    ((:v_y __v_y) 0.0)
    ((:v_z __v_z) 0.0)
    ((:clock_drift __clock_drift) 0.0)
    ((:pdop __pdop) 0.0)
    ((:hdop __hdop) 0.0)
    ((:vdop __vdop) 0.0)
    ((:tdop __tdop) 0.0)
    ((:firmware __firmware) (make-array 5 :initial-element 0 :element-type :integer))
    )
   (send-super :init)
   (setq _header __header)
   (setq _position_mode_enabled __position_mode_enabled)
   (setq _count_enabled __count_enabled)
   (setq _time_enabled __time_enabled)
   (setq _ecef_x_enabled __ecef_x_enabled)
   (setq _ecef_y_enabled __ecef_y_enabled)
   (setq _ecef_z_enabled __ecef_z_enabled)
   (setq _clock_offset_enabled __clock_offset_enabled)
   (setq _v_x_enabled __v_x_enabled)
   (setq _v_y_enabled __v_y_enabled)
   (setq _v_z_enabled __v_z_enabled)
   (setq _clock_drift_enabled __clock_drift_enabled)
   (setq _pdop_enabled __pdop_enabled)
   (setq _hdop_enabled __hdop_enabled)
   (setq _vdop_enabled __vdop_enabled)
   (setq _tdop_enabled __tdop_enabled)
   (setq _firmware_enabled __firmware_enabled)
   (setq _position_mode (round __position_mode))
   (setq _count (round __count))
   (setq _time __time)
   (setq _ecef_x (float __ecef_x))
   (setq _ecef_y (float __ecef_y))
   (setq _ecef_z (float __ecef_z))
   (setq _clock_offset (float __clock_offset))
   (setq _v_x (float __v_x))
   (setq _v_y (float __v_y))
   (setq _v_z (float __v_z))
   (setq _clock_drift (float __clock_drift))
   (setq _pdop (float __pdop))
   (setq _hdop (float __hdop))
   (setq _vdop (float __vdop))
   (setq _tdop (float __tdop))
   (setq _firmware __firmware)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:position_mode_enabled
   (&optional (__position_mode_enabled :null))
   (if (not (eq __position_mode_enabled :null)) (setq _position_mode_enabled __position_mode_enabled)) _position_mode_enabled)
  (:count_enabled
   (&optional (__count_enabled :null))
   (if (not (eq __count_enabled :null)) (setq _count_enabled __count_enabled)) _count_enabled)
  (:time_enabled
   (&optional (__time_enabled :null))
   (if (not (eq __time_enabled :null)) (setq _time_enabled __time_enabled)) _time_enabled)
  (:ecef_x_enabled
   (&optional (__ecef_x_enabled :null))
   (if (not (eq __ecef_x_enabled :null)) (setq _ecef_x_enabled __ecef_x_enabled)) _ecef_x_enabled)
  (:ecef_y_enabled
   (&optional (__ecef_y_enabled :null))
   (if (not (eq __ecef_y_enabled :null)) (setq _ecef_y_enabled __ecef_y_enabled)) _ecef_y_enabled)
  (:ecef_z_enabled
   (&optional (__ecef_z_enabled :null))
   (if (not (eq __ecef_z_enabled :null)) (setq _ecef_z_enabled __ecef_z_enabled)) _ecef_z_enabled)
  (:clock_offset_enabled
   (&optional (__clock_offset_enabled :null))
   (if (not (eq __clock_offset_enabled :null)) (setq _clock_offset_enabled __clock_offset_enabled)) _clock_offset_enabled)
  (:v_x_enabled
   (&optional (__v_x_enabled :null))
   (if (not (eq __v_x_enabled :null)) (setq _v_x_enabled __v_x_enabled)) _v_x_enabled)
  (:v_y_enabled
   (&optional (__v_y_enabled :null))
   (if (not (eq __v_y_enabled :null)) (setq _v_y_enabled __v_y_enabled)) _v_y_enabled)
  (:v_z_enabled
   (&optional (__v_z_enabled :null))
   (if (not (eq __v_z_enabled :null)) (setq _v_z_enabled __v_z_enabled)) _v_z_enabled)
  (:clock_drift_enabled
   (&optional (__clock_drift_enabled :null))
   (if (not (eq __clock_drift_enabled :null)) (setq _clock_drift_enabled __clock_drift_enabled)) _clock_drift_enabled)
  (:pdop_enabled
   (&optional (__pdop_enabled :null))
   (if (not (eq __pdop_enabled :null)) (setq _pdop_enabled __pdop_enabled)) _pdop_enabled)
  (:hdop_enabled
   (&optional (__hdop_enabled :null))
   (if (not (eq __hdop_enabled :null)) (setq _hdop_enabled __hdop_enabled)) _hdop_enabled)
  (:vdop_enabled
   (&optional (__vdop_enabled :null))
   (if (not (eq __vdop_enabled :null)) (setq _vdop_enabled __vdop_enabled)) _vdop_enabled)
  (:tdop_enabled
   (&optional (__tdop_enabled :null))
   (if (not (eq __tdop_enabled :null)) (setq _tdop_enabled __tdop_enabled)) _tdop_enabled)
  (:firmware_enabled
   (&optional (__firmware_enabled :null))
   (if (not (eq __firmware_enabled :null)) (setq _firmware_enabled __firmware_enabled)) _firmware_enabled)
  (:position_mode
   (&optional __position_mode)
   (if __position_mode (setq _position_mode __position_mode)) _position_mode)
  (:count
   (&optional __count)
   (if __count (setq _count __count)) _count)
  (:time
   (&optional __time)
   (if __time (setq _time __time)) _time)
  (:ecef_x
   (&optional __ecef_x)
   (if __ecef_x (setq _ecef_x __ecef_x)) _ecef_x)
  (:ecef_y
   (&optional __ecef_y)
   (if __ecef_y (setq _ecef_y __ecef_y)) _ecef_y)
  (:ecef_z
   (&optional __ecef_z)
   (if __ecef_z (setq _ecef_z __ecef_z)) _ecef_z)
  (:clock_offset
   (&optional __clock_offset)
   (if __clock_offset (setq _clock_offset __clock_offset)) _clock_offset)
  (:v_x
   (&optional __v_x)
   (if __v_x (setq _v_x __v_x)) _v_x)
  (:v_y
   (&optional __v_y)
   (if __v_y (setq _v_y __v_y)) _v_y)
  (:v_z
   (&optional __v_z)
   (if __v_z (setq _v_z __v_z)) _v_z)
  (:clock_drift
   (&optional __clock_drift)
   (if __clock_drift (setq _clock_drift __clock_drift)) _clock_drift)
  (:pdop
   (&optional __pdop)
   (if __pdop (setq _pdop __pdop)) _pdop)
  (:hdop
   (&optional __hdop)
   (if __hdop (setq _hdop __hdop)) _hdop)
  (:vdop
   (&optional __vdop)
   (if __vdop (setq _vdop __vdop)) _vdop)
  (:tdop
   (&optional __tdop)
   (if __tdop (setq _tdop __tdop)) _tdop)
  (:firmware
   (&optional __firmware)
   (if __firmware (setq _firmware __firmware)) _firmware)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _position_mode_enabled
    1
    ;; bool _count_enabled
    1
    ;; bool _time_enabled
    1
    ;; bool _ecef_x_enabled
    1
    ;; bool _ecef_y_enabled
    1
    ;; bool _ecef_z_enabled
    1
    ;; bool _clock_offset_enabled
    1
    ;; bool _v_x_enabled
    1
    ;; bool _v_y_enabled
    1
    ;; bool _v_z_enabled
    1
    ;; bool _clock_drift_enabled
    1
    ;; bool _pdop_enabled
    1
    ;; bool _hdop_enabled
    1
    ;; bool _vdop_enabled
    1
    ;; bool _tdop_enabled
    1
    ;; bool _firmware_enabled
    1
    ;; uint8 _position_mode
    1
    ;; uint8 _count
    1
    ;; time _time
    8
    ;; float64 _ecef_x
    8
    ;; float64 _ecef_y
    8
    ;; float64 _ecef_z
    8
    ;; float64 _clock_offset
    8
    ;; float64 _v_x
    8
    ;; float64 _v_y
    8
    ;; float64 _v_z
    8
    ;; float64 _clock_drift
    8
    ;; float64 _pdop
    8
    ;; float64 _hdop
    8
    ;; float64 _vdop
    8
    ;; float64 _tdop
    8
    ;; char[5] _firmware
    (* 1    5)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _position_mode_enabled
       (if _position_mode_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _count_enabled
       (if _count_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _time_enabled
       (if _time_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _ecef_x_enabled
       (if _ecef_x_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _ecef_y_enabled
       (if _ecef_y_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _ecef_z_enabled
       (if _ecef_z_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _clock_offset_enabled
       (if _clock_offset_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _v_x_enabled
       (if _v_x_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _v_y_enabled
       (if _v_y_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _v_z_enabled
       (if _v_z_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _clock_drift_enabled
       (if _clock_drift_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _pdop_enabled
       (if _pdop_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _hdop_enabled
       (if _hdop_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _vdop_enabled
       (if _vdop_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _tdop_enabled
       (if _tdop_enabled (write-byte -1 s) (write-byte 0 s))
     ;; bool _firmware_enabled
       (if _firmware_enabled (write-byte -1 s) (write-byte 0 s))
     ;; uint8 _position_mode
       (write-byte _position_mode s)
     ;; uint8 _count
       (write-byte _count s)
     ;; time _time
       (write-long (send _time :sec) s) (write-long (send _time :nsec) s)
     ;; float64 _ecef_x
       (sys::poke _ecef_x (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _ecef_y
       (sys::poke _ecef_y (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _ecef_z
       (sys::poke _ecef_z (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _clock_offset
       (sys::poke _clock_offset (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _v_x
       (sys::poke _v_x (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _v_y
       (sys::poke _v_y (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _v_z
       (sys::poke _v_z (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _clock_drift
       (sys::poke _clock_drift (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _pdop
       (sys::poke _pdop (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _hdop
       (sys::poke _hdop (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _vdop
       (sys::poke _vdop (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _tdop
       (sys::poke _tdop (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; char[5] _firmware
     (dotimes (i 5)
       (write-byte (elt _firmware i) s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _position_mode_enabled
     (setq _position_mode_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _count_enabled
     (setq _count_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _time_enabled
     (setq _time_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _ecef_x_enabled
     (setq _ecef_x_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _ecef_y_enabled
     (setq _ecef_y_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _ecef_z_enabled
     (setq _ecef_z_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _clock_offset_enabled
     (setq _clock_offset_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _v_x_enabled
     (setq _v_x_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _v_y_enabled
     (setq _v_y_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _v_z_enabled
     (setq _v_z_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _clock_drift_enabled
     (setq _clock_drift_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _pdop_enabled
     (setq _pdop_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _hdop_enabled
     (setq _hdop_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _vdop_enabled
     (setq _vdop_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _tdop_enabled
     (setq _tdop_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _firmware_enabled
     (setq _firmware_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; uint8 _position_mode
     (setq _position_mode (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _count
     (setq _count (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; time _time
     (send _time :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _time :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float64 _ecef_x
     (setq _ecef_x (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _ecef_y
     (setq _ecef_y (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _ecef_z
     (setq _ecef_z (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _clock_offset
     (setq _clock_offset (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _v_x
     (setq _v_x (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _v_y
     (setq _v_y (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _v_z
     (setq _v_z (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _clock_drift
     (setq _clock_drift (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _pdop
     (setq _pdop (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _hdop
     (setq _hdop (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _vdop
     (setq _vdop (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _tdop
     (setq _tdop (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; char[5] _firmware
   (dotimes (i (length _firmware))
     (setf (elt _firmware i) (sys::peek buf ptr- :char)) (incf ptr- 1)
     )
   ;;
   self)
  )

(setf (get gps_nmea::GpsCRT :md5sum-) "dab59c118ba3827d16dc59c7d383a215")
(setf (get gps_nmea::GpsCRT :datatype-) "gps_nmea/GpsCRT")
(setf (get gps_nmea::GpsCRT :definition-)
      "Header header

bool position_mode_enabled
bool count_enabled
bool time_enabled
bool ecef_x_enabled
bool ecef_y_enabled
bool ecef_z_enabled
bool clock_offset_enabled
bool v_x_enabled
bool v_y_enabled
bool v_z_enabled
bool clock_drift_enabled
bool pdop_enabled
bool hdop_enabled
bool vdop_enabled
bool tdop_enabled
bool firmware_enabled

# Position Mode
# 0 : Autonomous
# 1 : RTCM or SBAS differential
# 2 : RTK float
# 3 : RTK fixed
uint8 POSITION_MODE_AUTONOMOUS = 0
uint8 POSITION_MODE_RTCM = 1
uint8 POSITION_MODE_RTKFLOAT = 2
uint8 POSITION_MODE_RTKFIXED = 3
uint8 position_mode

# Count of SVs used in position computation
uint8 count
# UTC time
time time
# ECEF X coordinate, in meters
float64 ecef_x
# ECEF Y coordinate, in meters
float64 ecef_y
# ECEF Z coordinate, in meters
float64 ecef_z
# Receiver clock offset, in meters
float64 clock_offset
# Velocity vector, X component, in m/s
float64 v_x
# Velocity vector, Y component, in m/s
float64 v_y
# Velocity vector, Z component, in m/s
float64 v_z
# Receiver clock drift, in m/s
float64 clock_drift
# Positional dilution of precision
float64 pdop
# Horizontal dilution of precision
float64 hdop
# Vertical dilution of precision
float64 vdop
# Time dilution of precision
float64 tdop
# Firmware version ID (4 letters)
char[5] firmware


================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :gps_nmea/GpsCRT "dab59c118ba3827d16dc59c7d383a215")


