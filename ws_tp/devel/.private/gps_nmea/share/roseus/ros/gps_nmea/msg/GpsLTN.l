;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::GpsLTN)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'GpsLTN (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::GPSLTN")
  (make-package "GPS_NMEA::GPSLTN"))

(in-package "ROS")
;;//! \htmlinclude GpsLTN.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass gps_nmea::GpsLTN
  :super ros::object
  :slots (_header _latency_enabled _latency ))

(defmethod gps_nmea::GpsLTN
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:latency_enabled __latency_enabled) nil)
    ((:latency __latency) (instance ros::time :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _latency_enabled __latency_enabled)
   (setq _latency __latency)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:latency_enabled
   (&optional (__latency_enabled :null))
   (if (not (eq __latency_enabled :null)) (setq _latency_enabled __latency_enabled)) _latency_enabled)
  (:latency
   (&optional __latency)
   (if __latency (setq _latency __latency)) _latency)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _latency_enabled
    1
    ;; duration _latency
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _latency_enabled
       (if _latency_enabled (write-byte -1 s) (write-byte 0 s))
     ;; duration _latency
       (write-long (send _latency :sec) s) (write-long (send _latency :nsec) s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _latency_enabled
     (setq _latency_enabled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; duration _latency
     (send _latency :sec (sys::peek buf ptr- :integer)) (incf ptr- 4)  (send _latency :nsec (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get gps_nmea::GpsLTN :md5sum-) "18fa69ccf54af4a6a11e6712354a87c7")
(setf (get gps_nmea::GpsLTN :datatype-) "gps_nmea/GpsLTN")
(setf (get gps_nmea::GpsLTN :definition-)
      "
Header header

bool latency_enabled

#Latency IS ORIGINALLY FEATURED in milliseconds
duration latency

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :gps_nmea/GpsLTN "18fa69ccf54af4a6a11e6712354a87c7")


