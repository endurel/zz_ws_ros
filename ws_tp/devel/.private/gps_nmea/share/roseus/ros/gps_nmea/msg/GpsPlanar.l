;; Auto-generated. Do not edit!


(when (boundp 'gps_nmea::GpsPlanar)
  (if (not (find-package "GPS_NMEA"))
    (make-package "GPS_NMEA"))
  (shadow 'GpsPlanar (find-package "GPS_NMEA")))
(unless (find-package "GPS_NMEA::GPSPLANAR")
  (make-package "GPS_NMEA::GPSPLANAR"))

(in-package "ROS")
;;//! \htmlinclude GpsPlanar.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(intern "*TRANSFORM_TYPE_INVALID*" (find-package "GPS_NMEA::GPSPLANAR"))
(shadow '*TRANSFORM_TYPE_INVALID* (find-package "GPS_NMEA::GPSPLANAR"))
(defconstant gps_nmea::GpsPlanar::*TRANSFORM_TYPE_INVALID* 0)
(intern "*TRANSFORM_TYPE_LOCAL_PLANAR*" (find-package "GPS_NMEA::GPSPLANAR"))
(shadow '*TRANSFORM_TYPE_LOCAL_PLANAR* (find-package "GPS_NMEA::GPSPLANAR"))
(defconstant gps_nmea::GpsPlanar::*TRANSFORM_TYPE_LOCAL_PLANAR* 1)
(intern "*TRANSFORM_TYPE_UTM*" (find-package "GPS_NMEA::GPSPLANAR"))
(shadow '*TRANSFORM_TYPE_UTM* (find-package "GPS_NMEA::GPSPLANAR"))
(defconstant gps_nmea::GpsPlanar::*TRANSFORM_TYPE_UTM* 2)
(intern "*TRANSFORM_TYPE_LAMBERT2E*" (find-package "GPS_NMEA::GPSPLANAR"))
(shadow '*TRANSFORM_TYPE_LAMBERT2E* (find-package "GPS_NMEA::GPSPLANAR"))
(defconstant gps_nmea::GpsPlanar::*TRANSFORM_TYPE_LAMBERT2E* 3)

(defun gps_nmea::GpsPlanar-to-symbol (const)
  (cond
        ((= const 0) 'gps_nmea::GpsPlanar::*TRANSFORM_TYPE_INVALID*)
        ((= const 1) 'gps_nmea::GpsPlanar::*TRANSFORM_TYPE_LOCAL_PLANAR*)
        ((= const 2) 'gps_nmea::GpsPlanar::*TRANSFORM_TYPE_UTM*)
        ((= const 3) 'gps_nmea::GpsPlanar::*TRANSFORM_TYPE_LAMBERT2E*)
        (t nil)))

(defclass gps_nmea::GpsPlanar
  :super ros::object
  :slots (_header _transform_type _east _north _up ))

(defmethod gps_nmea::GpsPlanar
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:transform_type __transform_type) 0)
    ((:east __east) 0.0)
    ((:north __north) 0.0)
    ((:up __up) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _transform_type (round __transform_type))
   (setq _east (float __east))
   (setq _north (float __north))
   (setq _up (float __up))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:transform_type
   (&optional __transform_type)
   (if __transform_type (setq _transform_type __transform_type)) _transform_type)
  (:east
   (&optional __east)
   (if __east (setq _east __east)) _east)
  (:north
   (&optional __north)
   (if __north (setq _north __north)) _north)
  (:up
   (&optional __up)
   (if __up (setq _up __up)) _up)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8 _transform_type
    1
    ;; float64 _east
    8
    ;; float64 _north
    8
    ;; float64 _up
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8 _transform_type
       (write-byte _transform_type s)
     ;; float64 _east
       (sys::poke _east (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _north
       (sys::poke _north (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _up
       (sys::poke _up (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8 _transform_type
     (setq _transform_type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; float64 _east
     (setq _east (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _north
     (setq _north (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _up
     (setq _up (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get gps_nmea::GpsPlanar :md5sum-) "0c081549f0015bc62875d9599de89471")
(setf (get gps_nmea::GpsPlanar :datatype-) "gps_nmea/GpsPlanar")
(setf (get gps_nmea::GpsPlanar :definition-)
      "Header header

# Position type
# 0 : Transform type not available or invalid
# 1 : Local geographic planar conversion
# 2 : Local UTM planar conversion
uint8 TRANSFORM_TYPE_INVALID = 0
uint8 TRANSFORM_TYPE_LOCAL_PLANAR = 1
uint8 TRANSFORM_TYPE_UTM = 2
uint8 TRANSFORM_TYPE_LAMBERT2E = 3
uint8 transform_type              

float64 east
float64 north
float64 up

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :gps_nmea/GpsPlanar "0c081549f0015bc62875d9599de89471")


