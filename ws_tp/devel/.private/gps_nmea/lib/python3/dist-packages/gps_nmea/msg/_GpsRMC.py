# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from gps_nmea/GpsRMC.msg. Do not edit."""
import codecs
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import genpy
import gps_nmea.msg
import std_msgs.msg

class GpsRMC(genpy.Message):
  _md5sum = "ecae7bc96c99e205f05bee9f6e358c53"
  _type = "gps_nmea/GpsRMC"
  _has_header = True  # flag to mark the presence of a Header object
  _full_text = """Header header

bool time_enabled
bool position_valid_enabled
bool position_enabled
bool speedOverGround_enabled
bool trackAngle_enabled
bool date_enabled
bool magneticVariation_enabled
bool mode_enabled

# Current UTC time
time time

# Position Status
# 0 : NAV Receiver warning
# 1 : Valid position
uint8 POSITION_VALID_VOID = 0
uint8 POSITION_VALID_ACTIVE = 1
uint8 position_valid

# WGS84 latitude and longitude
WGS84 position
# Vitesse curviligne (m/s)
float64 speedOverGround
# Cap en degres (0deg=360deg is north)
float64 trackAngle

# Current UTC date
Date_gps date
# Magnetic variation
float64  magneticVariation

# Magnetic variation magnetic_var_direction
uint8 MAGNETIC_VARIATION_EAST = 0
uint8 MAGNETIC_VARIATION_WEST = 1
uint8 magnetic_var_direction

# Position mode
# 0 : Data not valid
# 1 : Autonomous mode
# 2 : Differential mode
# 3 : Estimated mode
uint8 GPS_ENABLED_MODE_INVALID = 0
uint8 GPS_ENABLED_MODE_AUTONOMOUS = 1
uint8 GPS_ENABLED_MODE_DIFFERENTIAL = 2
uint8 GPS_ENABLED_MODE_ESTIMATED = 3
uint8 mode

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: gps_nmea/WGS84
# Latitude (degrees). Positive is north of equator; negative is south. 
# +: N, -: S --- unit: degrees
float64 latitude

# Longitude (degrees). Positive is east of prime meridian, negative west.
# +: E, -: W --- unit: degrees, origin: Greenwich
float64 longitude
================================================================================
MSG: gps_nmea/Date_gps
Header header
# 1-31
uint8 day
# 1-12
uint8 month
# 00+ (2000)
uint8 year"""
  # Pseudo-constants
  POSITION_VALID_VOID = 0
  POSITION_VALID_ACTIVE = 1
  MAGNETIC_VARIATION_EAST = 0
  MAGNETIC_VARIATION_WEST = 1
  GPS_ENABLED_MODE_INVALID = 0
  GPS_ENABLED_MODE_AUTONOMOUS = 1
  GPS_ENABLED_MODE_DIFFERENTIAL = 2
  GPS_ENABLED_MODE_ESTIMATED = 3

  __slots__ = ['header','time_enabled','position_valid_enabled','position_enabled','speedOverGround_enabled','trackAngle_enabled','date_enabled','magneticVariation_enabled','mode_enabled','time','position_valid','position','speedOverGround','trackAngle','date','magneticVariation','magnetic_var_direction','mode']
  _slot_types = ['std_msgs/Header','bool','bool','bool','bool','bool','bool','bool','bool','time','uint8','gps_nmea/WGS84','float64','float64','gps_nmea/Date_gps','float64','uint8','uint8']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       header,time_enabled,position_valid_enabled,position_enabled,speedOverGround_enabled,trackAngle_enabled,date_enabled,magneticVariation_enabled,mode_enabled,time,position_valid,position,speedOverGround,trackAngle,date,magneticVariation,magnetic_var_direction,mode

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(GpsRMC, self).__init__(*args, **kwds)
      # message fields cannot be None, assign default values for those that are
      if self.header is None:
        self.header = std_msgs.msg.Header()
      if self.time_enabled is None:
        self.time_enabled = False
      if self.position_valid_enabled is None:
        self.position_valid_enabled = False
      if self.position_enabled is None:
        self.position_enabled = False
      if self.speedOverGround_enabled is None:
        self.speedOverGround_enabled = False
      if self.trackAngle_enabled is None:
        self.trackAngle_enabled = False
      if self.date_enabled is None:
        self.date_enabled = False
      if self.magneticVariation_enabled is None:
        self.magneticVariation_enabled = False
      if self.mode_enabled is None:
        self.mode_enabled = False
      if self.time is None:
        self.time = genpy.Time()
      if self.position_valid is None:
        self.position_valid = 0
      if self.position is None:
        self.position = gps_nmea.msg.WGS84()
      if self.speedOverGround is None:
        self.speedOverGround = 0.
      if self.trackAngle is None:
        self.trackAngle = 0.
      if self.date is None:
        self.date = gps_nmea.msg.Date_gps()
      if self.magneticVariation is None:
        self.magneticVariation = 0.
      if self.magnetic_var_direction is None:
        self.magnetic_var_direction = 0
      if self.mode is None:
        self.mode = 0
    else:
      self.header = std_msgs.msg.Header()
      self.time_enabled = False
      self.position_valid_enabled = False
      self.position_enabled = False
      self.speedOverGround_enabled = False
      self.trackAngle_enabled = False
      self.date_enabled = False
      self.magneticVariation_enabled = False
      self.mode_enabled = False
      self.time = genpy.Time()
      self.position_valid = 0
      self.position = gps_nmea.msg.WGS84()
      self.speedOverGround = 0.
      self.trackAngle = 0.
      self.date = gps_nmea.msg.Date_gps()
      self.magneticVariation = 0.
      self.magnetic_var_direction = 0
      self.mode = 0

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_get_struct_3I().pack(_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs))
      _x = self.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self
      buff.write(_get_struct_8B2IB4d3I().pack(_x.time_enabled, _x.position_valid_enabled, _x.position_enabled, _x.speedOverGround_enabled, _x.trackAngle_enabled, _x.date_enabled, _x.magneticVariation_enabled, _x.mode_enabled, _x.time.secs, _x.time.nsecs, _x.position_valid, _x.position.latitude, _x.position.longitude, _x.speedOverGround, _x.trackAngle, _x.date.header.seq, _x.date.header.stamp.secs, _x.date.header.stamp.nsecs))
      _x = self.date.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self
      buff.write(_get_struct_3Bd2B().pack(_x.date.day, _x.date.month, _x.date.year, _x.magneticVariation, _x.magnetic_var_direction, _x.mode))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    if python3:
      codecs.lookup_error("rosmsg").msg_type = self._type
    try:
      if self.header is None:
        self.header = std_msgs.msg.Header()
      if self.time is None:
        self.time = genpy.Time()
      if self.position is None:
        self.position = gps_nmea.msg.WGS84()
      if self.date is None:
        self.date = gps_nmea.msg.Date_gps()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs,) = _get_struct_3I().unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.header.frame_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.header.frame_id = str[start:end]
      _x = self
      start = end
      end += 61
      (_x.time_enabled, _x.position_valid_enabled, _x.position_enabled, _x.speedOverGround_enabled, _x.trackAngle_enabled, _x.date_enabled, _x.magneticVariation_enabled, _x.mode_enabled, _x.time.secs, _x.time.nsecs, _x.position_valid, _x.position.latitude, _x.position.longitude, _x.speedOverGround, _x.trackAngle, _x.date.header.seq, _x.date.header.stamp.secs, _x.date.header.stamp.nsecs,) = _get_struct_8B2IB4d3I().unpack(str[start:end])
      self.time_enabled = bool(self.time_enabled)
      self.position_valid_enabled = bool(self.position_valid_enabled)
      self.position_enabled = bool(self.position_enabled)
      self.speedOverGround_enabled = bool(self.speedOverGround_enabled)
      self.trackAngle_enabled = bool(self.trackAngle_enabled)
      self.date_enabled = bool(self.date_enabled)
      self.magneticVariation_enabled = bool(self.magneticVariation_enabled)
      self.mode_enabled = bool(self.mode_enabled)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.date.header.frame_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.date.header.frame_id = str[start:end]
      _x = self
      start = end
      end += 13
      (_x.date.day, _x.date.month, _x.date.year, _x.magneticVariation, _x.magnetic_var_direction, _x.mode,) = _get_struct_3Bd2B().unpack(str[start:end])
      self.time.canon()
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e)  # most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_get_struct_3I().pack(_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs))
      _x = self.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self
      buff.write(_get_struct_8B2IB4d3I().pack(_x.time_enabled, _x.position_valid_enabled, _x.position_enabled, _x.speedOverGround_enabled, _x.trackAngle_enabled, _x.date_enabled, _x.magneticVariation_enabled, _x.mode_enabled, _x.time.secs, _x.time.nsecs, _x.position_valid, _x.position.latitude, _x.position.longitude, _x.speedOverGround, _x.trackAngle, _x.date.header.seq, _x.date.header.stamp.secs, _x.date.header.stamp.nsecs))
      _x = self.date.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self
      buff.write(_get_struct_3Bd2B().pack(_x.date.day, _x.date.month, _x.date.year, _x.magneticVariation, _x.magnetic_var_direction, _x.mode))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    if python3:
      codecs.lookup_error("rosmsg").msg_type = self._type
    try:
      if self.header is None:
        self.header = std_msgs.msg.Header()
      if self.time is None:
        self.time = genpy.Time()
      if self.position is None:
        self.position = gps_nmea.msg.WGS84()
      if self.date is None:
        self.date = gps_nmea.msg.Date_gps()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs,) = _get_struct_3I().unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.header.frame_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.header.frame_id = str[start:end]
      _x = self
      start = end
      end += 61
      (_x.time_enabled, _x.position_valid_enabled, _x.position_enabled, _x.speedOverGround_enabled, _x.trackAngle_enabled, _x.date_enabled, _x.magneticVariation_enabled, _x.mode_enabled, _x.time.secs, _x.time.nsecs, _x.position_valid, _x.position.latitude, _x.position.longitude, _x.speedOverGround, _x.trackAngle, _x.date.header.seq, _x.date.header.stamp.secs, _x.date.header.stamp.nsecs,) = _get_struct_8B2IB4d3I().unpack(str[start:end])
      self.time_enabled = bool(self.time_enabled)
      self.position_valid_enabled = bool(self.position_valid_enabled)
      self.position_enabled = bool(self.position_enabled)
      self.speedOverGround_enabled = bool(self.speedOverGround_enabled)
      self.trackAngle_enabled = bool(self.trackAngle_enabled)
      self.date_enabled = bool(self.date_enabled)
      self.magneticVariation_enabled = bool(self.magneticVariation_enabled)
      self.mode_enabled = bool(self.mode_enabled)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.date.header.frame_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.date.header.frame_id = str[start:end]
      _x = self
      start = end
      end += 13
      (_x.date.day, _x.date.month, _x.date.year, _x.magneticVariation, _x.magnetic_var_direction, _x.mode,) = _get_struct_3Bd2B().unpack(str[start:end])
      self.time.canon()
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e)  # most likely buffer underfill

_struct_I = genpy.struct_I
def _get_struct_I():
    global _struct_I
    return _struct_I
_struct_3Bd2B = None
def _get_struct_3Bd2B():
    global _struct_3Bd2B
    if _struct_3Bd2B is None:
        _struct_3Bd2B = struct.Struct("<3Bd2B")
    return _struct_3Bd2B
_struct_3I = None
def _get_struct_3I():
    global _struct_3I
    if _struct_3I is None:
        _struct_3I = struct.Struct("<3I")
    return _struct_3I
_struct_8B2IB4d3I = None
def _get_struct_8B2IB4d3I():
    global _struct_8B2IB4d3I
    if _struct_8B2IB4d3I is None:
        _struct_8B2IB4d3I = struct.Struct("<8B2IB4d3I")
    return _struct_8B2IB4d3I
