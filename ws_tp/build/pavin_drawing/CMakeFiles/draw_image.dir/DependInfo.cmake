# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/src/pavin_drawing/src/draw_image.cpp" "/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/build/pavin_drawing/CMakeFiles/draw_image.dir/src/draw_image.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_SQL_LIB"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"pavin_drawing\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtSql"
  "/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/src/pavin_drawing/include"
  "/opt/ros/noetic/include"
  "/opt/ros/noetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv4"
  "/home/local.isima.fr/endurel/ws_tp/devel/.private/gps_nmea/include"
  "/home/local.isima.fr/endurel/ws_tp/src/gps_nmea/include"
  "/home/local.isima.fr/endurel/ws_tp/src/ip_drawing_tools/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
