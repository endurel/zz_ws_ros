#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/devel/.private/tp_sinus:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/devel/.private/tp_sinus/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/devel/.private/tp_sinus/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/build/tp_sinus'
export ROSLISP_PACKAGE_DIRECTORIES="/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/devel/.private/tp_sinus/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/local.isima.fr/endurel/ws_tp/src/tp_sinus:/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/src/tp_sinus:$ROS_PACKAGE_PATH"