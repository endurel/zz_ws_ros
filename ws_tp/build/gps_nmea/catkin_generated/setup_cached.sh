#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/devel/.private/gps_nmea:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/devel/.private/gps_nmea/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/devel/.private/gps_nmea/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/build/gps_nmea'
export PYTHONPATH="/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/devel/.private/gps_nmea/lib/python3/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/devel/.private/gps_nmea/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/local.isima.fr/endurel/ws_tp/src/gps_nmea:/home/local.isima.fr/endurel/zz_ws_ros/ws_tp/src/gps_nmea:$ROS_PACKAGE_PATH"